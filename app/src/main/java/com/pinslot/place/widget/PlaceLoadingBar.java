package com.pinslot.place.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pinslot.place.R;

public class PlaceLoadingBar extends LinearLayout {
    LayoutInflater mInflater;

    public PlaceLoadingBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mInflater = LayoutInflater.from(context);
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER);
        Init();
    }
    public PlaceLoadingBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);
        Init();
    }
    public PlaceLoadingBar(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);
        Init();
    }

    public void Init(){

        View v = mInflater.inflate(R.layout.custom_view, this, true);
        TextView tv = v.findViewById(R.id.textView3);
        tv.setText("Searching availability of rooms");
        GifMovieView iv2 =v.findViewById(R.id.gif1);
        iv2.setMovieResource(R.mipmap.waiting_gif);
        iv2.setClickable(false);
    }
}
