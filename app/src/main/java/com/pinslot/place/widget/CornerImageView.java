package com.pinslot.place.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.pinslot.place.Utils;

public class CornerImageView extends ImageView {
    private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    private static final int COLORDRAWABLE_DIMENSION = 2;
    private final Paint mBitmapPaint = new Paint();
    private float mDrawableRadius;
    private final RectF mDrawableRect = new RectF();
    private int width =0,height=0,radius =0;
    public CornerImageView(Context context, int width, int height, int radius) {
        super(context);
        this.width = width;
        this.height = height;
        this.radius = radius;
    }

    public CornerImageView(Context context){
        super(context);
    }

    public CornerImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CornerImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setRadius(int radius){
        this.radius = radius;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // load the bitmap
        Bitmap image = drawableToBitmap(getDrawable());
        if(radius==0){
            radius = (int)getpxfordp(8);
        }

        // init shader
        try {
            if (image == null) {
                return;
            }
            mDrawableRect.set(calculateBounds());
            mDrawableRadius = Math.min(mDrawableRect.height() / 2.0f, mDrawableRect.width() / 2.0f);

            Bitmap temp = Bitmap.createScaledBitmap(image,(int)getWidth(),(int)getHeight(),false);
            Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
            foreground.setStrokeWidth(4f);
            foreground.setColor(Color.BLUE);
            if(height>0&&width>0){
                canvas.drawBitmap(getBitMap(width,height, temp), 0f, 0f, foreground);
            } else {
                canvas.drawBitmap(getBitMap(getWidth(), getHeight(), temp), 0f, 0f, foreground);
            }

        } catch (OutOfMemoryError e){

        }
    }

    Bitmap getBitMap(int width,int height,Bitmap bitmap){
        Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Path path =new Path();
        if(Build.VERSION.SDK_INT>20) {
            path.arcTo(0f, 0f, 2*radius, 2*radius , 180f, 90f, true);
            path.lineTo(width-radius,0f);
            path.lineTo( width, height-radius);
            path.moveTo(width-2*radius,0f);
            path.arcTo(width-2*radius, 0f, width, 2*radius , 270f, 90f, true);
            path.lineTo(width, height-radius);
            path.lineTo(radius,height);
            path.moveTo(width,height-2*radius);
            path.arcTo( width-2*radius, height-2*radius, width, height , 0f, 90f, true);
            path.lineTo(radius,height);
            path.lineTo(0f,radius);
            path.moveTo(2*radius,height);
            path.arcTo(0f, height-2*radius, 2*radius, height , 90f, 90f, true);
            path.lineTo(0f,radius);

        }
        path.close();
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.BLUE);
        canvas.drawPath(path,paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap,0f,0f,paint);
        return output;
    }

    private RectF calculateBounds() {
        int availableWidth  = getWidth() - getPaddingLeft() - getPaddingRight();
        int availableHeight = getHeight() - getPaddingTop() - getPaddingBottom();

        int sideLength = Math.min(availableWidth, availableHeight);

        float left = getPaddingLeft() + (availableWidth - sideLength) / 2f;
        float top = getPaddingTop() + (availableHeight - sideLength) / 2f;

        return new RectF(left, top, left + sideLength, top + sideLength);
    }

    public float getpxfordp(int dp) {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        try {
            Bitmap bitmap;

            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(COLORDRAWABLE_DIMENSION, COLORDRAWABLE_DIMENSION, BITMAP_CONFIG);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), BITMAP_CONFIG);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());

            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setImageUrl(String url){
        new Utils.LoadImageTask(this,getContext()).execute(url);
    }
}
