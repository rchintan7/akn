package com.pinslot.place.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.fragment.Restaurant_History;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class Calendar_View_Greetings_History extends LinearLayout {
    // for logging
    private static final String LOGTAG = "Calendar View";

    // how many days to show, defaults to six weeks, 42 days
    private int DAYS_COUNT = 42;

    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";

    // date format
    private String dateFormat;

    // current displayed month
    public static Calendar currentDate = Calendar.getInstance();

    //event handling
    private Calendar_View_Greetings_History.EventHandler eventHandler = null;

    // internal components
    private LinearLayout header;
    public static ImageView btnPrev;
    public static ImageView btnNext;
    private TextView txtDate;
    private GridView grid;
    private ItemClickListener itemClickListener;
    public String type = "";
    public int todayPos = 0;
    boolean isWhiteTheme = false;
    int color = Color.WHITE;
    int rows_count = 5;
    Date minDate = null;
    // seasons' rainbow
    int[] rainbow = new int[]{
            R.color.summer,
            R.color.fall,
            R.color.winter,
            R.color.spring
    };

    // month-season association (northern hemisphere, sorry australia :)
    int[] monthSeason = new int[]{2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};

    public Calendar_View_Greetings_History(Context context) {
        super(context);
    }

    public Calendar_View_Greetings_History(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControl(context, attrs);
    }

    public Calendar_View_Greetings_History(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initControl(context, attrs);
    }

    /**
     * Load control xml layout
     */
    private void initControl(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.control_greeting_history, this);

        loadDateFormat(attrs);
        assignUiElements();
        assignClickHandlers();

        updateCalendar();
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    private void loadDateFormat(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CalendarViewHistory);

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(R.styleable.CalendarViewHistory_dateFormatHistory);
            color = getContext().getResources().getColor(R.color.colorAccent);//ta.getColor(R.styleable.CalendarViewHistory_themeColor, Color.WHITE);

            if (dateFormat == null)
                dateFormat = DATE_FORMAT;
        } finally {
            ta.recycle();
        }
    }

    public void setSelection(int position) {
        if (grid != null) {
            grid.setSelection(position);
        }
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = (LinearLayout) findViewById(R.id.calendar_header);
        btnPrev = (ImageView) findViewById(R.id.calendar_prev_button);
        btnNext = (ImageView) findViewById(R.id.calendar_next_button);
        txtDate = (TextView) findViewById(R.id.calendar_date_display);
        grid = (GridView) findViewById(R.id.calendar_grid);
    }

    public void ApplyLightTheme() {
        txtDate.setTextColor(Color.BLACK);
        ((TextView) findViewById(R.id.sun)).setTextColor(Color.BLACK);
        ((TextView) findViewById(R.id.mon)).setTextColor(Color.BLACK);
        ((TextView) findViewById(R.id.tue)).setTextColor(Color.BLACK);
        ((TextView) findViewById(R.id.wed)).setTextColor(Color.BLACK);
        ((TextView) findViewById(R.id.thu)).setTextColor(Color.BLACK);
        ((TextView) findViewById(R.id.fri)).setTextColor(Color.BLACK);
        ((TextView) findViewById(R.id.sat)).setTextColor(Color.BLACK);
        isWhiteTheme = true;
        if (grid != null && grid.getAdapter() != null) {
            ((CalendarAdapter) grid.getAdapter()).notifyDataSetChanged();
        }
    }

    private void assignClickHandlers() {
        // long-pressing a day
        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> view, View cell, int position, long id) {
                // handle long-press
                if (eventHandler == null)
                    return false;

                eventHandler.onDayLongPress((Date) view.getItemAtPosition(position));
                return true;
            }
        });

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (itemClickListener != null) {
                    if (((CalendarAdapter) grid.getAdapter()).Month == ((Date) adapterView.getItemAtPosition(i)).getMonth()) {
                        if (minDate == null || Utils.CompareTime(((Date) adapterView.getItemAtPosition(i)), minDate) > 0) {
                            boolean select = ((CalendarAdapter) grid.getAdapter()).updateSelectedDate((Date) adapterView.getItemAtPosition(i));
                            itemClickListener.ItemClick((Date) adapterView.getItemAtPosition(i), i, select);
                        } else {
                            Toast.makeText(getContext(), "Please choose feature date", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }


    /**
     * Display dates correctly in grid
     */
    public void updateCalendar() {
        updateCalendar(Calendar.getInstance(), Restaurant_History.events);
    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar(Calendar cal, HashSet<Date> events) {
        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar) cal.clone();

        // update title
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        txtDate.setText(sdf.format(calendar.getTime()));

        // set header color according to current season
        int month = calendar.get(Calendar.MONTH);
        int season = monthSeason[month];
        int color = rainbow[season];

        //header.setBackgroundColor(getResources().getColor(color));

        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int dayscount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        if (dayscount + monthBeginningCell > 35) {
            DAYS_COUNT = 42;
            rows_count = 6;
        } else {
            DAYS_COUNT = 35;
            rows_count = 5;
        }
        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);
        Date today = Calendar.getInstance().getTime();
        int currentdatepos = 0;

        // fill cells
        while (cells.size() < DAYS_COUNT) {
            if (calendar.getTime().getDate() == today.getDate() && calendar.getTime().getMonth() == today.getMonth() && calendar.getTime().getYear() == today.getYear())
                currentdatepos = cells.size() - 1;
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // update grid
        todayPos = currentdatepos;
        grid.setAdapter(new Calendar_View_Greetings_History.CalendarAdapter(getContext(), cells, events, month));
        grid.setSelection(currentdatepos);
    }

    public boolean isSameDate(Date d1, Date d2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(d1);
        cal2.setTime(d2);
        if (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) && cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE))
            return true;
        else
            return false;
    }

    public int getRows_count() {
        return rows_count;
    }

    private class CalendarAdapter extends ArrayAdapter<Date> {
        // days with events
        private HashSet<Date> eventDays;

        // for view inflation
        private LayoutInflater inflater;
        int Month;

        Date SelectedDate = null;

        public CalendarAdapter(Context context, ArrayList<Date> days, HashSet<Date> eventDays, int Month) {
            super(context, R.layout.control_calendar_day_history, days);
            this.eventDays = eventDays;
            inflater = LayoutInflater.from(context);
            this.Month = Month;
        }

        public boolean updateSelectedDate(Date date) {
            boolean updated = false;
            if (isSelectedDate(date)) {
                SelectedDate = null;
            } else {
                SelectedDate = date;
                updated = true;
            }
            this.notifyDataSetChanged();
            return updated;
        }

        public boolean isSelectedDate(Date date) {
            if (SelectedDate == null) {
                return false;
            } else if (isSameDate(date, SelectedDate)) {
                return true;
            } else
                return false;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            ViewHolder view_Holder;
            //if(view==null){
            view = inflater.inflate(R.layout.control_calendar_day_history, parent, false);

            view_Holder = new ViewHolder();

            view_Holder.day_txt = (TextView) view.findViewById(R.id.day_text);
            view_Holder.tag = (ImageView) view.findViewById(R.id.tag);
            if (type.equals("event")) {
                view_Holder.tag.setImageResource(R.mipmap.check_mark);
            } else if (type.equals("ext_exp")) {
                view_Holder.tag.setVisibility(GONE);
            } else {
                view_Holder.tag.setImageResource(R.mipmap.check_mark);
            }

            view.setTag(view_Holder);

            /*}else{
                view_Holder = (ViewHolder) view.getTag();
            }*/

            // day in question
            Date date = getItem(position);
            if (isSelectedDate(date)) {
                view_Holder.day_txt.setBackgroundResource(R.drawable.gray_circle_fill_with_yellow_border);
            } else {
                view_Holder.day_txt.setBackgroundResource(R.color.transparent);
            }
            int day = date.getDate();
            int month = date.getMonth();
            int year = date.getYear();

            // today
            Date today = new Date();

            // inflate item if it does not exist yet

            view_Holder.day_txt.setTypeface(null, Typeface.NORMAL);
            view_Holder.day_txt.setTextColor(color);
            if (eventDays != null) {
                for (Date eventDate : eventDays) {
                    if (eventDate.getDate() == day &&
                            eventDate.getMonth() == month &&
                            eventDate.getYear() == year) {
                        // mark this day for event
                        view_Holder.day_txt.setTextColor(getResources().getColor(R.color.btn_color));
                        view_Holder.day_txt.setTypeface(Typeface.DEFAULT_BOLD);
                        if (type.equals("ext_exp")) {
                            view_Holder.tag.setVisibility(GONE);
                        } else {
                            view_Holder.tag.setVisibility(VISIBLE);
                        }
                        break;
                    }
                }
            }

            // clear styling

            if (month != this.Month) {
                // if this day is outside current month, grey it out
                view_Holder.day_txt.setTextColor(getResources().getColor(R.color.gray));
            } else if (day == today.getDate()) {
                // if it is today, set it to blue/bold
                view_Holder.day_txt.setTypeface(null, Typeface.BOLD);
                view_Holder.day_txt.setTextColor(getResources().getColor(R.color.today));
                /*day_txt.setTextColor(getResources().getColor(R.color.white));
                day_txt.setBackgroundResource(R.drawable.round_blue_button);
                day_txt.setPadding(2,2,2,2);*/
            }

            // set text
            view_Holder.day_txt.setText(String.valueOf(date.getDate()));

            return view;
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    static class ViewHolder {
        TextView day_txt;
        ImageView tag;
    }

    /**
     * Assign event handler to be passed needed events
     */
    public void setEventHandler(Calendar_View_Greetings_History.EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    public interface EventHandler {
        void onDayLongPress(Date date);
    }

    public interface ItemClickListener {
        void ItemClick(Date clickedDate, int position, boolean select);
    }
}
