package com.pinslot.place.helpers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.fragment.OrderConfirmationFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Payment_Method {

    public static void orderConfirmation(final JSONObject jsonObject, final FragmentActivity activity, final BookingCallBack callBack) {
        try {
            if (jsonObject.has("pickupBy") && !jsonObject.getString("pickupBy").equals("THIRD_PARTY")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Notification Confirmation");
                builder.setMessage("Would you like to send Email/SMS notifications to your contacts regarding this order?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            jsonObject.put("notificationEnabled",true);
                            MakeOrder(jsonObject, activity,callBack);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            jsonObject.put("notificationEnabled",false);
                            MakeOrder(jsonObject,activity,callBack);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.show();
            } else {
                MakeOrder(jsonObject,activity,callBack);
            }
        } catch (JSONException e){

        }
    }

    public static void MakeOrder(JSONObject jsonObject, final FragmentActivity activity, final BookingCallBack callBack){
        /*if(((NavigationDrawerActivity)activity).deliveryReq.resReq!=null&&((NavigationDrawerActivity)activity).deliveryReq.resReq.selectedRestaurant.menuItemsCount>0){
            String type = "";
            try {
                if (jsonObject.getString("team").equals("COD")) {
                    type = "PAR";
                }
            } catch (JSONException e){

            }
            ((NavigationDrawerActivity)activity).deliveryReq.bookreq = jsonObject;
            BookRestaurant(((NavigationDrawerActivity)activity).deliveryReq.resReq.promo,(NavigationDrawerActivity) activity,type,callBack);
        } else {*/
        try {
            String tag_json_obj = "json_obj_req";
            final ProgressDialog pDialog = new ProgressDialog(activity);
            pDialog.setMessage("Loading...");
            pDialog.show();
            String url = Constants.SOCIAL_ORDER_CONFIRMATION;
            jsonObject.put("requesterAccountId", PrefManager.getInstance(activity).getUserId());
            Log.d("Contacts Request", url.toString() + " " + jsonObject);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Favorite Response", response.toString());
                            pDialog.dismiss();
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    String paymentToken = "";
                                    if(response.getJSONObject("data").has("paymentToken")){
                                        paymentToken = response.getJSONObject("data").getString("paymentToken");
                                    }
                                    if(callBack!=null)
                                        callBack.bookingSucess(response.getJSONObject("data").getString("requestCode"),paymentToken,response);
                                } else if(callBack!=null){
                                    if (response.has("message")) {
                                        callBack.bookingFailed(response.getString("message"));
                                    } else {
                                        callBack.bookingFailed("Failed to Process Your Request");
                                    }
                                }
                            } catch (Exception e) {
                                Log.d("JSON Parsing Error", e.toString());
                                if(callBack!=null){
                                    callBack.bookingFailed("Failed to Parse Data");
                                }
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    if(callBack==null)
                        callBack.bookingFailed("Failed to Process Your Request");
                }
            });

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
        //}
    }

    public static void placeBooking(FragmentActivity activity,JSONObject json,final BookingCallBack callBack) {
        final ProgressDialog pDialog = new ProgressDialog(activity);
        pDialog.show();
        pDialog.setCancelable(false);
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.PLACE_BOOKING;
            json.put("paymentType","RAZORPAY");
            Log.e("Book Place Request", url + " " + json.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, json,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.e("Book Place Response", response.toString());
                                if (response.getString("status").equals("SUCCESS")) {
                                    String bookingId = "";
                                    String paymentToken = "";
                                    if (response.has("data") && response.getJSONObject("data").has("bookingId"))
                                        bookingId = response.getJSONObject("data").getString("bookingId");
                                    if(response.has("data") && response.getJSONObject("data").has("paymentToken"))
                                        paymentToken = response.getJSONObject("data").getString("paymentToken");
                                    callBack.bookingSucess(bookingId,paymentToken,response);
                                } else {
                                    if (response.getString("errorCode").equals("DATES_NOT_AVIALBLE")) {
                                        callBack.bookingFailed("Already this slot has been booked, Please try some other slots");
                                    } else {
                                        callBack.bookingFailed("");
                                    }
                                }

                            } catch (Exception e) {
                                Log.e("Error", e.toString());
                                callBack.bookingFailed("");
                            }
                            pDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callBack.bookingFailed("");
                    pDialog.dismiss();
                }
            });
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            pDialog.dismiss();
        }
    }

    public static void ResExpBooking(FragmentActivity activity,String experienceId,JSONObject book,final BookingCallBack callBack){
        try {
            final ProgressDialog pDialog = new ProgressDialog(activity);
            pDialog.show();
            String tag_json_obj = "json_obj_req";
            String url = Constants.URL + "restaurants/experience/" + experienceId + "/book";
            Log.e("URL", url.toString());
            book.put("paymentType","RAZORPAY");
            Log.e("Payment Request", book.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, book,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.e("Add to Cart Response", response.toString());
                                String bookingId = "";
                                String paymentToken = "";
                                if (response.has("data") && response.getJSONObject("data").has("bookingId"))
                                    bookingId = response.getJSONObject("data").getString("bookingId");
                                if(response.has("data") && response.getJSONObject("data").has("paymentToken"))
                                    paymentToken = response.getJSONObject("data").getString("paymentToken");
                                callBack.bookingSucess(bookingId,paymentToken,response);
                            } catch (Exception e) {
                                Log.e("Error", e.toString());
                                callBack.bookingFailed("");
                            }
                            pDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    callBack.bookingFailed("");
                }
            });
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public static void expBooking(FragmentActivity activity, JSONObject book, final BookingCallBack callBack){
        try {
            final ProgressDialog pDialog = new ProgressDialog(activity);
            pDialog.show();
            String tag_json_obj = "json_obj_req";
            String url = Constants.URL + "external/experience/book";
            Log.e("URL", url.toString());
            book.put("paymentType","RAZORPAY");
            Log.e("Payment Request", book.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, book,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.e("Add to Cart Response", response.toString());
                                String bookingId = "";
                                String paymentToken = "";
                                if (response.has("data") && response.getJSONObject("data").has("bookingId"))
                                    bookingId = response.getJSONObject("data").getString("bookingId");
                                if(response.has("data") && response.getJSONObject("data").has("paymentToken"))
                                    paymentToken = response.getJSONObject("data").getString("paymentToken");
                                callBack.bookingSucess(bookingId,paymentToken,response);
                            } catch (Exception e) {
                                Log.e("Error", e.toString());
                                callBack.bookingFailed("");
                            }
                            pDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    callBack.bookingFailed("");
                }
            });
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }



    public static void BookRestaurant(String linkId,String promo, final NavigationDrawerActivity context, String paymentType, final BookingCallBack callBack){
        try {
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.show();
            String tag_json_obj = "json_obj_req";
            String url = Constants.URL + "customer/restaurant/" + context.resReq.selectedRestaurant.id + "/order";
            Log.e("URL", url.toString());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("restaurantId", context.resReq.selectedRestaurant.id);
            jsonObject.put("cartType", "ORDINARY");

            JSONObject resBook = new JSONObject();
            if (context.resReq.requestType.equals("DineIn"))
                resBook.put("orderType", "DineIn");
            else
                resBook.put("orderType", "Pickup");
            resBook.put("selectedPreOrder", "false");
            resBook.put("prefPickupTime", PrefManager.getInstance(context).getPref().getString("timeText", null));
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format3 = new SimpleDateFormat("hh:mm a");
            Date date = new Date(context.resReq.startDate);
            resBook.put("pickupDate", format1.format(date));
            resBook.put("prefPickupTime", format3.format(date));
            resBook.put("noOfPeople", context.resReq.peopleCount);
            if (PrefManager.getInstance(context).getPref().getString("loginSession", null) != null && PrefManager.getInstance(context).getPref().getString("loginSession", null).equals("Valid")) {
                resBook.put("firstName", PrefManager.getInstance(context).getPref().getString("name", ""));
                resBook.put("emailId", PrefManager.getInstance(context).getPref().getString("email", ""));
                resBook.put("phoneNumber", PrefManager.getInstance(context).getPref().getString("mobile", ""));
                resBook.put("customerId", PrefManager.getInstance(context).getPref().getString("userId", null));
            } else {
                resBook.put("firstName", "");
                resBook.put("emailId", "");
                resBook.put("phoneNumber", "");
            }
            resBook.put("currency", "Rs");
            resBook.put("cartType", "ORDINARY");
            resBook.put("totalPrice", context.resReq.total);
            resBook.put("tax", context.resReq.tax);
            resBook.put("orgPrice", context.resReq.subTotalval);
            if(!linkId.isEmpty())
                resBook.put("linkedOrderId",linkId);
            if(!paymentType.isEmpty())
                resBook.put("paymentType",paymentType);
            if (!promo.isEmpty()) {
                JSONArray promoCodeIds = new JSONArray();
                String[] promos = promo.split(",");
                for (String promocode : promos) {
                    promoCodeIds.put(promocode);
                }
                resBook.put("promoCodeIds", promoCodeIds);
            }
            if(context.resReq.flow.equals("Event")){
                resBook.put("bookingIdPrefix","ETR");
            }
            jsonObject.put("resBook", resBook);

            Log.e("Payment Request", jsonObject.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    Log.e("Add to Cart Response", response.toString());
                                    //if(!context.resReq.flow.equals("Restaurant_delivery")) {
                                    String bookingId = "";
                                    String paymentToken = "";
                                    if(response.getJSONObject("data").has("bookingId"))
                                        bookingId = response.getJSONObject("data").getString("bookingId");
                                    if(response.getJSONObject("data").has("paymentToken"))
                                        paymentToken = response.getJSONObject("data").getString("paymentToken");
                                    if(callBack!=null)
                                        callBack.bookingSucess(bookingId,paymentToken,response);
//                                        Fragment fragment = new OrderConfirmationFragment();
//                                        Bundle args = new Bundle();
//                                        args.putString("BookingId", response.getJSONObject("data").getString("bookingId"));
//                                        args.putString("orderId", response.getJSONObject("data").getString("id"));
//                                        args.putString("type", "menu");
//                                        fragment.setArguments(args);
//                                        FragmentManager fragmentManager = context.getSupportFragmentManager();
//                                        FragmentTransaction fragmentTransaction = fragmentManager
//                                                .beginTransaction();
//                                        fragmentTransaction.replace(R.id.fragment_place, fragment);
//                                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
//                                        fragmentTransaction.commit();
                                    //} else {
                                    //    context.deliveryReq.bookreq.put("restBookingId",response.getJSONObject("data").getString("bookingId"));
                                    //    context.deliveryReq.resReq = null;
                                    //     MakeOrder(context.deliveryReq.bookreq,context,callBack);
                                    //}
                                } else if (response.has("message")) {
                                    Toast.makeText(context, response.getString("message"), Toast.LENGTH_LONG).show();
                                    if(callBack!=null)
                                        callBack.bookingFailed(response.getString("message"));
                                } else {
                                    if(callBack!=null)
                                        callBack.bookingFailed("Failed to create order");
                                }
                            } catch (Exception e) {
                                Log.e("Error", e.toString());
                                if(callBack!=null)
                                    callBack.bookingFailed("Response handle error. booking failed");
                            }
                            pDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    if(callBack!=null)
                        callBack.bookingFailed("Failed to create order");
                }
            });
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public static void openRestaurantBookedScreen(JSONObject response, FragmentActivity context){
        Fragment fragment = new OrderConfirmationFragment();
        Bundle args = new Bundle();
        try {
            args.putString("BookingId", response.getJSONObject("data").getString("bookingId"));
            args.putString("orderId", response.getJSONObject("data").getString("id"));
        } catch (JSONException e){

        }
        args.putString("type", "menu");
        fragment.setArguments(args);
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.fragment_place, fragment);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.commit();
    }

    public static void webServiceCallForPaymentUpdate(String transactionId, String signature, String token, String status, Context context, final PaymentUpdateCallback callback){
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.UPDATE_PAYMENT_STATUS;
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Menu Request", url.toString());
            JSONObject obj = new JSONObject();
            obj.put("paymentStatus",status);
            obj.put("transactionId",transactionId);
            obj.put("paymentToken",token);
            obj.put("signature",signature);
            obj.put("paymentService","RazorPay");

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                    url,obj,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            try {
                                pDialog.dismiss();
                                if (response.getString("status").equals("SUCCESS")) {
                                    if(callback!=null){
                                        callback.PaymentUpdateSucess();
                                    }
                                } else {
                                    if(callback!=null)
                                        callback.PaymentUpdateFailed();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    if(callback!=null)
                        callback.PaymentUpdateFailed();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public interface PaymentUpdateCallback{
        void PaymentUpdateSucess();
        void PaymentUpdateFailed();
    }

    public interface BookingCallBack {
        void bookingSucess(String bookingId,String token,JSONObject response);
        void bookingFailed(String failed);
    }
}

