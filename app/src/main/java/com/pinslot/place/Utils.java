package com.pinslot.place;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.model.Restaurant;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static  int regvalue=0;
    public static  int roomvale=0;
    public static  double userlatitude,userlongitute;
    public  static  String cityname="";
    public  static  String locationstring="";
    public  static  String checkindate="";
    public  static  String checkoutdate="";

    public static void resWebServiceCall(final Context context, final NavigationDrawerActivity.ResCallBack callBack) {
        if(PrefManager.getInstance(context).getRestaurant()!=null&&callBack!=null)
            callBack.resSuccess(PrefManager.getInstance(context).getRestaurant());
        else {
            String url = Constants.GET_RESTAURANT_BY_ID + Constants.res_id;
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Constants.DELIVERY_NAVI = "TRUE";
                                PrefManager.getInstance(context).saveRestaurant(response.getJSONObject("data"));
                                if(callBack!=null)
                                    callBack.resSuccess(new Restaurant(response.getJSONObject("data")));
                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                                if(callBack!=null)
                                    callBack.resFailure("Invalid Response");
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error.toString());
                    callBack.resFailure(error.getMessage());
                }
            });

// Adding request to request queue.
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }

    }

    public static void ShowAlert(String title, String message,Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
    }

    public static void ShowAlert(int title, int message,Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(context.getResources().getString(title));
        alert.setMessage(context.getResources().getString(message));
        alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
    }

    public static int CompareTime(Date date1, Date date2){
        if(date1==null&&date2==null)
            return 0;
        else if(date1==null)
            return 1;
        else if(date2==null)
            return -1;
        else{
            Calendar c1 = Calendar.getInstance();
            c1.setTime(date1);
            Calendar c2 = Calendar.getInstance();
            c2.setTime(date2);
            if(c1.get(Calendar.YEAR)<c2.get(Calendar.YEAR))
                return -1;
            else if(c1.get(Calendar.YEAR)>c2.get(Calendar.YEAR))
                return 1;
            else if(c1.get(Calendar.MONTH)<c2.get(Calendar.MONTH))
                return -1;
            else if(c1.get(Calendar.MONTH)>c2.get(Calendar.MONTH))
                return 1;
            else if(c1.get(Calendar.DATE)<c2.get(Calendar.DATE))
                return -1;
            else if(c1.get(Calendar.DATE)>c2.get(Calendar.DATE))
                return 1;
            else if(c1.get(Calendar.AM_PM)<c2.get(Calendar.AM_PM))
                return -1;
            else if(c1.get(Calendar.AM_PM)>c2.get(Calendar.AM_PM))
                return 1;
            else if(c1.get(Calendar.HOUR)<c2.get(Calendar.HOUR))
                return -1;
            else if(c1.get(Calendar.HOUR)>c2.get(Calendar.HOUR))
                return 1;
            else if(c1.get(Calendar.MINUTE)>c2.get(Calendar.MINUTE))
                return 1;
            else if(c1.get(Calendar.MINUTE)<c2.get(Calendar.MINUTE))
                return -1;
            else
                return 0;
        }
    }

    public static class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        Context context;

        public LoadImageTask(ImageView bmImage,Context context) {
            this.bmImage = bmImage;
            this.context = context;
        }

        protected Bitmap doInBackground(String... urls) {
            Bitmap mIcon11 = null;
            try {
                mIcon11 = Picasso.with(context).load(urls[0]).get();
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    URL url = new URL(urls[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    mIcon11 = BitmapFactory.decodeStream(input);
                } catch (Exception e2){

                }
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if(result!=null)
                bmImage.setImageBitmap(result);
        }
    }

    public static Bitmap getRandomColorImage(Context context, String text) {
        final int textColor = Color.WHITE;
        float diameterDP = 80.0f;
        DisplayMetrics metrics = context.getResources().getSystem().getDisplayMetrics();
        float diameterPixels = diameterDP * (metrics.densityDpi / 160f);
        float radiusPixels = diameterPixels / 2;

        // Create the bitmap
        Bitmap output = Bitmap.createBitmap((int) diameterPixels, (int) diameterPixels,
                Bitmap.Config.ARGB_8888);

        // Create the canvas to draw on
        Canvas canvas = new Canvas(output);
        canvas.drawARGB(0, 0, 0, 0);

        // Draw the circle
        final Paint paintC = new Paint();
        paintC.setAntiAlias(true);
        Random random = new Random();
        paintC.setColor(Color.rgb(random.nextInt(128), random.nextInt(128), random.nextInt(128)));
        canvas.drawCircle(radiusPixels, radiusPixels, radiusPixels, paintC);

        // Draw the text
        if (text != null && text.length() > 0) {
            final Paint paintT = new Paint();
            paintT.setColor(textColor);
            paintT.setAntiAlias(true);
            paintT.setTextSize(radiusPixels * 1.5f);
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "Effra_Std_Bd.ttf");
            paintT.setTypeface(typeFace);
            final Rect textBounds = new Rect();
            paintT.getTextBounds(text, 0, text.length(), textBounds);
            canvas.drawText(text, radiusPixels - textBounds.exactCenterX(), radiusPixels - textBounds.exactCenterY(), paintT);
        }
        return output;
    }

    public static int CompareTimeString(String time1, String time2) {
        int hr1 = 0;
        int min1 = 0;
        int hr2 = 0;
        int min2 = 0;
        String[] a = time1.split(" ");
        if (a.length > 0) {
            String[] b = a[0].split(":");
            hr1 = Integer.parseInt(b[0]);
            min1 = Integer.parseInt(b[1]);
        }
        if (hr1 == 12)
            hr1 = 0;
        if (a.length > 1 && a[1].toUpperCase().equals("PM"))
            hr1 = hr1 + 12;
        a = time2.split(" ");
        if (a.length > 0) {
            String[] b = a[0].split(":");
            hr2 = Integer.parseInt(b[0]);
            min2 = Integer.parseInt(b[1]);
        }
        if (hr2 == 12)
            hr2 = 0;
        if (a.length > 1 && a[1].toUpperCase().equals("PM"))
            hr2 = hr2 + 12;
        if (hr1 > hr2)
            return 1;
        else if (hr1 < hr2)
            return -1;
        else if (min1 > min2)
            return 1;
        else if (min1 < min2)
            return -1;
        else
            return 0;
    }

    public static float getPXforDP(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static String getCountryCode(String phone,Context context){
        String code = "";
        if(phone.startsWith("+")||phone.length()>10){
            String [] country_codes = context.getResources().getStringArray(R.array.CountryCodes);
            for(String cc : country_codes){
                String[] val = cc.split(",");
                if(phone.startsWith(val[0])||phone.startsWith("+"+val[0])){
                    if(val[0].length()>code.length())
                        code = val[0];
                }
            }
        }
        if(!code.isEmpty())
            code = "+"+code;
        return code;
    }
}
