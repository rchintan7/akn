package com.pinslot.place;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.pinslot.place.widget.GifMovieView;

/**
 * Created by Selva on 10/23/2018.
 */

public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_view_gif);

        final GifMovieView gif=(GifMovieView)findViewById(R.id.gif1);
        gif.setMovieResource(R.mipmap.places_splash_gif);
        gif.setClickable(true);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        //gif.setMeasuredDimension(width,height);

        gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent two=new Intent(Splash.this,NavigationDrawerActivity.class);
                startActivity(two);
                finish();
            }
        });

       /* Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(5000);
                    Intent two=new Intent(Splash.this,MainActivity.class);
                    startActivity(two);
                    finish();
                } catch (Exception e) {
                    Log.d("Exception", e.toString());
                }
            }
        };
        background.start();*/

    }

}
