package com.pinslot.place;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class PaymentJobService extends Service {
    SharedPreferences pref;
    Timer timer;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createNotification(getApplicationContext(),true);
        startTimer();
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void startTimer(){
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(needtoStop(getApplicationContext())){
                    timer.cancel();
                    PaymentJobService.this.stopSelf();
                } else {
                    int diff = getDiff();
                    if(diff==180||diff==120||diff==60)
                        createNotification(getApplicationContext(),true);
                    else
                        createNotification(getApplicationContext(),false);
                }
            }
        },1000,1000);
    }

    public int getDiff(){
        return (int)(new Date().getTime()-PrefManager.getInstance(getApplicationContext()).getPref().getLong("payment_started_at",0))/1000;
    }

    public static boolean needtoStop(Context context){
        if(PrefManager.getInstance(context).getPref().getBoolean("payment_started",false)){
            if(new Date().getTime()-PrefManager.getInstance(context).getPref().getLong("payment_started_at",0)>600000) {
                if(AppController.paymentActivity!=null){
                    AppController.paymentActivity.finish();
                }
                stopPayment(context);
                return true;
            } else
                return false;
        } else {
            return true;
        }
    }

    public static void startScheduleQueue(Context context){
        Intent intent = new Intent(context,PaymentJobService.class);
        context.startService(intent);
    }

    public static void startPayment(Context context){
        startScheduleQueue(context);
        SharedPreferences.Editor editor = PrefManager.getInstance(context).getPref().edit();
        editor.putBoolean("payment_started",true);
        editor.putLong("payment_started_at",new Date().getTime());
        editor.apply();
    }

    public static void stopPayment(Context context){
        AppController.paymentActivity = null;
        SharedPreferences.Editor editor = PrefManager.getInstance(context).getPref().edit();
        editor.putBoolean("payment_started",false);
        editor.putLong("payment_started_at",0);
        editor.apply();
    }

    public void createNotification(Context context,boolean ishighPriority) {
        /**Creates an explicit intent for an Activity in your app */
        Intent intent = new Intent();
        final PendingIntent pendingIntent = PendingIntent.getService(
                getApplicationContext(), 0, intent, 0);
        int priority = Notification.PRIORITY_DEFAULT;
        Uri sound = null;
        if(ishighPriority) {
            priority = Notification.PRIORITY_HIGH;
            sound = Settings.System.DEFAULT_NOTIFICATION_URI;
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.mipmap.calendar_ic);
        mBuilder.setContentTitle("Payment Remainder")
                .setContentText("Payment Session expires in"+getPaymentTimeRemaining(context))
                .setOngoing(true)
                .setSound(sound)
                .setPriority(priority)
                .setContentIntent(pendingIntent);
        if(!ishighPriority)
            mBuilder.setOnlyAlertOnce(true);

        NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("11", "pinslots", NotificationManager.IMPORTANCE_HIGH);
            //notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            //if(!ishighPriority)
            //    notificationChannel.setSound(null,null);
            //notificationChannel.enableVibration(true);
            mBuilder.setChannelId("11");
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        startForeground(101,mBuilder.build());
    }

    public String getPaymentTimeRemaining(Context context){
        if(PrefManager.getInstance(context).getPref().getBoolean("payment_started",false)){
            long diff = new Date().getTime()-PrefManager.getInstance(context).getPref().getLong("payment_started_at",0);
            diff = diff/1000;
            if(diff>600) {
                return "";
            } else {
                diff = 600-diff;
                return diff/60+":"+(((diff%60)>9)?diff%60:"0"+diff%60);
            }
        } else {
            return "";
        }
    }
}
