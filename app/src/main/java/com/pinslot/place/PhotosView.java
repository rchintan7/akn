package com.pinslot.place;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PhotosView extends Fragment {
    ImageView share;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.photo_slide_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        final ArrayList<String> photosArr = bundle.getStringArrayList("photos");
        int position = bundle.getInt("position");
        final ViewPager viewPager = view.findViewById(R.id.photos);
        share = view.findViewById(R.id.share);

        viewPager.setAdapter(new PhotosView.CustomPagerAdapter(photosArr));
        viewPager.setCurrentItem(position);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, photosArr.get(viewPager.getCurrentItem()));
                startActivity(Intent.createChooser(sharingIntent, "Share using"));

            }
        });

    }

    class CustomPagerAdapter extends PagerAdapter {

        LayoutInflater mLayoutInflater;
        ArrayList<String> photos;

        public CustomPagerAdapter(ArrayList<String> photos) {
            mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.photos = photos;
        }

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.image_page_item, container, false);

            ImageView imageView = itemView.findViewById(R.id.imageView);
            String url = photos.get(position);
            if (!url.isEmpty())
                Picasso.with(getActivity()).load(url).into(imageView);

            container.addView(itemView);

            /*PhotoViewAttacher photoAttacher;
            photoAttacher = new PhotoViewAttacher(imageView);
            photoAttacher.update();*/

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}
