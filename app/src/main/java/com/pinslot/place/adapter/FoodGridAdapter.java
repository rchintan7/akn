package com.pinslot.place.adapter;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FoodGridAdapter extends BaseAdapter {

    ArrayList<JSONObject> objs;
    FragmentActivity activity;

    public FoodGridAdapter(ArrayList<JSONObject> objs, FragmentActivity activity){
        this.objs = objs;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return objs.size();
    }

    @Override
    public JSONObject getItem(int i) {
        return objs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_grid_item,viewGroup,false);
        ImageView food_image = view.findViewById(R.id.food_image);
        TextView food_name = view.findViewById(R.id.food_name);
        TextView price = view.findViewById(R.id.price);
        TextView availability = view.findViewById(R.id.availability);
        JSONObject obj = getItem(i);
        try {
            if (obj.has("imageUrl") && !obj.getString("imageUrl").isEmpty()) {
                PicassoUtil.with(viewGroup.getContext()).load(obj.getString("imageUrl")).into(food_image);
            }
            if (obj.has("menuItemName"))
                food_name.setText(obj.getString("menuItemName"));
            if(obj.has("price"))
                price.setText("Order ₹"+obj.getString("price"));
            if(obj.has("availability")&&obj.getString("availability").equals("available")) {
                availability.setText("Available");
                availability.setTextColor(viewGroup.getContext().getResources().getColor(R.color.green_text));
            } else {
                availability.setText("Available at "+ExpandableListAdapterFaq.getNextAvailability(obj,activity));
                availability.setTextColor(viewGroup.getContext().getResources().getColor(R.color.Red));
            }
        } catch (JSONException e){

        }
        return view;
    }
}
