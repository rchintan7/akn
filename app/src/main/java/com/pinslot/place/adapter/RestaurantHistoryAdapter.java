package com.pinslot.place.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.fragment.RestaurantReviewsFragment;
import com.pinslot.place.fragment.Restaurant_History;
import com.pinslot.place.model.Restaurant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class RestaurantHistoryAdapter extends BaseAdapter {

    Activity context;
    ArrayList<JSONObject> jsonArray = new ArrayList<>();
    Fragment fragment;
    public String CaptureImagepath = "";
    public JSONObject shareImageRestaurant = null;
    public RestaurantHistoryAdapter(Activity context, ArrayList<JSONObject> jsonArray, Fragment fragment){
        this.context = context;
        this.jsonArray = jsonArray;
        this.fragment = fragment;
    }
    @Override
    public int getCount() {
        return jsonArray.size();
    }

    @Override
    public Object getItem(int position) {
        return jsonArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder=null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.restaurent_history_item, parent, false);
            holder = new ViewHolder();
            holder.name=(TextView)convertView.findViewById(R.id.name);
            holder.place=(TextView)convertView.findViewById(R.id.place);
            holder.price=(TextView)convertView.findViewById(R.id.price);
            holder.image=(ImageView) convertView.findViewById(R.id.image);
            holder.type = (TextView) convertView.findViewById(R.id.order_type);
            holder.status = (TextView) convertView.findViewById(R.id.status);
            holder.pin= (LinearLayout) convertView.findViewById(R.id.pin);
            holder.cal = (LinearLayout) convertView.findViewById(R.id.cal);
            holder.share = (LinearLayout) convertView.findViewById(R.id.history_share);
            holder.camera = (LinearLayout) convertView.findViewById(R.id.history_camera);
            holder.cancel = (LinearLayout) convertView.findViewById(R.id.history_cancel);
            holder.rate = (LinearLayout) convertView.findViewById(R.id.rate);
            holder.contact = (LinearLayout) convertView.findViewById(R.id.contact);
            holder.more = (ImageView) convertView.findViewById(R.id.more);
            holder.more_options = (LinearLayout) convertView.findViewById(R.id.more_options);
            holder.time = convertView.findViewById(R.id.time);
            holder.call = convertView.findViewById(R.id.call);
            holder.waitTime = convertView.findViewById(R.id.wait_time);
            holder.reOrder = convertView.findViewById(R.id.re_order);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.waitTime.setText("");
        holder.waitTime.setVisibility(View.GONE);
        final ViewHolder finalholder = holder;
        final JSONObject object = (JSONObject) getItem(position);
        if(object != null) {
            try {
                JSONObject res = object.getJSONObject("restaurant");
                holder.name.setText(res.getString("name"));
                holder.call.setVisibility(View.VISIBLE);
                if(res.has("managerProfileIds")){
                    holder.call.setImageResource(R.mipmap.chat_icon);
                } else if(res.has("phone")){
                    holder.call.setImageResource(R.mipmap.call);
                } else{
                    holder.call.setVisibility(View.GONE);
                }
                double total = object.getDouble("finalPrice");
                if(object.has("pickupDate")){
                    Date date = new Date(object.getLong("pickupDate"));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy@hh:mm a");
                    holder.time.setText("Time : "+sdf.format(date));
                }
                if(object.has("approxWaitTimingInMin")){
                    double min = object.getDouble("approxWaitTimingInMin");
                    if(min>1){
                        holder.waitTime.setVisibility(View.VISIBLE);
                        holder.waitTime.setText("Wait time : "+min+" mins");
                    } else if(min == 1){
                        holder.waitTime.setVisibility(View.VISIBLE);
                        holder.waitTime.setText("Wait Time : 1 min");
                    }
                }
                int tax = 0;

                Double tot = total * (100 + tax)/100;
                if(tot>0) {
                    holder.price.setVisibility(View.VISIBLE);
                    holder.price.setText(object.getString("currency") + String.format("%.2f", tot));
                } else {
                    holder.price.setVisibility(View.GONE);
                }
                if (res.getJSONObject("location") != null && res.getJSONObject("location").getString("addressString") != null) {
                    holder.place.setText(res.getJSONObject("location").getString("addressString"));
                }
                holder.type.setText(object.getString("orderType"));
                String status = object.getString("status");
                if(status!=null) {
                    holder.status.setText(status);
                    if(status.equals("ACTIVE"))
                        holder.status.setText("Pending Confirmation");
                }

                try {
                    Date date=new Date(object.getLong("pickupDate"));
                    //SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    //String stringDate = sdf.format(date );
                    if (date.before(new Date())) {
                        Log.d("Past Date","True");
                        holder.cancel.setVisibility(View.GONE);
                    } else {
                        holder.cancel.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){

                }

                if (status.equals("CANCELLED") || status.equals("REJECTED"))
                {
                    holder.cancel.setVisibility(View.GONE);
                }
                if (res.getJSONArray("photoUrls").length() > 0) {
                    Picasso.with(context).load(res.getJSONArray("photoUrls").get(0).toString()).into(holder.image);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        holder.pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent map=new Intent(context, RestaurantActivity.class);
                map.putExtra("fragments","History_Map");
                context.startActivity(map);*/
                /*---try {
                    JSONObject eventObject = jsonArray.get(position);
                    Intent location = new Intent(context,LocationActivity.class);
                    location.putExtra("name",eventObject.getJSONObject("restaurant").getString("name"));
                    location.putExtra("address",eventObject.getJSONObject("restaurant").getJSONObject("location").getString("addressString"));
                    location.putExtra("lat",eventObject.getJSONObject("restaurant").getJSONObject("location").getJSONObject("coordinate").getDouble("latitude"));
                    location.putExtra("lon",eventObject.getJSONObject("restaurant").getJSONObject("location").getJSONObject("coordinate").getDouble("longitude"));
                    location.putExtra("showmap",true);
                    context.startActivity(location);
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }
        });

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject restaurant = jsonArray.get(position).getJSONObject("restaurant");
                    Restaurant res = new Restaurant(restaurant);
                    Log.d("JSON OBJECT", restaurant.toString());
                    String resManagerId = "";
                    if (res.managerProfile!=null&&res.managerProfile.has("profileId")) {
                        resManagerId = res.managerProfile.getString("profileId");
                    }
                    String phone_url = "tel:" + restaurant.getString("phone").trim();
                    if(Build.VERSION.SDK_INT>22&&context.checkSelfPermission(Manifest.permission.CALL_PHONE)== PackageManager.PERMISSION_DENIED){
                        context.requestPermissions(new String[]{Manifest.permission.CALL_PHONE},0);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse(phone_url));
                        context.startActivity(intent);
                    }
                } catch (Exception e) {
                }
            }
        });

        holder.cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    JSONObject resObject=jsonArray.get(position);
                    Date date=new Date(resObject.getLong("pickupDate"));
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    String stringDate = sdf.format(date );
                    if (new SimpleDateFormat("MMM dd").parse(stringDate).before(new Date())) {

                    }
                    JSONObject resdetail= resObject.getJSONObject("restaurant");
                    JSONObject location = resdetail.getJSONObject("location");
                    Constants.WHERE_STR=location.getString("addressString");
                    Constants.EVENT_NAME=resdetail.getString("name");
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, cal.getTimeInMillis());
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,cal.getTimeInMillis()+60*60*1000);
                    intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, 1);
                    intent.putExtra(CalendarContract.Events.DTSTART, cal.getTimeInMillis());
                    intent.putExtra(CalendarContract.Events.DTEND, cal.getTimeInMillis());
                    intent.putExtra(CalendarContract.Events.TITLE, Constants.EVENT_NAME);
                    intent.putExtra(CalendarContract.Events.DESCRIPTION, "Welcome All");
                    intent.putExtra(CalendarContract.Events.EVENT_LOCATION, Constants.WHERE_STR);
                    intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
                    context.startActivity(intent);
                }catch (Exception e){

                }
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject resObject= null;
                String id = null;
                String resName = null;
                try {
                    resObject = jsonArray.get(position);
                    JSONObject resdetail= resObject.getJSONObject("restaurant");
                    resName = resdetail.getString("name");
                    id = resdetail.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                if(id!=null&&!id.isEmpty()&&resName!=null&&!resName.isEmpty())
                    shareIntent.putExtra(Intent.EXTRA_TEXT, resName.toUpperCase() + " - " + "http://www.pinslots.com/#/restaurant/details?restaurantId=" + id);
                else
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "");
                context.startActivity(Intent.createChooser(shareIntent, "Share text using"));
            }
        });

        holder.camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareImageRestaurant = jsonArray.get(position);
                showCameraSharePopup();
            }
        });

        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Alert");
                alert.setMessage("Are you sure want to cancel your order?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            webrequestforCancelRestaurentOrder(object.getString("id"));
                        } catch (JSONException e){

                        }
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.show();
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(finalholder.more_options.getVisibility()==View.VISIBLE){
                    finalholder.more_options.setVisibility(View.GONE);
                } else {
                    finalholder.more_options.setVisibility(View.VISIBLE);
                }
            }
        });

        holder.rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestaurantReviewsFragment fragment = new RestaurantReviewsFragment();
                try {
                    fragment.restaurant = new Restaurant(jsonArray.get(position).getJSONObject("restaurant"));
                    Bundle args = new Bundle();
                    args.putString("title",fragment.restaurant.name);
                    fragment.setArguments(args);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fragment,fragment.getClass().getName());
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }
        });

        return convertView;
    }

    public void webrequestforCancelRestaurentOrder(String orderId){
        try{
            String tag_json_obj = "json_obj_req";
            String url = Constants.CANCEL_RESTAURANT + orderId;
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.show();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            pDialog.dismiss();
                            //jsonResponse=response.toString();
                            try {
                                if(response.getString("status").equals("SUCCESS")) {
                                    JSONObject cancel_deatil = response.getJSONObject("data");
                                    ((Restaurant_History)fragment).webServiceCallForHistory();
                                } else {
                                    if (response.has("message")) {
                                        Utils.ShowAlert("Sucess", response.getString("message"),context);
                                    } else {
                                        Utils.ShowAlert("Failure", "Update Failure",context);
                                    }
                                }
                            }catch (Exception e){
                                Log.d("JSON Parsing Error",e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }catch (Exception e){
            System.out.println("Error :"+e.toString());
        }
    }

    public void showCameraSharePopup(){
        final PopupWindow popup = new PopupWindow(context);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_image_share, null);
        Button take_image = (Button) layout.findViewById(R.id.take_image);
        popup.setContentView(layout);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        if ((int) Build.VERSION.SDK_INT > 22 && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        if ((int) Build.VERSION.SDK_INT > 22 && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, 3);
        if ((int) Build.VERSION.SDK_INT > 22 && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
            fragment.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 4);


        take_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle ("Select image using");
                //alert.SetMessage ("");
                alert.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if((int) Build.VERSION.SDK_INT>22&&ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)== PackageManager.PERMISSION_DENIED && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                        if((int) Build.VERSION.SDK_INT>22&&ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)== PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, 3);
                        if((int) Build.VERSION.SDK_INT>22&&ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 4);
                        else
                        {
                            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"Image_" + System.currentTimeMillis() + ".png");
                            Uri outputfileuri = Uri.fromFile(file);
                            CaptureImagepath = outputfileuri.getPath();
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputfileuri);
                            fragment.startActivityForResult(intent, 0);
                        }
                    }
                });
                alert.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if((int)Build.VERSION.SDK_INT>22&& ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                        else
                        {
                            Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            imageIntent.setType("image/*");
                            fragment.startActivityForResult(Intent.createChooser(imageIntent, "Select photo"), 1);
                        }
                    }
                });

                alert.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                Dialog dialog = alert.create();
                dialog.show();
            }
        });
    }

    static class ViewHolder {
        TextView name;
        TextView price;
        TextView place;
        TextView type;
        TextView status;
        TextView reOrder;
        TextView time,waitTime;
        ImageView image;
        LinearLayout pin;
        LinearLayout cal;
        LinearLayout share;
        LinearLayout camera;
        LinearLayout cancel;
        LinearLayout rate;
        LinearLayout contact;
        ImageView more;
        ImageView call;
        LinearLayout more_options;
    }
}
