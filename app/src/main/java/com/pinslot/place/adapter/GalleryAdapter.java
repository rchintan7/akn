package com.pinslot.place.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pinslot.place.R;
import com.pinslot.place.model.CSPhoto;
import com.pinslot.place.widget.PicassoUtil;

import java.util.ArrayList;

public class GalleryAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
   // private ArrayList<CSPhoto> items = new ArrayList<CSPhoto>();
    private ArrayList<Integer> items = new ArrayList<Integer>(){
        {
            add(R.mipmap.image_1);
            add(R.mipmap.image_2);
            add(R.mipmap.image_3);
            add(R.mipmap.image_4);
            add(R.mipmap.image_5);
            add(R.mipmap.image_6);
        }
    };
    FragmentActivity context;

    public GalleryAdapter(ArrayList<Integer> items, FragmentActivity context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.coupon_viewpager, container, false);
        ImageView imageview = (ImageView) view.findViewById(R.id.imageView3);
        if (items.size() > position ) {
            PicassoUtil.with(context)
                    .load(items.get(position))
                    .into(imageview);
        }/*else{
           imageview.setImageResource(items_2.get(position));
       }*/
        // imageview.setImageResource(items.get(position).imageUrl);
        ((ViewPager) container).addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == ((View) obj);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        ((ViewPager) container).removeView(view);
    }
}

