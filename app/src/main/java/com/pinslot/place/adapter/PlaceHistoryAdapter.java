package com.pinslot.place.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pinslot.place.R;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.model.PlaceBookedDetail;
import com.pinslot.place.model.PlaceSpace;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PlaceHistoryAdapter extends BaseAdapter {
    Activity context;
    ArrayList<JSONObject> hisItems;
    Fragment fragment;
    public PlaceHistoryAdapter(Activity context, ArrayList<JSONObject>hisItems, Fragment fragment){
        this.context = context;
        this.hisItems = hisItems;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return hisItems.size();
    }

    @Override
    public Object getItem(int position) {
        return hisItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder=null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.place_history_item, parent, false);
            holder = new ViewHolder();
            holder.time = (TextView) convertView.findViewById(R.id.date);
            holder.name = (TextView) convertView.findViewById(R.id.place_name);
            holder.room_name = convertView.findViewById(R.id.room_name);
            holder.address = (TextView) convertView.findViewById(R.id.address);
            holder.conformation_number = (TextView) convertView.findViewById(R.id.conformation_number);
            holder.place_image = (ImageView) convertView.findViewById(R.id.place_image);
            holder.status = (TextView) convertView.findViewById(R.id.status);
            holder.chat = convertView.findViewById(R.id.chat);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        final JSONObject object = (JSONObject) getItem(position);
        if(object != null) {
            try {
                JSONObject place = object.getJSONObject("place");
                String space_name = object.getJSONObject("space").getString("name");
                holder.room_name.setText(space_name);
                holder.name.setText(place.getString("name"));
                if (place.getJSONObject("location") != null && place.getJSONObject("location").getString("city") != null) {
                    holder.address.setText(place.getJSONObject("location").getString("addressString"));
                }
                if (object.getString("bookingId")!=null)
                    holder.conformation_number.setText(object.getString("bookingId"));
                String status = object.getString("status");
                if(status!=null)
                    holder.status.setText(status);
                Date date = new Date(object.getLong("createOn"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                String stringDate = sdf.format(date);
                PlaceBookedDetail detail = new PlaceBookedDetail(object);
                if(detail.bookedDates!=null&&detail.bookedDates.size()>0){
                    stringDate = "";
                    for(int i=0;i<detail.bookedDates.size();i++){
                        if(i==0)
                            stringDate = detail.bookedDates.get(0).getCheckinCheckout();
                        else
                            stringDate = stringDate +","+detail.bookedDates.get(i).getCheckinCheckout();
                    }
                }
                holder.time.setText(stringDate);
                holder.chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*try {
                            CSPlace place = new CSPlace(object.getJSONObject("place"));

                            String resManagerId = "";
                            String phone = "";
                            Bundle args = new Bundle();
                            if (place.managerProfile!=null) {
                                resManagerId = place.managerProfile.profileId;
                                phone = place.managerProfile.contactDetails.mobilePhone;
                                args.putString("obj", Utils.getChatObj(place.name, place.managerProfile.srcJson).toString());
                            }
                            if(resManagerId.equals(PrefManager.getInstance(context).getUserId())){
                                Toast.makeText(context,"Cannot chat yourself",Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Fragment fragment = new Communication();
                            args.putString("orderId", place.id);
                            args.putString("type", "PlaceDetails");
                            args.putString("opponentID", resManagerId);
                            args.putString("topicId", "PlaceDetails");
                            if(object.has("chatId"))
                                args.putString("chatId", object.getString("chatId"));
                            else
                                args.putString("chatId","");
                            args.putBoolean("disable_video",true);
                            args.putBoolean("disable_stream",true);
                            args.putString("phone", phone);
                            fragment.setArguments(args);
                            FragmentManager fragmentManager = PlaceHistoryAdapter.this.fragment.getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager
                                    .beginTransaction();
                            fragmentTransaction.add(R.id.fragment_place, fragment);
                            fragmentTransaction.addToBackStack(fragment.getClass().getName());
                            fragmentTransaction.commit();
                        } catch (Exception e) {
                            Log.d("Exception", e.toString());
                        }*/

                        /*try {
                            String resManagerId = "";
                            String phone = "";
                            if (object.getJSONObject("place").has("managerProfile")) {
                                resManagerId = object.getJSONObject("place").getJSONObject("managerProfile").getString("profileId");
                                if(object.getJSONObject("place").getJSONObject("managerProfile").has("contactDetails"))
                                    phone = object.getJSONObject("place").getJSONObject("managerProfile").getJSONObject("contactDetails").getString("mobilePhone");
                            }
                            Fragment fragment = new Communication();
                            Bundle args = new Bundle();
                            args.putString("orderId", object.getString("id"));
                            args.putString("type", "Places");
                            args.putString("opponentID", resManagerId);
                            args.putString("topicId", "Place");
                            args.putString("phone", phone);
                            if (object.has("chatId"))
                                args.putString("chatId", object.getString("chatId"));
                            fragment.setArguments(args);
                            FragmentManager fragmentManager = PlaceHistoryAdapter.this.fragment.getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager
                                    .beginTransaction();
                            fragmentTransaction.add(R.id.fragment_place, fragment);
                            fragmentTransaction.addToBackStack(fragment.getClass().getName());
                            fragmentTransaction.commit();
                        } catch (JSONException e){

                        }*/
                    }
                });
                if (place.getString("imageUrl")!=null) {
                    PicassoUtil.with(context).load(place.getString("imageUrl")).into(holder.place_image);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    static class ViewHolder{
        ImageView place_image,chat;
        TextView time;
        TextView name,room_name;
        TextView address;
        TextView conformation_number;
        TextView status;
    }
}
