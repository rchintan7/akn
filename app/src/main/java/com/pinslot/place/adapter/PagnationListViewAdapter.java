package com.pinslot.place.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.pinslot.place.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public abstract class PagnationListViewAdapter extends BaseAdapter {
    Context context;
    ListView list;
    boolean isLoading = false;
    boolean fullyloaded = false;
    public ArrayList<Object> objs = new ArrayList<>();
    int pageCount = 10;
    public PagnationListViewAdapter(Context context, ListView list,ArrayList<Object>objs){
        this.context = context;
        this.list = list;
        this.objs = objs;
        if(this.list!=null) {
            this.list.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount > 0 && totalItemCount % pageCount == 1 && !isLoading&&!fullyloaded) {
                        JSONObject pagination = new JSONObject();
                        if (totalItemCount % pageCount != 1||totalItemCount==1) {
                            return;
                        } else {
                            try {
                                pagination.put("pageNo", (totalItemCount / pageCount) + 1);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (totalItemCount == 0)
                                return;
                        }
                        isLoading = true;
                        PagnationListViewAdapter.this.loadMore(pagination);
                    }
                }
            });
        }
    }

    public void setPageCount(int pageCount){
        this.pageCount = pageCount;
    }

    @Override
    public int getCount() {
        if(list==null)
            return objs.size();
        else if(objs.size()==0)
            return 0;
        else if(objs.size()%pageCount==0&&!fullyloaded)
            return objs.size()+1;
        else
            return objs.size();
    }

    @Override
    public Object getItem(int position) {
        if(position<objs.size())
            return objs.get(position);
        else
            return null;
    }

    public void updateItem(Object item,int position){
        objs.remove(position);
        objs.add(position,item);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(position<objs.size())
            return getChildView(position,convertView,parent);
        else
            return LayoutInflater.from(context).inflate(R.layout.load_more_layout,parent,false);
    }

    public abstract View getChildView(int position, View convertView, ViewGroup parent);

    public void loadMore(JSONObject pagination){
    }
    public void loaded(ArrayList<Object> objects){
        if(objects.size()>0) {
            this.objs.addAll(objects);
        } else {
            fullyloaded = true;
        }
        isLoading = false;
        this.notifyDataSetChanged();
    }
}
