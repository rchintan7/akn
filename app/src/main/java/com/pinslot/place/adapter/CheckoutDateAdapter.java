package com.pinslot.place.adapter;

import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.fragment.ApplyCouponFragment;
import com.pinslot.place.fragment.PlaceBookingComplete;
import com.pinslot.place.model.Coupon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CheckoutDateAdapter extends BaseAdapter {
    Fragment fragment;
    //FoodPkg food_package = null;
    SharedPreferences pref;
    int peopleCount = 0;
    ArrayList<Date>dates;
    String type;
    double value;
    String currency;

    public CheckoutDateAdapter(Fragment fragment, ArrayList<Date>dates, String type, double value,String currency) {
        this.fragment = fragment;
        this.dates = dates;
        this.type = type;
        this.value = value;
        this.currency = currency;
        pref = fragment.getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
    }

    /*public CheckoutDateAdapter(Fragment fragment, ArrayList<MonthAvailability>monthAvailabilities,FoodPkg food_package,int peopleCount) {
        this.fragment = fragment;
        this.monthAvailabilities = monthAvailabilities;
        this.food_package = food_package;
        this.peopleCount = peopleCount;
        pref = fragment.getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
    }*/

    @Override
    public int getCount() {
        return dates.size();
    }

    public ArrayList<Date> getDates() {
        return dates;
    }

    @Override
    public Date getItem(int i) {
        return dates.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = fragment.getActivity().getLayoutInflater().inflate(R.layout.checkout_date_item, null);
        }
        final Object obj = getItem(position);
        TextView tv_date = (TextView) view.findViewById(R.id.date);
        TextView tv_amount = (TextView) view.findViewById(R.id.amount);
        TextView add_coupon = view.findViewById(R.id.add_coupon);
        TextView offer_detail = view.findViewById(R.id.off_detail);
        TextView food_pkg = view.findViewById(R.id.food_pkg);
        if(fragment instanceof PlaceBookingComplete){
            add_coupon.setVisibility(View.VISIBLE);
        }
        SimpleDateFormat sdf;
        if(type.equals("Hourly"))
            sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        else
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
        tv_amount.setText(currency + "  "+String.format("%.2f",value));
        tv_date.setText(sdf.format(getItem(position)));
        if(((NavigationDrawerActivity)fragment.getActivity()).placeRequest.coupons.containsKey(sdf.format(getItem(position)))){
            Coupon coupon = ((NavigationDrawerActivity)fragment.getActivity()).placeRequest.coupons.get(sdf.format(getItem(position)));
            tv_amount.setPaintFlags(tv_amount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            add_coupon.setText("Change Coupon");
            offer_detail.setText("("+coupon.promoName + " " + coupon.discount + "%) "+ currency+" "+String.format("%.2f",(value-(value*(coupon.discount / 100)))));
        }
        /*if (obj != null) {
            if(obj instanceof MonthAvailability) {
                MonthAvailability ma = (MonthAvailability)obj;
                tv_date.setText(ma.dateStr);
                if(food_package!=null){
                    tv_amount.setText(food_package.getCurrencySymbol()+" "+String.format("%.2f",peopleCount*food_package.price));
                    food_pkg.setVisibility(View.VISIBLE);
                    food_pkg.setText(Integer.toString(peopleCount)+" people * "+food_package.getCurrencySymbol()+String.format("%.2f",food_package.price)+" per Person");
                } else {
                    tv_amount.setText(ma.getCurrencySymbol() + " " + ma.price);

                }
                if(ma.coupon!=null){
                    if(fragment instanceof PlaceBookingComplete) {
                        tv_amount.setPaintFlags(tv_amount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        try {
                            add_coupon.setText("Change Coupon");
                            if(food_package==null)
                                offer_detail.setText("("+ma.coupon.getString("promoName") + " " + ma.coupon.getString("discount") + "%) "+ ma.getCurrencySymbol()+" "+String.format("%.2f",(ma.price-(ma.price*(ma.coupon.getDouble("discount") / 100)))));
                            else
                                offer_detail.setText("("+ma.coupon.getString("promoName") + " " + ma.coupon.getString("discount") + "%) "+ food_package.getCurrencySymbol()+" "+String.format("%.2f",(food_package.price*peopleCount-(food_package.price*peopleCount*(ma.coupon.getDouble("discount") / 100)))));
                            offer_detail.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            if(food_package==null) {
                                tv_amount.setText(ma.getCurrencySymbol() + " " + String.format("%.2f", (ma.price - (ma.price * (ma.coupon.getDouble("discount") / 100)))));
                            } else {
                                tv_amount.setText(food_package.getCurrencySymbol() + " " + String.format("%.2f", (food_package.price * peopleCount - (food_package.price * peopleCount * (ma.coupon.getDouble("discount") / 100)))));
                                food_pkg.setVisibility(View.VISIBLE);
                                food_pkg.setText(Integer.toString(peopleCount)+" people * "+food_package.getCurrencySymbol()+String.format("%.2f",food_package.price - (food_package.price * (ma.coupon.getDouble("discount") / 100)))+" per Person");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                DayAvailability da = (DayAvailability)obj;
                tv_date.setText(da.startDateTime);
                tv_amount.setText(da.getCurrencySymbol()+" "+da.hourValue);
                if(da.coupon!=null){
                    if(fragment instanceof PlaceBookingComplete) {
                        tv_amount.setPaintFlags(tv_amount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        try {
                            add_coupon.setText("Change coupon");
                            offer_detail.setText("("+da.coupon.getString("promoName") + " " + da.coupon.getString("discount") + "%) "+ da.getCurrencySymbol()+" "+(da.hourValue-(da.hourValue*(da.coupon.getDouble("discount") / 100))));
                            offer_detail.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            tv_amount.setText(da.getCurrencySymbol()+" "+(da.hourValue-(da.hourValue*(da.coupon.getDouble("discount") / 100))));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }*/
        add_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*ApplyCouponFragment fragment = new ApplyCouponFragment();
                if(obj instanceof MonthAvailability)
                    fragment.applied_coupon = ((MonthAvailability)obj).coupon;
                else
                    fragment.applied_coupon = ((DayAvailability)obj).coupon;
                PlaceAvailabilityFragment placeAvailabilityFragment = (PlaceAvailabilityFragment) CheckoutDateAdapter.this.fragment.getActivity().getSupportFragmentManager().findFragmentByTag(PlaceAvailabilityFragment.class.getName());
                if(placeAvailabilityFragment!=null)
                    fragment.applied_fbs = placeAvailabilityFragment.fbCoupons;
                Bundle args = new Bundle();
                args.putString("type","place");
                JSONObject jsonObject = new JSONObject();
                JSONArray dateTimeList = new JSONArray();
                try {
                    jsonObject.put("customerId",pref.getString("userId", ""));
                    jsonObject.put("businessId", PlaceSpaceTab.businessID);
                    jsonObject.put("businessType","PLACE");
                    if(obj instanceof MonthAvailability){
                        jsonObject.put("rentType","DAILY");
                        MonthAvailability availability = (MonthAvailability)obj;
                        JSONObject obj = new JSONObject();
                        obj.put("fromDateTimeStr",availability.dateStr+" "+availability.start_time);
                        obj.put("toDateTimeStr",availability.dateStr+" "+availability.end_time);
                        dateTimeList.put(obj);
                    } else {
                        jsonObject.put("rentType","HOURLY");
                        DayAvailability availability = (DayAvailability) obj;
                        JSONObject obj = new JSONObject();
                        obj.put("fromDateTimeStr",availability.startDateTime);
                        String[] val = availability.startDateTime.split(" ");
                        obj.put("toDateTimeStr",val[0]+" "+availability.endTime);
                        dateTimeList.put(obj);
                    }
                    jsonObject.put("dateTimeList",dateTimeList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                args.putString("json",jsonObject.toString());
                args.putString("url", Constants.GET_PLACE_PROMO);
                fragment.setArguments(args);
                fragment.listener = new Event_Promotion_Adapter.ApplyListener() {
                    @Override
                    public void Apply(JSONObject jsonObject) {
                        if(obj instanceof MonthAvailability){
                            ((MonthAvailability)obj).coupon = jsonObject;
                            CheckoutDateAdapter.this.notifyDataSetChanged();
                            updatePlaceCalenderInstance((MonthAvailability)obj,null);
                        } else {
                            ((DayAvailability)obj).coupon = jsonObject;
                            CheckoutDateAdapter.this.notifyDataSetChanged();
                            updatePlaceCalenderInstance(null,(DayAvailability) obj);
                        }
                    }

                    @Override
                    public void fbApply(JSONObject jsonObject) {
                        PlaceAvailabilityFragment pa = (PlaceAvailabilityFragment)CheckoutDateAdapter.this.fragment.getActivity().getSupportFragmentManager().findFragmentByTag(PlaceAvailabilityFragment.class.getName());
                        if(pa!=null){
                            boolean found = false;
                            for(JSONObject obj : pa.fbCoupons){
                                try {
                                    if(obj.getString("id").equals(jsonObject.getString("id"))) {
                                        found = true;
                                        break;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if(!found)
                                pa.fbCoupons.add(jsonObject);
                        }
                        if(obj instanceof MonthAvailability){
                            ((MonthAvailability)obj).coupon = jsonObject;
                            CheckoutDateAdapter.this.notifyDataSetChanged();
                            updatePlaceCalenderInstance((MonthAvailability)obj,null);
                        } else {
                            ((DayAvailability)obj).coupon = jsonObject;
                            CheckoutDateAdapter.this.notifyDataSetChanged();
                            updatePlaceCalenderInstance(null,(DayAvailability) obj);
                        }
                    }
                };
                FragmentTransaction tx = CheckoutDateAdapter.this.fragment.getActivity().getSupportFragmentManager().beginTransaction();
                tx.add(R.id.fragment_place,fragment);
                tx.addToBackStack(fragment.getClass().getName());
                tx.commit();*/
            }
        });
        return view;
    }

    /*--public void updatePlaceCalenderInstance(MonthAvailability ma,DayAvailability da){
        if(fragment instanceof PlaceBookingComplete){
            ((PlaceBookingComplete)fragment).UpdateTotal();
            PlaceAvailabilityFragment pa = (PlaceAvailabilityFragment) fragment.getActivity().getSupportFragmentManager().findFragmentByTag(PlaceAvailabilityFragment.class.getName());
            if(pa!=null&&ma!=null){
                pa.updateCoupon(ma);
            } else if(pa!=null&&da!=null){
                pa.updateCoupon(da);
            }
        }
    }*/
}
