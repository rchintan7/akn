package com.pinslot.place.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.fragment.BaseFragment;
import com.pinslot.place.fragment.RoomListingFragment;

import java.util.ArrayList;

/**
 * Created by Selva on 10/24/2018.
 */

public class FacilityAdapter  extends BaseAdapter {
    Context context;
    Fragment fragment;
    String[] facilityArr;
    int[] imageArr;

    public FacilityAdapter(Fragment fragment, String[] arr,int[] images) {
        this.fragment = fragment;
        this.context = fragment.getContext();
        this.facilityArr=arr;
        this.imageArr=images;

    }

    @Override
    public int getCount() {
        return facilityArr.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.facility_adapter, viewGroup, false);
        ImageView image = rowView.findViewById(R.id.image_view);
        TextView text = rowView.findViewById(R.id.text_view);
        text.setText(facilityArr[position]);
        image.setImageResource(imageArr[position]);
        return rowView;
    }
}

