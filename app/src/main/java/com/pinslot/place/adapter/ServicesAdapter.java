package com.pinslot.place.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pinslot.place.R;

public class ServicesAdapter extends RecyclerView.Adapter {
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       return new ServiceViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.services_item,viewGroup,false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {


    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class ServiceViewHolder extends RecyclerView.ViewHolder{

        public ServiceViewHolder(View itemView) {
            super(itemView);
        }


    }


}
