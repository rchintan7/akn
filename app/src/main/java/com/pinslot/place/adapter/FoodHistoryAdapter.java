package com.pinslot.place.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.fragment.FoodHistoryFragment;
import com.pinslot.place.widget.CornerImageView;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FoodHistoryAdapter extends BaseAdapter {

    ArrayList<JSONObject> items = new ArrayList<>();
    Fragment fragment;
    Context context;
    public FoodHistoryAdapter(ArrayList<JSONObject> items, Fragment fragment){
        this.items = items;
        this.fragment = fragment;
        this.context = fragment.getContext();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public JSONObject getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.food_history_item,viewGroup,false);
        CornerImageView image = view.findViewById(R.id.image);
        TextView order_id = view.findViewById(R.id.order_id);
        TextView date = view.findViewById(R.id.date);
        TextView price = view.findViewById(R.id.price);
        TextView status = view.findViewById(R.id.status);
        final JSONObject object = (JSONObject) getItem(i);
        try {
            order_id.setText("Order Id : " + object.getString("bookingId"));
            JSONObject res = object.getJSONObject("restaurant");
            double total = object.getDouble("finalPrice");
            if(object.has("pickupDate")){
                Date date1 = new Date(object.getLong("pickupDate"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy@hh:mm a");
                date.setText("Time : "+sdf.format(date1));
            }
            if(total>0) {
                price.setVisibility(View.VISIBLE);
                price.setText(object.getString("currency") + String.format("%.2f", total));
            } else {
                price.setVisibility(View.GONE);
            }
            if (res.getJSONArray("photoUrls").length() > 0) {
                PicassoUtil.with(context).load(res.getJSONArray("photoUrls").get(0).toString()).into(image);
            }
            status.setText(object.getString("status"));
        } catch (JSONException e){

        }
        return view;
    }
}
