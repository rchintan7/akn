package com.pinslot.place.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pinslot.place.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Coupon_Banner_Adapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private ArrayList<String> items = new ArrayList<>();
    FragmentActivity context;

    public Coupon_Banner_Adapter(ArrayList<String> items, FragmentActivity context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.coupon_viewpager, container, false);
        ImageView imageview = (ImageView) view.findViewById(R.id.imageView3);
        if (items.size() > position && !items.get(position).isEmpty()) {
            Picasso.with(context)
                    .load(items.get(position))
                    .into(imageview);
        }
        ((ViewPager) container).addView(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*ArrayList<GalleryData> galleryData = new ArrayList<>();
                galleryData.add(new GalleryData("Images", items));
                GalleryFragment galleryFragment = new GalleryFragment().newInstance(galleryData, position);
                android.support.v4.app.FragmentManager fragmentManager = context.getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, galleryFragment);
                fragmentTransaction.addToBackStack(galleryFragment.getClass().getName());
                fragmentTransaction.commit();*/
            }
        });
        return view;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == ((View) obj);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        ((ViewPager) container).removeView(view);
    }
}