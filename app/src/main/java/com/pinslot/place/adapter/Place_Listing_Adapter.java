package com.pinslot.place.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.fragment.BaseFragment;
import com.pinslot.place.fragment.RoomListingFragment;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.model.Coupon;
import com.pinslot.place.model.langlatimoel;
import com.pinslot.place.widget.PicassoUtil;

import java.util.ArrayList;

/**
 * Created by Selva on 10/23/2018.
 */

public class Place_Listing_Adapter  extends BaseAdapter {

    Context context;
    BaseFragment fragment;
    ArrayList<CSPlace> listvalues;;
    ArrayList<langlatimoel> langlatimoelArrayList;;

    public Place_Listing_Adapter(Context context,ArrayList<CSPlace> listvalues, ArrayList<langlatimoel> langlatimoelArrayList,BaseFragment fragment) {
        this.fragment = fragment;
        this.context = fragment.getContext();
        this.listvalues=listvalues;
        this.langlatimoelArrayList=langlatimoelArrayList;
    }

    @Override
    public int getCount() {
        return listvalues.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.place_list_items, viewGroup, false);
        TextView name=(TextView)rowView.findViewById(R.id.name);
        TextView address=(TextView)rowView.findViewById(R.id.address);
        TextView distance=(TextView)rowView.findViewById(R.id.distance);
        TextView price=(TextView)rowView.findViewById(R.id.price);
        ImageView image=(ImageView)rowView.findViewById(R.id.image);
        RatingBar ratingBar=(RatingBar)rowView.findViewById(R.id.ratingBar2);

        ImageView view_rooms = rowView.findViewById(R.id.view_rooms);
        TextView view_meeting = rowView.findViewById(R.id.view_meeting);
        if(((NavigationDrawerActivity)this.fragment.getActivity()).placeRequest.is_space){
            view_meeting.setVisibility(View.VISIBLE);
            view_rooms.setVisibility(View.GONE);
        } else {
            view_meeting.setVisibility(View.GONE);
            view_rooms.setVisibility(View.VISIBLE);
        }
        final CSPlace place=listvalues.get(position);
        name.setText(place.name);
        address.setText(place.location.addressLine2 +" - "+ place.location.city);

        Double destilatitiute=  langlatimoelArrayList.get(position).getLatitute();
        Double destilongitute= langlatimoelArrayList.get(position).getLongitute();

        double theta = Utils.userlongitute - destilongitute;
        double dist = Math.sin(deg2rad(Utils.userlatitude))
                * Math.sin(deg2rad(destilatitiute))
                + Math.cos(deg2rad(Utils.userlatitude))
                * Math.cos(deg2rad(destilatitiute))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        distance.setText(String.format("%.2f",dist)+" KM" +" from "+Utils.cityname);
        price.setText(place.getCurrencySymbol()+" "+Math.round(place.minPrice));

        if(place.rating!=0){
            ratingBar.setRating((float) place.rating);
        }else {
            ratingBar.setRating(Float.parseFloat("5"));
        }

        if(place.imageUrl.length()>0) {
            PicassoUtil.with(context)
                    .load(place.imageUrl).placeholder(R.mipmap.image_1)
                    .into(image);
        }
        view_rooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utils.roomvale=position;
                ((NavigationDrawerActivity)fragment.getActivity()).placeRequest.place = place;
                fragment.addFragment(new RoomListingFragment(),true);
            }
        });

        view_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerActivity)fragment.getActivity()).placeRequest.place = place;
                fragment.addFragment(new RoomListingFragment(),true);
            }
        });
        return rowView;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
