package com.pinslot.place.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.model.PlaceSpace;
import com.pinslot.place.widget.PicassoUtil;

import java.util.ArrayList;

public class RoomListingAdapter extends BaseAdapter {

    ArrayList<PlaceSpace> spaces;
    Context context;
    public RoomListingAdapter(Context context,ArrayList<PlaceSpace> spaces){
        this.spaces = spaces;
        this.context=context;
    }

    @Override
    public int getCount() {
        return spaces.size();
    }

    @Override
    public PlaceSpace getItem(int i) {
        return spaces.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.room_list_item, viewGroup, false);
        ImageView image=(ImageView)rowView.findViewById(R.id.image);

        TextView choose= rowView.findViewById(R.id.choose);
        ImageView img_open_room= rowView.findViewById(R.id.img_open_room);

        TextView name=rowView.findViewById(R.id.room_name);
        TextView gCount=rowView.findViewById(R.id.guest_count);
        TextView bedCount=rowView.findViewById(R.id.bed_count); //
        TextView breakfast=rowView.findViewById(R.id.breakfast); //
        TextView fundType=rowView.findViewById(R.id.refound); //
        TextView amenites=rowView.findViewById(R.id.wifi);
        TextView offer=rowView.findViewById(R.id.off); //
        TextView leftCount=rowView.findViewById(R.id.left); //
        TextView price=rowView.findViewById(R.id.price);
        TextView size=rowView.findViewById(R.id.size);
        TextView offerPrice=rowView.findViewById(R.id.offer_price);

        PlaceSpace space=spaces.get(position);

        name.setText(space.name);
        gCount.setText(space.capacity+" guests/Room");
        amenites.setText(space.amenities.get(0));
        price.setText(space.price.currencyType+" "+Math.round(space.price.dayValue));
        size.setText("Room Size :"+space.size+" "+space.unit);

        bedCount.setText(space.bedCount);
        //breakfast.setText();
        if(space.availRefund.equalsIgnoreCase("false"))
        fundType.setText("Not Refundable");
        else
            fundType.setText("Refundable");

        if(space.enableDiscount.equalsIgnoreCase("false")) {
            offer.setVisibility(View.INVISIBLE);
            offerPrice.setVisibility(View.INVISIBLE);
        }
        else {
            price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            double offerVal=(double)space.distcountPercent/(double)100*(space.price.dayValue);
            offerPrice.setText(space.price.currencyType+" "+Math.round(space.price.dayValue-offerVal));
            offer.setText("Sale off :" + space.distcountPercent + "%");
        }

        /*price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        double offerVal=(double) 13/(double) 100*(space.price.dayValue);
        offerPrice.setText(space.price.currencyType+" "+Math.round(space.price.dayValue-offerVal));
        offer.setText("Sale off :" + space.distcountPercent + "%");*/

        if( Utils.roomvale==0){
            img_open_room.setVisibility(View.VISIBLE);
        }else {
            img_open_room.setVisibility(View.GONE);
        }

        if(position==3){
            img_open_room.setVisibility(View.GONE);
        }else if(position==4){
            img_open_room.setVisibility(View.GONE);
        }



        img_open_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(position==0){


                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.open_room_layout);
                    ImageView img_one=(ImageView)dialog.findViewById(R.id.img_one);
                    ImageView img_two=(ImageView)dialog.findViewById(R.id.img_two);
                    ImageView img_three=(ImageView)dialog.findViewById(R.id.img_three);

                    img_one.setVisibility(View.VISIBLE);
                    img_two.setVisibility(View.GONE);
                    img_three.setVisibility(View.GONE);
                    dialog.show();

                }else if(position==1){
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.open_room_layout);
                    ImageView img_one=(ImageView)dialog.findViewById(R.id.img_one);
                    ImageView img_two=(ImageView)dialog.findViewById(R.id.img_two);
                    ImageView img_three=(ImageView)dialog.findViewById(R.id.img_three);

                    img_one.setVisibility(View.GONE);
                    img_two.setVisibility(View.VISIBLE);
                    img_three.setVisibility(View.GONE);
                    dialog.show();

                }else if(position==2){
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.open_room_layout);
                    ImageView img_one=(ImageView)dialog.findViewById(R.id.img_one);
                    ImageView img_two=(ImageView)dialog.findViewById(R.id.img_two);
                    ImageView img_three=(ImageView)dialog.findViewById(R.id.img_three);

                    img_one.setVisibility(View.GONE);
                    img_two.setVisibility(View.GONE);
                    img_three.setVisibility(View.VISIBLE);
                    dialog.show();
                }else {

                }

            }
        });

        leftCount.setText(space.availRoomCount);

        if(space.imageUrl.length()>0) {
            PicassoUtil.with(context)
                    .load(space.imageUrl).placeholder(R.mipmap.image_1)
                    .into(image);
        }else{
            if(space.photoUrls.size()>0)
            PicassoUtil.with(context)
                    .load(space.photoUrls.get(0)).placeholder(R.mipmap.image_1)
                    .into(image);
        }

        return rowView;
    }
}
