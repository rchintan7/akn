package com.pinslot.place.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.fragment.HistoryDetails;
import com.pinslot.place.model.CSPhoto;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Selva on 10/25/2018.
 */

public class AdminNotificationAdapter extends BaseAdapter {

    FragmentActivity context;
    Fragment fragment;
    //ArrayList<RetailAdminReq> items = new ArrayList<>();
    String type = "admin_incoming_notification";
    String navigationType;
    JSONArray orderList;
    public AdminNotificationAdapter(JSONArray orders, FragmentActivity context) {
        this.context = context;
        this.orderList = orders;
    }

    @Override
    public int getCount() {
        return orderList.length();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null || convertView.getTag() == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.admin_notification_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();


        holder.bindView(position);
        return convertView;
    }

    class ViewHolder {
        View v;
        public ImageView userImage;
        public TextView address;
        public TextView time;
        public TextView name;
        public TextView OrderId;
        public TextView price;

        public ViewHolder(View v) {
            this.v = v;
            this.userImage = v.findViewById(R.id.user_image);
            this.address = v.findViewById(R.id.address);
            this.time = v.findViewById(R.id.time);
            this.name = v.findViewById(R.id.name);
            this.OrderId = v.findViewById(R.id.order_id);
            this.price = v.findViewById(R.id.price);
        }

        public void bindView(final int position) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject jsonObject = orderList.getJSONObject(position);
                        android.support.v4.app.Fragment fragment = new HistoryDetails();
                        Bundle args = new Bundle();
                        args.putString("JSON", jsonObject.toString());
                        fragment.setArguments(args);
                        FragmentTransaction fragmentTransaction = context.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.add(R.id.fragment_place, fragment, fragment.getClass().getName());
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();
                    }catch (Exception e){
                    }
                }
            });

            //Data Plotting
            try{
                JSONObject jsonObject=orderList.getJSONObject(position);
                Date date = new Date(jsonObject.getLong("checkIn"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                String stringDate = sdf.format(date);
                time.setText(stringDate);
                name.setText(jsonObject.getString("name"));
                OrderId.setText("Order ID :"+jsonObject.getString("bookingId"));
                String currency="";
                if(jsonObject.getString("currencyCode").equals("RS")||jsonObject.getString("currencyCode").equals("INR"))
                    currency= "₹";
                else
                    currency= "$";
                price.setText(currency+""+jsonObject.getString("finalPrice"));


            }catch (Exception e){

            }


        }
    }
}

