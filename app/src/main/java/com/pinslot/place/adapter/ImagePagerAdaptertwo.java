package com.pinslot.place.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pinslot.place.R;
import com.pinslot.place.model.CSPhoto;
import com.pinslot.place.widget.PicassoUtil;

import java.util.ArrayList;

public class ImagePagerAdaptertwo extends PagerAdapter {
    private LayoutInflater layoutInflater;
  //  private ArrayList<CSPhoto> items = new ArrayList<CSPhoto>();
    private ArrayList<Integer> items_2 = new ArrayList<Integer>();
    FragmentActivity context;

    public ImagePagerAdaptertwo(ArrayList<Integer> items_2, FragmentActivity context) {
        this.items_2 = items_2;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.coupon_viewpager, container, false);
        ImageView imageview = (ImageView) view.findViewById(R.id.imageView3);


        if (items_2.size() > position ) {
            PicassoUtil.with(context)
                    .load(items_2.get(position))
                    .into(imageview);
        }
        else{
           imageview.setImageResource(items_2.get(position));
       }
        // imageview.setImageResource(items.get(position).imageUrl);
        ((ViewPager) container).addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return items_2.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == ((View) obj);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        ((ViewPager) container).removeView(view);
    }
}
