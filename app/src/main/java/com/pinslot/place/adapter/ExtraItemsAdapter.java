package com.pinslot.place.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.R;

public class ExtraItemsAdapter extends BaseAdapter {

    String serviceTxt[]={"Shampoo","Toothbrush","Cocktail","Towels","Bar Refill","Cleaning"};
    int serviceImage[]={R.mipmap.shampoo,R.mipmap.toothbrush_icon,R.mipmap.coctail,R.mipmap.towels,R.mipmap.bar_refill_icon,R.mipmap.housekeeping_icon};

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.extra_items_grid_item, viewGroup, false);
        ImageView image = rowView.findViewById(R.id.image);
        TextView text = rowView.findViewById(R.id.text);

        image.setImageResource(serviceImage[i]);
        text.setText(serviceTxt[i]);

        return rowView;
    }
}
