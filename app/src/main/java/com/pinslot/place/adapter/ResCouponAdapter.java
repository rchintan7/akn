package com.pinslot.place.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.pinslot.place.NavigationDrawerActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.pinslot.place.R;

import java.util.Date;

public class ResCouponAdapter extends BaseAdapter {

    Fragment fragment;
    JSONArray coupons;
    String applied_coupon = "", applied_fb_coupon = "";
    Dialog dialog;
    ApplyListener applyListener;
    ShareDialog shareDialog;
    CallbackManager fbManager;
    JSONObject fbCoupon;
    boolean fbCheck = true;

    public ResCouponAdapter(Fragment fragment, JSONArray coupons, String applied_coupon, String applied_fb_coupon, Dialog dialog, Date date) {
        this.fragment = fragment;
        this.coupons = coupons;
        this.applied_coupon = applied_coupon;
        this.applied_fb_coupon = applied_fb_coupon;
        this.dialog = dialog;
        fbManager = CallbackManager.Factory.create();
    }

    public void setApplyListener(ApplyListener applyListener) {
        this.applyListener = applyListener;
    }

    @Override
    public int getCount() {
        return coupons.length();
    }

    @Override
    public JSONObject getItem(int position) {
        try {
            return coupons.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void activityResult(int requestCode, int resultCode, Intent data) {
        fbManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && fbCheck == true) {
            Toast.makeText(fragment.getActivity(), " You shared this post", Toast.LENGTH_SHORT).show();
            try {
                SharedPreferences pref = fragment.getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
                if (pref.getString("loginSession", null) != null) {
                    if (pref.getString("loginSession", null).equals("Valid")) {
                        if (applyListener != null && fbCoupon != null)
                            applyListener.fbApply(fbCoupon);
                    } else {
                        //Intent i = new Intent();
                        //i.setClassName(fragment.getActivity(), SignIn.class.getName());
                        //fragment.startActivity(i);
                    }
                } else {
                    //Intent i = new Intent();
                    //i.setClassName(fragment.getActivity(), SignIn.class.getName());
                    //fragment.startActivity(i);
                }

            } catch (Exception e) {
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) fragment.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.promotion_offer_adapter, parent, false);
        TextView name = (TextView) rowView.findViewById(R.id.name);
        TextView code = (TextView) rowView.findViewById(R.id.code);
        TextView off = (TextView) rowView.findViewById(R.id.off);
        TextView menu_item = rowView.findViewById(R.id.menu_item);
        ImageView offImage = (ImageView) rowView.findViewById(R.id.offerImage);
        ImageView offtype = rowView.findViewById(R.id.offer_type);
        ImageView share = rowView.findViewById(R.id.btn_share);
        TextView apply = rowView.findViewById(R.id.apply);
        TextView actual_price = rowView.findViewById(R.id.actual_price);
        TextView offer_price = rowView.findViewById(R.id.offer_price);
        /*if (fragment instanceof PromotionalOffer) {
            apply.setVisibility(View.GONE);
        }*/
        final JSONObject jsonObject = getItem(position);
        try {
            if (applied_coupon.equals(jsonObject.getString("id"))) {
                apply.setText("Applied");
                apply.setEnabled(false);
            } else if (applied_fb_coupon.equals(jsonObject.getString("id"))) {
                apply.setText("Applied");
                apply.setEnabled(false);
            } else
                apply.setText("Apply");
            if (jsonObject.has("couponCode"))
                code.setText(Html.fromHtml(jsonObject.getString("couponCode")));
            if (jsonObject.has("discount"))
                off.setText(jsonObject.getString("discount") + "%");
            if (jsonObject.has("promoName"))
                name.setText(jsonObject.getString("promoName"));
            if (jsonObject.has("imageUrl") && !jsonObject.getString("imageUrl").isEmpty())
                Picasso.with(fragment.getContext()).load(jsonObject.getString("imageUrl")).into(offImage);
            actual_price.setVisibility(View.GONE);
            offer_price.setVisibility(View.GONE);
            JSONObject restriction = jsonObject.getJSONObject("restriction");
            if (restriction.getBoolean("onClientBirthday")) {
                offtype.setImageResource(R.mipmap.place_birthday_offer);
            } else if (restriction.getBoolean("onClientWedding")) {
                offtype.setImageResource(R.mipmap.place_wedding_offer);
            } else if (restriction.getBoolean("shareFacebook")) {
                offtype.setImageResource(R.mipmap.promotion_fb_button);
            } else if (restriction.getBoolean("shareInstagram")) {
                offtype.setImageResource(R.mipmap.instagram_enable);
            } else if (restriction.getBoolean("shareTwitter")) {
                offtype.setImageResource(R.mipmap.twitter_enable);
            } else if (restriction.getBoolean("newClientOnly")) {
                offtype.setImageResource(R.mipmap.new_client);
            } else if (restriction.getBoolean("oneUsePerClient")) {
                offtype.setImageResource(R.mipmap.one_time);
            } else if (restriction.getBoolean("isGeneral")) {
                offtype.setImageResource(R.mipmap.general_offer2);
            } else {
                offtype.setVisibility(View.GONE);
            }
        } catch (JSONException e) {

        }
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fbOffer = false;
                boolean dismiss = true;
                try {
                    if (jsonObject.has("restriction") && jsonObject.getJSONObject("restriction").has("shareFacebook") && jsonObject.getJSONObject("restriction").getBoolean("shareFacebook")) {
                        fbOffer = true;
                    }
                } catch (Exception e) {

                }
                if (fbOffer) {
                    fbCoupon = jsonObject;
                    String res_name = "";
                    if (((NavigationDrawerActivity) fragment.getActivity()).resReq != null && ((NavigationDrawerActivity) fragment.getActivity()).resReq.selectedRestaurant != null)
                        res_name = ((NavigationDrawerActivity) fragment.getActivity()).resReq.selectedRestaurant.name;
                    FacebookSdk.sdkInitialize(fragment.getContext());
                    shareDialog = new ShareDialog(fragment);
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle("PinSlots")
                            //.setQuote("I'm going to enjoy delicious food at " + res_name + " today.  I highly recommend this restaurant to everyone")
                            .setContentDescription("I'm going to enjoy delicious food at " + res_name + " today.  I highly recommend this restaurant to everyone")
                            .setContentUrl(Uri.parse("http://www.pinslots.com/#/home"))
                            .build();
                    shareDialog.show(linkContent);
                    shareDialog.registerCallback(fbManager, shareCallback);
                } else if (applyListener != null) {
                    applyListener.Apply(jsonObject);
                }
                if (dialog != null && dismiss)
                    dialog.dismiss();
            }
        });
        return rowView;
    }

    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            // TODO Auto-generated method stub
            fbCheck = true;
            Toast.makeText(fragment.getActivity(), "You shared this post", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(FacebookException error) {
            // TODO Auto-generated method stub
            Toast.makeText(fragment.getActivity(), "You shared this Error", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel() {
            // TODO Auto-generated method stub
            fbCheck = false;
            Toast.makeText(fragment.getActivity(), "You shared this Cancel", Toast.LENGTH_SHORT).show();
        }
    };

    public interface ApplyListener {
        public void Apply(JSONObject jsonObject);

        public void fbApply(JSONObject jsonObject);
    }
}
