package com.pinslot.place.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class AmmentiesAdapter extends BaseAdapter {

    String[] amenties;
    HashSet<String> selected;
    HashMap<String,Integer> items;
    public AmmentiesAdapter(String[] amenties, HashMap<String,Integer>items,HashSet<String> selected ){
        this.amenties = amenties;
        this.items = items;
        this.selected = selected;
    }

    @Override
    public int getCount() {
        return amenties.length;
    }

    @Override
    public Object getItem(int i) {
        return amenties[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.amenties_grid_item,viewGroup,false);
        ImageView icon = view1.findViewById(R.id.icon);
        TextView text = view1.findViewById(R.id.text);
        String item = (String) getItem(i);
        text.setText(item);
        icon.setImageResource(items.get(item));
        if(selected.contains(item)){
            icon.setColorFilter(viewGroup.getContext().getResources().getColor(R.color.colorAccent));
            text.setTextColor(viewGroup.getContext().getResources().getColor(R.color.colorAccent));
        } else {
            icon.setColorFilter(viewGroup.getContext().getResources().getColor(R.color.gray));
            text.setTextColor(viewGroup.getContext().getResources().getColor(R.color.gray));
        }
        return view1;
    }
}
