package com.pinslot.place.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.pinslot.place.R;
import com.pinslot.place.model.GoogleReview;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by New System 2 on 7/17/2017.
 */

public class RestaurantReviewsAdapter extends BaseAdapter {

    Context context;
    JSONArray reviews;
    ArrayList<GoogleReview> greviews = new ArrayList<>();
    public RestaurantReviewsAdapter(Context context, JSONArray reviews, JSONArray Greviews){
        this.context = context;
        this.reviews = reviews;
        this.greviews.clear();
        for(int i=0;i<Greviews.length();i++) {
            try {
                this.greviews.add(new GoogleReview(Greviews.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public RestaurantReviewsAdapter(Context context, ArrayList<GoogleReview> Greviews){
        this.context = context;
        this.reviews = new JSONArray();
        this.greviews = Greviews;
    }
    @Override
    public int getCount() {
        return reviews.length()+greviews.size();
    }
    @Override
    public Object getItem(int position) {
        try {
            if(position<greviews.size())
                return greviews.get(position);
            else
                return reviews.getJSONObject(position-greviews.size());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.comment_adapter, parent, false);
        final TextView message = (TextView) rowView.findViewById(R.id.text_message);
        final TextView name = (TextView) rowView.findViewById(R.id.name);
        final RatingBar rating = (RatingBar) rowView.findViewById(R.id.ratingBar);
        final ImageView userImage = (ImageView) rowView.findViewById(R.id.user_icon);
        final Button rating_btn = (Button) rowView.findViewById(R.id.rating_btn);
        TextView time = rowView.findViewById(R.id.time);
        TextView date = rowView.findViewById(R.id.date);
        date.setVisibility(View.GONE);

        try {
            Object object = getItem(position);
                name.setText(((JSONObject)object).getString("userFirstName"));
                message.setText(((JSONObject)object).getString("text"));
                rating.setRating(Float.parseFloat(((JSONObject)object).getString("rating")));
                if(((JSONObject)object).has("userImgUrl")&&!((JSONObject)object).getString("userImgUrl").isEmpty()){
                    PicassoUtil.with(context).load(((JSONObject)object).getString("userImgUrl")).into(userImage);
                }
                if(((JSONObject)object).has("createOn"))
                {
                    time.setText(((JSONObject)object).getString("createOn"));
                    time.setVisibility(View.VISIBLE);
                } else {
                    time.setVisibility(View.GONE);
                }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rowView;
    }
}
