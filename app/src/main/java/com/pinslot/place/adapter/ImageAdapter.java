package com.pinslot.place.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.model.CSPhoto;
import com.pinslot.place.widget.PicassoUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Selva on 10/25/2018.
 */

public class ImageAdapter extends BaseAdapter {

    ArrayList<Integer> items = new ArrayList<Integer>();
    Activity context;

    public ImageAdapter(ArrayList<Integer> items, Activity context){
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.coupon_viewpager,viewGroup,false);
        ImageView imageview = (ImageView) view.findViewById(R.id.imageView3);
        if (items.size() > i ) {
            PicassoUtil.with(context)
                    .load(items.get(i))
                    .into(imageview);
        }
        return view1;
    }
}
