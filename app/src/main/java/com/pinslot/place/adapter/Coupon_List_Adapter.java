package com.pinslot.place.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.helpers.GPSService;
import com.pinslot.place.model.Coupon;
import com.pinslot.place.model.Restaurant;
import com.pinslot.place.model.RestaurantMenuCoupon;
import com.pinslot.place.model.Restriction;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Coupon_List_Adapter  extends BaseAdapter {
    private final Context context;
    String type = "Restaurant", currencyType;
    Fragment fragment;
    String screentype = "res";
    double lat = 0.0;
    double lng = 0.0;
    boolean disable_apply = false;
    ArrayList<Coupon> general_coupons;
    ArrayList<Coupon> menu_coupons;

    public Coupon_List_Adapter(Fragment fragment, ArrayList<Coupon> general_coupons,ArrayList<Coupon> menu_coupons) {
        this.context = fragment.getContext();
        this.type = type;
        this.fragment = fragment;
        this.general_coupons = general_coupons;
        this.menu_coupons = menu_coupons;
    }

    public void setGeneral_coupons(ArrayList<Coupon> general_coupons) {
        this.general_coupons = general_coupons;
    }

    public void setMenu_coupons(ArrayList<Coupon> menu_coupons) {
        this.menu_coupons = menu_coupons;
    }

    @Override
    public int getCount() {
        /*int size = 0;
        if(general_coupons.size()>0){
            size =  general_coupons.size()+1;
        }
        if(menu_coupons.size()>0) {
            size = size+ general_coupons.size()+1;
        }
        return size;*/
        if(general_coupons.size()>0)
            return general_coupons.size();
        else
            return menu_coupons.size();
    }

    @Override
    public Object getItem(int i) {
        /*int generalCuponSize = general_coupons.size()>0?general_coupons.size()+1:0;
        if(i==0&&generalCuponSize>0){
            return "General Coupon";
        } else if(i==generalCuponSize&&menu_coupons.size()>0){
            return "Menu Coupon";
        } else if (i<generalCuponSize)
            return general_coupons.get(i-1);
        else {
            return menu_coupons.get(i-generalCuponSize-1);
        }*/
        if(general_coupons.size()>0){
            return general_coupons.get(i);
        } else {
            return menu_coupons.get(i);
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(getItem(position) instanceof String){
            View rowView = inflater.inflate(R.layout.coupon_list_header, viewGroup, false);
            TextView header = rowView.findViewById(R.id.header);
            header.setText((String)getItem(position));
            return rowView;
        }
        View rowView = inflater.inflate(R.layout.coupon_list_adapter, viewGroup, false);
        TextView expired = (TextView) rowView.findViewById(R.id.expired);
        TextView valid = (TextView) rowView.findViewById(R.id.valid);
        TextView price = (TextView) rowView.findViewById(R.id.price);
        TextView discount = (TextView) rowView.findViewById(R.id.discount);
        TextView itemName = (TextView) rowView.findViewById(R.id.item_name);
        TextView offer = (TextView) rowView.findViewById(R.id.offer);
        ImageView itemImage = (ImageView) rowView.findViewById(R.id.place_images);
        ImageView fav_click_img = (ImageView) rowView.findViewById(R.id.fav_click_img);
        ImageView facebookBanner = (ImageView) rowView.findViewById(R.id.facebook_banner);
        ImageView couponImage = (ImageView) rowView.findViewById(R.id.coupon_image);
        LinearLayout hourly_coupon = rowView.findViewById(R.id.hourly_coupon);
        LinearLayout multipledates = rowView.findViewById(R.id.multiple_dates);
        LinearLayout price_layout = rowView.findViewById(R.id.price_layout);
        TextView date1 = rowView.findViewById(R.id.date1);
        TextView date2 = rowView.findViewById(R.id.date2);
        try {
            final JSONObject jsonObject;
            //if (type.equals("Restaurant") || type.equals("socialorder")||type.equals("places")) {
            //    jsonObject = (Coupon) getItem(position);
            //} else {
            //    jsonObject = ((JSONObject) getItem(position)).getJSONObject("promoCode");
            //}
            Restaurant restaurant = null;
            final Coupon coupon = (Coupon)getItem(position);
            jsonObject = coupon.srcJson;
            double discount_amount = 0;
            double afterDiscount = 0;
            /*fav_click_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0);
                    if (pref.getString("loginSession", null) != null) {
                        if (pref.getString("loginSession", null).equals("Valid")) {
                            //Do Something
                            if (type.equalsIgnoreCase("Favorites")) {
                                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                alert.setMessage(R.string.remove_fav);
                                alert.setPositiveButton("PLEASE DO IT!", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        webServiceCallForSetFav(coupon.id, true);
                                    }
                                });
                                alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alert.show();
                            } else {
                                if (coupon.isFavourite) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                                    alert.setMessage(R.string.remove_fav);
                                    alert.setPositiveButton("PLEASE DO IT!", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            webServiceCallForSetFav(coupon.id, true);
                                        }
                                    });
                                    alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    alert.show();
                                } else {
                                    webServiceCallForSetFav(coupon.id, false);
                                }
                            }

                        }
                    } else {
                        Intent i = new Intent();
                        i.setClassName(context, ResSigninActivity.class.getName());
                        context.startActivity(i);
                    }
                }
            });*/
            Restriction restriction = coupon.restriction;
            int default_image = R.mipmap.image_1;
            if (restriction.onClientBirthday) {
                couponImage.setImageResource(R.mipmap.place_birthday_offer);
                default_image = R.mipmap.birthday_offer_banner;
            } else if (restriction.onClientWedding) {
                couponImage.setImageResource(R.mipmap.place_wedding_offer);
                default_image = R.mipmap.wedding_offer_banner;
            } else if (restriction.shareFacebook) {
                couponImage.setImageResource(R.mipmap.promotion_fb_button);
                default_image = R.mipmap.facebook_offer_banner;
            } else if (restriction.shareInstagram) {
                couponImage.setImageResource(R.mipmap.instagram_enable);
                default_image = R.mipmap.share_instagram_banner;
            } else if (restriction.shareTwitter) {
                couponImage.setImageResource(R.mipmap.twitter_enable);
                default_image = R.mipmap.twitter_offer_banner;
            } else if (restriction.newClientOnly) {
                couponImage.setImageResource(R.mipmap.new_client);
                default_image = R.mipmap.new_client_banner;
            } else if (restriction.oneUsePerClient) {
                couponImage.setImageResource(R.mipmap.one_time);
                default_image = R.mipmap.one_use_per_client_banner;
            } else if (restriction.isGeneral) {
                couponImage.setImageResource(R.mipmap.general_offer2);
                default_image = R.mipmap.general_offer_banner;
            }
            itemImage.setImageResource(default_image);
            if(coupon.isFavourite){
                fav_click_img.setColorFilter(context.getResources().getColor(R.color.colorAccent));
            } else {
                fav_click_img.setColorFilter(context.getResources().getColor(R.color.color_gray));
            }
            if (type.equalsIgnoreCase("Favorites")) {
                fav_click_img.setColorFilter(context.getResources().getColor(R.color.colorAccent));
            }

            if (coupon.business instanceof RestaurantMenuCoupon) {
                RestaurantMenuCoupon business = (RestaurantMenuCoupon) coupon.business;
                restaurant = business.restaurant;
                offer.setText(String.valueOf(coupon.discount));
                itemName.setText(coupon.promoName);
                if (restaurant.location.country.equalsIgnoreCase("india")) {
                    currencyType = context.getString(R.string.indian_currency);
                } else {
                    currencyType = context.getString(R.string.us_dollar);
                }
                price.setText(currencyType + String.format("%.2f",business.price));
                if (price.getText().toString().contains("null")) {
                    price.setText(price.getText().toString().replace("null", " "));
                }
                discount_amount = (double) coupon.discount / (double) 100 * (business.price);
                afterDiscount = ((business.price) - discount_amount);
                try {
                    Date sdate = new Date(coupon.dates.get(0).startDateTime);
                    Date edate = new Date(coupon.dates.get(0).endDateTime);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    expired.setText(sdf.format(sdate)+" - "+sdf.format(edate));

                    SimpleDateFormat date_format = new SimpleDateFormat("hh:mm a");
                    valid.setText(date_format.format(sdate)+" - "+date_format.format(edate));
                } catch (Exception e) {
                    Log.d("Error", e.toString());
                }

                if (coupon.images().size() > 0) {
                    Picasso.with(context)
                            .load(coupon.images().get(0)).placeholder(default_image)
                            .into(itemImage);
                } else {
                    if (!business.imageUrl.isEmpty()) {
                        Picasso.with(context)
                                .load(business.imageUrl).placeholder(default_image)
                                .into(itemImage);
                    }
                }

            } else if (coupon.business instanceof Restaurant) {
                restaurant = (Restaurant) coupon.business;
                offer.setText(String.valueOf(coupon.discount));
                itemName.setText(coupon.promoName);
                if (restaurant.location.country.equalsIgnoreCase("india")) {
                    currencyType = context.getString(R.string.indian_currency);
                } else {
                    currencyType = context.getString(R.string.us_dollar);
                }
                price_layout.setVisibility(View.GONE);
                try {
                    Date sdate = new Date(coupon.dates.get(0).startDateTime);
                    Date edate = new Date(coupon.dates.get(0).endDateTime);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    expired.setText(sdf.format(sdate)+" - "+sdf.format(edate));

                    SimpleDateFormat date_format = new SimpleDateFormat("hh:mm a");
                    valid.setText(date_format.format(sdate)+" - "+date_format.format(edate));
                } catch (Exception e) {
                    Log.d("Error", e.toString());
                }

                if (coupon.images().size() > 0) {
                    Picasso.with(context)
                            .load(coupon.images().get(0)).placeholder(default_image)
                            .into(itemImage);
                } else {
                    if (!restaurant.imageURL.isEmpty()) {
                        Picasso.with(context)
                                .load(restaurant.imageURL).placeholder(default_image)
                                .into(itemImage);
                    } else if (restaurant.bannerPhotoUrls.length > 0) {
                        Picasso.with(context)
                                .load(restaurant.bannerPhotoUrls[0]).placeholder(default_image)
                                .into(itemImage);
                    }
                }


            } else {
                JSONObject business = jsonObject.getJSONObject("business");
                restaurant = new Restaurant(business.getJSONObject("restaurant"));
                itemName.setText(business.getString("menuItemName"));
                offer.setText(String.valueOf(jsonObject.getInt("discount")));
                price.setText(currencyType + business.getInt("price"));
                price.setText(currencyType + business.getInt("price"));
                if (price.getText().toString().contains("null")) {
                    price.setText(price.getText().toString().replace("null", " "));
                }
                discount_amount = (double) jsonObject.getInt("discount") / (double) 100 * business.getInt("price");
                afterDiscount = business.getInt("price") - (int) discount_amount;
                Long dateStr = jsonObject.getJSONArray("dates").getJSONObject(0).getLong("endDateTime");
                try {
                    Date date = new Date(dateStr);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    String stringDate = sdf.format(date);
                    Log.d("Date String", stringDate);
                    expired.setText(stringDate);

                    SimpleDateFormat date_format = new SimpleDateFormat("HH:mm:ss");
                    System.out.println(date_format.format(date.getTime()));
                    valid.setText(String.valueOf(date_format.format(date.getTime())));
                } catch (Exception e) {
                    Log.d("Error", e.toString());
                }

                if (coupon.imageUrls.length() > 0) {
                    Picasso.with(context)
                            .load(coupon.imageUrls.getString(0)).placeholder(default_image)
                            .into(itemImage);
                } else {
                    if (!business.getString("imageUrl").isEmpty()) {
                        Picasso.with(context)
                                .load(business.getString("imageUrl")).placeholder(default_image)
                                .into(itemImage);
                    }
                }

            }


            price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            discount.setText(currencyType + String.format("%.2f",afterDiscount));
            if(restaurant!=null) {
                double lati = 0;
                double langi = 0;
                if(restaurant!=null){
                    lati = restaurant.location.coordinate.latitude;
                    langi = restaurant.location.coordinate.longitude;
                }
                // Get current lat and lan
                Location location1 = new Location("locationA");
                if(lat==0&&lng==0){
                    GPSService mGPSService = new GPSService(context);
                    mGPSService.getLocation();
                    lat = mGPSService.getLatitude();
                    lng = mGPSService.getLongitude();
                }
                location1.setLatitude(lat);
                location1.setLongitude(lng);
                Location location2 = new Location("locationB");
                location2.setLatitude(lati);
                location2.setLongitude(langi);
                double distances = location1.distanceTo(location2) / 1000;
            }
        } catch (Exception e) {
            Log.d("Error", e.toString());
        }
        /*if(!is_apply) {
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject jsonObject = null;
                    Boolean restriction = false, favorites = false;
                    try {
                        boolean show_res_login = false;
                        JSONObject json = ((Coupon) getItem(position)).srcJson;
                        JSONObject restrictionObject = json.getJSONObject("restriction");
                        if (restrictionObject.has("shareFacebook") && !restrictionObject.isNull("shareFacebook")) {
                            restriction = json.getJSONObject("restriction").getBoolean("shareFacebook");
                        }
                        if (json.has("isFavourite") && !json.isNull("isFavourite")) {
                            favorites = json.getBoolean("isFavourite");
                        }
                        String menuId = json.getJSONObject("business").getString("menuItemId");
                        String restId = json.getJSONObject("business").getJSONObject("restaurant").getString("id");

                        Log.d("RestaurantId", restId);
                        SharedPreferences.Editor editor = PrefManager.getInstance(context).getPref().edit();
                        editor.putString("restaurantId", restId);
                        editor.commit();
                        Fragment fragment = new Coupon_Details();
                        Bundle args = new Bundle();
                        args.putString("jsonResponse", json.toString());
                        args.putString("favorites", favorites.toString());
                        args.putString("menuId", menuId);
                        args.putString("restId", restId);
                        args.putString("type", "Restaurant");
                        args.putString("from", "coupon");
                        args.putBoolean("show_res_login", show_res_login);
                        args.putBoolean("restriction", restriction);
                        fragment.setArguments(args);
                        ((BaseFragment) Coupon_List_Adapter.this.fragment).addFragment(fragment, true);

                    } catch (Exception e) {
                        Log.d("Error", e.toString());
                    }
                }
            });
        }*/
        return rowView;
    }

    /*public String CheckAvalability(JSONObject item) {
        String Availablity = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String selectedTime = sdf.format(new Date(((NavigationDrawerActivity) context).resReq.startDate));
        if (item.has("availableSlot1")) {
            try {
                if (Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot1").getString("startTime")) >= 0 &&
                        Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot1").getString("endTime")) < 0)
                    Availablity = "available";
                else
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (item.has("availableSlot2")) {
            try {
                if (Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot2").getString("startTime")) >= 0 &&
                        Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot2").getString("endTime")) < 0)
                    Availablity = "available";
                else if (Availablity.isEmpty())
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (Availablity.isEmpty())
            Availablity = "available";
        return Availablity;
    }

    public String checkCouponAvailability(ArrayList<Coupon.CouponDate> dates) {
        String Availablity = "";
        Date selectedTime = new Date(((NavigationDrawerActivity) context).resReq.startDate);
        for (Coupon.CouponDate date : dates) {
            if (Utils.CompareTime(selectedTime, new Date(date.startDateTime)) >= 0 &&
                    Utils.CompareTime(selectedTime, new Date(date.endDateTime)) <= 0)
                Availablity = "available";
            else if (Availablity.isEmpty())
                Availablity = "not_available";
        }
        if (Availablity.isEmpty())
            Availablity = "available";
        return Availablity;
    }

    public void webServiceCallForSetFav(final String placeId, final boolean is_Remove) {
        try {
            String tag_json_obj = "json_obj_req";
            SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0);
            String url = Constants.ADD_COUPON_FAVORITE + pref.getString("userId", "") + "/promocode/" + placeId + "/favourite";
            final ProgressDialog pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Request", url);
            int requesttype = Request.Method.POST;
            if (is_Remove)
                requesttype = Request.Method.DELETE;
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(requesttype,
                    url,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("Response", response.toString());
                                pDialog.dismiss();

                                if (response.getString("status").equals("SUCCESS")) {
                                    if (fragment != null) {
                                        if (fragment instanceof Coupon_List) {
                                            ((Coupon_List) fragment).webServiceCallFavorites();
                                        }else {
                                            UpdateFav(placeId,!is_Remove);
                                        }
                                    }else {
                                        UpdateFav(placeId,!is_Remove);
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener()

            {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    error.printStackTrace();
                }
            });
            AppController.getInstance().
                    addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void UpdateFav(String id,boolean fav){
        for(int i=0;i<getCount();i++){
            JSONObject json = (JSONObject)getItem(i);
            try {
                if (json.getString("id").equals(id)) {
                    if (json.has("isFavourite"))
                        json.remove("isFavourite");
                    json.put("isFavourite", fav);
                    updateItem(json,i);
                    break;
                }
            } catch (JSONException e){

            }
        }
    }*/

    public interface ApplyListener {
        public void Apply(JSONObject jsonObject);

        public void fbApply(JSONObject jsonObject);
    }
}
