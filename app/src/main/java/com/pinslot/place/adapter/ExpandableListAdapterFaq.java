package com.pinslot.place.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.model.Coupon;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapterFaq extends BaseExpandableListAdapter {
    private FragmentActivity _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    //private HashMap<String, List<String>> _listDataChild;
    //private HashMap<String, List<String>> _listDataPunchChild;
    private HashMap<String, List<JSONObject>> childJsonValue;
    TextView listitem_location;
    String currency, mode;
    public ExpandableListAdapterFaq(FragmentActivity context, List<String> listDataHeader,
                                    HashMap<String, List<JSONObject>> childJsonValue, String currency, String mode) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        //this._listDataChild = listChildData;
        //this._listDataPunchChild = listChildPunchData;
        this.currency = currency;
        this.mode = mode;
        this.childJsonValue = childJsonValue;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.childJsonValue.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final JSONObject child = (JSONObject) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.menu_item_adapter, null);
        }

        TextView text=(TextView)convertView.findViewById(R.id.restaurant_name);
        TextView punch=(TextView)convertView.findViewById(R.id.punch);
        TextView price=(TextView)convertView.findViewById(R.id.price);
        ImageView restImage=(ImageView)convertView.findViewById(R.id.rest_image);
        ImageView promo=(ImageView)convertView.findViewById(R.id.promo);
        ImageView facebook=(ImageView)convertView.findViewById(R.id.facebook);
        TextView available = convertView.findViewById(R.id.available);
        TextView next_available = convertView.findViewById(R.id.next_available);
        TextView coupon = convertView.findViewById(R.id.coupon);
        available.setVisibility(View.VISIBLE);
        next_available.setVisibility(View.GONE);
        try {
            coupon.setVisibility(View.GONE);
            if(child.has("offers")&&!child.getString("offers").equals("null")){
                JSONArray offers = child.getJSONArray("offers");
                ArrayList<Coupon> coupons = new ArrayList<>();
                for (int i=0;i<offers.length();i++){
                    coupons.add(new Coupon(offers.getJSONObject(i)));
                }
                if(coupons.size()>0) {
                    coupon.setVisibility(View.VISIBLE);
                    coupon.setText(Integer.toString(coupons.get(0).discount) + "% off Coupon Available");
                }
            }

            if(child.getString("availability").equals("available")) {
                available.setText("Available");
                available.setBackgroundResource(R.color.Green);
            } else if(child.getString("availability").equals("not_available")) {
                available.setText("Not Available");
                available.setBackgroundResource(R.color.Red);
                next_available.setVisibility(View.VISIBLE);
                next_available.setText("Available at "+getNextAvailability(child,_context));
            }
            text.setText(child.getString("menuItemName"));

            if (child.has("menuItemPunchline"))
                punch.setText(Html.fromHtml(child.getString("menuItemPunchline")));

            price.setText(currency+child.getString("price"));

            if (child.getString("imageUrl").length() > 0) {
                Picasso.with(_context)
                        .load(child.getString("imageUrl"))
                        .into(restImage);
            }else{
                restImage.setImageResource(R.mipmap.restaurant_img);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*try {
            JSONObject jsonObject = new JSONObject(childText);
            JSONArray jsonArray=jsonObject.getJSONArray("menuItems");
            for(int i=0;i<jsonArray.length();i++){
                JSONObject menuItems=jsonArray.getJSONObject(i);
               // menuItemId.add(menuItems.getString("menuItemId"));
                text.setText(menuItems.getString("menuItemName"));
                punch.setText(menuItems.getString("menuItemPunchline"));
                price.setText("$ "+menuItems.getString("price"));
            }

        } catch (Exception e) {
        }*/
        /*
        lblListItem.setText(childText);*/

        return convertView;
    }

    public static String getNextAvailability(JSONObject item, FragmentActivity _context){
        String Availablity = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String selectedTime = sdf.format(new Date(((NavigationDrawerActivity)_context).resReq.startDate));
        if(item.has("availableSlot1")){
            try {
                if(!(CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("startTime"))>0 &&
                        CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("endTime"))<0))
                    if((CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("startTime"))<0)||((CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("startTime"))>0)&&!item.has("availableSlot2"))){
                        Availablity = item.getJSONObject("availableSlot1").getString("startTime");
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(item.has("availableSlot2")&&Availablity.isEmpty()){
            try {
                if(!(CompareTimeString(selectedTime,item.getJSONObject("availableSlot2").getString("startTime"))>0 &&
                        CompareTimeString(selectedTime,item.getJSONObject("availableSlot2").getString("endTime"))<0)) {
                    if (CompareTimeString(selectedTime, item.getJSONObject("availableSlot2").getString("startTime")) < 0)
                        Availablity = item.getJSONObject("availableSlot2").getString("startTime");
                    else
                        Availablity = item.getJSONObject("availableSlot1").getString("startTime");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Availablity;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.childJsonValue.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        TextView price = (TextView) convertView
                .findViewById(R.id.price);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        String[] items=headerTitle.split(",");
        lblListHeader.setText(items[0]);
        price.setText(items[1]);
        if (mode.equals("All")) {
            convertView.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
    public static int CompareTimeString(String time1, String time2) {
        int hr1 = 0;
        int min1 = 0;
        int hr2 = 0;
        int min2 = 0;
        String[] a = time1.split(" ");
        if (a.length > 0) {
            String[] b = a[0].split(":");
            hr1 = Integer.parseInt(b[0]);
            min1 = Integer.parseInt(b[1]);
        }
        if (hr1 == 12)
            hr1 = 0;
        if (a.length > 1 && a[1].toUpperCase().equals("PM"))
            hr1 = hr1 + 12;
        a = time2.split(" ");
        if (a.length > 0) {
            String[] b = a[0].split(":");
            hr2 = Integer.parseInt(b[0]);
            min2 = Integer.parseInt(b[1]);
        }
        if (hr2 == 12)
            hr2 = 0;
        if (a.length > 1 && a[1].toUpperCase().equals("PM"))
            hr2 = hr2 + 12;
        if (hr1 > hr2)
            return 1;
        else if (hr1 < hr2)
            return -1;
        else if (min1 > min2)
            return 1;
        else if (min1 < min2)
            return -1;
        else
            return 0;
    }

}
