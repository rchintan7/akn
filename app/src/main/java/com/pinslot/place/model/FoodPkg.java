package com.pinslot.place.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FoodPkg {
    public String id = "";
    public String spaceId = "";
    public String packageName = "";
    public String currencyCode = "";
    public double price = 0;
    public JSONObject srcJson = null;
    public ArrayList<PlaceMenu> menuItems = new ArrayList<>();
    public FoodPkg (JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("id"))
                id = srcJson.getString("id");
            if(srcJson.has("spaceId"))
                spaceId = srcJson.getString("spaceId");
            if(srcJson.has("packageName"))
                packageName = srcJson.getString("packageName");
            if(srcJson.has("currencyCode"))
                currencyCode = srcJson.getString("currencyCode");
            if(srcJson.has("price"))
                price = srcJson.getDouble("price");
            if(srcJson.has("menuItems")){
                JSONArray jsonArray = srcJson.getJSONArray("menuItems");
                menuItems.clear();
                for(int i=0;i<jsonArray.length();i++){
                    menuItems.add(new PlaceMenu(jsonArray.getJSONObject(i)));
                }
            }
        }catch (JSONException e){

        }
    }

    public static ArrayList<FoodPkg> getFoodPkgList(JSONArray array){
        ArrayList<FoodPkg> pkgs = new ArrayList<>();
        for (int i=0;i<array.length();i++){
            try {
                pkgs.add(new FoodPkg(array.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return pkgs;
    }

    public String getCurrencyName(){
        if(currencyCode.toUpperCase().equals("INR"))
            return "RS";
        else
            return "$";
    }

    public String getCurrencySymbol(){
        if(currencyCode.toUpperCase().equals("INR"))
            return "₹";
        else
            return "$";
    }

    public class PlaceMenu{
        public String itemName = "";
        public int qty = 0;
        public JSONObject srcJson = null;
        public PlaceMenu(JSONObject srcJson){
            this.srcJson = srcJson;
            try {
                if (srcJson.has("itemName"))
                    itemName = srcJson.getString("itemName");
                if(srcJson.has("qty"))
                    qty = srcJson.getInt("qty");
            } catch (JSONException e){

            }
        }
    }
}
