package com.pinslot.place.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CashBack {
    public JSONObject srcJson;
    public int offerInPercent = 0;
    public String startDateTimeStr = "";
    public String endDateTimeStr = "";
    public CashBack(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("offerInPercent"))
                offerInPercent = srcJson.getInt("offerInPercent");
            if(srcJson.has("startDateTimeStr"))
                startDateTimeStr = srcJson.getString("startDateTimeStr");
            if(srcJson.has("endDateTimeStr"))
                endDateTimeStr = srcJson.getString("endDateTimeStr");
        } catch (JSONException e){

        }
    }
}
