package com.pinslot.place.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class RestaurantRequest implements Parcelable {
    public Restaurant selectedRestaurant = null;
    public String flow = "";
    public String requestType = "";
    public long startDate = 0;
    public int peopleCount = 0;
    public Double total = 0.0, tax = 0.0;
    public int subTotalval = 0;
    public Coupon coupon = null;
    public String promo = "";

    public RestaurantRequest(){

    }

    protected RestaurantRequest(Parcel in) {
        String resStr = in.readString();
        if(!resStr.isEmpty()){
            try {
                selectedRestaurant = new Restaurant(new JSONObject(resStr));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        flow = in.readString();
        requestType = in.readString();
        startDate = in.readLong();
        peopleCount = in.readInt();
        if (in.readByte() == 0) {
            total = null;
        } else {
            total = in.readDouble();
        }
        if (in.readByte() == 0) {
            tax = null;
        } else {
            tax = in.readDouble();
        }
        subTotalval = in.readInt();
        String cpnStr = in.readString();
        if(!cpnStr.isEmpty()){
            try {
                coupon = new Coupon(new JSONObject(cpnStr));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        promo = in.readString();
    }

    public static final Creator<RestaurantRequest> CREATOR = new Creator<RestaurantRequest>() {
        @Override
        public RestaurantRequest createFromParcel(Parcel in) {
            return new RestaurantRequest(in);
        }

        @Override
        public RestaurantRequest[] newArray(int size) {
            return new RestaurantRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if(selectedRestaurant!=null)
            dest.writeString(selectedRestaurant.srcJson.toString());
        else
            dest.writeString("");
        dest.writeString(flow);
        dest.writeString(requestType);
        dest.writeLong(startDate);
        dest.writeInt(peopleCount);
        if (total == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(total);
        }
        if (tax == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(tax);
        }
        dest.writeInt(subTotalval);
        if(coupon!=null)
            dest.writeString(coupon.srcJson.toString());
        else
            dest.writeString("");
        dest.writeString(promo);
    }
}
