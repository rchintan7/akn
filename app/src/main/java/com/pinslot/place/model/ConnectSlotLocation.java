package com.pinslot.place.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConnectSlotLocation {
    public String venue = "";
    public String addressLine1 = "";
    public String addressLine2 = "";
    public String city = "";
    public String state = "";
    public String country = "";
    public String zip = "";
    public String addressString = "";
    public ConnectSlotCoordinates coordinate = null;
    public JSONObject locationObject;

    public ConnectSlotLocation(){
        coordinate = new ConnectSlotCoordinates();
    }

    public static ConnectSlotLocation createFromCurrentLocation(JSONObject jsonObject){
        ConnectSlotLocation location = new ConnectSlotLocation();
        try {
            if(jsonObject.has("name"))
                location.venue = jsonObject.getString("name");
            if (jsonObject.has("address_components")) {
                JSONArray address_components = jsonObject.getJSONArray("address_components");
                for(int i=0;i<address_components.length();i++){
                    JSONObject obj = address_components.getJSONObject(i);
                    if(obj.has("types")){
                        JSONArray types = obj.getJSONArray("types");
                        for(int j=0;j<types.length();j++){
                            if(types.getString(j).equals("sublocality")||types.getString(j).contains("sublocality_level_")) {
                                if(location.addressLine2.isEmpty())
                                    location.addressLine2 = obj.getString("long_name");
                                else
                                    location.addressLine2 = location.addressLine2 +", "+ obj.getString("long_name");
                                break;
                            } else if(types.getString(j).equals("locality")||types.getString(j).equals("locality_level_2")||types.getString(j).equals("locality_level_1")){
                                if(location.city.isEmpty())
                                    location.city = obj.getString("long_name");
                                else
                                    location.city = location.city +", "+ obj.getString("long_name");
                                break;
                            } else if(types.getString(j).equals("administrative_area")||types.getString(j).equals("administrative_area_level_1")){
                                if(location.state.isEmpty())
                                    location.state = obj.getString("long_name");
                                else
                                    location.state = location.state +", "+ obj.getString("long_name");
                                break;
                            } else if(types.getString(j).contains("administrative_area_level_")){
                                break;
                            }else if(types.getString(j).equals("country")){
                                if(location.country.isEmpty())
                                    location.country = obj.getString("long_name");
                                else
                                    location.country = location.country +", "+ obj.getString("long_name");
                                break;
                            } else if(types.getString(j).equals("postal_code")){
                                location.zip = obj.getString("long_name");
                                break;
                            } else if(types.getString(j).equals("postal_code_suffix")){
                                location.zip = location.zip+"-"+obj.getString("long_name");
                            } else if(j==(types.length()-1)){
                                if(location.addressLine1.isEmpty())
                                    location.addressLine1 = obj.getString("long_name");
                                else
                                    location.addressLine1 = location.addressLine1 +", "+ obj.getString("long_name");
                                break;
                            }
                        }
                    }
                }
            }
            if(jsonObject.has("geometry")){
                location.coordinate.latitude = jsonObject.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                location.coordinate.longitude = jsonObject.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
            }
        }catch (JSONException e){

        }
        return location;
    }

    public String getAddress(){
        String address = "";
        if(!addressLine1.isEmpty())
            address = addressLine1;
        if(!addressLine2.isEmpty()){
            if(address.isEmpty())
                address = addressLine2;
            else
                address = address +", "+addressLine2;
        }
        if(!city.isEmpty()){
            if(address.isEmpty())
                address = city;
            else
                address = address +", "+city;
        }
        if(!state.isEmpty()){
            if(address.isEmpty())
                address = state;
            else
                address = address+", "+state;
        }
        if(!country.isEmpty()){
            if(address.isEmpty())
                address = country;
            else
                address = address+", "+country;
        }
        if(!zip.isEmpty()){
            if(address.isEmpty())
                address = zip;
            else
                address = address+", "+zip;
        }
        return address;
    }

    public String getDisplayName(){
        if(!venue.isEmpty())
            return venue;
        else if(!addressString.isEmpty()){
            String[] vals = addressString.split(",");
            if(vals.length==1)
                return vals[0];
            else if(vals[0].length()<5&&vals[1].length()<15)
                return vals[0]+","+vals[1];
            else
                return vals[0];
        } else
            return "";
    }

    public ConnectSlotLocation(JSONObject jobj){
        locationObject = jobj;
        try {
            if (jobj.has("venue"))
                venue = jobj.getString("venue");
            if (jobj.has("addressLine1"))
                addressLine1 = jobj.getString("addressLine1");
            if (jobj.has("addressLine2"))
                addressLine2 = jobj.getString("addressLine2");
            if (jobj.has("city"))
                city = jobj.getString("city");
            if (jobj.has("state"))
                state = jobj.getString("state");
            if (jobj.has("country"))
                country = jobj.getString("country");
            if (jobj.has("zip"))
                zip = jobj.getString("zip");
            if (jobj.has("addressString"))
                addressString = jobj.getString("addressString");
            if(jobj.has("coordinate"))
                coordinate = new ConnectSlotCoordinates(jobj.getJSONObject("coordinate"));
        } catch (JSONException e){

        }
    }

    public JSONObject getJsonValue(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("venue", venue);
            jsonObject.put("addressLine1", addressLine1);
            jsonObject.put("addressLine2", addressLine2);
            jsonObject.put("city", city);
            jsonObject.put("state", state);
            jsonObject.put("country", country);
            jsonObject.put("zip", zip);
            jsonObject.put("addressString", addressString);
            if (coordinate != null)
                jsonObject.put("coordinate", coordinate.getJsonValue());
        } catch (Exception e){

        }
        return jsonObject;
    }

    public class ConnectSlotCoordinates{
        public double longitude = 0.0;
        public double latitude = 0.0;
        public ConnectSlotCoordinates(JSONObject jobj){
            try {
                if (jobj.has("latitude"))
                    latitude = jobj.getDouble("latitude");
                if (jobj.has("longitude"))
                    longitude = jobj.getDouble("longitude");
            } catch (Exception e){

            }
        }
        public ConnectSlotCoordinates(){

        }
        public JSONObject getJsonValue(){
            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("latitude",latitude);
                jsonObject.put("longitude",longitude);
            } catch (JSONException e){

            }
            return jsonObject;
        }
    }
}
