package com.pinslot.place.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Tax {
    public JSONObject srcJson;
    public String name = "";
    public int percent = 0;
    public Tax(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("name"))
                name = srcJson.getString("name");
            if(srcJson.has("percent"))
                percent = srcJson.getInt("percent");
        } catch (JSONException e){

        }
    }

    public static ArrayList<Tax> getTaxList(JSONArray array){
        ArrayList<Tax> taxList = new ArrayList<>();
        for(int i=0;i<array.length();i++){
            try {
                taxList.add(new Tax(array.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return taxList;
    }
}
