package com.pinslot.place.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CSPhoto {
    public String id = "";
    public String imageUrl = "";
    public JSONObject srcJson;
    public CSPhoto(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("id")) {
                id = srcJson.getString("id");
                imageUrl = srcJson.getString("imageUrl");
            }
        } catch (JSONException e){

        }
    }

    public static ArrayList<CSPhoto> getPhotos (JSONArray images){
        ArrayList<CSPhoto> photos = new ArrayList<>();
        for(int i=0;i<images.length();i++){
            try {
                photos.add(new CSPhoto(images.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return photos;
    }
}
