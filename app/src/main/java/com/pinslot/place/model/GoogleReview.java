package com.pinslot.place.model;

import org.json.JSONException;
import org.json.JSONObject;

public class GoogleReview {
    public JSONObject srcJson;
    public String author_name = "";
    public String author_url = "";
    public String language = "";
    public String profile_photo_url = "";
    public double rating = 0;
    public String relative_time_description = "";
    public String text = "";
    public long time = 0;
    public GoogleReview(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("author_name"))
                author_name = srcJson.getString("author_name");
            if(srcJson.has("author_url"))
                author_url = srcJson.getString("author_url");
            if(srcJson.has("language"))
                language = srcJson.getString("language");
            if(srcJson.has("profile_photo_url"))
                profile_photo_url = srcJson.getString("profile_photo_url");
            if(srcJson.has("rating"))
                rating = srcJson.getDouble("rating");
            if(srcJson.has("relative_time_description"))
                relative_time_description = srcJson.getString("relative_time_description");
            if(srcJson.has("text"))
                text = srcJson.getString("text");
            if(srcJson.has("time"))
                time = srcJson.getLong("time");
        } catch (JSONException e){

        }
    }
}
