package com.pinslot.place.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Restaurant {
    public String id = "";
    public String imageURL = "";
    public String cuisines = "";
    public String name = "";
    public String phone = "";
    public String website = "";
    public String chatId = "";
    public String source = "";
    public String desc = "";
    public String address = "";
    public boolean isFavourite = false;
    public double rating = 0.0f;
    public int reviewCount = 0;
    public int experienceCount = 0;
    public double distance = 0.0;
    //var pagination: [String]?
    public String[] bannerPhotoUrls;
    public ConnectSlotLocation location;
    public JSONArray taxList = new JSONArray();
    public JSONArray storeOpen;
    public JSONObject managerProfile = null;
    public JSONArray menuCardImageUrls;
    public String[] remainingTimeList;
    public String currency = "";
    public String email = "";
    public boolean dealsAvailable = false;
    public boolean hasFacebookOffer = false;
    public boolean hasCashbackOffer = false;
    public CashbackOffer cashbackOffer = null;
    public boolean manual = false;
    public boolean isManu = false;
    public boolean isMenuFound = false;
    public int menuItemsCount = 0;
    public JSONObject srcJson;
    public Restaurant(JSONObject jsonObject){
        srcJson = jsonObject;
        Log.d("SrcJSON", jsonObject.toString());
        try {
            if(jsonObject.has("id"))
                id = jsonObject.getString("id");
            if(jsonObject.has("chatId"))
                chatId = jsonObject.getString("chatId");
            if(jsonObject.has("imageUrl"))
                imageURL = jsonObject.getString("imageUrl");
            if(jsonObject.has("experienceCount"))
                experienceCount = jsonObject.getInt("experienceCount");
            if(jsonObject.has("hasCashbackOffer"))
                hasCashbackOffer = jsonObject.getBoolean("hasCashbackOffer");
            if(jsonObject.has("cashbackOffer"))
                cashbackOffer = new CashbackOffer(jsonObject.getJSONObject("cashbackOffer"));
            try{
                if(jsonObject.has("photoUrls")){
                    JSONArray urls = jsonObject.getJSONArray("photoUrls");
                    bannerPhotoUrls = new String[urls.length()];
                    for(int i = 0;i<urls.length();i++){
                        bannerPhotoUrls[i] = urls.getString(i);
                    }
                }
            } catch (JSONException e){

            }
            if(jsonObject.has("cuisines"))
                cuisines = jsonObject.getString("cuisines");
            if(jsonObject.has("name"))
                name = jsonObject.getString("name");
            //priceIndicator <- map["priceIndicator"]
            if(jsonObject.has("rating"))
                rating = jsonObject.getDouble("rating");
            if(jsonObject.has("reviewCount"))
                reviewCount = jsonObject.getInt("reviewCount");
            if(jsonObject.has("distance"))
                distance = jsonObject.getDouble("distance");
            //pagination <- map["photoUrls"]
            if(jsonObject.has("phone"))
                phone = jsonObject.getString("phone");
            if(jsonObject.has("website"))
                website = jsonObject.getString("website");
            if(jsonObject.has("desc"))
                desc = jsonObject.getString("desc");
            if(jsonObject.has("source"))
                source = jsonObject.getString("source");
            if(jsonObject.has("location")&&jsonObject.getJSONObject("location").has("addressString"))
                address = jsonObject.getJSONObject("location").getString("addressString");
            if(jsonObject.has("isFavourite"))
                isFavourite = jsonObject.getBoolean("isFavourite");
            if(jsonObject.has("location"))
                location = new ConnectSlotLocation(jsonObject.getJSONObject("location"));
            if(jsonObject.has("currency"))
                currency = jsonObject.getString("currency");
            if(jsonObject.has("dealsAvailable"))
                dealsAvailable = jsonObject.getBoolean("dealsAvailable");
            if(jsonObject.has("menuCardImageUrls"))
                menuCardImageUrls = jsonObject.getJSONArray("menuCardImageUrls");
            if(jsonObject.has("hasFacebookOffer"))
                hasFacebookOffer = jsonObject.getBoolean("hasFacebookOffer");
            if(jsonObject.has("manual"))
                manual = jsonObject.getBoolean("manual");
            if(jsonObject.has("email"))
                email = jsonObject.getString("email");
            if(jsonObject.has("settings")){
                if(jsonObject.getJSONObject("settings").has("taxList"))
                    taxList = jsonObject.getJSONObject("settings").getJSONArray("taxList");
                if(jsonObject.getJSONObject("settings").has("storeOpen"))
                    storeOpen = jsonObject.getJSONObject("settings").getJSONArray("storeOpen");
            }
            if(jsonObject.has("managerProfile"))
                managerProfile = jsonObject.getJSONObject("managerProfile");
            if (jsonObject.has("menuItemsCount")) {
                if (jsonObject.getInt("menuItemsCount") == 0) {
                    isManu = false;
                } else {
                    isManu = true;
                }
            }
            if (jsonObject.has("menuItemsCount"))
                menuItemsCount = jsonObject.getInt("menuItemsCount");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class CashbackOffer {
        public int offerInPercent = 0;
        public String startDateTimeStr = "";
        public String endDateTimeStr = "";
        public CashbackOffer(JSONObject jsonObject) throws JSONException {
            if(jsonObject.has("offerInPercent"))
                offerInPercent = jsonObject.getInt("offerInPercent");
            if(jsonObject.has("startDateTimeStr"))
                startDateTimeStr = jsonObject.getString("startDateTimeStr");
            if(jsonObject.has("endDateTimeStr"))
                endDateTimeStr = jsonObject.getString("endDateTimeStr");
        }
    }
}
