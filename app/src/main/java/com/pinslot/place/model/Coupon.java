package com.pinslot.place.model;

import com.pinslot.place.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Coupon {
    public String id = "";
    public JSONObject srcJson = null;
    public Object business = null;
    public String businessType = "";
    public String promoName = "";
    public String desc = "";
    public int availQty = 0;
    public String imageUrl = "";
    public JSONArray imageUrls = new JSONArray();
    public String couponCode = "";
    public int discount = 0;
    public String promoType = "";
    public ArrayList<CouponDate> dates = new ArrayList<>();
    public JSONObject dayCriteria = null;
    public Restriction restriction = null;
    public boolean isFavourite = false;
    public String itemName = "";
    public double itemPrice = 0;
    public String status = "";
    public String youtubeLink = "";
    public String termsAndConditions = "";
    public String userManualUrl = "";
    public ArrayList<String> busSubCatIdList = new ArrayList<>();
    public String rentType = "";
    public String chatId = "";

    public Coupon (JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("id"))
                id = srcJson.getString("id");
            if (srcJson.has("businessType"))
                businessType = srcJson.getString("businessType");
            if (srcJson.has("business")) {
                if(businessType.equals("MENU_ITEM")){
                    business = new RestaurantMenuCoupon(srcJson.getJSONObject("business"));
                } else if(businessType.equals("RESTAURANT")){
                    business = new Restaurant(srcJson.getJSONObject("business"));
                } else {
                    business = srcJson.getJSONObject("business");
                }
            }
            if (srcJson.has("promoName"))
                promoName = srcJson.getString("promoName");
            if (srcJson.has("rentType"))
                rentType = srcJson.getString("rentType");
            if (srcJson.has("desc"))
                desc = srcJson.getString("desc");
            if (srcJson.has("availQty"))
                availQty = srcJson.getInt("availQty");
            if(srcJson.has("imageUrl"))
                imageUrl = srcJson.getString("imageUrl");
            if(srcJson.has("imageUrls"))
                imageUrls = srcJson.getJSONArray("imageUrls");

            if (srcJson.has("userManualUrl"))
                userManualUrl = srcJson.getString("userManualUrl");
            if(srcJson.has("couponCode"))
                couponCode = srcJson.getString("couponCode");
            if(srcJson.has("discount"))
                discount = srcJson.getInt("discount");
            if(srcJson.has("promoType"))
                promoType = srcJson.getString("promoType");
            if(srcJson.has("dates")){
                dates.clear();
                JSONArray jsonArray = srcJson.getJSONArray("dates");
                for(int i=0;i<jsonArray.length();i++){
                    dates.add(new CouponDate(jsonArray.getJSONObject(i)));
                }
            }
            if(srcJson.has("dayCriteria"))
                dayCriteria = srcJson.getJSONObject("dayCriteria");
            if(srcJson.has("restriction"))
                restriction = new Restriction(srcJson.getJSONObject("restriction"));
            if(srcJson.has("isFavourite"))
                isFavourite = srcJson.getBoolean("isFavourite");
            if(srcJson.has("itemName"))
                itemName = srcJson.getString("itemName");
            if(srcJson.has("itemPrice"))
                itemPrice = srcJson.getDouble("itemPrice");
            if(srcJson.has("status"))
                status = srcJson.getString("status");
            if(srcJson.has("youtubeLink"))
                youtubeLink = srcJson.getString("youtubeLink");
            if(srcJson.has("termsAndConditions"))
                termsAndConditions = srcJson.getString("termsAndConditions");
            if(srcJson.has("busSubCatIdList")) {
                JSONArray array = srcJson.getJSONArray("busSubCatIdList");
                for(int i=0;i<array.length();i++){
                    busSubCatIdList.add(array.getString(i));
                }
            }
            if(srcJson.has("chatId")){
                chatId = srcJson.getString("chatId");
            }

        }catch (JSONException e){

        }
    }

    public ArrayList<String> images(){
        ArrayList<String> images = new ArrayList<>();
        if(!imageUrl.isEmpty())
            images.add(imageUrl);
        if(imageUrls!=null&&imageUrls.length()>0){
            for (int i=0;i<imageUrls.length();i++){
                try {
                    images.add(imageUrls.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return images;
    }

    public int getDefaultimageId(){
        int default_image = R.mipmap.general_offer_banner;
        if(restriction!=null) {
            if (restriction.onClientBirthday) {
                default_image = R.mipmap.birthday_offer_banner;
            } else if (restriction.onClientWedding) {
                default_image = R.mipmap.wedding_offer_banner;
            } else if (restriction.shareFacebook) {
                default_image = R.mipmap.facebook_offer_banner;
            } else if (restriction.shareInstagram) {
                default_image = R.mipmap.share_instagram_banner;
            } else if (restriction.shareTwitter) {
                default_image = R.mipmap.twitter_offer_banner;
            } else if (restriction.newClientOnly) {
                default_image = R.mipmap.new_client_banner;
            } else if (restriction.oneUsePerClient) {
                default_image = R.mipmap.one_use_per_client_banner;
            } else if (restriction.isGeneral) {
                default_image = R.mipmap.general_offer_banner;
            }
        }
        return default_image;
    }

    public class CouponDate{
        JSONObject srcJson = null;
        public long startDateTime = 0;
        public long endDateTime = 0;
        public CouponDate(JSONObject srcJson) throws JSONException {
            this.srcJson = srcJson;
            if(srcJson.has("startDateTime"))
                startDateTime = srcJson.getLong("startDateTime");
            if(srcJson.has("endDateTime"))
                endDateTime = srcJson.getLong("endDateTime");
        }
    }
}
