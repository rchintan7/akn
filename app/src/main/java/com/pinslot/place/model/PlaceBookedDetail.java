package com.pinslot.place.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PlaceBookedDetail {
    public JSONObject srcJson = null;
    public String placeName = "";
    public int guestCount = 0;
    public JSONObject place = null;
    public PlaceSpace bookedSpace = null;
    public double finalPrice = 0, discountPrice = 0, totalPrice = 0,taxPrice = 0;
    public JSONArray taxList = null;
    public ArrayList<PlaceBookedDate> bookedDates = new ArrayList<>();
    public FoodPkg foodMenuPackage = null;
    public CSUser customer = null;
    public String name = "";
    public String email = "";
    public String phone = "";
    public String id = "";
    public String status = "";
    public String chatId = "";
    public PlaceBookedDetail(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if(srcJson.has("place")){
                place = srcJson.getJSONObject("place");
            }
            if(place==null){
                if(srcJson.has("places")){
                    place = srcJson.getJSONObject("places");
                }
            }
            if(srcJson.has("space"))
                bookedSpace = new PlaceSpace(srcJson.getJSONObject("space"));
            if (place!=null&&place.has("name")) {
                placeName = place.getString("name");
                if (bookedSpace==null&&place.has("space_details")) {
                    for (int i = 0; i < place.getJSONArray("space_details").length(); i++) {
                        JSONObject space = place.getJSONArray("space_details").getJSONObject(i);
                        if (space.getString("id").equals(srcJson.getString("spaceId"))) {
                            bookedSpace = new PlaceSpace(space);
                            break;
                        }
                    }
                }
            }
            if(srcJson.has("taxList"))
                taxList = srcJson.getJSONArray("taxList");
            if(taxList==null&&bookedSpace!=null&&bookedSpace.srcJson.has("taxList")){
                taxList = bookedSpace.srcJson.getJSONArray("taxList");
            }
            if(srcJson.has("guestCount"))
                guestCount = srcJson.getInt("guestCount");
            if (srcJson.has("finalPrice"))
                finalPrice = srcJson.getDouble("finalPrice");
            if (srcJson.has("discountPrice"))
                discountPrice = srcJson.getDouble("discountPrice");
            if (srcJson.has("totalPrice"))
                totalPrice = srcJson.getDouble("totalPrice");
            if(srcJson.has("taxPrice"))
                taxPrice = srcJson.getDouble("taxPrice");
            if(srcJson.has("bookedDates")){
                JSONArray jsonArray = srcJson.getJSONArray("bookedDates");
                for(int i=0;i<jsonArray.length();i++){
                    bookedDates.add(new PlaceBookedDate(jsonArray.getJSONObject(i)));
                }
            }
            if(srcJson.has("foodMenuPackage")){
                foodMenuPackage = new FoodPkg(srcJson.getJSONObject("foodMenuPackage"));
            }
            if(srcJson.has("customer")){
                customer = new CSUser(srcJson.getJSONObject("customer"));
            }
            if(srcJson.has("name"))
                name = srcJson.getString("name");
            if(srcJson.has("email"))
                email = srcJson.getString("email");
            if(srcJson.has("phone"))
                phone = srcJson.getString("phone");
            if(srcJson.has("id"))
                id = srcJson.getString("id");
            if(srcJson.has("status"))
                status = srcJson.getString("status");
            if(srcJson.has("chatId"))
                chatId = srcJson.getString("chatId");
        } catch (JSONException e){

        }
    }

    public class PlaceBookedDate{
        public String customerId = "";
        public String businessId = "";
        public String spaceId = "";
        public String place_book_id = "";
        public boolean cancelled = false;
        public long checkIn = 0;
        public long checkOut = 0;
        public JSONObject srcJson = null;
        public PlaceBookedDate(JSONObject srcJson) throws JSONException {
            this.srcJson = srcJson;
            if(srcJson.has("customerId"))
                customerId = srcJson.getString("customerId");
            if(srcJson.has("businessId"))
                businessId = srcJson.getString("businessId");
            if(srcJson.has("spaceId"))
                spaceId = srcJson.getString("spaceId");
            if(srcJson.has("cancelled"))
                cancelled = srcJson.getBoolean("cancelled");
            if(srcJson.has("place_book_id"))
                place_book_id= srcJson.getString("place_book_id");
            if(srcJson.has("checkIn"))
                checkIn = srcJson.getLong("checkIn");
            if(srcJson.has("checkOut"))
                checkOut = srcJson.getLong("checkOut");
        }

        public String getCheckinCheckout(){
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
            String result = "";
            if(checkIn>0){
                result = sdf.format(new Date(checkIn));
            }
            //if(checkOut>0){
            //    result = result + " to "+sdf.format(new Date(checkOut));
            //}
            return result;
        }
    }
}
