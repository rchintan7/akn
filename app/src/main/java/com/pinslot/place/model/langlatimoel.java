package com.pinslot.place.model;

public class langlatimoel {

    private  String id;
    private Double longitute,latitute;

    public Double getLatitute() {
        return latitute;
    }

    public Double getLongitute() {
        return longitute;
    }

    public langlatimoel(Double longitute, Double latitute,String id) {
        this.longitute = longitute;
        this.latitute = latitute;
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
