package com.pinslot.place.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactDetail {
    public JSONObject srcJson = null;
    public String firstName = "";
    public String lastName = "";
    public String emailAddress = "";
    public String homePhoneNo = "";
    public String mobilePhoneNo = "";
    public String mobilePhone = "";
    public String mobilePhoneCountryCode = "";
    public ContactDetail(JSONObject srcJson) throws JSONException {
        this.srcJson = srcJson;
        if(srcJson.has("firstName"))
            firstName = srcJson.getString("firstName");
        if(srcJson.has("lastName"))
            lastName = srcJson.getString("lastName");
        if(srcJson.has("emailAddress"))
            emailAddress = srcJson.getString("emailAddress");
        if(srcJson.has("homePhoneNo"))
            homePhoneNo = srcJson.getString("homePhoneNo");
        if(srcJson.has("mobilePhoneNo"))
            mobilePhoneNo = srcJson.getString("mobilePhoneNo");
        if(srcJson.has("mobilePhone"))
            mobilePhone = srcJson.getString("mobilePhone");
        if(srcJson.has("mobilePhoneCountryCode"))
            mobilePhoneCountryCode = srcJson.getString("mobilePhoneCountryCode");
    }

    public ContactDetail(){

    }

    public String getName(){
        String name = "";
        if(!firstName.isEmpty())
            name = firstName;
        if(!lastName.isEmpty()){
            if(name.isEmpty())
                name = lastName;
            else
                name = name+" "+lastName;
        }
        return name;
    }
}
