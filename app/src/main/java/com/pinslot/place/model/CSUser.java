package com.pinslot.place.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CSUser {
    public String profileId = "";
    public String jobTitle = "";
    public String companyName = "";
    public String companyImgUrl = "";
    public String photoLocation = "";
    public String bannerImgUrl = "";
    public JSONObject srcJson = null;
    public ContactDetail contactDetails = null;
    public String syncType = "";
    public boolean notificationEnabled = false;
    public boolean active = false;
    public ConnectSlotLocation location = null;
    public String specialization = "";
    public String facebookUrl = "";
    public String twitterUrl = "";
    public String websiteUrl = "";

    public CSUser(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if(srcJson.has("profileId"))
                profileId = srcJson.getString("profileId");
            else if(srcJson.has("id"))
                profileId = srcJson.getString("id");
            if(srcJson.has("jobTitle"))
                jobTitle = srcJson.getString("jobTitle");
            if(srcJson.has("companyName"))
                companyName = srcJson.getString("companyName");
            if(srcJson.has("companyImgUrl"))
                companyImgUrl = srcJson.getString("companyImgUrl");
            if(srcJson.has("photoLocation"))
                photoLocation = srcJson.getString("photoLocation");
            if(srcJson.has("bannerImgUrl"))
                bannerImgUrl = srcJson.getString("bannerImgUrl");
            if(srcJson.has("contactDetails"))
                contactDetails = new ContactDetail(srcJson.getJSONObject("contactDetails"));
            else
                contactDetails = new ContactDetail();
            if(srcJson.has("syncType"))
                syncType = srcJson.getString("syncType");
            if(srcJson.has("notificationEnabled"))
                notificationEnabled = srcJson.getBoolean("notificationEnabled");
            if(srcJson.has("active"))
                active = srcJson.getBoolean("active");
            if(srcJson.has("location"))
                location = new ConnectSlotLocation(srcJson.getJSONObject("location"));
            if(srcJson.has("specialization"))
                specialization = srcJson.getString("specialization");
            if(srcJson.has("facebookUrl"))
                facebookUrl = srcJson.getString("facebookUrl");
            if(srcJson.has("twitterUrl"))
                twitterUrl = srcJson.getString("twitterUrl");
            if(srcJson.has("websiteUrl"))
                websiteUrl = srcJson.getString("websiteUrl");
        }catch (JSONException e){

        }
    }

    public CSUser(){
        contactDetails = new ContactDetail();
    }
}
