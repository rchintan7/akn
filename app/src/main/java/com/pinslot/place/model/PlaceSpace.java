package com.pinslot.place.model;


import com.pinslot.place.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlaceSpace {
    public JSONObject srcJson;
    public String id = "";
    public boolean isActive = false;
    public String name = "";
    public String spaceName = "";
    public String description = "";
    public PlaceSpacePrice price = null;
    public int capacity = 0;
    public String imageUrl = "";
    public ArrayList<String> photoUrls = new ArrayList<>();
    public String videoUrl = "";
    public String youtubeLink = "";
    public boolean spaceEnabled = false;
   // public ArrayList<FoodPkg> foodMenuPackages = new ArrayList<>();
    public JSONArray eventTypes = new JSONArray();
    public ArrayList<String> amenities = new ArrayList<>();
    public JSONArray roomType = new JSONArray();
    public ArrayList<Tax> taxList =  new ArrayList<>();
    public JSONObject operationalHours = null;
    public int size = 0;
    public String unit = "";
    public String rentType = "";
    public String bedCount = "";
    public String enableDiscount = "";
    public int distcountPercent = 0;
    public String availRoomCount = "";
    public String availRefund = "";

    public PlaceSpace(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("id"))
                id = srcJson.getString("id");
            if(srcJson.has("isActive"))
                isActive = srcJson.getBoolean("isActive");
            if(srcJson.has("name"))
                name = srcJson.getString("name");
            if(srcJson.has("spaceName"))
                spaceName = srcJson.getString("spaceName");
            if(srcJson.has("description"))
                description = srcJson.getString("description");
            if(srcJson.has("price"))
                price = new PlaceSpacePrice(srcJson.getJSONObject("price"));
            if(srcJson.has("capacity"))
                capacity = srcJson.getInt("capacity");
            if(srcJson.has("imageUrl"))
                imageUrl = srcJson.getString("imageUrl");
            if(srcJson.has("photoUrls"))
                //photoUrls = Utils.JsonArraytoArrayListString(srcJson.getJSONArray("photoUrls"));
            if(srcJson.has("videoUrl"))
                videoUrl = srcJson.getString("videoUrl");
            if(srcJson.has("youtubeLink"))
                youtubeLink = srcJson.getString("youtubeLink");
            if(srcJson.has("spaceEnabled"))
                spaceEnabled = srcJson.getBoolean("spaceEnabled");
            if(srcJson.has("foodMenuPackages"))
               // foodMenuPackages = FoodPkg.getFoodPkgList(srcJson.getJSONArray("foodMenuPackages"));
            if(srcJson.has("eventTypes"))
                eventTypes = srcJson.getJSONArray("eventTypes");
            if(srcJson.has("amenities"))
                amenities = JsonArraytoArrayListString(srcJson.getJSONArray("amenities"));
            if(srcJson.has("roomType"))
                roomType = srcJson.getJSONArray("roomType");
            if(srcJson.has("taxList"))
                taxList = Tax.getTaxList(srcJson.getJSONArray("taxList"));
            if(srcJson.has("operationalHours"))
                operationalHours = srcJson.getJSONObject("operationalHours");
            if(srcJson.has("size"))
                size = srcJson.getInt("size");
            if(srcJson.has("unit"))
                unit = srcJson.getString("unit");
            if(srcJson.has("rentType"))
                rentType = srcJson.getString("rentType");

            if(srcJson.has("bedCount"))
                bedCount = srcJson.getString("bedCount");

            if(srcJson.has("enableDiscount"))
                enableDiscount = srcJson.getString("enableDiscount");

            if(srcJson.has("distcountPercent"))
                distcountPercent = srcJson.getInt("distcountPercent");

            if(srcJson.has("availRoomCount"))
                availRoomCount = srcJson.getString("availRoomCount");

            if(srcJson.has("availRefund"))
                availRefund = srcJson.getString("availRefund");
        } catch (JSONException e){

        }
    }

    public class PlaceSpacePrice{
        public JSONObject srcJson;
        public String currencyType = "";
        public double hourValue = 0;
        public double dayValue = 0;
        public double weekValue = 0;
        public double priceStartsFrom = 0;
        public boolean foodIncluded = false;
        public PlaceSpacePrice(JSONObject srcJson){
            this.srcJson = srcJson;
            try {
                if (srcJson.has("currencyType")) {
                    currencyType = srcJson.getString("currencyType");

                    if(currencyType.toUpperCase().equals("RS"))
                        currencyType= "₹";
                    else
                        currencyType="$";
                }
                if (srcJson.has("hourValue"))
                    hourValue = srcJson.getDouble("hourValue");
                if (srcJson.has("dayValue"))
                    dayValue = srcJson.getDouble("dayValue");
                if (srcJson.has("weekValue"))
                    weekValue = srcJson.getDouble("weekValue");
                if(srcJson.has("priceStartsFrom"))
                    priceStartsFrom = srcJson.getDouble("priceStartsFrom");
                if(srcJson.has("foodIncluded"))
                    foodIncluded = srcJson.getBoolean("foodIncluded");
            } catch (JSONException e){

            }
        }
    }


    public static ArrayList<PlaceSpace> getSpaceList(JSONArray array){
        ArrayList<PlaceSpace> arrayList = new ArrayList<>();
        for (int i=0;i<array.length();i++){
            try {
                arrayList.add(new PlaceSpace(array.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static ArrayList<String> JsonArraytoArrayListString(JSONArray jsonArray) {
        ArrayList<String>arrayList = new ArrayList<>();
        for (int i=0;i<jsonArray.length();i++){
            try {
                arrayList.add(jsonArray.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }
}
