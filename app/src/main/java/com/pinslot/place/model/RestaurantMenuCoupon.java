package com.pinslot.place.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RestaurantMenuCoupon {

    public JSONObject srcJson;
    public String menuItemId = "";
    public String menuItemName = "";
    public String imageUrl = "";
    public String menuItemPunchline = "";
    public JSONArray photoLocations = new JSONArray();
    public double price = 0;
    public float rating = 0;
    public String businessId = "";
    public Restaurant restaurant = null;
    public AvailableSlot availableSlot1 = null;
    public AvailableSlot availableSlot2 = null;
    public boolean dealsAvailable = false;
    public boolean hasFacebookOffer = false;
    public boolean favourite = false;
    public RestaurantMenuCoupon(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("menuItemId"))
                menuItemId = srcJson.getString("menuItemId");
            if (srcJson.has("menuItemName"))
                menuItemName = srcJson.getString("menuItemName");
            if (srcJson.has("imageUrl"))
                imageUrl = srcJson.getString("imageUrl");
            if (srcJson.has("menuItemPunchline"))
                menuItemPunchline = srcJson.getString("menuItemPunchline");
            if (srcJson.has("photoLocations"))
                photoLocations = srcJson.getJSONArray("photoLocations");
            if (srcJson.has("price"))
                price = srcJson.getDouble("price");
            if (srcJson.has("rating"))
                rating = (float) srcJson.getDouble("rating");
            if (srcJson.has("businessId"))
                businessId = srcJson.getString("businessId");
            if (srcJson.has("restaurant"))
                restaurant = new Restaurant(srcJson.getJSONObject("restaurant"));
            if(srcJson.has("availableSlot1"))
                availableSlot1 = new AvailableSlot(srcJson.getJSONObject("availableSlot1"));
            if(srcJson.has("availableSlot2"))
                availableSlot2 = new AvailableSlot(srcJson.getJSONObject("availableSlot2"));
            if(srcJson.has("dealsAvailable"))
                dealsAvailable = srcJson.getBoolean("dealsAvailable");
            if(srcJson.has("hasFacebookOffer"))
                hasFacebookOffer = srcJson.getBoolean("hasFacebookOffer");
            if(srcJson.has("favourite"))
                favourite = srcJson.getBoolean("favourite");
        } catch (JSONException e){

        }
    }

    class AvailableSlot{
        public JSONObject srcJson = null;
        public String startTime = "";
        public String endTime = "";
        public AvailableSlot(JSONObject srcJson) throws JSONException {
            this.srcJson = srcJson;
            if(srcJson.has("startTime"))
                startTime = srcJson.getString("startTime");
            if(srcJson.has("endTime"))
                endTime = srcJson.getString("endTime");
        }
    }
}
