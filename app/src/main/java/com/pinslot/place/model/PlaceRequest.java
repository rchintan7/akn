package com.pinslot.place.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class PlaceRequest {
    public CSPlace place = null;
    public PlaceSpace space = null;
    public Date checkin = null;
    public Date checkout = null;
    public int adult = 1;
    public int child = 0;
    public HashMap<String,Coupon> coupons = new HashMap<>();
    public String type = "";
    public ArrayList<Date> dates = new ArrayList<>();
    public boolean is_space = false;
}
