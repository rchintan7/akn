package com.pinslot.place.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Restriction {
    public JSONObject srcJson = null;
    public boolean isGeneral = false;
    public boolean oneUsePerClient = false;
    public boolean onClientBirthday = false;
    public boolean newClientOnly = false;
    public boolean onClientWedding = false;
    public boolean onHappyHours = false;
    public boolean shareFacebook = false;
    public boolean shareInstagram = false;
    public boolean shareTwitter = false;
    public Restriction(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("isGeneral"))
                isGeneral = srcJson.getBoolean("isGeneral");
            if (srcJson.has("oneUsePerClient"))
                oneUsePerClient = srcJson.getBoolean("oneUsePerClient");
            if (srcJson.has("onClientBirthday"))
                onClientBirthday = srcJson.getBoolean("onClientBirthday");
            if(srcJson.has("newClientOnly"))
                newClientOnly = srcJson.getBoolean("newClientOnly");
            if(srcJson.has("onClientWedding"))
                onClientWedding = srcJson.getBoolean("onClientWedding");
            if(srcJson.has("onHappyHours"))
                onHappyHours = srcJson.getBoolean("onHappyHours");
            if(srcJson.has("shareFacebook"))
                shareFacebook = srcJson.getBoolean("shareFacebook");
            if(srcJson.has("shareInstagram"))
                shareInstagram = srcJson.getBoolean("shareInstagram");
            if(srcJson.has("shareTwitter"))
                shareTwitter = srcJson.getBoolean("shareTwitter");
        } catch (JSONException e){

        }
    }
}
