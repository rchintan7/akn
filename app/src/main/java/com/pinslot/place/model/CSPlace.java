package com.pinslot.place.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CSPlace {
    public JSONObject srcJson;
    public String id = "";
    public String name = "";
    public String imageUrl = "";
    public String youtubeLink = "";
    public ArrayList<CSPhoto> galleries = new ArrayList<>();
    public JSONArray photoUrls;
    public ConnectSlotLocation location = new ConnectSlotLocation();
    public double distance = 0;
    public double rating = 0;
    public String description = "";
    public int experienceCount = 0;
    JSONObject shiftInfo;
    public JSONArray shifts;
    public JSONArray reviews;
    public int reviewCount = 0;
    public int noOfUsrsFav = 0;
    public int capacity = 0;
    public boolean dealsAvailable = false;
    public ArrayList<PlaceSpace> space_details = new ArrayList<>();
    public boolean isFavourite = false;
    public String placeType = "";
    public String currencyCodeA = "";
    public double minPrice = 0;
    public double maxPrice = 0;
    public String minAndMaxOfPlace = "";
    public String priceRange = "";
    public boolean showCalendar = false;
    public boolean showPrice = false;
    public boolean enableFoodPackage = false;
    public boolean manual = false;
    public ArrayList<Tax> taxList = new ArrayList<>();
    public boolean hasFacebookOffer = false;
    public boolean hasCashbackOffer = false;
    public CashBack cashbackOffer = null;
    public CSUser managerProfile = null;
    public int carParkingCapacity = 0;
    public int diningCapacity = 0;
    public CSPlace(JSONObject srcJson){
        this.srcJson = srcJson;
        try {
            if (srcJson.has("id"))
                id = srcJson.getString("id");
            if(srcJson.has("name"))
                name = srcJson.getString("name");
            if(srcJson.has("imageUrl"))
                imageUrl = srcJson.getString("imageUrl");
            if(srcJson.has("youtubeLink"))
                youtubeLink = srcJson.getString("youtubeLink");
            if(srcJson.has("galleries"))
                galleries = CSPhoto.getPhotos(srcJson.getJSONArray("galleries"));
            if(srcJson.has("photoUrls"))
                photoUrls = srcJson.getJSONArray("photoUrls");
            if(srcJson.has("location"))
                location = new ConnectSlotLocation(srcJson.getJSONObject("location"));
            if(srcJson.has("distance"))
                distance = srcJson.getDouble("distance");
            if(srcJson.has("rating"))
                rating = srcJson.getDouble("rating");
            if(srcJson.has("description"))
                description = srcJson.getString("description");
            if(srcJson.has("experienceCount"))
                experienceCount = srcJson.getInt("experienceCount");
            if(srcJson.has("shiftInfo"))
                shiftInfo = srcJson.getJSONObject("shiftInfo");
            if(srcJson.has("shifts"))
                shifts = srcJson.getJSONArray("shifts");
            if(srcJson.has("reviews"))
                reviews = srcJson.getJSONArray("reviews");
            if(srcJson.has("reviewCount"))
                reviewCount = srcJson.getInt("reviewCount");
            if(srcJson.has("noOfUsrsFav"))
                noOfUsrsFav = srcJson.getInt("noOfUsrsFav");
            if(srcJson.has("space_details"))
                space_details = PlaceSpace.getSpaceList(srcJson.getJSONArray("space_details"));
            if(srcJson.has("capacity"))
                capacity = srcJson.getInt("capacity");
            if(srcJson.has("dealsAvailable"))
                dealsAvailable= srcJson.getBoolean("dealsAvailable");
            if(srcJson.has("isFavourite"))
                isFavourite = srcJson.getBoolean("isFavourite");
            if(srcJson.has("placeType"))
                placeType = srcJson.getString("placeType");
            if(srcJson.has("currencyCodeA"))
                currencyCodeA = srcJson.getString("currencyCodeA");
            if(srcJson.has("minPrice"))
                minPrice = srcJson.getDouble("minPrice");
            if(srcJson.has("maxPrice"))
                maxPrice = srcJson.getDouble("maxPrice");
            if(srcJson.has("minAndMaxOfPlace"))
                minAndMaxOfPlace = srcJson.getString("minAndMaxOfPlace");
            if(srcJson.has("priceRange"))
                priceRange = srcJson.getString("priceRange");
            if(srcJson.has("showCalendar"))
                showCalendar = srcJson.getBoolean("showCalendar");
            if(srcJson.has("showPrice"))
                showPrice = srcJson.getBoolean("showPrice");
            if(srcJson.has("enableFoodPackage"))
                enableFoodPackage = srcJson.getBoolean("enableFoodPackage");
            if(srcJson.has("manual"))
                manual = srcJson.getBoolean("manual");
            if(srcJson.has("taxList"))
                taxList = Tax.getTaxList(srcJson.getJSONArray("taxList"));
            if(srcJson.has("hasFacebookOffer"))
                hasFacebookOffer = srcJson.getBoolean("hasFacebookOffer");
            if(srcJson.has("hasCashbackOffer"))
                hasCashbackOffer = srcJson.getBoolean("hasCashbackOffer");
            if(srcJson.has("cashbackOffer"))
                cashbackOffer = new CashBack(srcJson.getJSONObject("cashbackOffer"));
            if(srcJson.has("managerProfile"))
                managerProfile = new CSUser(srcJson.getJSONObject("managerProfile"));
            if(srcJson.has("carParkingCapacity"))
                carParkingCapacity = srcJson.getInt("carParkingCapacity");
            if(srcJson.has("diningCapacity"))
                diningCapacity = srcJson.getInt("diningCapacity");
        } catch (JSONException e){

        }
    }

    public String getCurrencySymbol(){
        if(currencyCodeA.toUpperCase().equals("RS"))
            return "₹";
        else
            return "$";
    }
}
