package com.pinslot.place;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.pinslot.place.fragment.AboutUsFragment;
import com.pinslot.place.fragment.AboutUsPlaceFragment;
import com.pinslot.place.fragment.FoodServiceFragment;
import com.pinslot.place.fragment.HomeFragment;
import com.pinslot.place.fragment.IntroFragment;
import com.pinslot.place.fragment.LoginFragment;
import com.pinslot.place.fragment.MangerHistory;
import com.pinslot.place.fragment.NearByattractionFragment;
import com.pinslot.place.fragment.PlaceHomeFragment;
import com.pinslot.place.fragment.ProfileFragment;
import com.pinslot.place.fragment.ProfileHome;
import com.pinslot.place.fragment.ReferAndEarn;
import com.pinslot.place.fragment.PromotionalOffer;
import com.pinslot.place.fragment.RestaurantCheckin;
import com.pinslot.place.fragment.Restaurant_History;
import com.pinslot.place.fragment.Restaurant_Menu_Items;
import com.pinslot.place.model.PlaceRequest;
import com.pinslot.place.model.Restaurant;
import com.pinslot.place.model.RestaurantRequest;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class NavigationDrawerActivity extends FragmentActivity
        implements NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,PaymentResultWithDataListener {

    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;
    ImageView menu_icon;
    boolean is_drawer_opened = false;
    boolean skip_login_check = false;
    public RestaurantRequest resReq = null;
    public String RestaurantBookingType = "", meetupEventId = "";
    public String[]resOutTime = null;
    public ArrayList<String> menuItems = new ArrayList<>();
    public PaymentCallback paymentCallback;
    public JSONObject selectedResGoogleDetail;
    public static double lat = 0.0f, lng = 0.0f;
    LinearLayout user,logout;
    TextView user_name;
    Button login;
    public GoogleApiClient mGoogleApiClient;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    final String clientId = "326574856125-53d0apj8l9np0odimtgmsscqtipi4qlg.apps.googleusercontent.com";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final String LOG_TAG = "googlefilter";
    public PlaceRequest placeRequest = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        menu_icon = findViewById(R.id.menu_icon);
        Fragment fragment = new PlaceHomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction.add(R.id.fragment_place, fragment);
        fragmentTransaction.commit();
        if(PrefManager.getInstance(getApplicationContext()).getUserId().isEmpty()) {
            replaceFragment(new IntroFragment());
        } else {
            skip_login_check = true;
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            public void onDrawerClosed(View view) {
                menu_icon.setImageResource(R.mipmap.menu_icon);
                is_drawer_opened = false;
            }

            public void onDrawerOpened(View drawerView) {
                //menu_icon.setImageResource(R.mipmap.back);
                is_drawer_opened = true;
            }
        };
        drawer.addDrawerListener(toggle);

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        user = headerView.findViewById(R.id.user);
        logout = headerView.findViewById(R.id.logout);
        user_name = headerView.findViewById(R.id.user_name);
        login = headerView.findViewById(R.id.login);
        refresh();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getSupportFragmentManager().getBackStackEntryCount()>0){
                    addFragment(new LoginFragment());
                } else {
                    replaceFragment(new LoginFragment());
                }
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(NavigationDrawerActivity.this);
                alert.setTitle("Confirmation");
                alert.setMessage("Are you sure want to logout?");
                alert.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        PrefManager.getInstance(getApplicationContext()).clearPref();
                        refresh();
                    }
                });
                alert.setNegativeButton("No, Close",null);
                alert.show();
                drawer.closeDrawer(navigationView);
            }
        });
        navigationView.setNavigationItemSelectedListener(this);
        setDrawerState(true);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if(getSupportFragmentManager().getBackStackEntryCount()>0){
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place);
                    if(fragment instanceof IntroFragment||fragment instanceof LoginFragment){
                        setDrawerState(false);
                    } else {
                        setDrawerState(true);
                    }
                } else {
                    setDrawerState(true);
                }
            }
        });
        menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_drawer_opened){
                    drawer.openDrawer(navigationView);
                } else {
                    drawer.closeDrawer(navigationView);
                }
            }
        });

        getRestaurant(new ResCallBack() {
            @Override
            public void resSuccess(Restaurant restaurant) {
                resReq = new RestaurantRequest();
                resReq.selectedRestaurant = restaurant;
                resReq.flow = "Restaurant";
            }

            @Override
            public void resFailure(String error) {

            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestServerAuthCode(clientId, false)
                .requestIdToken(clientId)
                .requestId()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(NavigationDrawerActivity.this)
                .addApi(com.google.android.gms.location.places.Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .enableAutoManage(NavigationDrawerActivity.this, GOOGLE_API_CLIENT_ID, NavigationDrawerActivity.this)
                .addConnectionCallbacks(NavigationDrawerActivity.this)
                .build();
    }

    public void refresh(){
        if(PrefManager.getInstance(getApplicationContext()).getUserId().isEmpty()){
            login.setVisibility(View.VISIBLE);
            user.setVisibility(View.GONE);
        } else {
            user.setVisibility(View.VISIBLE);
            login.setVisibility(View.GONE);
            user_name.setText(PrefManager.getInstance(getApplicationContext()).getName());
        }
    }

    public void getRestaurant(ResCallBack callBack){
        Utils.resWebServiceCall(getApplicationContext(),callBack);
    }

    public void setDrawerState(boolean isEnabled) {
        if ( isEnabled ) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.setDrawerIndicatorEnabled(true);
            toggle.syncState();
            menu_icon.setVisibility(View.VISIBLE);
        }
        else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.syncState();
            menu_icon.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(getSupportFragmentManager().getBackStackEntryCount()>0&&!skip_login_check){
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place);
                if(fragment instanceof IntroFragment||fragment instanceof LoginFragment){
                    finish();
                } else {
                    super.onBackPressed();
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu) {
            ClearFragment();
            //showMenu();
        } else if (id == R.id.orders) {
            ClearFragment();
            addFragment(new MangerHistory());
        } else if (id == R.id.dine_in) {
            ClearFragment();
            addFragment(new FoodServiceFragment());
        } else if (id == R.id.invite) {
            Fragment fragment = new ReferAndEarn();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            fragmentTransaction.add(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();
        } else if (id == R.id.offer){
            ClearFragment();
            addFragment(new PromotionalOffer());
        } else if (id == R.id.about) {
            Fragment fragment = new AboutUsPlaceFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            fragmentTransaction.add(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();
        }


        else if (id == R.id.profile) {
            Fragment fragment = new ProfileHome();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            fragmentTransaction.add(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();
        }
        else if (id == R.id.nearby) {
            Fragment fragment = new NearByattractionFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            fragmentTransaction.add(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showMenu(){
        resReq.startDate = Calendar.getInstance().getTime().getTime();
        RestaurantBookingType = "Delivery";
        resReq.requestType = "Delivery";
        if(getSupportFragmentManager().getBackStackEntryCount()>0)
            addFragment(new Restaurant_Menu_Items());
        else
            replaceFragment(new Restaurant_Menu_Items());
    }

    public void setDelivery(){
        resReq.startDate = Calendar.getInstance().getTime().getTime();
        RestaurantBookingType = "Delivery";
        resReq.requestType = "Delivery";
    }

    public void showTakeAway(RestaurantCheckin.RequestTypeChangeCallbak callbak){
        if(getSupportFragmentManager().getBackStackEntryCount()>0)
            addFragment(RestaurantCheckin.Instance(callbak,"TakeAway"));
        else
            replaceFragment(RestaurantCheckin.Instance(callbak,"TakeAway"));
    }

    public void showDineIn(RestaurantCheckin.RequestTypeChangeCallbak callbak){
        if(getSupportFragmentManager().getBackStackEntryCount()>0)
            addFragment(RestaurantCheckin.Instance(callbak,"DineIn"));
        else
            replaceFragment(RestaurantCheckin.Instance(callbak,"DineIn"));
    }

    public void ClearFragment() {
        //resReq = null;
        selectedResGoogleDetail = null;
        resOutTime = null;
        RestaurantBookingType = "";
        FragmentManager fm = this.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public void showMain(){
        skip_login_check = true;
    }

    interface ResCallBack{
        void resSuccess(Restaurant restaurant);
        void resFailure(String error);
    }

    public interface PaymentCallback {
        void paymentSuccess(String paymentId,String Signature,String orderId);
        void paymentError(int i,String s);
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction2 = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction2.replace(R.id.fragment_place, fragment);
        fragmentTransaction2.addToBackStack(fragment.getClass().getName());
        fragmentTransaction2.commit();
    }

    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction2 = getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction2.add(R.id.fragment_place, fragment);
        fragmentTransaction2.addToBackStack(fragment.getClass().getName());
        fragmentTransaction2.commit();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(LOG_TAG, "Google Places API connected.");
        if (client != null && client.isConnected()) {
            /*locationRequest = new LocationRequest();

            locationRequest.setInterval(1000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);*/

            /*if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this);
            }*/
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(NavigationDrawerActivity.this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    public void startPayment(PaymentCallback callback, String order_id,String description,boolean session){
        /**
         * Instantiate Checkout
         */
        if(session){
            PaymentJobService.startPayment(getApplicationContext());
        }
        if(order_id.isEmpty()){
            callback.paymentError(0,"Server Error. Failed to generate order id");
        } else {
            paymentCallback = callback;
            Checkout checkout = new Checkout();
            checkout.setImage(R.mipmap.home);
            final Activity activity = this;
            try {
                JSONObject options = new JSONObject();
                options.put("name", "PinSlots");
                options.put("order_id", order_id);
                options.put("description", description);
                JSONObject json = new JSONObject();
                json.put("contact", PrefManager.getInstance(getApplicationContext()).getPhone());
                json.put("email", PrefManager.getInstance(getApplicationContext()).getEmail());
                options.put("prefill", json);

                checkout.open(activity, options);
            } catch (Exception e) {
                Log.e("RazorPay:", "Error in starting Razorpay Checkout", e);
            }
        }
    }
    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        String paymentId = s;
        String signature = "";
        String orderId = "";
        if(paymentData!=null){
            paymentId = paymentData.getPaymentId();
            signature = paymentData.getSignature();
            orderId = paymentData.getOrderId();
        }
        PaymentJobService.stopPayment(getApplicationContext());
        if(paymentCallback!=null) {
            paymentCallback.paymentSuccess(paymentId,signature,orderId);
        }
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        PaymentJobService.stopPayment(getApplicationContext());
        if(paymentCallback!=null) {
            paymentCallback.paymentError(i,s);
        }
    }


}
