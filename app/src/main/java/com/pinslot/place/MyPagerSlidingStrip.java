package com.pinslot.place;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

/**
 * Created by New System 2 on 7/20/2017.
 */

public class MyPagerSlidingStrip extends PagerSlidingTabStrip {
    ViewPager pager;
    int currentPosition =0;
    float currentPositionOffset = 0;
    float padding = 0.0f;
    int selectedTextColor = Color.BLACK;
    int normalTextColor = Color.WHITE;
    public MyPagerSlidingStrip(Context context) {
        super(context);
    }

    public MyPagerSlidingStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyPagerSlidingStrip(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void SetPager(ViewPager pager){
        setViewPager(pager);
        this.pager = pager;
    }

    public void setPadding(float padding){
        this.padding = padding;
    }

    public void setSelectedTextColor(int selectedTextColor){
        this.selectedTextColor = selectedTextColor;
    }

    public void setNormalTextColor(int normalTextColor){
        this.normalTextColor = normalTextColor;
    }

    public void setCurrentPosition(int currentPosition){
        this.currentPosition = currentPosition;
        if(getChildCount()>0) {
            LinearLayout tabsContainer = (LinearLayout) getChildAt(0);
            if(tabsContainer.getChildCount()>0) {
                for(int i = 0;i<tabsContainer.getChildCount();i++) {
                    View v = tabsContainer.getChildAt(i);
                    if (v instanceof TextView) {
                        if(i == currentPosition)
                            ((TextView) v).setTextColor(selectedTextColor);
                        else
                            ((TextView) v).setTextColor(normalTextColor);
                    }
                }
            }
        }
    }

    public void setCurrentPositionOffset(float currentPositionOffset){
        this.currentPositionOffset = currentPositionOffset;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int tabCount = pager.getAdapter().getCount();
        if (isInEditMode() || tabCount == 0) {
            return;
        }

        final int height = getHeight();

        // draw indicator line

        Paint rectPaint = new Paint();
        rectPaint.setAntiAlias(true);
        rectPaint.setStyle(Paint.Style.FILL);
        rectPaint.setColor(getIndicatorColor());

        // default: line below current tab
        if (getChildCount() > 0) {
            LinearLayout tabsContainer = (LinearLayout) getChildAt(0);
            View currentTab = tabsContainer.getChildAt(currentPosition);
            float lineLeft = currentTab.getLeft();
            float lineRight = currentTab.getRight();

            // if there is an offset, start interpolating left and right coordinates between current and next tab
            if (currentPositionOffset > 0f && currentPosition < tabCount - 1) {

                View nextTab = tabsContainer.getChildAt(currentPosition + 1);
                final float nextTabLeft = nextTab.getLeft();
                final float nextTabRight = nextTab.getRight();

                lineLeft = (currentPositionOffset * nextTabLeft + (1f - currentPositionOffset) * lineLeft);
                lineRight = (currentPositionOffset * nextTabRight + (1f - currentPositionOffset) * lineRight);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                float radius = 0.0f;
                if(height>2*padding){
                    radius = height-2*padding;
                }
                canvas.drawRoundRect(lineLeft+padding, padding, lineRight-padding, height-padding,radius,radius,rectPaint);
            }

            // draw underline

            //rectPaint.setColor(underlineColor);
            //canvas.drawRect(0, height - underlineHeight, tabsContainer.getWidth(), height, rectPaint);

            // draw divider

        /*dividerPaint.setColor(dividerColor);
        for (int i = 0; i < tabCount - 1; i++) {
            View tab = tabsContainer.getChildAt(i);
            canvas.drawLine(tab.getRight(), dividerPadding, tab.getRight(), height - dividerPadding, dividerPaint);
        }*/

        }
    }
}
