package com.pinslot.place;

import org.json.JSONObject;

import java.util.Date;

public class Constants {

    //    public static String URL = "http://connectslot.com:8090/connectslot-mobile-customer-api/";//Local karthees
    //public static String URL=url()+"connectslot-mobile-customer-api/"; //Live
    public static String URL = "http://54.148.10.188:8081/wl-cst-customer-api/";
    public static String res_id = "59d4b876e4b021dd0322c15e";

    public static String RESTAURENT_HISTORY = URL + "restaurants/booking/history/";
    public static String RESTAURANT_HISTORY_DETAIL = URL + "mobile/restaurant/order/";
    public static String RESTAURANT_FEEDBACK = URL + "restaurant/reviews";
    public static String CANCEL_RESTAURANT = URL + "customer/restaurant/";
    public static String CANCEL_RES_EXP = URL + "/restaurants/experience/book/";
    public static String CANCEL_PLACE_EXP = URL + "places/experience/book/";
    public static String PLACES_HISTORY = URL + "places/booking/history/";
    public static String PLACES_EXP_HISTORY = URL+"places/experience/customer/";
    public static String PLACES_ENQUIRY = URL + "places/customer/book/interest";
    public static String EXP_ENQUIRY = URL + "external/experience/customer/book/interest";
    public static String CANCEL_ENQUIRY = URL + "places/book/interest/";
    public static String CANCEL_EXP_ENQUIRY = URL + "external/experience/book/interest/";
    public static String MANAGER_PLACE_ENQUIRY = URL+"/places/managers/book/interest";
    public static String MANAGER_EXP_ENQUIRY = URL+"/external/experience/managers/book/interest";

    public static String RESTAURANTS_EXP_HISTORY = URL+"restaurants/experience/customer/";
    public static String SAVE_MENU_REQUEST = URL+"restaurants/menuitem/available/notification/request";
    public static String SAVE_RES_EXP_REVIEW = URL+"restaurant/experience/reviews";
    public static String SAVE_EXT_EXP_REVIEW = URL+"external/experience/reviews";
    public static String PLACE_EXP_REVIEW = URL + "places/experience/reviews";
    public static String GET_EXT_EXP = URL+"external/experience/category/getall";
    public static String EXT_EXP_SEARCH = URL+"external/experience/search";
    public static String GET_EVENT_BOOKING_BY_ID = URL+"events/ticket/book/";

    public static String SEARCH_RESTAURANT=URL+"restaurants/search";
    public static String MENU_ITEM=URL+"businesses/restaurants/menuitems?businessId=";
    public static String MENU_ITEM_FILTER = URL + "businesses/restaurants/";

    public static String SEARCH_REST_MENU=URL+"restaurantscart/searchresmenu/";
    public static String GET_ORDER_LIST=URL;
    public static String REVIEWS=URL+"restaurant/";
    public static String EXTERNAL_REVIEWS = URL + "external/experience/";

    public static String EVENT_REVIEWS = URL+"event/";
    public static String GET_RESTAURANT_BY_ID=URL+"restaurants/";
    public static String LOGIN=URL+"auth/customer/login";
    public static String FORGOT_PASSWORD=URL+"auth/customer/forgotpassword";
    public static String UPDATE_LOCATION=URL+"user/device/location/update";
    public static String UPDATE_DEVICE_INFO = URL+"user/device/info";
    public static String SIGNUP=URL+"auth/customer/registration";
    public static String RESET_PASSWORD = URL + "customer/profile/";

    public static String CHECK_EMAIL_AVAILABILITY = URL + "customer/profile/exist-checking";
    public static String SITE_REVIEW=URL+"site/reviews";
    public static String DEVICE_INFO=URL+"user/device/info";
    public static String OFFER=URL+"restaurants/coupons";
    public static String UPCOMING_EVENTS=URL+"events/search";
    public static String EVENTS_DETAILS=URL+"events/";
    public static String EVENTS_BOOKING=URL+"events/";
    public static String MEETUP_DETAIL=URL+"invite/getInviteDetails";
    public static String GET_PLACE_DETAIL = URL+"business/";
    public static String GET_RES_DETAIL = URL +"restaurants/";
    public static String EVENT_HISTORY=URL+"events/meventHistory";
    public static String CREATE_GROUP=URL+"invite/groupInvite";
    public static String GET_GROUP=URL+"invite/getListOfGroup/";
    public static String ADD_CONTACT=URL+"gc/contacts";
    public static String GET_GETOGETHER=URL+"invite/getHistoryOfGetToGether";
    public static String GET_GEHOSTGETHER=URL+"invite/getHostedGetTogether";
    public static String GET_GEATTENDEDGETHER=URL+"invite/getAttendedGetTogether";
    public static String GET_ATTENDEES_LOC=URL+"invite/";
    public static String COUPON_SEARCH=URL+"promoCode/search";
    public static String COUPON_SEARCH_WITH_CATEGORY = URL+"promoCode/filter/deals";
    public static String ADD_COUPON_FAVORITE=URL+"customer/";
    public static String GET_COUPON_FAVORITE=URL+"customer/";
    public static String COUPON_REDEEM=URL+"promoCode/redeem";
    public static String CHECK_RES_GOOGLEID= URL+"google/restaurants/";
    public static String GET_RETAIL_BUSINESS = URL+"retail/business/managers/filter";
    public static String ADD_PROMO = URL+"promoCode/addPromoCode";
    public static String CHECK_REFERRAL_CODE = URL+"customer/profile/verify/referralCode";

    public static String GET_HOST_HISTORY=URL+"events/hosted/booking/history/";
    public static String GET_ATTENDED_HISTORY=URL+"events/attended/booking/history/";

    public static String CANCEL_HOST_EVENT=URL+"events/tickets/HosterCancel/";
    public static String EXTERNAL_EXP_CANCEL = URL + "external/experience/book/";

    public static String CANCEL_ATTENDES_EVENT=URL+"events/tickets/orderCancel";
    public static String GET_EVENT_ATTENDESS=URL+"events/getAttendeesByEventId";
    public static String GET_EVENT_ATTENDESS_NEW = URL+"event/";
    public static String EVENT_CONFIRM_PARTICIPATE=URL+"events/";

    //public static String GET_PLACES_OFFERS=url()+"connectslot-customer-api/places/";
    public static String GET_PLACES=URL+"businesses/search/places";
    public static String GET_MONTH_AVAILABILITY=URL+"businesses/places/";
    public static String PROMOCODE_SEARCH = URL + "promoCode/search";
    public static String PLACE_PROMOCODE_SEARCH_BY_DATE = URL + "promoCode/filter/by-dates";
    public static String GET_DAY_AVAILABILITY=URL+"businesses/places/mavailable?businessId=";
    public static String GET_PLACE_REVIEWS=URL+"businesses/reviews/getBusinessReviewsByOverAllRating/";
    public static String PLACE_BOOKING=URL+"place/book";
    public static String GET_REWARDS=URL+"rewards/mTransactionalBusinessRewards/";
    public static String EXPENSE_TRACK=URL + "rewards/mExpenseTracker";
    public static String EXPENSE_CATEGORY_TRACK = URL + "/rewards/mgetCategoryExpenseDetails";
    public static String PROFILE = URL+"/customer/profile/";
    public static String BANNER_IMAGE_UPDATE = URL+"customer/profile/{profileId}/banner-image/update";
    public static String CANCEL_PLACES=URL+"place/book/orderCancel/";
    //public static String GET_OFFER=url()+"connectslot-customer-api/"+"businesses/";
    public static String GET_PLACE_EXPERIENCE=URL+"experienceplaces/places/";
    public static String CREATE_EVENT=URL+"events";
    public static String NOTIFICATION_ENABLE = URL + "/customer/";
    public static String GET_PLACE_PROMO = URL+"promoCode/filter/by-dates";
    public static String GET_RES_HISTORY_BY_BOOKING_ID = URL+"restaurant/order/by-booking-id/";
    public static String CLAIM_AMOUNT = URL + "customer/social/delivery/earn/withdraw";

    public static String WRITE_PLACE_REVIEW=URL+"businesses/reviews/createBusinessReview";
    public static String WRITE_EVENT_REVIEW = URL+"event/reviews";

    public static String WRITE_RESTAURANT_REVIEW=URL+"/restaurant/reviews";

    //public static String ADD_WAIT_LIST = url()+"connectslot-admin-api/waitlist/createWaitList/";
    //public static String GET_WAIT_LIST = url()+"connectslot-admin-api/waitlist/getWaitlistRestaurantById";
    //public static String UPDATE_WAIT_LIST=url()+"connectslot-admin-api/waitlist/updateStatusAndWatingTime";
    //public static String TOTAL_REWARD_POINTS = url()+"connectslot-mobile-customer-api/customer/dashboard/totalpoints/";

    public static String GET_EXPERIENCE=URL+"experience/restaurant/";
    public static String RES_EXPERIENCE=URL+"restaurants/";
    public static String PLACE_EXPERIENCE=URL+"places/";
    public static String ADD_EXP_CART=URL+"customer/";
    public static String EXP_CHECKOUT=URL+"customer/restaurant/";

    public static String GETTOGETHER_WHERE=URL+"restaurants/mSearchByCategoryName";
    public static String GETTOGETHER_SEARCH=URL+"restaurants/mSearchByRestName";
    public static String GETTOGETHER_SEND=URL+"invite/sendInvitation";
    public static String GETTOGETHER_UPDATE=URL+"invite/UpdateSendInvitation";

    public static String GET_INVITE_DETAIlS=URL+"invite/getInviteDetails";
    public static String USER_INVITE_UPDATE=URL+"invite/updateAttendeesStatus";
    public static String USER_INVITE_CANCEL=URL+"invite/cancelInvite";
    public static String UPDATE_CHECKIN = URL+"invite/";
    public static String UPDATE_CHECKIN_COUNT = URL+"events/ticket/book/";
    public static String GET_REQUESTOR_REVIEW = URL+"social/delivery/requester/profile/";
    public static String EXP_ENQURY = URL+"external/experience/book/interest";
    public static String TIP_DETAIL = URL+"customers/{accountId}/social/delivery/revenue";
    public static String MANAGER_DETAIL=URL+"managers/{managerId}/retail/coupon/revenue";

    public static String GET_COUPON_CATEGORY = URL+"customer/business/category/list";
    public static String GET_COUPON_SUBCATEGORY = URL+"customer/business/subcategory/list";
    public static String GET_EVENT_CATEGORY = URL+"admin/event/type/list";
    public static String UPDATE_PAYMENT_ADDRESS = URL+"customer/payment/option";

    /* GREETING APIS */

    public static String GET_CARD_TYPES = URL+"gc/category";
    public static String GET_CARD=URL+"greetingcard/getbycardtype/";
    public static String SEND_CARD=URL+"greetingcard/transaction/save";
    public static String GET_CARD_TOSIGN=URL+"greetingcard/invited/transaction/";
    public static String SIGN_SAVE=URL+"greetingcard/transaction/sign/save";
    public static String GET_SIGN_LIST=URL+"greetingcard/transaction/sign/";
    public static String HISTORY_CARD=URL+"greetingcard/transaction/history/";
    public static String SEND_BOOKING_REQUEST=URL+"places/create/placeBookingRequest";
    public static String DELETE_GROUP=URL+"invite/deleteGroup/";
    public static String GET_FESTIVAL=URL+"gc/events/filter";
    public static String GET_FESTIVAL_CONTACTS=URL+"gc/contacts/events/suggest";
    public static String GET_ALL_CONTACTS=URL+"gc/contacts/customer/";
    public static String GREETINGS_CREATE_GROUP=URL+"gc/contacts/groups";
    public static String GET_GROUP_BY_ID=URL+"gc/contacts/groups/";
    public static String GET_MERGED_CONTACTS=URL+"gc/contacts/groups/merged/list";
    public static String GREETINGS_LIVE_STATUS=URL+"greetingcard/transaction/";
    public static String GET_ALL_NOTIFICATION=URL+"customer/";
    public static String NOTIFICATION_READ_UPDATE=URL+"customer/notifications/read-status/update";

    public static String SOCIAL_LOGIN=URL+"gc/contacts/groups/";
    public static String GET_NEAR_CONTACTS=URL+"customers/";
    public static String SOCIAL_ORDER_CONFIRMATION=URL+"social/delivery/request";
    public static String SOCIAL_DELIVERY_ORDER_HISTORY = URL+"customers/";
    public static String PICK_DELIVERY_LOCATIONS = URL+"social/delivery/requests/between/locations";
    public static String SOCIAL_DELIVERY_ORDER_FILTER = URL+"customers";
    public static String SOCIAL_ORDER_DETAILS = URL+"social/delivery/request/";
    public static String SOCIAL_STATUS_UPDATE = URL+"social/delivery/requests/";
    public static String GREETING_LIVE_STREAM = URL+"greetingcard/transaction/";
    public static String SOCIAL_REGISTRATION= URL+"social/delivery/customer/registration";
    public static String SOCIAL_DELIVERY_STATUS = URL+"/social/delivery/customer/registration/status";
    public static String OTP_REQUEST= URL+"customer/phone/verification/otp/request";
    public static String OTP_ACCOUNT = URL + "customer/account/verification/otp/request";
    public static String OTP_VERIFICATION= URL+"customer/phone/verification/otp/validate";
    public static String OTP_VERIFICATION_ACCOUNT = URL + "customer/account/verification/otp/validate";
    public static String EMAIL_REQUEST= URL+"customer/email/verification/otp/request";
    public static String EMAIL_VERIFICATION= URL+"customer/email/verification/otp/validate";
    public static String SEND_SMS_INVITE = URL + "app/invite/sms";
    public static String RESTAURANT_STATUS_UPDATE = URL + "restaurant/order/";
    public static String PLACE_STATUS_UPDATE = URL + "places/book/";
    public static String INVITE_MEETUP_REQUEST = URL + "invite/meetup/nearest";
    public static String MEETUP_GROUP_STATUS_UPDATE = URL + "meetup/";
    public static String EVENT_GROUP_STATUS_UPDATE = URL + "event/";
    public static String MEETUP_IND_STATUS_UPDATE = URL + "meetup/chat-invite?";
    public static String PLACE_DETAILS_STATUS_UPDATE = URL + "places/chat-invite?";
    public static String RESTAURANT_DETAILS_STATUS_UPDATE = URL + "restaurants/chat-invite?";
    public static String EVENT_DETAILS_STATUS_UPDATE = URL + "event/chat-invite?";
    public static String GET_TIP_REPORT_EARN = URL + "customers/{customer_id}/social/delivery/earn/history/daily";
    public static String GET_TIP_REPORT_SPENT = URL + "customers/{customer_id}/social/delivery/spent/history/daily";
    public static String GET_ADMIN_REPORT = URL + "managers/{managerId}/retail/coupon/earn/history/daily";
    public static String GET_SPACE_LIST = URL +"businesses/getMultipleSpacesByPlaceId/";
    public static String GET_DELIVERY_COUPON = URL + "customer/";
    public static String GET_DELIVERY_BID = URL + "social/delivery/request/make/offer/list";
    public static String DELIVERY_UPDATE_TIP_STATUS = URL + "social/delivery/request/make/offer/";
    public static String POST_BUSINESS = URL + "social/delivery/business";
    public static String TAXI_BOOK = URL + "taxi/book";


    public static String DELIVERY_MY_OFFER = URL + "social/delivery/request/make/offer";
    //public static String DELIVERY_MY_OFFER = URL + "social/delivery/coupon";
    public static String GET_PLACE_BOOKING = URL + "/place/book/";
    public static String GET_EXP_BOOKING = URL + "/external/experience/book/";
    public static String UPDATE_PAYMENT_STATUS = URL + "payment/status/update";

    public static String GET_TWILLIO_TOKEN = URL+"twilio/chat/token?userName=";


    public static String RestaurantDetailsNavigation;
    //http://50.112.196.72/my-appointments-api/restaurant/588b5f0de4b013be8cedb484/reviews
    public static String search_Text = "", WHERE_STR, WHEN_STR, EVENT_NAME, MODE, EVENT_ID, START_TIME, END_TIME, PLACE_ID, WHAT_STR, DELIVERY_NAVI = "", ORDER_ID;
    public static JSONObject WHERE_ADDRESS;
    public static Date WHEN_DATE;
    public static boolean MANUAL,FACEBOOK_OFFER,PROMO_CODE;

    //REDEEM REWARD
    public static String GET_GIFT_CARDS = URL + "giftcard/list";
    public static String REDEEM_GIFT = URL + "rewards/";
    public static String SEND_OTP = URL + "/rewards/";

    public static String GET_EXP_ALBUM = URL + "external/experience/";


    //Youtube

    public static final String DEVELOPER_KEY = "AIzaSyBfr7lIIwfEfwHCpP-0tn_JLXMEVK9mNvU";
    public static final String YOUTUBE_VIDEO_CODE = "_oEA18Y8gM0";

}

