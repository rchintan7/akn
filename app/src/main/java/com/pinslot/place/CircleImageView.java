package com.pinslot.place;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CircleImageView extends ImageView {

    private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    private static final int COLORDRAWABLE_DIMENSION = 2;
    private final Paint mBitmapPaint = new Paint();
    private float mDrawableRadius;
    private final RectF mDrawableRect = new RectF();
    public CircleImageView(Context context) {
        super(context);
    }

    public CircleImageView(Context context, AttributeSet attrs){
        super(context,attrs);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onDraw(Canvas canvas) {
        // load the bitmap
        Bitmap image = drawableToBitmap(getDrawable());

        // init shader
        try {
//            if (image != null) {
//                Bitmap temp = null;
//                Bitmap roundimage = null;
//                temp = image.copy(Bitmap.Config.ARGB_8888, true);
//                roundimage = Bitmap.createBitmap(temp.getWidth(), temp.getHeight(), Bitmap.Config.ARGB_8888);
//                canvas = new Canvas(roundimage);
//                Paint paint = new Paint();
//                Rect rect = new Rect(0, 0, temp.getWidth(), temp.getHeight());
//                RectF rectF = new RectF(rect);
//                paint.setAntiAlias(true);
//                paint.setColor(Color.WHITE);
//                BitmapShader shader = new BitmapShader(temp, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
//                paint.setShader(shader);
//                canvas.drawOval(rectF, paint);
//                /*int width = canvas.getWidth();
//                if(width<=0)
//                    width = (int)getpxfordp(50);
//                int ht = canvas.getHeight();
//                if(ht<=0)
//                    ht = (int)getpxfordp(50);
//                Bitmap temp = ThumbnailUtils.extractThumbnail(image, width, ht);
//                Paint paint = new Paint();
//                Rect rect = new Rect(0, 0, width, ht);
//                RectF rectF = new RectF(rect);
//                paint.setAntiAlias(true);
//                paint.setColor(Color.WHITE);
//                BitmapShader shader = new BitmapShader(temp, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
//                paint.setShader(shader);
//                canvas.drawOval(rectF, paint);*/
//            }
            if (image == null) {
                return;
            }

            mDrawableRect.set(calculateBounds());
            mDrawableRadius = Math.min(mDrawableRect.height() / 2.0f, mDrawableRect.width() / 2.0f);

            Bitmap temp = Bitmap.createScaledBitmap(image,(int)mDrawableRect.width(),(int)mDrawableRect.height(),false);

            BitmapShader mBitmapShader = new BitmapShader(temp, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

            mBitmapPaint.setAntiAlias(true);
            mBitmapPaint.setShader(mBitmapShader);

            canvas.drawCircle(mDrawableRect.centerX(), mDrawableRect.centerY(), mDrawableRadius, mBitmapPaint);

        } catch (OutOfMemoryError e){

        }
    }

    private RectF calculateBounds() {
        int availableWidth  = getWidth() - getPaddingLeft() - getPaddingRight();
        int availableHeight = getHeight() - getPaddingTop() - getPaddingBottom();

        int sideLength = Math.min(availableWidth, availableHeight);

        float left = getPaddingLeft() + (availableWidth - sideLength) / 2f;
        float top = getPaddingTop() + (availableHeight - sideLength) / 2f;

        return new RectF(left, top, left + sideLength, top + sideLength);
    }

    public float getpxfordp(int dp) {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        try {
            Bitmap bitmap;

            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(COLORDRAWABLE_DIMENSION, COLORDRAWABLE_DIMENSION, BITMAP_CONFIG);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), BITMAP_CONFIG);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
