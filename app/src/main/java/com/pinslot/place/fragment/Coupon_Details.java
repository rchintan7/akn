package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.adapter.Coupon_Banner_Adapter;
import com.pinslot.place.helpers.GPSService;
import com.pinslot.place.model.Coupon;
import com.pinslot.place.model.Restaurant;
import com.pinslot.place.model.RestaurantMenuCoupon;
import com.pinslot.place.model.Restriction;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Coupon_Details extends BaseFragment implements View.OnClickListener {
    LinearLayout redeem, chat;
    String JSONValue, promoCodeId, type, favorites, promoImageUrl;
    private TextView expired, distance, discount, itemName, resName, offer, Description, address, price, criteria, radeem_txt,valid;
    ImageView defaultImage, favImg, addCoupon, share, manual;
    ViewPager itemPager;
    String menuName, couponCode, descriptionString = "", country, currencyType;
    JSONObject business;
    String from = "";
    JSONObject jsonObject = null;
    TextView date1,date2;
    LinearLayout price_layout, hourly_coupon, multipledates, dis_layout;
    CardView redeem_options;
    ProgressDialog fDialog;
    Coupon coupon;
    boolean place_redeem_processing = false;
    ArrayList<String> images = new ArrayList<>();
    ImageView next,prev;
    TextView warrentyLabel,warrenty;
    LinearLayout video_layout;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.coupon_details, container, false);
        from = getArguments().getString("from", "");
        JSONValue = getArguments().getString("jsonResponse");
        type = getArguments().getString("type","");
        favorites = getArguments().getString("favorites","");
        expired = (TextView) view.findViewById(R.id.expired);
        distance = (TextView) view.findViewById(R.id.distance);
        price = (TextView) view.findViewById(R.id.price);
        discount = (TextView) view.findViewById(R.id.discount);
        itemName = (TextView) view.findViewById(R.id.item_name);
        resName = (TextView) view.findViewById(R.id.res_name);
        offer = (TextView) view.findViewById(R.id.offer);
        Description = (TextView) view.findViewById(R.id.description);
        criteria = view.findViewById(R.id.criteria);
        address = (TextView) view.findViewById(R.id.address);
        addCoupon = (ImageView) view.findViewById(R.id.add_coupon);
        radeem_txt = view.findViewById(R.id.redeem_txt);
        favImg = (ImageView) view.findViewById(R.id.fav_img);
        itemPager = view.findViewById(R.id.place_images);
        defaultImage = view.findViewById(R.id.default_image);
        redeem = (LinearLayout) view.findViewById(R.id.redeem);
        chat = (LinearLayout) view.findViewById(R.id.chat);
        share = (ImageView) view.findViewById(R.id.share);
        manual = (ImageView) view.findViewById(R.id.manual);
        hourly_coupon = view.findViewById(R.id.hourly_coupon);
        multipledates = view.findViewById(R.id.multiple_dates);
        price_layout = view.findViewById(R.id.price_layout);
        date1 = view.findViewById(R.id.date1);
        date2 = view.findViewById(R.id.date2);
        valid = view.findViewById(R.id.valid);
        dis_layout = view.findViewById(R.id.dis_layout);
        redeem_options = view.findViewById(R.id.redeem_options);
        next = view.findViewById(R.id.next);
        prev = view.findViewById(R.id.prev);
        warrentyLabel = view.findViewById(R.id.warrenty_label);
        warrenty = view.findViewById(R.id.warrenty);
        video_layout = view.findViewById(R.id.video_layout);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemPager.setCurrentItem(itemPager.getCurrentItem()+1);
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemPager.setCurrentItem(itemPager.getCurrentItem()-1);
            }
        });

        itemPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0)
                    prev.setVisibility(View.GONE);
                else
                    prev.setVisibility(View.VISIBLE);
                if(position == images.size()-1){
                    next.setVisibility(View.GONE);
                } else {
                    next.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        price_layout = view.findViewById(R.id.price_layout);

        if (from.equals("coupon")) {
            radeem_txt.setText("BOOK");
        }
        redeem.setOnClickListener(this);
        chat.setOnClickListener(this);
        share.setOnClickListener(this);
        addCoupon.setOnClickListener(this);
        Description.setOnClickListener(this);
        address.setOnClickListener(this);
        video_layout.setOnClickListener(this);
        manual.setOnClickListener(this);


        favImg.setOnClickListener(this);
        if (favorites.equals("true")) {
            favImg.setColorFilter(getResources().getColor(R.color.colorAccent));
        }
        JSONParsing(JSONValue, type);
        return view;
    }

    public void onClick(View v) {
        if (v == redeem) {
            if (from.equals("coupon")) {
                if (coupon.business instanceof Restaurant) {
                    ((NavigationDrawerActivity) getActivity()).resReq.coupon = coupon;
                    ((NavigationDrawerActivity)getActivity()).showMenu();
                } else if (coupon.business instanceof RestaurantMenuCoupon) {
                    ((NavigationDrawerActivity) getActivity()).resReq.coupon = coupon;
                    ((NavigationDrawerActivity)getActivity()).showMenu();
                }
            } else {
                if (business == null || CheckAvalability(business).equals("available")) {
                    if (checkCouponAvailability(coupon.dates).equals("available")) {
                        if (!PrefManager.getInstance(getContext()).getUserId().isEmpty()) {
                            /*Fragment fragment = new Coupon_Code();
                            Bundle args = new Bundle();
                            args.putString("couponCode", couponCode);
                            args.putString("menuName", menuName);
                            args.putString("promoId", promoCodeId);
                            args.putString("restId", getArguments().getString("restId"));
                            args.putString("customerId", PrefManager.getInstance(getContext()).getUserId());
                            args.putBoolean("show_res_login", getArguments().getBoolean("show_res_login"));
                            args.putBoolean("restriction", getArguments().getBoolean("restriction"));
                            args.putString("promoImage", promoImageUrl);
                            fragment.setArguments(args);
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager
                                    .beginTransaction();
                            fragmentTransaction.replace(R.id.fragment_place, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();*/
                        } else {
                            addFragment(new Coupon_Details(),true);
                        }
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Message");
                        alert.setMessage("This Coupon not available in your " + ((NavigationDrawerActivity) getActivity()).resReq.requestType + " Time");
                        alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }
                } else {
                    String availableTime = "";
                    if (business.has("availableSlot1")) {
                        try {
                            availableTime = business.getJSONObject("availableSlot1").getString("startTime") + " - " + business.getJSONObject("availableSlot1").getString("endTime");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (business.has("availableSlot2")) {
                        try {
                            if (!availableTime.isEmpty())
                                availableTime = availableTime + " and ";
                            availableTime = availableTime + business.getJSONObject("availableSlot2").getString("startTime") + " - " + business.getJSONObject("availableSlot2").getString("endTime");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Message");
                    alert.setMessage("This item not available in your " + ((NavigationDrawerActivity) getActivity()).resReq.requestType + " Time, Its available " + availableTime);
                    alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.show();
                }
            }
        } else if(v == favImg){
            if (!PrefManager.getInstance(getContext()).getUserId().isEmpty()) {
                webServiceCallFavorites();
            } else {
                addFragment(new LoginFragment(),true);
            }
        } else if (v == share) {
            /*Alert_Dialog alert_dialog = new Alert_Dialog();
            alert_dialog.AlertView(getActivity(), "Test Message");*/
            if (fDialog != null) {
                fDialog.dismiss();
            }
            String shareBody = "Hello,\n" +
                    "\n" +
                    "I would like to share this coupon exclusively for you from PinSlots App.\n" +
                    "\n" +
                    "This offers expires soon. \n" +
                    "\n" +
                    "Click the below link to redeem this immediately. \n"; //+ getActivity().getString(R.string.share_link) + resId;
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share Coupon");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share"));
        } else if (v == Description) {
            if (descriptionString != null && !descriptionString.isEmpty()) {
                /*----Description.setText(Html.fromHtml(descriptionString));
                Alert_Dialog alert_dialog = new Alert_Dialog();
                alert_dialog.Description_AlertView(getActivity(), "" + Html.fromHtml(descriptionString));*/

            } else {
                Description.setText("No Description available");
            }
        } else if (v == address) {
            //---Alert_Dialog alert_dialog = new Alert_Dialog();
            //---alert_dialog.AlertView(getActivity(), address.getText().toString());
        } else if(v==video_layout){
            /*String videoId = EventDetailsFragment.getVideoId(coupon.youtubeLink);
            Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + videoId));
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://www.youtube.com/watch?v=" + videoId));
            try {
                startActivity(appIntent);
            } catch (Exception ex) {
                startActivity(webIntent);
            }*/
        } else if (v == manual) {
            if (coupon.userManualUrl != null && coupon.userManualUrl.length() > 0) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + coupon.userManualUrl), "text/html");
                startActivity(intent);

            } else {
                Utils.ShowAlert("No Manual","There is no user manual for this coupon",getContext());
                //Alert_Dialog alert_dialog = new Alert_Dialog();
                //alert_dialog.AlertView(getActivity(), "There is no user manual for this coupon");
            }
        }
    }

    public void JSONParsing(String json, String type) {
        images = new ArrayList<>();
        try {
            if (type.equals("Restaurant")) {
                jsonObject = new JSONObject(json);
                coupon = new Coupon(jsonObject);
            } else {
                jsonObject = new JSONObject(json).getJSONObject("promoCode");
                coupon = new Coupon(jsonObject);
            }
            Restriction restriction = coupon.restriction;
            if (restriction.onClientBirthday) {
                defaultImage.setImageResource(R.mipmap.place_birthday_offer);
                criteria.setText("Birthday offer");
            } else if (restriction.onClientWedding) {
                defaultImage.setImageResource(R.mipmap.place_wedding_offer);
                criteria.setText("Wedding offer");
            } else if (restriction.shareFacebook) {
                defaultImage.setImageResource(R.mipmap.promotion_fb_button);
                criteria.setText("Facebook offer");
            } else if (restriction.shareInstagram) {
                defaultImage.setImageResource(R.mipmap.instagram_enable);
                criteria.setText("Instagram offer");
            } else if (restriction.shareTwitter) {
                defaultImage.setImageResource(R.mipmap.twitter_enable);
                criteria.setText("Twitter offer");
            } else if (restriction.newClientOnly) {
                defaultImage.setImageResource(R.mipmap.new_client);
                criteria.setText("New User offer");
            } else if (restriction.oneUsePerClient) {
                defaultImage.setImageResource(R.mipmap.one_time);
                criteria.setText("Single Time offer");
            } else if (restriction.isGeneral) {
                defaultImage.setImageResource(R.mipmap.general_offer2);
                criteria.setText("General offer");
            }
            if(!coupon.termsAndConditions.isEmpty()){
                warrentyLabel.setVisibility(View.VISIBLE);
                warrenty.setVisibility(View.VISIBLE);
                warrenty.setText(Html.fromHtml(coupon.termsAndConditions));
            }
            if(coupon.youtubeLink.isEmpty()){
                video_layout.setAlpha(0.5f);
                video_layout.setEnabled(false);
            }
            if(coupon.business instanceof Restaurant){
                Restaurant business = (Restaurant) coupon.business;
                if (business.location.country.equalsIgnoreCase("india")) {
                    currencyType = getActivity().getString(R.string.indian_currency);
                } else {
                    currencyType = getActivity().getString(R.string.us_dollar);
                }
                menuName = business.name;
                couponCode = coupon.couponCode;
                itemName.setText(coupon.promoName);
                offer.setText(Integer.toString(coupon.discount));
                resName.setText(business.name);
                if (descriptionString != null && !descriptionString.isEmpty()) {
                    Description.setText(Html.fromHtml(descriptionString));
                } else {
                    Description.setText("No Description available");
                }
                promoCodeId = jsonObject.getString("id");
                address.setText(business.location.addressString);
                promoImageUrl = business.imageURL;
                GPSService mGPSService = new GPSService(getActivity());
                mGPSService.getLocation();
                double latitude = mGPSService.getLatitude();
                double longitude = mGPSService.getLongitude();

                Location location1 = new Location("locationA");
                location1.setLatitude(latitude);
                location1.setLongitude(longitude);
                Location location2 = new Location("locationB");
                location2.setLatitude(business.location.coordinate.latitude);
                location2.setLongitude(business.location.coordinate.longitude);

                double distances = location1.distanceTo(location2) / 1000;
                distance.setText(String.format("%.2f",distances) + " " + "KM");

                try {
                    Date sdate = new Date(coupon.dates.get(0).startDateTime);
                    Date edate = new Date(coupon.dates.get(0).endDateTime);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    expired.setText(sdf.format(sdate)+" - "+sdf.format(edate));

                    SimpleDateFormat date_format = new SimpleDateFormat("hh:mm a");
                    valid.setText(date_format.format(sdate)+" - "+date_format.format(edate));
                } catch (Exception e) {
                    Log.d("Error", e.toString());
                }
                if(!business.imageURL.isEmpty()) {
                    images.add(business.imageURL);
                }
                price_layout.setVisibility(View.GONE);
            } else if(coupon.business instanceof RestaurantMenuCoupon){
                RestaurantMenuCoupon business = (RestaurantMenuCoupon) coupon.business;
                Restaurant res = business.restaurant;
                if (res.location.country.equalsIgnoreCase("india")) {
                    currencyType = getActivity().getString(R.string.indian_currency);
                } else {
                    currencyType = getActivity().getString(R.string.us_dollar);
                }
                if (res.managerProfile != null && res.managerProfile.has("profileId")) {
                } else {
                    chat.setAlpha(0.5f);
                }

                menuName = business.menuItemName+" ("+res.name+")";
                couponCode = coupon.couponCode;
                descriptionString = coupon.desc;
                distance.setVisibility(View.GONE);
                itemName.setText(coupon.promoName);
                offer.setText(Integer.toString(coupon.discount));
                resName.setText(menuName);
                if (descriptionString != null && !descriptionString.isEmpty()) {
                    Description.setText(Html.fromHtml(descriptionString));
                } else {
                    Description.setText("No Description available");
                }
                promoCodeId = jsonObject.getString("id");
                address.setText(res.location.addressString);
                promoImageUrl = business.imageUrl;
                GPSService mGPSService = new GPSService(getActivity());
                mGPSService.getLocation();
                double latitude = mGPSService.getLatitude();
                double longitude = mGPSService.getLongitude();

                Location location1 = new Location("locationA");
                location1.setLatitude(latitude);
                location1.setLongitude(longitude);
                Location location2 = new Location("locationB");
                location2.setLatitude(res.location.coordinate.latitude);
                location2.setLongitude(res.location.coordinate.longitude);

                price.setText(currencyType + String.format("%.2f",business.price));
                price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                double discount_amount = ( coupon.discount/ (double) 100) * business.price;
                double afterDiscount = business.price - discount_amount;
                discount.setText(currencyType + String.format("%.2f",afterDiscount));

                double distances = location1.distanceTo(location2) / 1000;
                distance.setText(String.format("%.2f",distances) + " " + "KM");

                try {
                    Date sdate = new Date(coupon.dates.get(0).startDateTime);
                    Date edate = new Date(coupon.dates.get(0).endDateTime);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    expired.setText(sdf.format(sdate)+" - "+sdf.format(edate));

                    SimpleDateFormat date_format = new SimpleDateFormat("hh:mm a");
                    valid.setText(date_format.format(sdate)+" - "+date_format.format(edate));
                } catch (Exception e) {
                    Log.d("Error", e.toString());
                }
                if(!business.imageUrl.isEmpty()) {
                    images.add(business.imageUrl);
                }
            }else {
                business = jsonObject.getJSONObject("business");
                JSONObject restaurant = business.getJSONObject("restaurant");
                JSONObject locationJsonObject = restaurant.getJSONObject("location");
                country = locationJsonObject.getString("country");
                if (country.equalsIgnoreCase("india")) {
                    currencyType = getActivity().getString(R.string.indian_currency);
                } else {
                    currencyType = getActivity().getString(R.string.us_dollar);
                }
                menuName = business.getString("menuItemName");
                couponCode = jsonObject.getString("couponCode");
                descriptionString = jsonObject.getString("desc");
                price.setText(currencyType + String.format("%.2f",business.getDouble("price")));
                price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                double discount_amount = ( jsonObject.getDouble("discount") / (double) 100) * business.getDouble("price");
                double afterDiscount = business.getDouble("price") - discount_amount;
                discount.setText(currencyType + String.format("%.2f",afterDiscount));
                itemName.setText(business.getString("menuItemName"));
                offer.setText(jsonObject.getString("discount"));
                resName.setText(restaurant.getString("name"));
                if (descriptionString != null && !descriptionString.isEmpty()) {
                    Description.setText(Html.fromHtml(descriptionString));
                } else {
                    Description.setText("No Description available");
                }
                promoCodeId = jsonObject.getString("id");
                String lati = restaurant.getJSONObject("location").getJSONObject("coordinate").getString("latitude");
                String langi = restaurant.getJSONObject("location").getJSONObject("coordinate").getString("longitude");
                address.setText(restaurant.getJSONObject("location").getString("addressString"));
                // Get current lat and lan
                GPSService mGPSService = new GPSService(getActivity());
                mGPSService.getLocation();
                double latitude = mGPSService.getLatitude();
                double longitude = mGPSService.getLongitude();

                Location location1 = new Location("locationA");
                location1.setLatitude(latitude);
                location1.setLongitude(longitude);
                Location location2 = new Location("locationB");
                location2.setLatitude(Double.valueOf(lati));
                location2.setLongitude(Double.valueOf(langi));

                double distances = location1.distanceTo(location2) / 1000;
                distance.setText(String.format("%.2f",distances) + " " + "KM");

                Long dateStr = jsonObject.getJSONArray("dates").getJSONObject(0).getLong("endDateTime");
                try {
                    Date date = new Date(dateStr);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    SimpleDateFormat tf = new SimpleDateFormat("hh:mm a");
                    String stringDate = sdf.format(date);
                    Log.d("Date String", stringDate);
                    expired.setText(stringDate);
                    valid.setText(tf.format(date));

                } catch (Exception e) {
                }
            }
            if(!coupon.imageUrl.isEmpty())
                images.add(coupon.imageUrl);
            if(coupon.imageUrls!=null&&coupon.imageUrls.length()>0){
                for(int i=0;i<coupon.imageUrls.length();i++){
                    images.add(coupon.imageUrls.getString(i));
                }
            }
            itemPager.setAdapter(new Coupon_Banner_Adapter(images, getActivity()));
            if(images.size()>1){
                next.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        }
    }

    public String CheckAvalability(JSONObject item) {
        String Availablity = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String selectedTime = sdf.format(new Date(((NavigationDrawerActivity) getActivity()).resReq.startDate));
        if (item.has("availableSlot1")) {
            try {
                if (Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot1").getString("startTime")) >= 0 &&
                        Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot1").getString("endTime")) < 0)
                    Availablity = "available";
                else
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (item.has("availableSlot2")) {
            try {
                if (Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot2").getString("startTime")) >= 0 &&
                        Utils.CompareTimeString(selectedTime, item.getJSONObject("availableSlot2").getString("endTime")) < 0)
                    Availablity = "available";
                else if (Availablity.isEmpty())
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (Availablity.isEmpty())
            Availablity = "available";
        return Availablity;
    }

    public String checkCouponAvailability(ArrayList<Coupon.CouponDate> dates) {
        String Availablity = "";
        if (((NavigationDrawerActivity) getActivity()).resReq != null) {
            Date selectedTime = new Date(((NavigationDrawerActivity) getActivity()).resReq.startDate);
            for (Coupon.CouponDate date : dates) {
                if (Utils.CompareTime(selectedTime, new Date(date.startDateTime)) >= 0 &&
                        Utils.CompareTime(selectedTime, new Date(date.endDateTime)) <= 0)
                    Availablity = "available";
                else if (Availablity.isEmpty())
                    Availablity = "not_available";
            }
        }
        if (Availablity.isEmpty())
            Availablity = "available";
        return Availablity;
    }


    public void webServiceCallFavorites() {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.ADD_COUPON_FAVORITE + PrefManager.getInstance(getContext()).getUserId() + "/promocode/" + getArguments().getString("couponId") + "/favourite";
            fDialog = new ProgressDialog(getActivity());
            fDialog.setMessage("Loading...");
            fDialog.show();
            JSONObject jsonObject = new JSONObject();
            Log.d("Menu Request", url.toString());
            int method = Request.Method.POST;
            if (favorites.equals("true")) {
                method = Request.Method.DELETE;
            }
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(method,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    fDialog.dismiss();
                                    if (favorites.equals("true")) {
                                        favorites = "false";
                                        favImg.setColorFilter(getResources().getColor(R.color.color_gray));
                                        //---Alert_Dialog alert = new Alert_Dialog();
                                        //---alert.AlertView(getActivity(), "Coupon successfully removed from your favorites");
                                    } else {
                                        favorites = "true";
                                        favImg.setColorFilter(getResources().getColor(R.color.colorAccent));
                                        //---Alert_Dialog alert = new Alert_Dialog();
                                        //---alert.AlertView(getActivity(), "Coupon successfully added to your favorites");
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    fDialog.dismiss();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

}
