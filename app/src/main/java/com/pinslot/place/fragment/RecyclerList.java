package com.pinslot.place.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.adapter.AdminNotificationAdapter;
import com.pinslot.place.model.CSPhoto;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Selva on 10/25/2018.
 */

public class RecyclerList extends Fragment {

    ListView rv_list;
    LinearLayout no_result;
    String search = "";
    String items_str = "";

    public static RecyclerList instance(String type) {
        RecyclerList fragment = new RecyclerList();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv_list = view.findViewById(R.id.rv_list);
        no_result = view.findViewById(R.id.no_result);
        items_str = getArguments().getString("items_str", "");
        webServiceCallCoupons(null, "all_accept_reject");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void webServiceCallCoupons(final JSONObject pagination, final String type) {
        try {
            String tag_json_obj = "json_obj_req";
            String url = "";
            url = Constants.URL + "places/managers/orders/filter";
            final JSONObject jsonObject = new JSONObject();
            JSONArray array = new JSONArray();
            if (getArguments().getString("type").equalsIgnoreCase("REJECTED")) {
                array.put(getArguments().getString("type"));
                array.put("CANCELLED");
            } else {
                array.put(getArguments().getString("type"));
            }
            jsonObject.put("statusList", array);
            jsonObject.put("managerId", PrefManager.getInstance(getActivity()).getUserId());
            JSONObject page = new JSONObject();
            page.put("pageNo", 1);
            page.put("limit", 20);
            jsonObject.put("pagination", page);
            final ProgressDialog pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("History Request", url.toString() + " " + jsonObject.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("History Response", response.toString());
                            pDialog.dismiss();
                            try {
                                if (pagination == null)
                                    if (response.getString("status").equals("SUCCESS")) {
                                        JSONArray orders=response.getJSONObject("data").getJSONArray("orders");
                                        rv_list.setAdapter(new AdminNotificationAdapter(orders,getActivity()));
                                    } else {
                                        Toast.makeText(getContext(), "Failed to get list", Toast.LENGTH_SHORT).show();
                                    }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }


}

