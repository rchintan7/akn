package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.model.PlaceRequest;

import static com.facebook.FacebookSdk.getApplicationContext;

public class PlaceHomeFragment extends BaseFragment {
    LinearLayout bookRooms;
    LinearLayout history;
    LinearLayout services;
    LinearLayout space;
    TextView txt_welcome;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_home,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bookRooms=view.findViewById(R.id.book_rooms);
        txt_welcome=view.findViewById(R.id.txt_welcome);
        bookRooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationDrawerActivity)getActivity()).placeRequest = new PlaceRequest();
                replaceFragment(new Book_Room(),true);
            }
        });
        history = view.findViewById(R.id.history);
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new Restaurant_History(),true);
            }
        });
        services = view.findViewById(R.id.services);
        services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new ServiceHomeFragment(),true);
            }
        });
        space = view.findViewById(R.id.space);
        space.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerActivity)getActivity()).placeRequest = new PlaceRequest();
                ((NavigationDrawerActivity)getActivity()).placeRequest.is_space = true;
                replaceFragment(new Book_Room(),true);
            }
        });

        if(PrefManager.getInstance(getActivity()).getUserId().isEmpty()){
            txt_welcome.setText("Welcome GUEST USER");
        } else {
            txt_welcome.setText("Welcome "+PrefManager.getInstance(getActivity()).getName());

        }
    }
}
