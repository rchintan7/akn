package com.pinslot.place.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.adapter.PlaceArrayAdapter;
import com.pinslot.place.model.PlaceRequest;
import com.razorpay.Checkout;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Selva on 10/23/2018.
 */

public class Book_Room extends BaseFragment {

    Button avail;
    //PlaceWhereAdapter adapter;
    // Google API Client
    //private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final String LOG_TAG = "Social_Delivery_Where";
    public static String lattitude, longitudes;
    AutoCompleteTextView locationEdit;
    DatePickerDialog startTime,endtime;
    String formattedDate, checkinDate,checkoutDate;
    EditText checkIn,checkOut;
    String type="";
    AutoCompleteTextView acTV1,Children;
    String selection,selection2;
    Calendar startdate;
    TextView fromdate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.book_rooms,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        avail=view.findViewById(R.id.availability);
        checkIn=view.findViewById(R.id.checkin);
        checkOut=view.findViewById(R.id.checkout);
        fromdate=view.findViewById(R.id.fromdate);


        locationEdit=view.findViewById(R.id.location_edit);
        locationEdit.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        locationEdit.setAdapter(mPlaceArrayAdapter);

        mPlaceArrayAdapter = new PlaceArrayAdapter((NavigationDrawerActivity) getActivity(), android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        //mPlaceArrayAdapter.notifyDataSetChanged();
        locationEdit.setAdapter(mPlaceArrayAdapter);
        if (((NavigationDrawerActivity) getActivity()).mGoogleApiClient.isConnected())
            mPlaceArrayAdapter.setGoogleApiClient(((NavigationDrawerActivity) getActivity()).mGoogleApiClient);

        checkIn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type="Checkin";
                Calendar calendar = Calendar.getInstance();
                startTime.getDatePicker().setMinDate(calendar.getTimeInMillis());
                startTime.show();
                return false;
            }
        });

        checkOut.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type="Checkout";
               // Calendar calendar = Calendar.getInstance();
               // startdate.add(Calendar.DATE, 0);

                String getfromdate = fromdate.getText().toString().trim();
                String getfrom[] = getfromdate.split("/");
                int year,month,day;
                year= Integer.parseInt(getfrom[2]);
                month = Integer.parseInt(getfrom[1]);
                day = Integer.parseInt(getfrom[0]);

             final    Calendar c  = Calendar.getInstance();;
                c.set(year,month,day+1);

               endtime.getDatePicker().setMinDate(c.getTimeInMillis());

                endtime.show();
                return false;
            }
        });

        final SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final SimpleDateFormat dateS = new SimpleDateFormat("dd MMM yyyy");
        final SimpleDateFormat times = new SimpleDateFormat("hh:mm aa");
        Calendar newCalendar = Calendar.getInstance();

         startdate = Calendar.getInstance();
        formattedDate = dateS.format(newCalendar.getTime());
        checkinDate = df.format(newCalendar.getTime());
        startTime = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                try {


                          startdate.set(year, monthOfYear, dayOfMonth);

                           formattedDate = dateS.format(startdate.getTime());

                            checkinDate = df.format(startdate.getTime());
                            checkIn.setText(formattedDate);

                    fromdate.setText(dayOfMonth + "/" + monthOfYear  + "/" + year);

                } catch (Exception e) {
                    Log.d("Exception", e.toString());
                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        endtime = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                try {



                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);


                        formattedDate = dateS.format(newDate.getTime());

                            checkoutDate = df.format(newDate.getTime());
                            checkOut.setText(formattedDate);


                } catch (Exception e) {
                    Log.d("Exception", e.toString());
                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        avail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //addFragment(new Place_Listing(),true);
                if(!checkIn.getText().toString().isEmpty()&&!checkOut.getText().toString().isEmpty()) {
                    Fragment fragment = new Place_Listing();
                    Bundle args = new Bundle();
                    args.putString("fromdate", checkIn.getText().toString());
                    args.putString("todate", checkOut.getText().toString()); // commented for server side issue
                    SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM yyyy hh:mm a");
                    try {
                        ((NavigationDrawerActivity) getActivity()).placeRequest.checkin = sdf2.parse(checkIn.getText().toString() + " 12:00 AM");
                        ((NavigationDrawerActivity) getActivity()).placeRequest.checkout = sdf2.parse(checkOut.getText().toString() + " 11:59 PM");
                    } catch (ParseException e){

                    }
                  Utils.locationstring= locationEdit.getText().toString();
                  Utils.checkindate= checkIn.getText().toString();
                  Utils.checkoutdate= checkOut.getText().toString();
                    args.putString("fromtime", "12:00 AM");
                    args.putString("totime", "11:59 PM"); // commented for server side issue
                    args.putString("lat", lattitude);
                    args.putString("lon", longitudes);
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.add(R.id.fragment_place, fragment, fragment.getClass().getName());
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                } else {
                    Toast.makeText(getContext(),"Choose CheckIn and Checkout Time",Toast.LENGTH_SHORT).show();
                }
            }
        });
         acTV1 = view.findViewById(R.id.adult);
        Children = view.findViewById(R.id.Children);

         ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.adultvalue));

        acTV1.setAdapter(arrayAdapter);
        acTV1.setCursorVisible(false);
        acTV1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                acTV1.showDropDown();
                selection = (String) parent.getItemAtPosition(position);
                acTV1.setText(selection);
            }
        });

        acTV1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                acTV1.showDropDown();
            }
        });

        Children.setAdapter(arrayAdapter);
        Children.setCursorVisible(false);
        Children.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Children.showDropDown();
                selection2= (String) parent.getItemAtPosition(position);

                Children.setText(selection2);
            }
        });

        Children.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                Children.showDropDown();
            }
        });


    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(((NavigationDrawerActivity) getActivity()).mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);

        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place myPlace = places.get(0);
            LatLng queriedLocation = myPlace.getLatLng();
            Log.v("Latitude is", "" + queriedLocation.latitude);
            Log.v("Longitude is", "" + queriedLocation.longitude);
            //Constants.GET_HUB=Constants.URL+"hub/search?lat="+String.valueOf(queriedLocation.latitude)+"&lng="+String.valueOf(queriedLocation.longitude);

            CharSequence attributions = places.getAttributions();
            lattitude = String.valueOf(queriedLocation.latitude);
            longitudes = String.valueOf(queriedLocation.longitude);

            if (attributions != null) {
                //mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }
        }
    };



}

