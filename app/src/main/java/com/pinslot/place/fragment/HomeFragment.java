package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;

public class HomeFragment extends BaseFragment implements View.OnClickListener {

    CardView menu,take_away,dine_in,orders;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menu = view.findViewById(R.id.menu);
        take_away = view.findViewById(R.id.take_away);
        dine_in = view.findViewById(R.id.dine_in);
        orders = view.findViewById(R.id.orders);
        menu.setOnClickListener(this);
        take_away.setOnClickListener(this);
        dine_in.setOnClickListener(this);
        orders.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu : ((NavigationDrawerActivity)getActivity()).showMenu();
                break;
            case R.id.take_away: ((NavigationDrawerActivity)getActivity()).showTakeAway(null);
                break;
            case R.id.dine_in : ((NavigationDrawerActivity)getActivity()).showDineIn(null);
                break;
            case R.id.orders: replaceFragment(new Restaurant_History(),true);
                break;
        }
    }
}
