package com.pinslot.place.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.adapter.Coupon_List_Adapter;
import com.pinslot.place.model.Coupon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ApplyCouponFragment extends BaseFragment implements Coupon_List_Adapter.ApplyListener {
    ListView list;
    TextView login_text;
    ScrollView no_promos;
    String type = "";
    JSONArray couponsArray;
    SharedPreferences pref;
    public JSONObject applied_coupon;
    public JSONObject fb_coupon;
    public Coupon_List_Adapter.ApplyListener listener;
    public ArrayList<JSONObject> applied_fbs = new ArrayList<>();
    Coupon_List_Adapter adapter;
    LinearLayout title_layout;
    boolean hide_title = false;
    boolean fbCheck = true;
    CallbackManager fbManager;
    JSONObject fbCoupon;
    ProgressBar loading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_show_coupons, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        type = getArguments().getString("type");
        fbManager = CallbackManager.Factory.create();
        list = view.findViewById(R.id.list);
        loading = view.findViewById(R.id.loading);
        no_promos = view.findViewById(R.id.no_promos_layout);
        login_text = view.findViewById(R.id.login_text);
        title_layout = view.findViewById(R.id.promo);
        hide_title = getArguments().getBoolean("hide_title",false);
        if(hide_title)
            title_layout.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        getPromo();
    }

    public void getPromo(){
        try {
            String tag_json_obj = "json_obj_req";
            String url = "";
            if(type.equals("place"))
                url = Constants.GET_PLACE_PROMO;
            else
                url = Constants.PROMOCODE_SEARCH;
            JSONObject jsonObject = new JSONObject(getArguments().getString("json"));
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Place promo Response", response.toString());
                            loading.setVisibility(View.GONE);
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    couponsArray = response.getJSONObject("data").getJSONArray("result");


                                    if (!pref.getString("userId", "").equals("")) {
                                        if (couponsArray != null && couponsArray.length() > 0) {
                                            list.setVisibility(View.VISIBLE);
                                            no_promos.setVisibility(View.GONE);
                                            login_text.setVisibility(View.GONE);
                                            String coupon = "", applied_fb_coupon = "";
                                            if (applied_coupon != null) {
                                                try {
                                                    coupon = applied_coupon.getString("id");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            if (fb_coupon != null) {
                                                try {
                                                    applied_fb_coupon = fb_coupon.getString("id");
                                                } catch (JSONException e) {

                                                }
                                            }
                                            ArrayList<Coupon> coupons = new ArrayList<>();
                                            for (int i=0;i<couponsArray.length();i++){
                                                coupons.add(new Coupon(couponsArray.getJSONObject(i)));
                                            }
                                            adapter = new Coupon_List_Adapter(ApplyCouponFragment.this,coupons,new ArrayList<Coupon>());
                                            list.setAdapter(adapter);
                                            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    CouponApply(position);
                                                }
                                            });
                                        } else {
                                            list.setVisibility(View.GONE);
                                            login_text.setVisibility(View.GONE);
                                            no_promos.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        list.setVisibility(View.GONE);
                                        login_text.setVisibility(View.VISIBLE);
                                        no_promos.setVisibility(View.GONE);
                                        login_text.setMovementMethod(LinkMovementMethod.getInstance());
                                        String clickableText = "You may get special offer or personalized coupon if you login. {click here} to login";
                                        login_text.setText(addClickablePart(clickableText), TextView.BufferType.SPANNABLE);
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.setVisibility(View.GONE);
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void CouponApply(int position){
        try {
            final JSONObject jsonObject = couponsArray.getJSONObject(position);
            boolean fbOffer = false;
            try {
                if (jsonObject.has("restriction") && jsonObject.getJSONObject("restriction").has("shareFacebook") && jsonObject.getJSONObject("restriction").getBoolean("shareFacebook")) {
                    fbOffer = true;
                }
            } catch (Exception e) {

            }
            if (fbOffer) {
                fbCoupon = jsonObject;
                boolean found = false;
                for (JSONObject obj : applied_fbs) {
                    try {
                        if (obj.getString("id").equals(jsonObject.getString("id"))) {
                            found = true;
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (!found) {
                    String res_name = "";
                    if (((NavigationDrawerActivity) getActivity()).resReq != null && ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant != null)
                        res_name = ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.name;
                    FacebookSdk.sdkInitialize(getContext());
                    ShareDialog shareDialog = new ShareDialog(this);
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle("PinSlots")
                            //.setQuote("I'm going to enjoy delicious food at " + res_name + " today.  I highly recommend this restaurant to everyone")
                            .setContentDescription("I'm going to enjoy delicious food at " + res_name + " today.  I highly recommend this restaurant to everyone")
                            .setContentUrl(Uri.parse("http://www.pinslots.com/#/home"))
                            .build();
                    shareDialog.show(linkContent);
                    shareDialog.registerCallback(fbManager, shareCallback);
                } else {
                    fbApply(fbCoupon);
                }
            } else{
                Log.d("Promotion Listener", "true");
                Apply(jsonObject);
            }
        } catch (JSONException e){

        }
    }

    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            // TODO Auto-generated method stub
            fbCheck = true;
            Toast.makeText(getActivity(), "You shared this post", Toast.LENGTH_SHORT).show();
            //Log.d("Facebook Call back check", "True");
            // new LongOperation().execute("");
        }

        @Override
        public void onError(FacebookException error) {
            // TODO Auto-generated method stub
            Toast.makeText(getActivity(), "You shared this Error", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel() {
            // TODO Auto-generated method stub
            fbCheck = false;
            Toast.makeText(getActivity(), "You shared this Cancel", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(adapter!=null) {
            fbManager.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK && fbCheck) {
                Toast.makeText(getActivity(), " You shared this post", Toast.LENGTH_SHORT).show();
                try {
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
                    if (pref.getString("loginSession", null) != null) {
                        if (pref.getString("loginSession", null).equals("Valid")) {
                            if (fbCoupon != null)
                                fbApply(fbCoupon);
                        } else {
                            addFragment(new LoginFragment(),true);
                        }
                    } else {
                        addFragment(new LoginFragment(),true);
                    }

                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void Apply(JSONObject jsonObject) {
        if(listener!=null){
            listener.Apply(jsonObject);
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void fbApply(JSONObject jsonObject) {
        if(listener!=null){
            listener.fbApply(jsonObject);
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private SpannableStringBuilder addClickablePart(String str) {
        String original = str.replace('{', ' ').replace('}', ' ');
        SpannableStringBuilder ssb = new SpannableStringBuilder(original);
        int idx1 = str.indexOf("{");
        int idx2 = 0;
        while (idx1 != -1) {
            idx2 = str.indexOf("}", idx1) + 1;

            final String clickString = str.substring(idx1, idx2);
            ssb.setSpan(new MyClickableSpan() {
                @Override
                public void onClick(View widget) {
                    if (clickString.equals("{click here}")) {
                        addFragment(new LoginFragment(),true);
                    }
                }
            }, idx1, idx2, 0);
            idx1 = str.indexOf("{", idx2);
        }
        return ssb;
    }

    abstract class MyClickableSpan extends ClickableSpan {
        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(getActivity().getResources().getColor(R.color.colorAccent));
            ds.setUnderlineText(false);
            ds.linkColor = getActivity().getResources().getColor(R.color.colorAccent);
        }
    }
}