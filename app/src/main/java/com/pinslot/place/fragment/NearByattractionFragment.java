package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pinslot.place.R;

public class NearByattractionFragment extends Fragment {


    ImageView nearscreenone_img,nearscreentwo_img;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nearbyattraction,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nearscreenone_img=view.findViewById(R.id.nearscreenone_img);
        nearscreentwo_img=view.findViewById(R.id.nearscreentwo_img);
        nearscreenone_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nearscreentwo_img.setVisibility(View.VISIBLE);
            }
        });


        nearscreentwo_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(new PlaceHomeFragment());
            }
        });
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction2 = getActivity().getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction2.replace(R.id.fragment_place, fragment);
        fragmentTransaction2.addToBackStack(fragment.getClass().getName());
        fragmentTransaction2.commit();
    }
    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction2 = getActivity().getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction2.add(R.id.fragment_place, fragment);
        fragmentTransaction2.addToBackStack(fragment.getClass().getName());
        fragmentTransaction2.commit();
    }
}
