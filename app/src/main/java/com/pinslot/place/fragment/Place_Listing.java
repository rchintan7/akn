package com.pinslot.place.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.MapviewActivity;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.adapter.Place_Listing_Adapter;
import com.pinslot.place.locationupdate.LocationHelper;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.model.ConnectSlotLocation;
import com.pinslot.place.model.langlatimoel;
import com.pinslot.place.widget.PicassoUtil;
import com.pinslot.place.widget.PlaceLoadingBar;
import com.squareup.okhttp.internal.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Selva on 10/23/2018.
 */

public class Place_Listing extends BaseFragment   implements LocationHelper.OnLocationReceived {


    ListView list;
    Place_Listing_Adapter adapter;
    ArrayList<CSPlace> listvalues = new ArrayList<>();
    PlaceLoadingBar pleaseText;
    JSONArray resArray;
    LinearLayout lay_mapview, lay_filter, lay_map;
    private MapView mMapView;
    private GoogleMap mMap;
    private View clientRequestView;
    private Marker markerlocation;
    Double longitude, latitude;
    private ArrayList<langlatimoel> arrayListlonglati = new ArrayList();
    private Location location;
    private LocationHelper locationHelper;
    boolean mapvalue=false;
    TextView txt_view;
    ImageView img_s_room,img_close;
    TextView txt_s_title,txt_s_address,txt_s_rating,txt_s_price,textView9,txtfromdate,txttodate;
    CardView cart_single;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        clientRequestView = inflater.inflate(R.layout.place_listing, container, false);

        checkPermission();
        mMapView = (MapView) clientRequestView.findViewById(R.id.clientReqMap);
        mMapView.onCreate(savedInstanceState);
        setUpMap();

        if(checkPermission()){
            locationHelper = new LocationHelper(getActivity());
            locationHelper.setLocationReceivedLister(this);
            locationHelper.onStart();
        }


        return clientRequestView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = view.findViewById(R.id.listview);
        pleaseText = view.findViewById(R.id.please_txt);
        lay_mapview = view.findViewById(R.id.lay_mapview);
        lay_filter = view.findViewById(R.id.lay_filter);
        lay_map = view.findViewById(R.id.lay_map);
        txt_view = view.findViewById(R.id.txt_view);
        img_s_room = view.findViewById(R.id.img_s_room);
        txt_s_title = view.findViewById(R.id.txt_s_title);
        txt_s_address = view.findViewById(R.id.txt_s_address);
        txt_s_rating = view.findViewById(R.id.txt_s_rating);
        txt_s_price = view.findViewById(R.id.txt_s_price);
        cart_single = view.findViewById(R.id.cart_single);
        img_close = view.findViewById(R.id.img_close);
        textView9 = view.findViewById(R.id.textView9);
        txtfromdate = view.findViewById(R.id.txtfromdate);
        txttodate = view.findViewById(R.id.txttodate);

        textView9.setText(Utils.locationstring);
        txtfromdate.setText(Utils.checkindate);
        txttodate.setText(Utils.checkoutdate);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    JSONObject selectedPlace = resArray.getJSONObject(i);
                    addFragmentPlaceDetails(new PlaceDetailsFragment(), selectedPlace.toString());
                } catch (Exception e) {
                }

            }
        });

        lay_mapview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mapvalue==false){
                    lay_map.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);
                    mapvalue=true;
                    txt_view.setText("LIST VIEW");
                }else {
                    lay_map.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    mapvalue=false;
                    txt_view.setText("MAP VIEW");
                }


              /*  Intent intent=new Intent(getActivity(),MapviewActivity.class);
                startActivity(intent);*/
            }
        });


        lay_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.filter_dialog);


                Button okbtn = dialog.findViewById(R.id.okbtn);

                okbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cart_single.setVisibility(View.GONE);
            }
        });
        webServiceCall();
    }

    private void setUpMap() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (mMap == null) {
            mMapView = ((MapView) clientRequestView.findViewById(R.id.clientReqMap));

            mMapView.onResume();
            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    mMap = googleMap;

                    mMap.getUiSettings().setZoomControlsEnabled(false);

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                       checkPermission();
                        return ;
                    }
                    mMap.setMyLocationEnabled(false);
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);





                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {

                            cart_single.setVisibility(View.VISIBLE);
                            String id;
                            LatLng latLng =marker.getPosition();
                            Double clicklaiti=latLng.latitude;



                            for(int k=0;k<arrayListlonglati.size();k++){

                                longitude=    arrayListlonglati.get(k).getLongitute();
                                latitude=  arrayListlonglati.get(k).getLatitute();

                                if(clicklaiti.equals(latitude)){
                                    id=  arrayListlonglati.get(k).getId();
                                    for(int j=0;j<listvalues.size();j++){

                                        String allid= listvalues.get(j).id;

                                        if(id.equalsIgnoreCase(allid)){

                                            txt_s_title.setText(listvalues.get(j).name);
                                            txt_s_rating.setText(String.valueOf(listvalues.get(j).rating));
                                            txt_s_price.setText(String.valueOf(listvalues.get(j).minPrice));
                                            txt_s_address.setText(listvalues.get(j).location.addressLine2 +" - "+ listvalues.get(j).location.city);
                                            PicassoUtil.with(getActivity())
                                                    .load(listvalues.get(j).imageUrl).placeholder(R.mipmap.image_1)
                                                    .into(img_s_room);

                                        }
                                    }

                                }


                            }
                            return false;
                        }
                    });





                }
            });


        }
    }

    public void webServiceCall(){
        try{
            String tag_json_obj = this.getClass().getName()+"_Search";
            String url = Constants.URL+"businesses/search/places";//Constants.GET_PLACES;
            JSONObject jsonObject=new JSONObject();

            jsonObject.put("fromDateStr",""/*getArguments().getString("fromdate")*/);
            jsonObject.put("fromTimeStr",getArguments().getString("fromtime"));
            jsonObject.put("toDateStr",""/*getArguments().getString("todate")*/);
            jsonObject.put("toTimeStr",getArguments().getString("totime"));
            jsonObject.put("distInKm",100);
            jsonObject.put("latitude", getArguments().getString("lat"));
            jsonObject.put("longitude",getArguments().getString("lon"));

            JSONObject pagination = new JSONObject();
            pagination.put("pageNo", 1);
            pagination.put("limit", 10);
            jsonObject.put("paginate", pagination);
            Log.d("Request", jsonObject.toString() + url);


            pleaseText.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(final JSONObject response) {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        try{
                                            Log.e("Response", response.toString());
                                            resArray = response.getJSONObject("data").getJSONArray("businesses");
                                            if(resArray.length()>0){
                                                pleaseText.setVisibility(View.GONE);
                                                list.setVisibility(View.VISIBLE);
                                                for (int i = 0;i<resArray.length();i++){

                                                    JSONObject jsonObject1=resArray.getJSONObject(i);
                                                    JSONObject locationobject=jsonObject1.getJSONObject("location");

                                                    JSONObject coobject=locationobject.getJSONObject("coordinate");
                                                     longitude= Double.valueOf(coobject.getString("longitude"));
                                                     latitude= Double.valueOf(coobject.getString("latitude"));

                                                   String id= jsonObject1.getString("id");
                                                    listvalues.add(new CSPlace(resArray.getJSONObject(i)));
                                                    arrayListlonglati.add(new langlatimoel(longitude,latitude,id));
                                                   // Toast.makeText(getActivity(),id,Toast.LENGTH_SHORT).show();

                                                }



                                                adapter = new Place_Listing_Adapter(getActivity(),listvalues, arrayListlonglati,Place_Listing.this);
                                                list.setAdapter(adapter);


                                                for(int k=0;k<arrayListlonglati.size();k++){

                                                    longitude=    arrayListlonglati.get(k).getLongitute();
                                                    latitude=  arrayListlonglati.get(k).getLatitute();

                                                    markerlocation = mMap.addMarker(new MarkerOptions()
                                                            .position(
                                                                    new LatLng(latitude, longitude))
                                                            .icon(BitmapDescriptorFactory
                                                                    .fromResource(R.drawable.pin_client_org))
                                                            .title(""));

                                                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                                            new LatLng(latitude, longitude), 10));

                                                }



                                            }  else {
                                                list.setVisibility(View.GONE);
                                                pleaseText.setVisibility(View.VISIBLE);
                                                //pleaseText.setText("No Places Found");
                                            }
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }, 2000);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
// Adding request to request queue.
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().cancelPendingRequests(tag_json_obj);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }catch (Exception e){
            System.out.println("Error :"+e.toString());
        }
    }

    public boolean checkPermission() {


        if (Build.VERSION.SDK_INT >= 23) {

           boolean isfine=false;


            try {
                int hasWriteContactsPermission = getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
                if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                    isfine = false;
                } else {
                    isfine = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                isfine = true;
            }

            if ( !isfine) {
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 007);
            }


            if (isfine  ) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    public void onDestroyView() {
        mMapView.onDestroy();
        mMap=null;
        super.onDestroyView();
    }

    @Override
    public void onLocationReceived(LatLng latlong) {

        if (latlong != null) {
            if (mMap != null) {


             //  userlatitiure= location.getLatitude();
            //  userlongitute=  location.getLongitude();
               // Toast.makeText(getActivity(),String.valueOf(userlatitiure+userlongitute),Toast.LENGTH_SHORT).show();
                }

            }

        }

    @Override
    public void onLocationReceived(Location location) {

    }


    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onConntected(Location location) {

        this.location = location;
        if (location != null) {
            if (mMap != null) {

                Utils.userlatitude = location.getLatitude();
                Utils.userlongitute=  location.getLongitude();

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Utils.cityname = addresses.get(0).getLocality();
              //  Toast.makeText(getActivity(),String.valueOf(userlatitiure+userlongitute),Toast.LENGTH_SHORT).show();
            }
        } else {

        }
    }
}