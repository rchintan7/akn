package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.pinslot.place.R;

public class BaseFragment extends Fragment {

    public void addFragment(Fragment fragment, boolean backstack){
        FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
        tx.add(R.id.fragment_place,fragment,fragment.getClass().getName());
        tx.addToBackStack(fragment.getClass().getName());
        tx.commit();
    }
    public void replaceFragment(Fragment fragment, boolean backstack){
        FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.fragment_place,fragment,fragment.getClass().getName());
        tx.addToBackStack(fragment.getClass().getName());
        tx.commit();
    }

    public void addFragmentPlaceDetails(Fragment fragment, String obj){
        Bundle b=new Bundle();
        b.putString("JSON",obj);
        fragment.setArguments(b);
        FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
        tx.add(R.id.fragment_place,fragment,fragment.getClass().getName());
        tx.addToBackStack(fragment.getClass().getName());
        tx.commit();
    }
}
