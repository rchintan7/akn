package com.pinslot.place.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.adapter.PlaceArrayAdapter;
import com.pinslot.place.hbb20.CountryCodePicker;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Selva on 10/12/2018.
 */

public class ProfileFragment extends Fragment {
    EditText firstname, lastname,phone, email, street, state, zipcode, country, dob, wedDay;
    AutoCompleteTextView city;
    ImageView edit;
    ImageView profile_image;
    ImageView profile_image_edit;
    SharedPreferences pref;
    JSONObject ProfileDetail;
    Button save;
    String CaptureImagepath = "";
    Bitmap bitmap = null;
    TextView nameStr, stateCountry;

    CountryCodePicker cc_picker;
    JSONObject saveobj = null;
    String birthDate, birthMonth, wedDate, wedMonth;
    private final int RESULT_CROP = 400;
    private final int RESULT_CROP2 = 401;
    String isDob = "", isWed = "";
    private static final String LOG_TAG = "googlefilter";
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    LinearLayout changePassword;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        changePassword = view.findViewById(R.id.change_password);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reset_Popup(getActivity());
            }
        });
        firstname = (EditText) view.findViewById(R.id.first_name);
        lastname = (EditText) view.findViewById(R.id.last_name);
        phone = (EditText) view.findViewById(R.id.phone);
        email = (EditText) view.findViewById(R.id.email);

        dob = (EditText) view.findViewById(R.id.dob);
        wedDay = (EditText) view.findViewById(R.id.wed_day);

        street = (EditText) view.findViewById(R.id.street_address);
        city = (AutoCompleteTextView) view.findViewById(R.id.city);
        state = (EditText) view.findViewById(R.id.state);
        zipcode = (EditText) view.findViewById(R.id.zip_code);
        country = (EditText) view.findViewById(R.id.country);

        edit = (ImageView) view.findViewById(R.id.edit);
        profile_image = (ImageView) view.findViewById(R.id.profile_image);
        cc_picker = view.findViewById(R.id.cc_picker);
        cc_picker.showFlag(false);
        cc_picker.showNameCode(false);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(1950, dm.heightPixels);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        profile_image.setLayoutParams(params);
        profile_image_edit = (ImageView) view.findViewById(R.id.profile_image_edit);
        save = (Button) view.findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(Agree_notification.isChecked()) {
                final JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("profileId", getActivity().getApplicationContext().getSharedPreferences("MyPref", 0).getString("userId", ""));

                    jsonObject.put("birthDate", birthDate);
                    jsonObject.put("birthMonth", birthMonth);
                    jsonObject.put("weddingDate", wedDate);
                    jsonObject.put("weddingMonth", wedMonth);

                    JSONObject contactDetails = new JSONObject();
                    contactDetails.put("firstName", firstname.getText().toString());
                    contactDetails.put("lastName", lastname.getText().toString());
                    contactDetails.put("mobilePhone", /*cc_picker.getSelectedCountryCodeWithPlus()+*/phone.getText().toString());
                    contactDetails.put("mobilePhoneCountryCode",/*cc_picker.getSelectedCountryCodeWithPlus()*/"");
                    contactDetails.put("mobilePhoneNo", phone.getText().toString());
                    jsonObject.put("contactDetails", contactDetails);
                    JSONObject location = new JSONObject();
                    location.put("addressLine1", street.getText());
                    location.put("city", city.getText());
                    location.put("state", state.getText());
                    location.put("zip", zipcode.getText());
                    location.put("country", country.getText());
                    JSONObject co_ordinate = new JSONObject();
                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        String add_str = "";

                        if (!street.getText().toString().isEmpty()) {
                            if (add_str.isEmpty())
                                add_str = street.getText().toString();
                            else
                                add_str = add_str + "," + street.getText().toString();
                        }
                        if (!city.getText().toString().isEmpty()) {
                            if (add_str.isEmpty())
                                add_str = city.getText().toString();
                            else
                                add_str = add_str + "," + city.getText().toString();
                        }
                        if (!state.getText().toString().isEmpty()) {
                            if (add_str.isEmpty())
                                add_str = state.getText().toString();
                            else
                                add_str = add_str + "," + state.getText().toString();
                        }
                        if (!country.getText().toString().isEmpty()) {
                            if (add_str.isEmpty())
                                add_str = country.getText().toString();
                            else
                                add_str = add_str + "," + country.getText().toString();
                        }
                        if (!zipcode.getText().toString().isEmpty()) {
                            if (add_str.isEmpty())
                                add_str = zipcode.getText().toString();
                            else
                                add_str = add_str + "," + zipcode.getText().toString();
                        }
                        if (!add_str.isEmpty()) {
                            List<Address> address = geocoder.getFromLocationName(add_str, 5);
                            if (address != null && address.size() > 0) {
                                Address location1 = address.get(0);
                                co_ordinate.put("latitude", location1.getLatitude());
                                co_ordinate.put("longitude", location1.getLongitude());
                                location.put("coordinate", co_ordinate);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    jsonObject.put("location", location);
                } catch (JSONException e) {

                }
                webServiceCallForSaveProfile(jsonObject);

            }
        });

        city.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        //mPlaceArrayAdapter.notifyDataSetChanged();
        city.setAdapter(mPlaceArrayAdapter);
        if (((NavigationDrawerActivity) getActivity()).mGoogleApiClient.isConnected())
            mPlaceArrayAdapter.setGoogleApiClient(((NavigationDrawerActivity) getActivity()).mGoogleApiClient);
        if (!pref.getString("image", "").isEmpty())
            PicassoUtil.with(getActivity()).load(pref.getString("image", "")).placeholder(R.mipmap.profile_img).into(profile_image);

        if (!pref.getString("email", "").isEmpty())
            email.setText(pref.getString("email", ""));

        firstname.setText(pref.getString("firstName", ""));
        lastname.setText(pref.getString("lastName", ""));
        if (pref.getString("mobile", "").startsWith("+1")) {
            cc_picker.setCountryForPhoneCode(1);
            phone.setText(pref.getString("mobile", "").replace("+1", ""));
        } else if (pref.getString("mobile", "").startsWith("+91")) {
            cc_picker.setCountryForPhoneCode(91);
            phone.setText(pref.getString("mobile", "").replace("+91", ""));
        } else {
            phone.setText(pref.getString("mobile", ""));
        }
        if (!pref.getString("mobilePhoneCountryCode", "").isEmpty()) {
            phone.setText(pref.getString("mobile", "").replace(pref.getString("mobilePhoneCountryCode", ""), ""));
            cc_picker.setCountryForPhoneCode(Integer.parseInt(pref.getString("mobilePhoneCountryCode", "").replace("+", "")));
        }
        editEnable(false);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editEnable(true);
            }
        });
        profile_image_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCameraPopup(0);
            }
        });

        webServiceCallForGetProfile();

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDatePicker("dob");
            }
        });
        wedDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDatePicker("wedDay");
            }
        });


        String[] permissions = {"android.permission.CAMERA"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(permissions[0]) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    11);
        }
    }

    public void ShowDatePicker(final String type) {
        DatePickerDialog datepicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(0);
                cal.set(year, month, dayOfMonth);
                Date date = cal.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("MMM");
                if (type.equals("dob")) {
                    birthDate = String.valueOf(dayOfMonth);
                    birthMonth = sdf.format(date);
                    dob.setText(sdf.format(date) + "-" + dayOfMonth);
                } else {
                    wedDate = String.valueOf(dayOfMonth);
                    wedMonth = sdf.format(date);
                    wedDay.setText(sdf.format(date) + "-" + dayOfMonth);
                }
            }
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE)) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int year = getContext().getResources()
                        .getIdentifier("android:id/year", null, null);
                if (year != 0) {
                    View yearPicker = findViewById(year);
                    if (yearPicker != null) {
                        yearPicker.setVisibility(View.GONE);
                    }
                }
            }
        };
        datepicker.show();
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = com.google.android.gms.location.places.Places.GeoDataApi
                    .getPlaceById(((NavigationDrawerActivity) getActivity()).mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);

        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place myPlace = places.get(0);
            LatLng queriedLocation = myPlace.getLatLng();
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            // Get the current location from the input parameter list
            // Create a list to contain the result address
            List<Address> addresses = null;
            try {
                /*
                 * Return 1 address.
				 */
                addresses = geocoder.getFromLocation(queriedLocation.latitude, queriedLocation.longitude, 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments "
                        + Double.toString(queriedLocation.latitude) + " , "
                        + Double.toString(queriedLocation.longitude)
                        + " passed to address service";
                e2.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                mPlaceArrayAdapter.setEnable(false);
                city.setText(address.getLocality());
                Runnable run = new Runnable() {
                    @Override
                    public void run() {
                        mPlaceArrayAdapter.setEnable(true);
                    }
                };
                Handler handler = new Handler();
                handler.postDelayed(run, 50);
                state.setText(address.getAdminArea());
                country.setText(address.getCountryName());
                zipcode.setText(address.getPostalCode());
            }
        }
    };

    public void editEnable(boolean enable) {
        firstname.setEnabled(enable);
        lastname.setEnabled(enable);

        phone.setEnabled(enable);
        //email.setEnabled(enable);
        dob.setEnabled(enable);
        wedDay.setEnabled(enable);
        street.setEnabled(enable);
        city.setEnabled(enable);
        state.setEnabled(enable);
        zipcode.setEnabled(enable);
        country.setEnabled(enable);

        if (!enable) {
            profile_image_edit.setVisibility(View.GONE);

            //profile_image.setVisibility(View.VISIBLE);
            //companyImage.setVisibility(View.VISIBLE);

            save.setVisibility(View.GONE);
            edit.setVisibility(View.VISIBLE);
        } else {
            profile_image_edit.setVisibility(View.VISIBLE);

            //profile_image.setVisibility(View.GONE);
            //companyImage.setVisibility(View.GONE);

            save.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
        }
    }

    public void showCameraPopup(final int navi) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Select image using");
        alert.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String[] permissions = {"android.permission.CAMERA"};
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(permissions[0]) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(permissions, 7);
                } else {
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 0);
                }

            }
        });

        alert.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String[] permissions = {"android.permission.READ_EXTERNAL_STORAGE"};
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(permissions[0]) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(permissions, 8);
                } else {
                    Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    imageIntent.setType("image/*");
                    startActivityForResult(Intent.createChooser(imageIntent, "Select photo"), 1);
                }
            }
        });
        alert.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        Dialog dialog = alert.create();
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                //Do the Save Profile Call
                if (data != null) {

                    try {
                        Bitmap bitmap1;
                        if (data.getData() == null) {
                            bitmap1 = (Bitmap) data.getExtras().get("data");
                        } else {
                            bitmap1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        }
                        profile_image.setImageBitmap(bitmap1);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap1.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        bitmap = bitmap1;
                        webServiceCallForSaveImage("Profile");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if ((requestCode == 1 || requestCode == 8) && resultCode == Activity.RESULT_OK) {
            String path = getExternalFilePath(data.getData());
            File file = new File(path);
            Bitmap imageBitmap = null;
            if (file.exists()) {
                imageBitmap = getThumbNail(getActivity(), path, 80);
            } else {
                path = getPathToImage(data.getData());
                file = new File(path);
                if (file.exists()) {
                    imageBitmap = getThumbNail(getActivity(), path, 80);
                }
            }
            profile_image.setImageBitmap(imageBitmap);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            bitmap = imageBitmap;
            webServiceCallForSaveImage("Profile");
        }

    }
    public static int getPxForDp(Context activity, int px) {
        float density = activity.getResources().getDisplayMetrics().density;
        return (int) Math.round((float) px * density);
    }

    private String getExternalFilePath(Uri uri) {
        String[] proj = {MediaStore.Images.ImageColumns.DATA};
        Cursor cursor = getActivity().managedQuery(uri, proj, null, null, null);
        Integer colIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(colIndex);
    }
    private String getPathToImage(Uri uri) {
        String path = "";
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.split(":")[1];
            cursor.close();

            cursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            if (cursor != null) {
                cursor.moveToFirst();
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                cursor.close();
            }
        }
        return path;
    }

    public static Bitmap getThumbNail(Context context, String filepath, int height) {
        Bitmap image = getBitmap(filepath, getPxForDp(context, height));
        if (image == null)
            return null;
        return ThumbnailUtils.extractThumbnail(image, getPxForDp(context, height), getPxForDp(context, height));
    }

    private static Bitmap getBitmap(String url, int maxWidth) {
        try {
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inJustDecodeBounds = true; //for get image width and height without load the image
            Bitmap image = BitmapFactory.decodeFile(url, option);
            int ht = option.outHeight;
            int width = option.outWidth;
            float scale = (float) width / maxWidth;
            int sampleSize = 1;
            while (sampleSize < scale) {
                sampleSize = sampleSize * 2;  //sample size must be multiple of zero
            }
            option.inSampleSize = sampleSize;
            option.inJustDecodeBounds = false; //for load image in sample size
            Bitmap decoded_image = BitmapFactory.decodeFile(url, option);
            //Bitmap finalimage = ThumbnailUtils.ExtractThumbnail (decoded_image, width, ht);
            return decoded_image;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }


    }
    private void performCrop(String picUri, int cropType) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, cropType);
        }
        // respond to users whose devices do not support the crop action
        catch (Exception anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
            if (!picUri.isEmpty()) {
                bitmap = BitmapFactory.decodeFile(picUri);
                // Set The Bitmap Data To ImageView
                if (cropType == RESULT_CROP) {

                    webServiceCallForSaveImage("Company");
                } else if (cropType == RESULT_CROP2) {
                    profile_image.setVisibility(View.VISIBLE);
                    profile_image.setImageBitmap(bitmap);
                    webServiceCallForSaveImage("Profile");
                }
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        android.database.Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                imageIntent.setType("image/*");
                this.startActivityForResult(Intent.createChooser(imageIntent, "Select photo"), 1);
            } else if ((requestCode == 2 && grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                    || (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    || (requestCode == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Image_" + System.currentTimeMillis() + ".png");
                Uri outputfileuri = Uri.fromFile(file);
                CaptureImagepath = outputfileuri.getPath();
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputfileuri);
                this.startActivityForResult(intent, 0);
            }
        }
    }

    public void webServiceCallForGetProfile() {

        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.PROFILE + getActivity().getApplicationContext().getSharedPreferences("MyPref", 0).getString("userId", "");
            ;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Profile Request", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            pDialog.dismiss();
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    ProfileDetail = response.getJSONObject("data");
                                    SetValues();
                                }
                            } catch (Exception e) {
                                Log.d("JSON Parsing Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void webServiceCallForSaveProfile(JSONObject jsonObject) {

        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.PROFILE;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("PROFILE Request", url + " " + jsonObject);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("PROFILE Response", response.toString());
                                pDialog.dismiss();
                                if (response.getJSONObject("data") != null) {
                                    ProfileDetail = response.getJSONObject("data");
                                    editEnable(false);
                                    SetValues();
                                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    error.printStackTrace();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void webServiceCallForSaveImage(String type) {
        try {
            String tag_json_obj = "json_obj_req";
            String url;
            JsonObjectRequest jsonObjReq;
            if (type.equals("Profile")) {
                url = Constants.PROFILE + getActivity().getApplicationContext().getSharedPreferences("MyPref", 0).getString("userId", "") + "/image/upload";
                final ProgressDialog pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("Loading...");
                pDialog.show();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("imageBase64", getBase64String(bitmap));
                jsonObject.put("imageType", "jpeg");
                Log.d("Request", url+jsonObject);

                jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                        url, jsonObject,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.d("Response", response.toString());
                                    pDialog.dismiss();
                                    if (response.getString("status").equals("SUCCESS")) {
                                        if (response.getJSONObject("data") != null) {
                                            bitmap = null;
                                            SharedPreferences.Editor editor = pref.edit();
                                            editor.putString("image", response.getJSONObject("data").getString("imageUrl"));
                                            editor.commit();
                                            //((NavigationDrawerActivity) getActivity()).adapter.notifyDataSetChanged();
//                                        PicassoUtil.with(getActivity()).load(Constants.URL + response.getJSONObject("data").getString("imageUrl")).into(profile_image);
                                        }
                                    }

                                } catch (Exception e) {

                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        error.printStackTrace();
                    }
                });
            } else {
                url = Constants.PROFILE + getActivity().getApplicationContext().getSharedPreferences("MyPref", 0).getString("userId", "") + "/company-image/update";
                final ProgressDialog pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("Loading...");
                pDialog.show();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("imageBase64", getBase64String(bitmap));
                jsonObject.put("imageType", "jpeg");
                Log.d("Request", url);

                jsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                        url, jsonObject,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.d("Response", response.toString());
                                    pDialog.dismiss();
                                    if (response.getString("status").equals("SUCCESS")) {
                                        if (response.getJSONObject("data") != null) {
                                            bitmap = null;
                                            SharedPreferences.Editor editor = pref.edit();
                                            editor.putString("image", response.getJSONObject("data").getString("imageUrl"));
                                            editor.commit();
                                            //((NavigationDrawerActivity) getActivity()).adapter.notifyDataSetChanged();
//                                        PicassoUtil.with(getActivity()).load(Constants.URL + response.getJSONObject("data").getString("imageUrl")).into(profile_image);
                                        }
                                    }

                                } catch (Exception e) {

                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        error.printStackTrace();
                    }
                });
            }
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public void SetValues() {
        try {
            String address = "";
            String fname="",lname="";
            SharedPreferences.Editor editor = pref.edit();
            if (!ProfileDetail.getJSONObject("contactDetails").getString("firstName").equals("null")) {
                 fname = ProfileDetail.getJSONObject("contactDetails").getString("firstName");
                firstname.setText(fname);
                //nameStr.setText(fname);
                editor.putString("firstName", fname);

            }
            if (!ProfileDetail.getJSONObject("contactDetails").getString("lastName").equals("null")) {
                 lname = ProfileDetail.getJSONObject("contactDetails").getString("lastName");
                lastname.setText(lname);
                //nameStr.setText(fname);
                editor.putString("lastName", lname);

            }

            editor.putString("fname", fname);
            editor.putString("lname", lname);
            if (ProfileDetail.getJSONObject("contactDetails").has("mobilePhone") && !ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone").equals("null")) {
                editor.putString("mobile", ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone"));
                if (ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone").startsWith("+1")) {
                    cc_picker.setCountryForPhoneCode(1);
                    phone.setText(ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone").replace("+1", ""));
                } else if (ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone").startsWith("+91")) {
                    cc_picker.setCountryForPhoneCode(91);
                    phone.setText(ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone").replace("+91", ""));
                } else {
                    phone.setText(ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone"));
                }
                if (ProfileDetail.getJSONObject("contactDetails").has("mobilePhoneCountryCode") && !ProfileDetail.getJSONObject("contactDetails").getString("mobilePhoneCountryCode").isEmpty()) {
                    phone.setText(ProfileDetail.getJSONObject("contactDetails").getString("mobilePhone").replace(ProfileDetail.getJSONObject("contactDetails").getString("mobilePhoneCountryCode"), ""));
                    cc_picker.setCountryForPhoneCode(Integer.parseInt(ProfileDetail.getJSONObject("contactDetails").getString("mobilePhoneCountryCode").replace("+", "")));
                    editor.putString("mobilePhoneCountryCode", ProfileDetail.getJSONObject("contactDetails").getString("mobilePhoneCountryCode"));
                }
            }
            if (!ProfileDetail.getJSONObject("contactDetails").getString("emailAddress").equals("null")) {
                email.setText(ProfileDetail.getJSONObject("contactDetails").getString("emailAddress"));
                editor.putString("email", ProfileDetail.getJSONObject("contactDetails").getString("emailAddress"));
            }

            if (ProfileDetail.has("birthDate") && !ProfileDetail.getString("birthDate").equals("null") && !ProfileDetail.getString("birthMonth").equals("null")) {
                dob.setText(ProfileDetail.getString("birthMonth") + "-" + ProfileDetail.getString("birthDate"));
                dob.setEnabled(false);
                isDob = ProfileDetail.getString("birthMonth") + "-" + ProfileDetail.getString("birthDate");
                editor.putString("birthDate", isDob);
            } else {
                editor.putString("birthDate", "");
            }
            if (ProfileDetail.has("weddingDate") && !ProfileDetail.getString("weddingDate").equals("null") && !ProfileDetail.getString("weddingMonth").equals("null")) {
                wedDay.setText(ProfileDetail.getString("weddingMonth") + "-" + ProfileDetail.getString("weddingDate"));
                wedDay.setEnabled(false);
                isWed = ProfileDetail.getString("weddingMonth") + "-" + ProfileDetail.getString("weddingDate");
                editor.putString("weddingDate", isWed);

            } else {
                editor.putString("weddingDate", "");
            }
            if (ProfileDetail.has("photoLocation") && !ProfileDetail.getString("photoLocation").equals("null")) {
                PicassoUtil.with(getActivity()).load(/*Constants.URL + "resources" +*/ ProfileDetail.getString("photoLocation")).into(profile_image);
                editor.putString("image", ProfileDetail.getString("photoLocation"));
            }

            if (ProfileDetail.has("location") && !ProfileDetail.getJSONObject("location").equals("null")) {
                editor.putString("address", ProfileDetail.getJSONObject("location").getString("addressLine1"));
                editor.putString("city", ProfileDetail.getJSONObject("location").getString("city"));
                editor.putString("state", ProfileDetail.getJSONObject("location").getString("state"));
                editor.putString("country", ProfileDetail.getJSONObject("location").getString("country"));
                editor.putString("zip", ProfileDetail.getJSONObject("location").getString("zip"));
            }


            editor.commit();
            if (!ProfileDetail.getJSONObject("location").getString("addressLine1").equals("null"))
                address = address + ProfileDetail.getJSONObject("location").getString("addressLine1");
            /*if (!ProfileDetail.getJSONObject("location").getString("addressLine2").equals("null"))
                address = address + " " + ProfileDetail.getJSONObject("location").getString("addressLine2");*/
            street.setText(address);
            if (!ProfileDetail.getJSONObject("location").getString("city").equals("null"))
                city.setText(ProfileDetail.getJSONObject("location").getString("city"));
            if (!ProfileDetail.getJSONObject("location").getString("state").equals("null"))
                state.setText(ProfileDetail.getJSONObject("location").getString("state"));
            if (!ProfileDetail.getJSONObject("location").getString("zip").equals("null"))
                zipcode.setText(ProfileDetail.getJSONObject("location").getString("zip"));
            if (!ProfileDetail.getJSONObject("location").getString("country").equals("null")) {
                country.setText(ProfileDetail.getJSONObject("location").getString("country"));
            }

        } catch (JSONException e) {
            Log.d("Error",e.toString());
            e.printStackTrace();
        }
    }

    public void Reset_Popup(final Context act) {

        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.login_popup);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);

        final EditText oldPassword = (EditText) dialog.findViewById(R.id.old);
        final EditText newPassword = (EditText) dialog.findViewById(R.id.new_password);
        final EditText confirmPassword = (EditText) dialog.findViewById(R.id.confirm_password);

        Button submit = (Button) dialog.findViewById(R.id.sign_in);

        pref = act.getApplicationContext().getSharedPreferences("MyPref", 0);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!oldPassword.getText().toString().isEmpty() && !newPassword.getText().toString().isEmpty() && !confirmPassword.getText().toString().isEmpty()) {
                    if (newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                        try {
                            String tag_json_obj = "json_obj_req";
                            String url = Constants.RESET_PASSWORD + pref.getString("userId", "") + "/update/password";
                            final ProgressDialog pDialog = new ProgressDialog(act);
                            pDialog.setMessage("Loading...");
                            pDialog.show();
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("oldPassword", oldPassword.getText().toString());
                            jsonObject.put("newPassword", newPassword.getText().toString());
                            Log.d("Request", jsonObject.toString());
                            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                                    url, jsonObject,
                                    new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                Log.d("Response", response.toString());
                                                pDialog.dismiss();

                                                if (response.getString("status").equals("SUCCESS")) {
                                                    Toast.makeText(act, "Successfully updated", Toast.LENGTH_LONG).show();
                                                    dialog.dismiss();
                                                } else {
                                                    if(response.has("message"))
                                                        Toast.makeText(act,response.getString("message"),Toast.LENGTH_LONG).show();
                                                    else
                                                        Toast.makeText(act, "Failure", Toast.LENGTH_LONG).show();
                                                    dialog.dismiss();
                                                }

                                            } catch (Exception e) {

                                            }
                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    pDialog.dismiss();
                                }
                            });

                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                        } catch (Exception e) {
                            System.out.println("Error :" + e.toString());
                        }
                    } else {
                        Toast.makeText(act, "New password and Confirm password should be same", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(act, "All the fields are mandatory", Toast.LENGTH_LONG).show();
                }
            }
        });


        dialog.show();

    }
}
