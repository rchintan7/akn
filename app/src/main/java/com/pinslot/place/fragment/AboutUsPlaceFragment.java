package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pinslot.place.R;

/**
 * Created by Selva on 10/24/2018.
 */

public class AboutUsPlaceFragment extends BaseFragment implements View.OnClickListener {

    ViewPager pager;
    LinearLayout about1, about2, about3, about4;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_us_new, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pager = view.findViewById(R.id.pager);
        about1 = view.findViewById(R.id.about1);
        about2 = view.findViewById(R.id.about2);
        about3 = view.findViewById(R.id.about3);
        about4 = view.findViewById(R.id.about4);

        about1.setOnClickListener(this);
        about2.setOnClickListener(this);
        about3.setOnClickListener(this);
        about4.setOnClickListener(this);

        about1.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
        about2.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
        about3.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
        about4.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));

        pager.setAdapter(new AboutUsPlaceFragment.ViewPagerAdapter(getChildFragmentManager()));
        pager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == about1) {
            about1.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
            about2.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about3.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about4.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            pager.setCurrentItem(0);
        }
        if (v == about2) {
            about2.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
            about1.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about3.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about4.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            pager.setCurrentItem(1);

        }
        if (v == about3) {
            about3.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
            about1.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about2.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about4.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            pager.setCurrentItem(2);

        }
        if (v == about4) {
            about4.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
            about1.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about2.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            about3.setBackgroundColor(getActivity().getResources().getColor(R.color.about_bg));
            pager.setCurrentItem(3);
        }
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new AboutDetails();
                case 1:
                    return new PlacePhotosFragment();
                case 2:
                    return new About_History();
                case 3:
                    return new AboutFacilities();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

}
