package com.pinslot.place.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pinslot.place.R;
import com.pinslot.place.adapter.ImageAdapter;
import com.pinslot.place.adapter.ImagePagerAdapter;
import com.pinslot.place.adapter.ImagePagerAdaptertwo;
import com.pinslot.place.model.CSPhoto;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.widget.CustomViewPager;

import org.json.JSONObject;

import java.util.ArrayList;

public class PlacePhotosFragment extends BaseFragment {



    public static PlacePhotosFragment instance(String place){
        PlacePhotosFragment fragment = new PlacePhotosFragment();
        Bundle args = new Bundle();
        args.putString("JSON",place);
        fragment.setArguments(args);
        return fragment;
    }

    private ArrayList<Integer> images;
    private BitmapFactory.Options options;
    private ViewPager viewPager;
    private View btnNext, btnPrev;
    private ImagePagerAdaptertwo adapter;
    private LinearLayout thumbnailsContainer;


    private final static int[] resourceIDs = new int[]{
            R.drawable.galleryone,R.drawable.gallerytwo,R.drawable.gallerythree,R.drawable.galleryfour,R.drawable.galleryfive
            };
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_photos,container,false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      //  imagePager = view.findViewById(R.id.image_pager);
        //rvList=view.findViewById(R.id.rv_list);


/*
        try{

                imagePager.setAdapter(new ImagePagerAdaptertwo(items_2, getActivity()));
                imagePager.setEnabled(false);

           //rvList.setAdapter(new ImagePagerAdaptertwo(items_2, getActivity()));
        }catch (Exception e){
        }*/

        images = new ArrayList<>();

        //find view by id
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        thumbnailsContainer = (LinearLayout) view.findViewById(R.id.container);
        btnNext = view.findViewById(R.id.next);
        btnPrev = view.findViewById(R.id.prev);

        btnPrev.setOnClickListener(onClickListener(0));
        btnNext.setOnClickListener(onClickListener(1));

        setImagesData();

        // init viewpager adapter and attach
        adapter = new ImagePagerAdaptertwo(images,getActivity() );
        viewPager.setAdapter(adapter);


        inflateThumbnails();
    }

    private View.OnClickListener onClickListener(final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i > 0) {
                    //next page
                    if (viewPager.getCurrentItem() < viewPager.getAdapter().getCount() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    }
                } else {
                    //previous page
                    if (viewPager.getCurrentItem() > 0) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                    }
                }
            }
        };
    }

    private void setImagesData() {
        for (int i = 0; i < resourceIDs.length; i++) {
            images.add(resourceIDs[i]);
        }
    }

    private void inflateThumbnails() {
        for (int i = 0; i < images.size(); i++) {
            View imageLayout = getLayoutInflater().inflate(R.layout.item_image, null);
            ImageView imageView = (ImageView) imageLayout.findViewById(R.id.img_thumb);
            imageView.setOnClickListener(onChagePageClickListener(i));
            options = new BitmapFactory.Options();
            options.inSampleSize = 3;
            options.inDither = false;
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), images.get(i), options );
            imageView.setImageBitmap(bitmap);
            //set to image view
            imageView.setImageBitmap(bitmap);
            //add imageview
            thumbnailsContainer.addView(imageLayout);
        }
    }

    private View.OnClickListener onChagePageClickListener(final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(i);
            }
        };
    }
}
