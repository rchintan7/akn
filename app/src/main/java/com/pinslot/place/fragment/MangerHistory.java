package com.pinslot.place.fragment;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.pinslot.place.MyPagerSlidingStrip;
import com.pinslot.place.R;

/**
 * Created by Selva on 10/25/2018.
 */

public class MangerHistory extends Fragment {
    ImageView search;
    EditText search_et;
    String type = "";
    PagerSlidingTabStrip tabs;
    ViewPager viewPager;
    TextView title;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_notification,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //type = getArguments().getString("requestType","");
        search = view.findViewById(R.id.search);
        search_et = view.findViewById(R.id.search_et);
        tabs = view.findViewById(R.id.strip);
        title = view.findViewById(R.id.title);
        viewPager = view.findViewById(R.id.view_pager);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // callSearchCallbacks();
            }
        });
        search_et.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_SEARCH)) {
                    //callSearchCallbacks();
                }
                return false;
            }
        });

            viewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager(),new String[]{"NEW","ACCEPTED","REJECTED"}));
            tabs.setShouldExpand(true);
            tabs.setViewPager(viewPager);
            //tabs.setPadding(20.0f);
            tabs.setIndicatorColor(getResources().getColor(R.color.white));
            if(Build.VERSION.SDK_INT>19)
                tabs.setIndicatorHeight(10);
            else
                tabs.setIndicatorHeight(20);
            tabs.setTextColor(Color.WHITE);
            //tabs.setSelectedTextColor(Color.BLACK);
            //tabs.setNormalTextColor(Color.WHITE);
            tabs.setFillViewport(true);
            //tabs.setShouldExpand(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        private final String[] TITLES;
        private String items_str = "";

        public MyPagerAdapter(FragmentManager fm, String[] TITLES) {
            super(fm);
            this.TITLES = TITLES;
        }

        public MyPagerAdapter(FragmentManager fm, String items_str) {
            super(fm);
            this.TITLES = new String[]{"Detail"};
            this.items_str = items_str;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            return RecyclerList.instance(TITLES[position]);

        }
    }
}

