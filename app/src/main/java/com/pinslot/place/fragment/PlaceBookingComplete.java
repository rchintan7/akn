package com.pinslot.place.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.adapter.CheckoutDateAdapter;
import com.pinslot.place.model.PlaceSpace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

public class PlaceBookingComplete extends BaseFragment {

    Button addCart;
    ListView list;
    JSONObject spaceJSONData;
    String selectedType;
    TextView totalPrice, subTotal, cGST, sGST, totalAmount, spaceName, gstName, cstName;
    double subTotalStr, total;
    LinearLayout sgstLayout;
    CheckoutDateAdapter adapter;
    LinearLayout tax_layout;
    String currenyType = "";
    PlaceSpace space;
    String type = "";
    double value = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.place_booking_complete_two, container, false);
        addCart = (Button) view.findViewById(R.id.pay_now);
        list = (ListView) view.findViewById(R.id.list);
        totalPrice = (TextView) view.findViewById(R.id.total_price);
        spaceName = (TextView) view.findViewById(R.id.space_name);
        subTotal = (TextView) view.findViewById(R.id.sub_total);
        cGST = (TextView) view.findViewById(R.id.cgst);
        sGST = (TextView) view.findViewById(R.id.sgst);
        gstName = (TextView) view.findViewById(R.id.gst_name);
        cstName = (TextView) view.findViewById(R.id.cst_name);
        totalAmount = (TextView) view.findViewById(R.id.total_amount);
        sgstLayout = (LinearLayout) view.findViewById(R.id.sgst_layout);
        tax_layout = view.findViewById(R.id.tax_layout);
        spaceName.setText(((NavigationDrawerActivity)getActivity()).placeRequest.place.name.toUpperCase());
        ArrayList<Date> dates = new ArrayList<>();
        if(type.equals("Hourly")){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(((NavigationDrawerActivity)getActivity()).placeRequest.checkin);
            dates.add(calendar.getTime());
            calendar.add(Calendar.HOUR,1);
            while (((NavigationDrawerActivity)getActivity()).placeRequest.checkout.getTime()-calendar.getTime().getTime()>0){
                dates.add(calendar.getTime());
                calendar.add(Calendar.HOUR,1);
            }
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(((NavigationDrawerActivity)getActivity()).placeRequest.checkin);
            dates.add(calendar.getTime());
            calendar.add(Calendar.DATE,1);
            while (((NavigationDrawerActivity)getActivity()).placeRequest.checkout.getTime()-calendar.getTime().getTime()>0){
                dates.add(calendar.getTime());
                calendar.add(Calendar.DATE,1);
            }
        }
        adapter = new CheckoutDateAdapter(this,dates,type,value,"Rs");
        list.setAdapter(adapter);
        /*if (monthAvailabilities != null && monthAvailabilities.size() > 0) {
            if(getArguments().containsKey("selected_package")){
                try {
                    adapter = new CheckoutDateAdapter(this, monthAvailabilities, new FoodPkg(new JSONObject(getArguments().getString("selected_package"))),Integer.parseInt(getArguments().getString("capacity")));
                    list.setAdapter(adapter);
                }catch (JSONException e){

                }
            } else {
                adapter = new CheckoutDateAdapter(this, monthAvailabilities, null);
                list.setAdapter(adapter);
            }
        }
        if (dayAvailabilities != null && dayAvailabilities.size() > 0) {
            adapter = new CheckoutDateAdapter(this, null, dayAvailabilities);
            list.setAdapter(adapter);
        }*/
        list.post(new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams lp = list.getLayoutParams();
                View v = list.getChildAt(0);
                if(v!=null) {
                    v.measure(0, 0);
                    int childht = v.getMeasuredHeight();
                    lp.height = adapter.getCount() * childht;
                    list.setLayoutParams(lp);
                }
                list.requestLayout();
            }
        });

        addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PrefManager.getInstance(getContext()).getUserId().isEmpty()){
                    addFragment(new LoginFragment(),true);
                }else {
                    try {
                        final JSONObject jsonObject = new JSONObject();
                        jsonObject.put("customerId", PrefManager.getInstance(getContext()).getUserId());
                        jsonObject.put("spaceId", space.id);
                        jsonObject.put("businessId", ((NavigationDrawerActivity)getActivity()).placeRequest.place.id);
                        //jsonObject.put("firstName", name.getText().toString());
                        //jsonObject.put("email", email.getText().toString());
                        //jsonObject.put("phone", cc_picker.getSelectedCountryCodeWithPlus() + phone.getText().toString());
                        jsonObject.put("guestCount", ((NavigationDrawerActivity)getActivity()).placeRequest.adult);
                        jsonObject.put("totalPrice", totalPrice.getText().toString());
                        jsonObject.put("orgPrice", totalPrice.getText().toString());
                        //JSONArray amenites = new JSONArray(getArguments().getString("amenites"));
                        //jsonObject.put("reqAmenities", amenites);
                        JSONArray jsonArray = new JSONArray();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                        ((NavigationDrawerActivity)getActivity()).placeRequest.type = type;
                        ((NavigationDrawerActivity)getActivity()).placeRequest.dates = adapter.getDates();
                        if(type.equals("Hourly")){
                            jsonObject.put("rentType", "HOURLY");
                            for (Date date : adapter.getDates()){
                                JSONObject selectedDates = new JSONObject();
                                try {
                                    selectedDates.put("checkIn", df.format(date));
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    calendar.add(Calendar.HOUR, 1);
                                    selectedDates.put("checkOut", df.format(calendar.getTime()));
                                    jsonArray.put(selectedDates);
                                } catch (JSONException e){

                                }
                            }
                        } else {
                            jsonObject.put("rentType", "DAILY");
                            for (Date date : adapter.getDates()){
                                JSONObject selectedDates = new JSONObject();
                                try {
                                    selectedDates.put("checkIn", df.format(date));
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    calendar.add(Calendar.DATE, 1);
                                    selectedDates.put("checkOut", df.format(calendar.getTime()));
                                    jsonArray.put(selectedDates);
                                } catch (JSONException e){

                                }
                            }
                        }

                        jsonObject.put("selectedDates", jsonArray);
                        //jsonObject.put("checkIn", df.format(Collections.min(dates)));
                        //jsonObject.put("checkOut", df.format(Collections.max(dates)));
                        //jsonObject.put("date", df.format(Calendar.getInstance().getTime()));
                        //if (getArguments().containsKey("selected_package")) {
                        //    JSONObject pkg = new JSONObject(getArguments().getString("selected_package"));
                        //    jsonObject.put("foodMenuPackageId", pkg.getString("id"));
                        //}

                        Fragment fragment = new Book_Place();
                        Bundle arguments = new Bundle();
                        arguments.putString("totalPrice", String.valueOf(total));
                        arguments.putString("currency", ((NavigationDrawerActivity)getActivity()).placeRequest.place.getCurrencySymbol());
                        arguments.putString("json",jsonObject.toString());
                        fragment.setArguments(arguments);
                        addFragment(fragment,true);

                    } catch (Exception e) {
                        Log.d("Exception", e.toString());
                    }
                }
            }
        });
        UpdateTotal();
        return view;
    }

    public void UpdateTotal() {
        try {
            int SGSTAX = 0, CGSTAX = 0;
            if (spaceJSONData.has("taxList")) {
                tax_layout.setVisibility(View.VISIBLE);
                if (spaceJSONData.getJSONArray("taxList").length() >= 2) {
                    CGSTAX = spaceJSONData.getJSONArray("taxList").getJSONObject(0).getInt("percent");
                    SGSTAX = spaceJSONData.getJSONArray("taxList").getJSONObject(1).getInt("percent");
                    gstName.setText(spaceJSONData.getJSONArray("taxList").getJSONObject(0).getString("name") + "-" + CGSTAX + "%");
                    cstName.setText(spaceJSONData.getJSONArray("taxList").getJSONObject(1).getString("name") + "-" + SGSTAX + "%");

                } else {
                    CGSTAX = spaceJSONData.getJSONArray("taxList").getJSONObject(0).getInt("percent");
                    sgstLayout.setVisibility(View.GONE);
                    gstName.setText(spaceJSONData.getJSONArray("taxList").getJSONObject(0).getString("name"));
                    gstName.setText(spaceJSONData.getJSONArray("taxList").getJSONObject(0).getString("name") + "-" + CGSTAX + "%");
                    cstName.setVisibility(View.GONE);
                }
            } else
                tax_layout.setVisibility(View.GONE);
            double totalAmountStr = adapter.getCount()*value;
            /*if (adapter.monthAvailabilities != null && adapter.monthAvailabilities.size() > 0) {
                currenyType = adapter.monthAvailabilities.get(0).currencyType;
                for (MonthAvailability availability : adapter.monthAvailabilities) {
                    double price = availability.price;
                    if(getArguments().containsKey("selected_package")){
                        FoodPkg obj = new FoodPkg(new JSONObject(getArguments().getString("selected_package")));
                        currenyType = obj.getCurrencySymbol();
                        price = obj.price*Integer.parseInt(getArguments().getString("capacity"));
                    }
                    if (availability.coupon != null) {
                        price = price - (price * availability.coupon.getDouble("discount") / 100);
                    }
                    totalAmountStr += price;
                }
            } else {
                currenyType = adapter.dayAvailabilities.get(0).currencyType;
                for (DayAvailability availability : adapter.dayAvailabilities) {
                    double price = availability.hourValue;
                    if (availability.coupon != null) {
                        price = price - (price * availability.coupon.getDouble("discount") / 100);
                    }
                    totalAmountStr += price;
                }
            }*/
            totalPrice.setText(currenyType + " " + String.format("%.2f", totalAmountStr));
            subTotalStr = totalAmountStr;
            subTotal.setText(currenyType + " " + String.format("%.2f", subTotalStr));
            double CGST = (double) CGSTAX / (double) 100 * subTotalStr;
            double SGST = (double) SGSTAX / (double) 100 * subTotalStr;
            total = CGST + SGST + subTotalStr;
            totalAmount.setText(currenyType + " " + String.format("%.2f", total));
            sGST.setText(currenyType + " " + String.format("%.2f", SGST));
            cGST.setText(currenyType + " " + String.format("%.2f", CGST));
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        spaceJSONData = ((NavigationDrawerActivity)getActivity()).placeRequest.space.srcJson;
        space = ((NavigationDrawerActivity)getActivity()).placeRequest.space;
        getPrice();
    }

    private void getPrice(){
        if(space.price.hourValue>0&&(space.price.dayValue==0 || hourDiff()*space.price.hourValue < space.price.dayValue)){
            type = "Hourly";
            value = space.price.hourValue;
        } else {
            type = "Daily";
            value = space.price.dayValue;
        }
    }

    private long hourDiff(){
        long diff = ((NavigationDrawerActivity)getActivity()).placeRequest.checkout.getTime() - ((NavigationDrawerActivity)getActivity()).placeRequest.checkin.getTime();
        diff = diff/(1000*60*60);
        return diff;
    }
}
