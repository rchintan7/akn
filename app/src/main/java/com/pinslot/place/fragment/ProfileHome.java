package com.pinslot.place.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pinslot.place.R;
import com.pinslot.place.widget.PicassoUtil;

/**
 * Created by Selva on 10/24/2018.
 */

public class ProfileHome extends BaseFragment implements View.OnClickListener {
    RelativeLayout favHotel;
    ImageView profileIamge;
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        favHotel=view.findViewById(R.id.fav_hotel);
        profileIamge=view.findViewById(R.id.profile_image);

        favHotel.setOnClickListener(this);
        profileIamge.setOnClickListener(this);
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);

        if (!pref.getString("image", "").isEmpty())
            PicassoUtil.with(getActivity()).load(pref.getString("image", "")).placeholder(R.mipmap.profile_img).into(profileIamge);

    }

    @Override
    public void onClick(View v) {
        if(v==favHotel){
            replaceFragment(new FavoriteHotel(),true);
        }
        if(v==profileIamge){
            replaceFragment(new ProfileFragment(),true);
        }
    }
}
