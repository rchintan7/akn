package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.hbb20.CountryCodePicker;
import com.pinslot.place.helpers.Payment_Method;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Book_Place extends BaseFragment {

    public static Button bookEvent;
    LinearLayout loginOption;
    TextView loginText, bookText, totalMoney;
    String actualPrice, businessId, spaceId;
    public static String totalPrice;
    EditText name, email, phone, lname;
    int discount;
    public static int newTotalPrice;
    double total = 0;
    List<Date> dates = new ArrayList<Date>();
    CountryCodePicker cc_picker;
    ImageView banner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.book_event, container, false);

        totalMoney = view.findViewById(R.id.total_money);
        bookEvent = (Button) view.findViewById(R.id.payment);
        loginText = (TextView) view.findViewById(R.id.login_text);
        bookText = (TextView) view.findViewById(R.id.book_text);
        loginOption = (LinearLayout) view.findViewById(R.id.login_option_layout);
        name = (EditText) view.findViewById(R.id.fname);
        lname = view.findViewById(R.id.lname);
        email = (EditText) view.findViewById(R.id.email);
        phone = (EditText) view.findViewById(R.id.phone);
        banner = view.findViewById(R.id.banner_img);
        cc_picker = view.findViewById(R.id.cc_picker);
        cc_picker.showFlag(false);
        cc_picker.showNameCode(false);
        total = Double.parseDouble(getArguments().getString("totalPrice"));
        totalPrice = String.format("%.2f", total);
        totalMoney.setText(getArguments().getString("currency", "Rs") + " " + totalPrice);
        bookText.setText("Book Your Place");
        CSPlace place = ((NavigationDrawerActivity)getActivity()).placeRequest.place;
        if (place != null && !place.imageUrl.isEmpty()) {
            PicassoUtil.with(getContext()).load(place.imageUrl).placeholder(R.mipmap.image_1).into(banner);
        }
        if (!PrefManager.getInstance(getContext()).getUserId().isEmpty()) {
            name.setEnabled(false);
            lname.setEnabled(false);
            email.setEnabled(false);
            phone.setEnabled(false);
            cc_picker.setEnabled(false);
        }
        loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(new LoginFragment(),true);
            }
        });
        bookEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PrefManager.getInstance(getContext()).getUserId().isEmpty()) {
                    addFragment(new LoginFragment(),true);
                } else if (name.getText().toString().isEmpty() && lname.getText().toString().isEmpty())
                    Toast.makeText(getActivity(), "Enter name", Toast.LENGTH_SHORT).show();
                else if (email.getText().toString().isEmpty())
                    Toast.makeText(getActivity(), "Enter email address", Toast.LENGTH_SHORT).show();
                else if (!Utils.validateEmail(email.getText().toString()))
                    Toast.makeText(getActivity(), "Enter valid email address", Toast.LENGTH_SHORT).show();
                else {
                    try {
                        JSONObject jsonObject = new JSONObject(getArguments().getString("json"));
                        jsonObject.put("firstName", name.getText().toString());
                        jsonObject.put("email", email.getText().toString());
                        jsonObject.put("phone", cc_picker.getSelectedCountryCodeWithPlus() + phone.getText().toString());
                        Payment_Method.placeBooking(getActivity(), jsonObject, new Payment_Method.BookingCallBack() {
                            @Override
                            public void bookingSucess(final String bookingId, final String token, final JSONObject response) {
                                if(Double.parseDouble(totalPrice)>0) {
                                    ((NavigationDrawerActivity) getActivity()).startPayment(new NavigationDrawerActivity.PaymentCallback() {
                                        @Override
                                        public void paymentSuccess(String paymentId,String signature,String orderId) {
                                            Payment_Method.webServiceCallForPaymentUpdate(paymentId,signature,orderId,"SUCCESS",getContext(),new Payment_Method.PaymentUpdateCallback(){
                                                @Override
                                                public void PaymentUpdateSucess() {
                                                    showFinalScreen(response,bookingId);
                                                }
                                                @Override
                                                public void PaymentUpdateFailed() {
                                                    Utils.ShowAlert(R.string.failed,R.string.payment_succes_booking_failed,getContext());
                                                }
                                            });
                                        }

                                        @Override
                                        public void paymentError(int i,String s) {
                                            if(i==0&&!s.isEmpty())
                                                Utils.ShowAlert(getResources().getString(R.string.failed),s.toString(),getContext());
                                            else
                                                Utils.ShowAlert("Failed","Payment Failed",getContext());
                                            Payment_Method.webServiceCallForPaymentUpdate("","",token,"FAILED",getContext(),null);
                                        }
                                    }, token, "Place Booking",true);
                                } else {
                                    showFinalScreen(response,bookingId);
                                }
                            }

                            @Override
                            public void bookingFailed(String failed) {
                                if(failed.isEmpty())
                                    Utils.ShowAlert("Failed","Failed to booking.",getContext());
                                else
                                    Utils.ShowAlert("Failed",failed,getContext());
                            }
                        });
                            /*Fragment fragment = new Payment_Method();
                            Bundle args = new Bundle();
                            args.putString("Type", "Places");
                            args.putString("JSON", jsonObject.toString());
                            args.putString("placeObj", getArguments().getString("placeObj"));
                            args.putParcelableArrayList("selectedDateObjs", getArguments().getParcelableArrayList("selectedDateObjs"));
                            args.putParcelableArrayList("selectedMonthObjs", getArguments().getParcelableArrayList("selectedMonthObjs"));
                            args.putString("capacity", getArguments().getString("capacity"));
                            if (getArguments().containsKey("selected_package"))
                                args.putString("selected_package", getArguments().getString("selected_package"));
                            args.putString("time_calender_day", getArguments().getString("time_calender_day"));
                            args.putString("calendertype", getArguments().getString("calendertype"));
                            args.putString("currency", getArguments().getString("currency"));
                            fragment.setArguments(args);
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager
                                    .beginTransaction();
                            fragmentTransaction.add(R.id.fragment_place, fragment);
                            fragmentTransaction.addToBackStack(fragment.getClass().getName());
                            fragmentTransaction.commit();*/
                    } catch (Exception e) {
                        System.out.println("Error :" + e.toString());
                    }
                }

            }
        });
        return view;
    }

    public void showFinalScreen(JSONObject jsonObject,String bookingId){
        Fragment fragment = new Ticket_Confirmation();
        Bundle args = new Bundle();
        /*try {
            args.putString("totalPrice", jsonObject.getJSONObject("data").getString("totalPrice"));

        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        args.putString("totalPrice",totalPrice);
        args.putString("bookingId",bookingId);
        args.putString("capacity", Integer.toString(((NavigationDrawerActivity)getActivity()).placeRequest.adult));
        if (getArguments().containsKey("selected_package"))
            args.putString("selected_package", getArguments().getString("selected_package"));
        args.putString("currency", getArguments().getString("currency"));
        fragment.setArguments(args);
        addFragment(fragment,true);
    }

    @Override
    public void onResume() {
        super.onResume();
        email.setEnabled(true);
        phone.setEnabled(true);
        cc_picker.setCcpClickable(true);
        if (PrefManager.getInstance(getContext()).getUserId().isEmpty()) {
            loginOption.setVisibility(View.VISIBLE);
        } else {
            loginOption.setVisibility(View.VISIBLE);
            loginText.setText("Go ahead ! You are already logged in");
            loginText.setEnabled(false);
            name.setText(PrefManager.getInstance(getContext()).getFirstName());
            lname.setText(PrefManager.getInstance(getContext()).getLastName());
            email.setText(PrefManager.getInstance(getContext()).getEmail());
            email.setEnabled(false);
            String mobile_text = PrefManager.getInstance(getContext()).getPhone();
            String cc = Utils.getCountryCode(mobile_text,getContext());
            if (!mobile_text.isEmpty() && !cc.isEmpty()) {
                mobile_text = mobile_text.replace(cc, "");
            }
            phone.setText(mobile_text);
            phone.setEnabled(false);
            if (!cc.isEmpty())
                cc_picker.setCountryForPhoneCode(Integer.parseInt(cc.replace("+", "")));
            cc_picker.setCcpClickable(false);
        }
    }
}
