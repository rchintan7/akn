package com.pinslot.place.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.helpers.GPSService;
import com.pinslot.place.model.Restaurant;
import com.pinslot.place.model.RestaurantRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class OrderConfirmationFragment extends BaseFragment implements OnMapReadyCallback {
    private SupportMapFragment mapFragment;
    TextView tv_bookingId,history,address,res_name,tv_date,tv_month,tv_time,orderType,wait_text;
    ImageView callIcon;
    Restaurant res_object;
    GoogleMap googleMap;
    Date date = Calendar.getInstance().getTime();
    String phone_url = "";
    String[] months = {"January","Februray","March","April","May","June","July","Augest","Septemper","October","Nevember","December"};
    LinearLayout deliveryLayout;
    //---ResExpSlot.TimeSlot Slot;
    //---RestaurantExperience experience;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_confirmation,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final String bookingId = getArguments().getString("BookingId", "");
        if(!getArguments().getString("type").equals("ext_exp")) {
            Long dateStr = ((NavigationDrawerActivity) getActivity()).resReq.startDate;
            date = new Date(dateStr);
        }
        if(getArguments().getString("type").equals("ext_exp")||getArguments().getString("type").equals("res_exp")){
            /*try {
                ---experience = new RestaurantExperience(new JSONObject(getArguments().getString("exp")));
                Slot = new ResExpSlot.TimeSlot(new JSONObject(getArguments().getString("slot")));
                date = new Date(getArguments().getLong("date"));
            } catch (JSONException e){

            }*/
        }
        tv_bookingId = (TextView)view.findViewById(R.id.booking_id);
        res_name = (TextView)view.findViewById(R.id.restaurant_name);
        address = (TextView)view.findViewById(R.id.address);
        tv_date = (TextView)view.findViewById(R.id.date);
        tv_month = (TextView)view.findViewById(R.id.month);
        tv_time = (TextView)view.findViewById(R.id.time);
        callIcon = (ImageView) view.findViewById(R.id.call_icon);
        orderType = (TextView) view.findViewById(R.id.order_type);
        deliveryLayout = (LinearLayout) view.findViewById(R.id.delivery_layout);
        LinearLayout history_layout = view.findViewById(R.id.history_layout);
        TextView flow_text = view.findViewById(R.id.flow_text);
        ImageView share = view.findViewById(R.id.share);
        ImageView cal = view.findViewById(R.id.cal);
        wait_text = view.findViewById(R.id.wait_text);


        if(getArguments().getString("type").equals("res_exp")||getArguments().getString("type").equals("ext_exp")){
            deliveryLayout.setVisibility(View.GONE);
        }else if (((NavigationDrawerActivity) getContext()).resReq.flow.equals("Delivery") || ((NavigationDrawerActivity) getContext()).resReq.flow.equals("Meetup")) {
            history_layout.setVisibility(View.GONE);
            if(((NavigationDrawerActivity) getContext()).resReq.flow.equals("Meetup"))
                flow_text.setText("Continue to Meetup");
            else
                flow_text.setText("Back to Delivery");
        } else {
            if(((NavigationDrawerActivity)getActivity()).resReq.requestType.equals("DineIn")) {
                flow_text.setText("Create Meetup");
                wait_text.setVisibility(View.VISIBLE);
            } else
                flow_text.setText("Need Delivery?");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        tv_date.setText(Integer.toString(calendar.get(Calendar.DATE)));
        tv_month.setText(months[calendar.get(Calendar.MONTH)]);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        tv_time.setText(sdf.format(date));
        Constants.DELIVERY_NAVI = "FALSE";

        /*callIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(res_object!=null)
                    phone_url = "tel:" + res_object.phone.trim();
                else if(experience!=null&&experience.managerProfile!=null){
                    CSUser user = new CSUser(experience.managerProfile);
                    phone_url = "tel:" + user.contactDetails.mobilePhone;
                } else if(getArguments().containsKey("placeExpType") && getArguments().getString("placeExpType").equals("place_exp")&&getArguments().containsKey("placeObject")){
                    try {
                        CSPlace place = new CSPlace(new JSONObject(getArguments().getString("placeObject")));
                        if(place.managerProfile!=null){
                            phone_url = "tel:" + place.managerProfile.contactDetails.mobilePhone;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(!phone_url.isEmpty()) {
                    if (Build.VERSION.SDK_INT > 22 && getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
                        OrderConfirmationFragment.this.requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 0);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse(phone_url));
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(getActivity(),"Phone number not available",Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        history_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        deliveryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        tv_bookingId.setText("ORDER ID : "+bookingId);
        if(!getArguments().getString("type").equals("ext_exp"))
            res_object = ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant;
        if(getArguments().getString("type").equals("res_exp")||getArguments().getString("type").equals("ext_exp")) {
            //---tv_time.setText(Slot.startTime);
            orderType.setText("Experience Address");
        } else if (((NavigationDrawerActivity) getActivity()).RestaurantBookingType.equals("DineIn")) {
            if (((NavigationDrawerActivity) getContext()).resReq.flow.equals("Meetup"))
                orderType.setText("Meetup Address");
            else
                orderType.setText("Dine-In Address");
        }else {
            if (((NavigationDrawerActivity) getContext()).resReq.flow.equals("Delivery"))
                orderType.setText("Delivery Pickup Address");
            else
                orderType.setText("Take Away Address");
        }
        if(res_object!=null)
            res_name.setText(res_object.name);
        if(getArguments().getString("type").equals("ext_exp")||getArguments().getString("type").equals("res_exp")){
            //----res_name.setText((res_object!=null)?res_object.name:""+"("+experience.title+")");
        }
        if(getArguments().getString("type").equals("ext_exp")) {
            /*----if (experience.location != null && experience.location.addressString != null) {
                address.setText(experience.location.addressString);
                try {
                    if (getArguments().containsKey("placeObject")) {
                        JSONObject placeObject = new JSONObject(getArguments().getString("placeObject"));
                        address.setText(placeObject.getJSONObject("location").getString("addressString"));
                    }
                } catch (Exception e) {

                }
            } else {
                try {
                    if (getArguments().containsKey("placeObject")) {
                        JSONObject placeObject = new JSONObject(getArguments().getString("placeObject"));
                        address.setText(placeObject.getJSONObject("location").getString("addressString"));
                    }
                } catch (Exception e) {

                }
            }*/
        } else
            address.setText(res_object.location.venue + " " + res_object.location.addressLine1 + "," + res_object.location.addressLine2 + "," + res_object.location.city);
        CreateMap();

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    FragmentManager.BackStackEntry backStackEntry = getActivity().getSupportFragmentManager().getBackStackEntryAt(getActivity().getSupportFragmentManager().getBackStackEntryCount()-1);
                    if(backStackEntry.getName()!=null&&backStackEntry.getName().equals(OrderConfirmationFragment.class.getName())) {
                        ((NavigationDrawerActivity) getActivity()).ClearFragment();
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = "";
                if(getArguments().getString("type").equals("ext_exp")||getArguments().getString("type").equals("res_exp")){
                    //---shareBody = "I'm going to enjoy good experience "+ experience.title +" from www.pinslots.com today";
                } else if(((NavigationDrawerActivity) getActivity()).resReq!=null) {
                    RestaurantRequest req = ((NavigationDrawerActivity) getActivity()).resReq;
                    shareBody = "I'm going to enjoy delicious food at (" + req.selectedRestaurant.name + ") from www.Pinslots.com today.  I highly recommend this restaurant to everyone.\n" + "http://www.connectslot.com/#/restaurant/details?restaurantId=" + req.selectedRestaurant.id;
                }
                /*"Hello,\n" +
                        "\n" +
                        "I would like to share this " + req.selectedRestaurant.name + "coupon exclusively for you from PinSlots App.\n" +
                        "\n" +
                        "Click here to redirect" +
                        "\n" +
                        req.selectedRestaurant.name.toUpperCase() + " http://www.pinslots.com/#/restaurant/details?restaurantId=" + req.selectedRestaurant.id;*/

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Restaurant");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                getActivity().startActivity(Intent.createChooser(sharingIntent, "Share"));
            }
        });

        if(getArguments().getString("type").equals("ext_exp")){
            cal.setVisibility(View.GONE);
        }

        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!getArguments().getString("type").equals("ext_exp")) {
                    RestaurantRequest req = ((NavigationDrawerActivity)getActivity()).resReq;
                    Date date = new Date(req.startDate);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                    String stringDate = sdf.format(date);
                    Constants.WHERE_STR = req.selectedRestaurant.location.addressString;
                    Constants.EVENT_NAME = req.selectedRestaurant.name;
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, cal.getTimeInMillis());
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, cal.getTimeInMillis() + 60 * 60 * 1000);
                    intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, 1);
                    intent.putExtra(CalendarContract.Events.DTSTART, cal.getTimeInMillis());
                    intent.putExtra(CalendarContract.Events.DTEND, cal.getTimeInMillis());
                    intent.putExtra(CalendarContract.Events.TITLE, Constants.EVENT_NAME);
                    intent.putExtra(CalendarContract.Events.DESCRIPTION, "Welcome All");
                    intent.putExtra(CalendarContract.Events.EVENT_LOCATION, Constants.WHERE_STR);
                    intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
                    getActivity().startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == 0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(phone_url));
            startActivity(intent);
        }
    }

    private void CreateMap() {
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            try {
                mapFragment = new SupportMapFragment();
                mapFragment.getMapAsync(this);
                FragmentTransaction tx = fm.beginTransaction();
                tx.add(R.id.map_container, mapFragment);
                tx.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        String name = "";
        LatLng latLng = null;
        if(!getArguments().getString("type").equals("ext_exp")) {
            res_object = ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant;
            name = res_object.name;
        }
        latLng = new LatLng(res_object.location.coordinate.latitude,res_object.location.coordinate.longitude);
        if(latLng!=null) {
            googleMap.addMarker(new MarkerOptions().title(name).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_res)));
            GPSService gpsService = new GPSService(getActivity());
            gpsService.getLocation();
            LatLng mylocaton = new LatLng(gpsService.getLatitude(), gpsService.getLongitude());
            googleMap.addMarker(new MarkerOptions().title("Home").position(mylocaton).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_home)));
            String directionUrl = getDirectionsUrl(mylocaton, latLng);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(directionUrl);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(mylocaton);
            builder.include(latLng);
            LatLngBounds bounds = builder.build();
            int padding = 20;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cu);
        }
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }
    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            //Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                //DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                //routes = parser.parse(jObject);
                routes = parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLACK);
            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions!=null)
                googleMap.addPolyline(lineOptions);
        }
    }

    public List<List<HashMap<String,String>>> parse(JSONObject jObject){

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        try {

            jRoutes = jObject.getJSONArray("routes");


            for(int i=0;i<jRoutes.length();i++){
                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<HashMap<String, String>>();


                for(int j=0;j<jLegs.length();j++){
                    jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");


                    for(int k=0;k<jSteps.length();k++){
                        String polyline = "";
                        polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);


                        for(int l=0;l<list.size();l++){
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                            hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return routes;
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }
}