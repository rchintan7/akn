package com.pinslot.place.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.helpers.Payment_Method;
import com.pinslot.place.model.Coupon;
import com.pinslot.place.model.Restaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RestaurantCheckin extends Fragment implements View.OnClickListener{

    TextView dateText,dayText,time,timeStr,people,typeText,couponValidity;
    ImageView leftArrow,rightArrow,timeLeft,timeRight,minus,plus;
    String formattedDate,formattedService;
    Calendar c;
    SimpleDateFormat df,dayOfWeek,dateFormatService;
    //String timeArr[]={"10:00","11:00","12:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00"};
    //String timeStrArr[]={"AM","AM","PM","PM","PM","PM","PM","PM","PM","PM","PM","PM","PM","PM","PM"};
    String timeArr[] = {"Closed"};
    int timePosition=0,peopleCount=1;
    private TextView mAttTextView;
    ImageView submit;
    SharedPreferences pref;
    LinearLayout people_layout;
    LinearLayout date_layout;
    android.os.Handler handler;
    DatePickerDialog StartTime;
    Spinner time_spinner;
    int selected_pos = 0;
    int minTime = 0;
    JSONObject json = null;
    final String RESET_STRING = "hh:mm am/pm";
    JSONArray timings = null;
    double lati= 0.0, langi = 0.0;
    String res_name= "";
    SharedPreferences.Editor editor;
    RequestTypeChangeCallbak changeCallbak;

    public static RestaurantCheckin Instance (RequestTypeChangeCallbak callbak,String type){
        RestaurantCheckin checkin = new RestaurantCheckin();
        Bundle args = new Bundle();
        args.putString("type",type);
        checkin.setArguments(args);
        checkin.changeCallbak = callbak;
        return checkin;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rest_checkin,container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        dateText=(TextView)view.findViewById(R.id.date);
        typeText=(TextView)view.findViewById(R.id.type_text);
        dayText=(TextView)view.findViewById(R.id.day);
        time=(TextView)view.findViewById(R.id.time);
        timeStr=(TextView)view.findViewById(R.id.time_str);
        people=(TextView)view.findViewById(R.id.people);
        people_layout=(LinearLayout)view.findViewById(R.id.people_layout);
        date_layout=(LinearLayout)view.findViewById(R.id.date_layout);
        couponValidity = view.findViewById(R.id.coupon_validity);
        date_layout.setOnClickListener(this);
        String type = getArguments().getString("type");
        if(type.equals("DineIn")){
            people_layout.setVisibility(View.VISIBLE);
            typeText.setText("Dine In");
        }else{
            people_layout.setVisibility(View.GONE);
            typeText.setText("Take Out");
        }

        leftArrow=(ImageView)view.findViewById(R.id.left_arrow);
        rightArrow=(ImageView)view.findViewById(R.id.right_arrow);
        timeLeft=(ImageView)view.findViewById(R.id.time_lef);
        timeRight=(ImageView)view.findViewById(R.id.time_right);

        minus=(ImageView)view.findViewById(R.id.minus);
        plus=(ImageView)view.findViewById(R.id.plus);
        submit=view.findViewById(R.id.submit);

        c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        df = new SimpleDateFormat("dd-MMM-yyyy");
        dateFormatService = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        dayOfWeek = new SimpleDateFormat("EEEE");

        formattedDate = df.format(c.getTime());
        formattedService=dateFormatService.format(c.getTime());
        dateText.setText(formattedDate);
        dayText.setText(dayOfWeek.format(c.getTime()));
        if(((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.storeOpen!=null)
            timings = ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.storeOpen;
        Restaurant jsonObject = ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant;
        lati = jsonObject.location.coordinate.latitude;
        langi = jsonObject.location.coordinate.longitude;
        res_name = jsonObject.name;

        if(timings == null){
            placeWebServiceCall(res_name);
        }
        ResetTime();

        leftArrow.setOnClickListener(this);
        rightArrow.setOnClickListener(this);

        timeLeft.setOnClickListener(this);
        timeRight.setOnClickListener(this);

        minus.setOnClickListener(this);
        plus.setOnClickListener(this);
        submit.setOnClickListener(this);
        time.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        StartTime = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                try{
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    c = newDate;
                    formattedDate = df.format(c.getTime());
                    formattedService=dateFormatService.format(c.getTime());
                    if(!formattedDate.equals(dateText.getText().toString())) {
                        dateText.setText(formattedDate);
                        dayText.setText(dayOfWeek.format(c.getTime()));
                        ResetTime();
                    }

                }catch (Exception e){
                    Log.d("Exception",e.toString());
                }


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        StartTime.getDatePicker().setMinDate(newCalendar.getTime().getTime());
        if(((NavigationDrawerActivity)getActivity()).resReq!=null&&((NavigationDrawerActivity)getActivity()).resReq.coupon!=null){
            Coupon coupon = ((NavigationDrawerActivity)getActivity()).resReq.coupon;
            if(coupon.dates!=null&&coupon.dates.size()>0){
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                couponValidity.setVisibility(View.VISIBLE);
                couponValidity.setText("Your selected coupon valid from "+sdf.format(new Date(coupon.dates.get(0).startDateTime))+" to "+sdf.format(new Date(coupon.dates.get(0).endDateTime)));
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

    }

    public void showDatePickDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Time");
        builder.setSingleChoiceItems(timeArr,selected_pos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!timeArr[which].equals("Closed")){
                    time.setText(timeArr[which]);
                    String[]val1 = timeArr[which].split(" ");
                    String[]val2 = val1[0].split(":");
                    int hr = Integer.parseInt(val2[0]);
                    int min = Integer.parseInt(val2[1]);
                    if(hr==12)
                        hr = 0;
                    c.set(Calendar.HOUR,hr);
                    c.set(Calendar.MINUTE,min);
                    if(val1[1].equals("PM"))
                        c.set(Calendar.AM_PM,1);
                    else
                        c.set(Calendar.AM_PM,0);
                    selected_pos = which;
                }
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void ResetTime(){
        timeArr = getAvailableTimes();
        selected_pos = 0;
        time.setText(timeArr[0]);
        if(timeArr.length>0&&!timeArr[0].isEmpty()&&!timeArr[0].equals("Closed")){
            String[]val1 = timeArr[0].split(" ");
            String[]val2 = val1[0].split(":");
            int hr = Integer.parseInt(val2[0]);
            int min = Integer.parseInt(val2[1]);
            if(hr==12)
                hr = 0;
            c.set(Calendar.HOUR,hr);
            c.set(Calendar.MINUTE,min);
            if(val1[1].equals("PM"))
                c.set(Calendar.AM_PM,1);
            else
                c.set(Calendar.AM_PM,0);
        }
        //timeStr.setText("am/pm");
    }

    public String[] getAvailableTimes(){
        boolean isClosed = true;
        String [] times = {"Closed"};
        float starttime1 = 0;
        float endtime1 = 23.5f;
        float starttime2 = 0;
        float endtime2 = 0;
        if(isToday()){
            starttime1 = Calendar.getInstance().get(Calendar.HOUR);
            if(starttime1 == 12)
                starttime1 = 0;
            if(Calendar.getInstance().get(Calendar.AM_PM)>0)
                starttime1 = starttime1+12;
            if(Calendar.getInstance().get(Calendar.MINUTE)>29)
                starttime1 = starttime1+1;
            else
                starttime1 = starttime1+0.5f;
        }
        if(timings!=null){
            String day = dayOfWeek.format(c.getTime());
            for(int i = 0;i<timings.length();i++){
                try {
                    if(timings.getJSONObject(i).getString("day").equals(day)){
                        float timeS1 = 0.0f;
                        if(timings.getJSONObject(i).has("checkInOne")){
                            timeS1 = getTimeFloatValue(timings.getJSONObject(i).getString("checkInOne"));
                        }
                        if(timings.getJSONObject(i).has("checkOutOne"))
                            endtime1 = getTimeFloatValue(timings.getJSONObject(i).getString("checkOutOne"));
                        if(timings.getJSONObject(i).has("checkInTwo")&&!timings.getJSONObject(i).getString("checkInTwo").isEmpty())
                            starttime2 = getTimeFloatValue(timings.getJSONObject(i).getString("checkInTwo"));
                        if(timings.getJSONObject(i).has("checkOutTwo")&&!timings.getJSONObject(i).getString("checkOutTwo").isEmpty())
                            endtime2 = getTimeFloatValue(timings.getJSONObject(i).getString("checkOutTwo"));
                        isClosed = false;
                        if(timeS1>starttime1)
                            starttime1 = timeS1;
                        else if(starttime1>timeS1&&starttime1>endtime1&&starttime2>starttime1){
                            starttime1 = starttime2;
                            endtime1 = endtime2;
                            starttime2 =0.0f;
                            endtime2 = 0.0f;
                        } else if(starttime1>timeS1&&starttime1>endtime1&&starttime1>starttime2&&endtime2>starttime1){
                            endtime1 = endtime2;
                            starttime2 =0.0f;
                            endtime2 = 0.0f;
                        } else if(starttime1>timeS1&&starttime1>endtime1&&starttime1>endtime1&&starttime2>endtime2){
                            isClosed = true;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            isClosed = false;
        }
        if(starttime1>endtime1){
            isClosed = true;
            //endtime1 = 23.5f;
        }
        if(!isClosed) {
            if (starttime2 == 0)
                times = new String[(((int) (endtime1 * 2)) + 1) - ((int) (starttime1 * 2))];
            else
                times = new String[((((int) (endtime1 * 2)) + 1) - ((int) (starttime1 * 2))) + ((((int) (endtime2 * 2)) + 1) - ((int) (starttime2 * 2)))];
            for (float time = starttime1; time < (endtime1 + 0.5f); time += 0.5f) {
                String AM_PM = "AM";
                if (time >= 12) {
                    AM_PM = "PM";
                    time = time - 12;
                }
                String value = "";
                if (time < 1) {
                    value = "12";
                } else if (time < 10) {
                    value = "0" + Integer.toString((int) time);
                } else {
                    value = Integer.toString((int) time);
                }
                if (time % 1 == 0)
                    value = value + ":00 " + AM_PM;
                else
                    value = value + ":30 " + AM_PM;
                if (AM_PM.equals("PM"))
                    time = time + 12;
                times[(int) (time * 2) - (int) (starttime1 * 2)] = value;
            }
            if (starttime2 > 0) {
                for (float time = starttime2; time < (endtime2 + 0.5f); time += 0.5f) {
                    String AM_PM = "AM";
                    if (time >= 12) {
                        AM_PM = "PM";
                        time = time - 12;
                    }
                    String value = "";
                    if (time < 1) {
                        value = "12";
                    } else if (time < 10) {
                        value = "0" + Integer.toString((int) time);
                    } else {
                        value = Integer.toString((int) time);
                    }
                    if (time % 1 == 0)
                        value = value + ":00 " + AM_PM;
                    else
                        value = value + ":30 " + AM_PM;
                    if (AM_PM.equals("PM"))
                        time = time + 12;
                    times[((((int) (endtime1 * 2)) + 1) - ((int) (starttime1 * 2))) + (int) (time * 2) - (int) (starttime2 * 2)] = value;
                }
            }
        }
        return times;
    }

    public float getTimeFloatValue(String time){
        float val = 0.0f;
        String[]val1 = time.split(" ");
        String[]val2 = val1[0].split(":");
        int hr = Integer.parseInt(val2[0]);
        int min = Integer.parseInt(val2[1]);
        val = hr;
        if(val == 12)
            val = 0;
        if(val1[1].equals("PM"))
            val = val+12;
        if(min>30)
            val = val+1;
        else if(min>0)
            val = val+0.5f;
        return val;
    }

    public boolean isToday(){
        Calendar cal = Calendar.getInstance();
        if(cal.get(Calendar.DATE)==c.get(Calendar.DATE)&&cal.get(Calendar.MONTH)==c.get(Calendar.MONTH)&&cal.get(Calendar.YEAR)==c.get(Calendar.YEAR))
            return true;
        else
            return false;
    }

    public void onClick(View v){
        if(v==time){
            showDatePickDialog();
        }

        if(v==leftArrow){
            Date Today = Calendar.getInstance().getTime();
            if(Today.getDate()!=c.getTime().getDate()||Today.getMonth() != c.getTime().getMonth()|| Today.getYear() != c.getTime().getYear()) {

                c.add(Calendar.DATE, -1);
                formattedDate = df.format(c.getTime());
                formattedService = dateFormatService.format(c.getTime());
                Log.v("PREVIOUS DATE : ", formattedDate);
                dateText.setText(formattedDate);
                dayText.setText(dayOfWeek.format(c.getTime()));
                ResetTime();
            }
        }
        if(v==rightArrow){
            c.add(Calendar.DATE, 1);
            formattedDate = df.format(c.getTime());
            formattedService=dateFormatService.format(c.getTime());
            Log.v("PREVIOUS DATE : ", formattedDate);
            dateText.setText(formattedDate);
            dayText.setText(dayOfWeek.format(c.getTime()));
            ResetTime();
        }
        if(v==timeLeft){
            if(selected_pos>0){
                selected_pos--;
                time.setText(timeArr[selected_pos]);
                String[]val1 = timeArr[selected_pos].split(" ");
                String[]val2 = val1[0].split(":");
                int hr = Integer.parseInt(val2[0]);
                int min = Integer.parseInt(val2[1]);
                if(hr ==12)
                    hr = 0;
                c.set(Calendar.HOUR,hr);
                c.set(Calendar.MINUTE,min);
                if(val1[1].equals("PM"))
                    c.set(Calendar.AM_PM,1);
                else
                    c.set(Calendar.AM_PM,0);
            }
        }
        if(v==timeRight){
            if(selected_pos<timeArr.length-2){
                selected_pos++;
                time.setText(timeArr[selected_pos]);
                String[]val1 = timeArr[selected_pos].split(" ");
                String[]val2 = val1[0].split(":");
                int hr = Integer.parseInt(val2[0]);
                int min = Integer.parseInt(val2[1]);
                if(hr == 12)
                    hr = 0;
                c.set(Calendar.HOUR,hr);
                c.set(Calendar.MINUTE,min);
                if(val1[1].equals("PM"))
                    c.set(Calendar.AM_PM,1);
                else
                    c.set(Calendar.AM_PM,0);
            }
        }

        if(v==minus){
            if(peopleCount>1){
                peopleCount=peopleCount-1;
                people.setText("0"+String.valueOf(peopleCount));
            }
        }
        if(v==plus){
            if(peopleCount>=0 && peopleCount<=8){
                peopleCount=peopleCount+1;
                people.setText("0"+String.valueOf(peopleCount));
            }
        }

        if(v==submit){
            //if(pref.getString("loginSession",null)!=null && pref.getString("loginSession", null).equals("Valid")) {
            submitAndMove();
            //webServiceCallForSubmitCartItems();
            //} else {
            //    Intent signin = new Intent(getActivity(),ResSigninActivity.class);
            //    RestaurantCheckin.this.startActivityForResult(signin,21);
            //}
        }
        if(v == date_layout){
            StartTime.show();
        }
    }

    public void submitAndMove(){
        editor.putString("RestDate", formattedDate);
        editor.commit();
        Date date = c.getTime();
        if(time.getText().toString().equals("Closed"))
            Toast.makeText(getActivity(),"Restaurant Closed on selected date. Choose another Date",Toast.LENGTH_SHORT).show();
        else {
            ((NavigationDrawerActivity)getActivity()).resReq.requestType = getArguments().getString("type");
            ((NavigationDrawerActivity)getActivity()).RestaurantBookingType = getArguments().getString("type");
            if (((NavigationDrawerActivity)getActivity()).resReq.flow.equals("Event")||((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.menuItemsCount>0) {
                ((NavigationDrawerActivity) getActivity()).resOutTime = new String[timeArr.length - selected_pos];
                for (int i = selected_pos; i < timeArr.length; i++) {
                    ((NavigationDrawerActivity) getActivity()).resOutTime[i - selected_pos] = timeArr[i];
                }
                Fragment fragment = new Restaurant_Menu_Items();
                Bundle args = new Bundle();
                args.putString("jsonResponse", ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.toString());
                fragment.setArguments(args);
                ((NavigationDrawerActivity) getActivity()).resReq.startDate = date.getTime();
                if (((NavigationDrawerActivity) getActivity()).resReq.requestType.equals("DineIn"))
                    ((NavigationDrawerActivity) getActivity()).resReq.peopleCount = peopleCount;
                //fragment.setArguments(args);
                if(changeCallbak==null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager
                            .beginTransaction();
                    fragmentTransaction.add(R.id.fragment_place, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                } else {
                    changeCallbak.requestTypeChanged();
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            } else {
                Fragment fragment;
                ((NavigationDrawerActivity) getActivity()).resReq.startDate = date.getTime();
                ((NavigationDrawerActivity) getActivity()).RestaurantBookingType = "DineIn";
                ((NavigationDrawerActivity) getActivity()).resReq.requestType = "DineIn";
                ((NavigationDrawerActivity) getActivity()).resReq.peopleCount = peopleCount;
                //fragment = new Payment_Method();
                //Bundle args = new Bundle();
                //args.putString("Type", "Restaurant");
                ((NavigationDrawerActivity) getActivity()).resReq.total = 0.0;
                ((NavigationDrawerActivity) getActivity()).resReq.tax = 0.0;
                ((NavigationDrawerActivity) getActivity()).resReq.subTotalval = 0;
                /*fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();*/
                Payment_Method.BookRestaurant("","", ((NavigationDrawerActivity) getActivity()), "", new Payment_Method.BookingCallBack() {
                    @Override
                    public void bookingSucess(String bookingId, String token, JSONObject response) {
                        Payment_Method.openRestaurantBookedScreen(response,getActivity());
                    }

                    @Override
                    public void bookingFailed(String failed) {

                    }
                });
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 21 && resultCode == Activity.RESULT_OK){
            submitAndMove();
        }
    }

    public void placeWebServiceCall(String SearchString) {
        try {
            SearchString = SearchString.toLowerCase().replace(" ", "%20");
            String search_type = "";

            String tag_json_obj = "json_obj_req";
            String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lati + "," + langi + "&types=" + "Restaurant" + "&radius=5000&name=" + SearchString + "&key="+getString(R.string.place_web_api_key);
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("URL", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pDialog.dismiss();
                                Log.d("Response", response.toString());
                                JSONObject jsonObject = response.getJSONArray("results").getJSONObject(0);
                                String placeID = jsonObject.getString("place_id");
                                placeDetailsCall(placeID);
                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error.toString());

                    Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                    //webServiceCall();

                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void placeDetailsCall(String placeId) {
        try {

            String tag_json_obj = "json_obj_req";

            String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeId + "&key="+getString(R.string.place_web_api_key);
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("URL", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pDialog.dismiss();
                                Log.d("Response", response.toString());
                                JSONObject jsonObject = response.getJSONObject("result");
                                JSONArray weekdays = jsonObject.getJSONObject("opening_hours").getJSONArray("weekday_text");
                                if (weekdays.length() > 0) {
                                    timings = new JSONArray();
                                    for (int i = 0; i < weekdays.length(); i++) {
                                        String value = weekdays.getString(i);
                                        String day = value.substring(0,value.indexOf(':'));
                                        value = value.substring(day.length()+1,value.length());
                                        String times[] = value.trim().split("–");
                                        String startTime = times[0].trim().toUpperCase();
                                        String endTime = times[1].trim().toUpperCase();
                                        JSONObject jsonObject1 = new JSONObject();
                                        jsonObject1.put("day",day);
                                        jsonObject1.put("checkInOne",startTime);
                                        jsonObject1.put("checkOutOne",endTime);
                                        timings.put(jsonObject1);
                                    }
                                    ResetTime();
                                } else {
                                }

                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error.toString());

                    Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                    //webServiceCall();

                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public interface RequestTypeChangeCallbak{
        void requestTypeChanged();
    }
}
