package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.adapter.FoodHistoryAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class FoodHistoryFragment extends BaseFragment {

    ListView list;
    ArrayList<JSONObject>hisItems;
    public static FoodHistoryFragment getInstance(String linkedOrderId){
        FoodHistoryFragment fragment = new FoodHistoryFragment();
        Bundle args = new Bundle();
        args.putString("linkedOrderId",linkedOrderId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_food_ordered_history,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = view.findViewById(R.id.list);
        webServiceCallForHistory();
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                JSONObject obj = (JSONObject) list.getAdapter().getItem(i);
                try {
                    android.support.v4.app.Fragment fragment = new RestaurantHistoryDetailFragment();
                    Bundle args = new Bundle();
                    args.putString("id", obj.getString("id"));
                    args.putString("json", obj.toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager
                            .beginTransaction();
                    fragmentTransaction.add(R.id.fragment_place, fragment, fragment.getClass().getName());
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void webServiceCallForHistory(){
        try{
            String tag_json_obj = "json_obj_req";
            String url = Constants.RESTAURENT_HISTORY;
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("searchText","");
            JSONObject pagination = new JSONObject();
            pagination.put("pageNo",1);
            pagination.put("limit",10);
            //jsonObject.put("pagination",pagination);
            JSONObject sort = new JSONObject();
            sort.put("fieldName","createOn");
            sort.put("sortType","DESC");
            jsonObject.put("sort",sort);
            jsonObject.put("linkedOrderId",getArguments().getString("linkedOrderId",""));
            url = url + PrefManager.getInstance(getContext()).getUserId();
            Log.d("Request", url + " " + jsonObject.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                Log.d("Response", response.toString());
                                if(response.getJSONObject("data")!=null) {
                                    JSONObject data = response.getJSONObject("data");
                                    if(data.getJSONArray("orders").length()>0) {
                                        list.setVisibility(View.VISIBLE);
                                        hisItems = getArrayList(data.getJSONArray("orders"));
                                        FoodHistoryAdapter adapter = new FoodHistoryAdapter(hisItems ,FoodHistoryFragment.this);
                                        list.setAdapter(adapter);
                                    } else {
                                    }

                                }else{
                                }
                                /*Wait_dine_adapter adapter=new Wait_dine_adapter(getActivity(),response.getJSONObject("data").getJSONArray("Dining"));
                                list.setAdapter(adapter);*/


                            }catch (Exception e){

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }catch (Exception e){
            System.out.println("Error :"+e.toString());
        }

    }

    public ArrayList<JSONObject> getArrayList(JSONArray jsonArray){
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        if(jsonArray!=null){
            for (int i = 0;i<jsonArray.length();i++)
                try {
                    arrayList.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
        return arrayList;
    }
}
