package com.pinslot.place.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.hbb20.CountryCodePicker;

import org.json.JSONObject;

public class LoginFragment extends BaseFragment implements View.OnClickListener{

    LinearLayout login_layout, signup_layout;
    TextView signin,signup,guest,forgotPassword;
    EditText email,password,r_email,r_password,r_re_password,r_phone,f_name,l_name;
    Button signin_btn,signup_btn;
    ImageView select1,select2;
    CountryCodePicker cc_picker_sign_up;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        login_layout = view.findViewById(R.id.login_layout);
        signup_layout = view.findViewById(R.id.signup_layout);
        signin = view.findViewById(R.id.signin);
        signup = view.findViewById(R.id.signup);
        guest = view.findViewById(R.id.guest);
        signin_btn = view.findViewById(R.id.signin_btn);
        signup_btn = view.findViewById(R.id.signup_btn);
        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        f_name = view.findViewById(R.id.f_name);
        l_name = view.findViewById(R.id.l_name);
        r_phone = view.findViewById(R.id.r_phone);
        r_email = view.findViewById(R.id.r_email);
        r_password = view.findViewById(R.id.r_password);
        r_re_password = view.findViewById(R.id.r_re_password);
        forgotPassword = view.findViewById(R.id.forgot_password);
        select1 = view.findViewById(R.id.select_1);
        select2 = view.findViewById(R.id.select_2);
        cc_picker_sign_up = view.findViewById(R.id.cc_picker_sign_up);



        signin.setOnClickListener(this);
        signup.setOnClickListener(this);
        guest.setOnClickListener(this);
        signup_btn.setOnClickListener(this);
        signin_btn.setOnClickListener(this);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ForgotPassword();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }
        });


        if(Utils.regvalue==1){
            signin.setBackgroundResource(R.color.color_accent_disable_dark);
            signup.setBackgroundResource(R.color.colorAccent);
            login_layout.setVisibility(View.GONE);
            signup_layout.setVisibility(View.VISIBLE);
            select2.setVisibility(View.VISIBLE);
            select1.setVisibility(View.GONE);
            Utils.regvalue=0;
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signin : {
                signin.setBackgroundResource(R.color.colorAccent);
                signup.setBackgroundResource(R.color.color_accent_disable_dark);
                login_layout.setVisibility(View.VISIBLE);
                signup_layout.setVisibility(View.GONE);
                select1.setVisibility(View.VISIBLE);
                select2.setVisibility(View.GONE);
            }
            break;
            case R.id.signup : {
                signin.setBackgroundResource(R.color.color_accent_disable_dark);
                signup.setBackgroundResource(R.color.colorAccent);
                login_layout.setVisibility(View.GONE);
                signup_layout.setVisibility(View.VISIBLE);
                select2.setVisibility(View.VISIBLE);
                select1.setVisibility(View.GONE);
            }
            break;
            case R.id.guest: getActivity().getSupportFragmentManager().popBackStack();
                ((NavigationDrawerActivity)getActivity()).showMain();
            break;
            case R.id.signup_btn : {
                if(f_name.getText().toString().isEmpty()||l_name.getText().toString().isEmpty()||r_email.getText().toString().isEmpty()||r_phone.getText().toString().isEmpty()||r_password.getText().toString().isEmpty()||r_re_password.getText().toString().isEmpty()){
                    Toast.makeText(getContext(),"All fields Are mandatory",Toast.LENGTH_SHORT).show();
                } else if(!Utils.validateEmail(r_email.getText().toString())){
                    Toast.makeText(getContext(), "Enter valid email",Toast.LENGTH_SHORT).show();
                } else if(!r_password.getText().toString().equals(r_re_password.getText().toString()))
                    Toast.makeText(getContext(), "Password and Confirm Password Not matching",Toast.LENGTH_SHORT).show();
                else
                    webServiceCallForSignUp();
            }
            case R.id.signin_btn : {
                if(email.getText().toString().isEmpty())
                    Toast.makeText(getContext(),"Enter Email address",Toast.LENGTH_SHORT).show();
                else if(password.getText().toString().isEmpty())
                    Toast.makeText(getContext(),"Enter Password",Toast.LENGTH_SHORT).show();
                else if(!Utils.validateEmail(email.getText().toString()))
                    Toast.makeText(getContext(),"Enter Valid Email",Toast.LENGTH_SHORT).show();
                else
                    webServiceCallSignin();
            }
            break;

        }
    }

    public void webServiceCallSignin(){

        try {
            String url = Constants.LOGIN;
            String tag_json_obj = "json_obj_req";
            final ProgressDialog pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userName", email.getText().toString());
            jsonObject.put("password", password.getText().toString());
            Log.d("Request", jsonObject.toString());
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("Response", response.toString());
                                pDialog.dismiss();

                                if (response.getString("status").equals("SUCCESS")) {
                                    SharedPreferences.Editor editor = PrefManager.getInstance(getContext().getApplicationContext()).getPref().edit();
                                    editor.putBoolean("isAdmin", false);
                                    editor.putString("loginSession", "Valid");
                                    editor.putString("userId", response.getJSONObject("data").getJSONObject("customerProfile").getString("profileId"));
                                    String fname = "";
                                    String lname = "";
                                    String email = "";
                                    String mobile = "";
                                    String image = "";
                                    String city = "";
                                    String cc = "";
                                    String firstname = response.getJSONObject("data").getJSONObject("customerProfile").getJSONObject("contactDetails").getString("firstName");
                                    if (!firstname.equals("null")) {
                                        editor.putString("firstName", firstname);
                                        fname = firstname;
                                    }
                                                    /*String middlename = response.getJSONObject("data").getJSONObject("customerProfile").getJSONObject("contactDetails").getString("middleName");
                                                    if (!middlename.equals("null")) {
                                                        if (!name.equals(""))
                                                            name = name + " ";
                                                        name = name + middlename;
                                                    }*/
                                    String lastName = response.getJSONObject("data").getJSONObject("customerProfile").getJSONObject("contactDetails").has("lastName")?response.getJSONObject("data").getJSONObject("customerProfile").getJSONObject("contactDetails").getString("lastName"):"";
                                    if (!lastName.equals("null")) {
                                        editor.putString("lastName", lastName);
                                        if (!lname.equals(""))
                                            lname =  lastName;
                                    }
                                    JSONObject customerprofileOject = response.getJSONObject("data").getJSONObject("customerProfile");
                                    if(customerprofileOject.has("facebookUrl"))
                                        PrefManager.getInstance(getContext().getApplicationContext()).setUserFb(customerprofileOject.getString("facebookUrl"));
                                    if(customerprofileOject.has("twitterUrl"))
                                        PrefManager.getInstance(getContext().getApplicationContext()).setUserTw(customerprofileOject.getString("twitterUrl"));
                                    if(customerprofileOject.has("instagramUrl"))
                                        PrefManager.getInstance(getContext().getApplicationContext()).setUserIn(customerprofileOject.getString("instagramUrl"));
                                    if(customerprofileOject.has("linkedinUrl"))
                                        PrefManager.getInstance(getContext().getApplicationContext()).setUserLi(customerprofileOject.getString("linkedinUrl"));
                                    email = customerprofileOject.getJSONObject("contactDetails").has("emailAddress") ? customerprofileOject.getJSONObject("contactDetails").getString("emailAddress") : "";
                                    mobile = customerprofileOject.getJSONObject("contactDetails").has("mobilePhone") ? customerprofileOject.getJSONObject("contactDetails").getString("mobilePhone") : "";
                                    cc = customerprofileOject.getJSONObject("contactDetails").has("mobilePhoneCountryCode")?customerprofileOject.getJSONObject("contactDetails").getString("mobilePhoneCountryCode"):"";
                                    image = customerprofileOject.has("photoLocation") ? customerprofileOject.getString("photoLocation") : "";
                                    //city = customerprofileOject.getJSONObject("location").getString("city");
                                    if(customerprofileOject.has("location")&& customerprofileOject.getJSONObject("location").has("city"))
                                        city = customerprofileOject.getJSONObject("location").getString("city");

                                    if (image.startsWith("http")) {
                                        editor.putString("image", image);
                                    } else {
                                        editor.putString("image", Constants.URL + "resources" + image);
                                    }

                                    editor.putString("fname", fname);
                                    editor.putString("lname", lname);
                                    editor.putString("email", email);
                                    editor.putString("mobile", mobile);
                                    editor.putString("city", city);
                                    //editor.putString("image", image);
                                    editor.putString("mobilePhoneCountryCode",cc);
                                    if(customerprofileOject.has("referralCode"))
                                        editor.putString("referralCode",customerprofileOject.getString("referralCode"));
                                    editor.commit();
                                    if(customerprofileOject.has("birthDate")&&!customerprofileOject.getString("birthDate").isEmpty()){
                                        PrefManager.getInstance(getActivity().getApplicationContext()).setBirthDate(customerprofileOject.getInt("birthDate"));
                                    }
                                    if(customerprofileOject.has("birthMonth"))
                                        PrefManager.getInstance(getActivity().getApplicationContext()).setBirthMonth(customerprofileOject.getString("birthMonth"));
                                    Toast.makeText(getContext(), "Login Successfully", Toast.LENGTH_LONG).show();

                                    ((NavigationDrawerActivity)getActivity()).refresh();
                                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                                    ((NavigationDrawerActivity)getActivity()).showMain();

                                } else {
                                    Toast.makeText(getContext(), "Invalid user Name OR Password", Toast.LENGTH_LONG).show();
                                }

                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    pDialog.dismiss();
                }
            });

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void webServiceCallForSignUp(){
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.SIGNUP;

            JSONObject contactData = new JSONObject();
            contactData.put("firstName", f_name.getText().toString());
            contactData.put("lastName", l_name.getText().toString());
            contactData.put("emailAddress", r_email.getText().toString());
            contactData.put("mobilePhone", r_phone.getText().toString());
            contactData.put("mobilePhoneCountryCode", cc_picker_sign_up.getSelectedCountryCode());
            //Login Data

            JSONObject loginData = new JSONObject();
            loginData.put("password", r_password.getText().toString());
            loginData.put("con_password", r_re_password.getText().toString());
            loginData.put("userName", r_email.getText().toString());

            JSONObject customerProfile = new JSONObject();
            customerProfile.put("contactDetails", contactData);
            customerProfile.put("login", loginData);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("customerProfile", customerProfile);
            Signup(jsonObject);

        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void Signup(JSONObject signupObject){
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.SIGNUP;
            final ProgressDialog pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, signupObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("Response", response.toString());
                                pDialog.dismiss();

                                if (response.getString("status").equals("SUCCESS")) {
                                    if (!response.getString("message").equalsIgnoreCase("EMAIL_EXIST"))
                                    {
                                        SharedPreferences.Editor editor = PrefManager.getInstance(getContext()).getPref().edit();
                                    editor.putBoolean("isAdmin", false);
                                    editor.putString("loginSession", "Valid");
                                    editor.putString("userId", response.getJSONObject("data").getJSONObject("customerProfile").getString("profileId"));
                                    String fname = "";
                                    String lname = "";
                                    String email = "";
                                    String mobile = "";
                                    String image = "";
                                    String city = "";
                                    String firstname = response.getJSONObject("data").getJSONObject("customerProfile").getJSONObject("contactDetails").getString("firstName");
                                    if (!firstname.equals("null")) {
                                        editor.putString("firstName", firstname);
                                        fname = firstname;
                                    }
                                                    /*String middlename = response.getJSONObject("data").getJSONObject("customerProfile").getJSONObject("contactDetails").getString("middleName");
                                                    if (!middlename.equals("null")) {
                                                        if (!name.equals(""))
                                                            name = name + " ";
                                                        name = name + middlename;
                                                    }*/
                                    String lastName = response.getJSONObject("data").getJSONObject("customerProfile").getJSONObject("contactDetails").getString("lastName");
                                    if (!lastName.equals("null")) {
                                        editor.putString("lastName", lastName);
                                        if (!lname.equals(""))
                                        lname =  lastName;
                                    }
                                    JSONObject customerprofileOject = response.getJSONObject("data").getJSONObject("customerProfile");
                                    email = customerprofileOject.getJSONObject("contactDetails").has("emailAddress") ? customerprofileOject.getJSONObject("contactDetails").getString("emailAddress") : "";
                                    mobile = customerprofileOject.getJSONObject("contactDetails").has("mobilePhone") ? customerprofileOject.getJSONObject("contactDetails").getString("mobilePhone") : "";
                                    image = customerprofileOject.has("photoLocation") ? customerprofileOject.getString("photoLocation") : "";
                                    if (customerprofileOject.has("location") && customerprofileOject.getJSONObject("location").has("city"))
                                        city = customerprofileOject.getJSONObject("location").getString("city");

                                    editor.putString("fname", fname);
                                    editor.putString("lname", lname);
                                    editor.putString("email", email);
                                    editor.putString("mobile", mobile);
                                    editor.putString("city", city);
                                    editor.putString("image", image);
                                    editor.commit();
                                    Toast.makeText(getContext(), "Successfully Signup", Toast.LENGTH_LONG).show();
                                    ((NavigationDrawerActivity) getActivity()).refresh();
                                    getActivity().getSupportFragmentManager().popBackStack();
                                    ((NavigationDrawerActivity) getActivity()).showMain();

                                }else {
                                        Toast.makeText(getContext(), "Email Already Exists", Toast.LENGTH_LONG).show();

                                    }
                                } else {
                                    Toast.makeText(getContext(), "User Registration Failed", Toast.LENGTH_LONG).show();
                                }

                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    ((NavigationDrawerActivity)getActivity()).showMain();
                    pDialog.dismiss();
                }
            });
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }
}
