package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.CircleImageView;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.adapter.FoodGridAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FoodsFragment extends BaseFragment implements FoodServiceFragment.ListDataChangeListener {

    GridView menu_grid;
    int food_count = 1;

    public static FoodsFragment instance(String key){
        FoodsFragment fragment = new FoodsFragment();
        Bundle args = new Bundle();
        args.putString("key",key);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_foods,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menu_grid = view.findViewById(R.id.menu_grid);
        if(getParentFragment() instanceof FoodServiceFragment){
            ArrayList<JSONObject>objects = ((FoodServiceFragment)getParentFragment()).listDataChild.get(getArguments().getString("key"));
            menu_grid.setAdapter(new FoodGridAdapter(objects,getActivity()));
            ((FoodServiceFragment)getParentFragment()).addListener(this);
        }

        menu_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final JSONObject item = (JSONObject) menu_grid.getAdapter().getItem(i);

                try {
                    String tag_json_obj = "json_obj_req";
                    String url = Constants.SEARCH_REST_MENU + item.getString("menuItemId");
                    Log.d("Request :", url);
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                            url, "",
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("Response", response.toString());
                                    //jsonResponse=response.toString();
                                    try {
                                        if (response.getString("status").equals("SUCCESS")) {
                                            //JSONObject data = new JSONObject(response.toString());
                                            JSONObject top = null;
                                            if (response.isNull("data")) {
                                            } else {
                                                top = response.getJSONObject("data");
                                            }
                                            Food_Details(getActivity(), top, ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.id, getParentFragment(), ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.currency, item);

                                        }
                                    } catch (Exception e) {
                                        Log.d("Error", e.toString());
                                    }
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });

// Adding request to request queue
                    AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                } catch (Exception e) {
                    System.out.println("Error :" + e.toString());
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getParentFragment() instanceof FoodServiceFragment){
            ((FoodServiceFragment)getParentFragment()).removeListener(this);
        }
    }

    public void Food_Details(final Context act, JSONObject jsonStr, final String resId, final Fragment fragment, String currency, final JSONObject item) {
        //Log.d("JSON String", jsonStr.toString());
        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.food_details);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);

        Button add = (Button) dialog.findViewById(R.id.add);
        Button less = (Button) dialog.findViewById(R.id.less);
        final Button count = (Button) dialog.findViewById(R.id.count);
        final Button add_cart = (Button) dialog.findViewById(R.id.add_cart_btn);
        LinearLayout topping = (LinearLayout) dialog.findViewById(R.id.topping_view);
        TextView smallPrice = (TextView) dialog.findViewById(R.id.small_price);
        TextView mediumPrice = (TextView) dialog.findViewById(R.id.medium_price);
        TextView largePrice = (TextView) dialog.findViewById(R.id.large_price);
        TextView itemName = (TextView) dialog.findViewById(R.id.item_name);
        final EditText instruction = (EditText) dialog.findViewById(R.id.instruction);
        final TextView priceTxt = (TextView) dialog.findViewById(R.id.price);
        LinearLayout smallLinear = (LinearLayout) dialog.findViewById(R.id.small_linear);
        LinearLayout mediumLinear = (LinearLayout) dialog.findViewById(R.id.medium_linear);
        LinearLayout largeLinear = (LinearLayout) dialog.findViewById(R.id.large_linear);
        final ImageView smallCheck = (ImageView) dialog.findViewById(R.id.small_check);
        final ImageView mediumCheck = (ImageView) dialog.findViewById(R.id.medium_check);
        final ImageView largeCheck = (ImageView) dialog.findViewById(R.id.large_check);
        final CircleImageView dishImage = (CircleImageView) dialog.findViewById(R.id.imageView7);
        final TextView or = dialog.findViewById(R.id.or);
        final Button dinein = dialog.findViewById(R.id.dine_in);
        final Button takeAway = dialog.findViewById(R.id.take_away);
        final LinearLayout buttons = dialog.findViewById(R.id.buttons);
        LinearLayout avail_layout = dialog.findViewById(R.id.avail_layout);
        TextView avail_time = dialog.findViewById(R.id.avail_time);
        LinearLayout sizes_layout = dialog.findViewById(R.id.sizes_layout);
        LinearLayout toppings_layout = dialog.findViewById(R.id.toppings_layout);
        final HashMap<String,String> selTopping = new HashMap<>();
        String availability = "";
        //if(((NavigationDrawerActivity)getActivity()).resReq.requestType.equals("Delivery")){
        //    add_cart.setText("Add To Cart For Delivery");
        //} else {
            add_cart.setText("Add To Cart");
            or.setVisibility(View.GONE);
            buttons.setVisibility(View.GONE);
        //}
        if(item.has("availableSlot1")){
            try {
                availability = item.getJSONObject("availableSlot1").getString("startTime")+" to "+item.getJSONObject("availableSlot1").getString("endTime");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(item.has("availableSlot2")){
            if(!availability.isEmpty())
                availability = availability + "\n";
            try {
                availability = availability+item.getJSONObject("availableSlot2").getString("startTime")+" to "+item.getJSONObject("availableSlot2").getString("endTime");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(!availability.isEmpty()){
            avail_layout.setVisibility(View.VISIBLE);
            avail_time.setText(availability);
        }

        final Double[] priceval = new Double[1];
        try {
            if (item.getString("imageUrl").length() > 0) {
                Picasso.with(act)
                        .load(item.getString("imageUrl"))
                        .into(dishImage);
            }

            //final String[]menuId = new String[1];
            priceval[0] = Double.parseDouble(item.getString("price"));

            itemName.setText(item.getString("menuItemName"));
            priceTxt.setText(item.getString("price"));
        }catch (Exception e){

        }
        final HashMap<String, JSONObject> MenuChos = new HashMap<>();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                food_count = food_count + 1;
                count.setText(String.valueOf(food_count));

                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (food_count > 0) {
                    food_count = food_count - 1;
                    count.setText(String.valueOf(food_count));

                    //price=price-Integer.parseInt(priceTxt.getText().toString());
                    priceTxt.setText(Double.toString(food_count * priceval[0]));
                }

            }
        });

        smallLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smallCheck.setVisibility(View.VISIBLE);
                mediumCheck.setVisibility(View.GONE);
                largeCheck.setVisibility(View.GONE);
                try {
                    priceval[0] = MenuChos.get("Small").getDouble("menuchosPrice");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (String key : selTopping.keySet()){
                    priceval[0] = priceval[0] + Double.parseDouble(selTopping.get(key));
                }
                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        mediumLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smallCheck.setVisibility(View.GONE);
                mediumCheck.setVisibility(View.VISIBLE);
                largeCheck.setVisibility(View.GONE);
                try {
                    priceval[0] = MenuChos.get("Medium").getDouble("menuchosPrice");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (String key : selTopping.keySet()){
                    priceval[0] = priceval[0] + Double.parseDouble(selTopping.get(key));
                }
                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        largeLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                smallCheck.setVisibility(View.GONE);
                mediumCheck.setVisibility(View.GONE);
                largeCheck.setVisibility(View.VISIBLE);
                try {
                    priceval[0] = MenuChos.get("Large").getDouble("menuchosPrice");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (String key : selTopping.keySet()){
                    priceval[0] = priceval[0] + Double.parseDouble(selTopping.get(key));
                }
                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        dinein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerActivity)getActivity()).showDineIn(new RestaurantCheckin.RequestTypeChangeCallbak() {
                    @Override
                    public void requestTypeChanged() {
                        //RenderingList("");
                    }
                });
                dialog.dismiss();
            }
        });

        takeAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerActivity)getActivity()).showTakeAway(new RestaurantCheckin.RequestTypeChangeCallbak() {
                    @Override
                    public void requestTypeChanged() {
                        //RenderingList("");
                    }
                });
                dialog.dismiss();
            }
        });

        add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String availability = "";
                /*try {
                    availability = item.getString("availability");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }*/
                if(availability.equals("not_available")){
                    AlertDialog.Builder alert = new AlertDialog.Builder(act);
                    alert.setTitle("Not Available");
                    if(((NavigationDrawerActivity)getActivity()).resReq.requestType.equals("Delivery"))
                        alert.setMessage("This menu Item not Available Now so we are unable to deliver this item now. Choose DineIn or TakeAway with Available");
                    else
                        alert.setMessage("This menu Item not Available in your "+((NavigationDrawerActivity)act).resReq.requestType+" timing. adjest your timing for add this item");
                    alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.show();
                }else if (!PrefManager.getInstance(getContext()).getUserId().isEmpty()) {
                    try {
                        String tag_json_obj = "json_obj_req";
                        String url = Constants.URL + "customer/" + PrefManager.getInstance(getContext()).getUserId() + "/business/" + Constants.res_id + "/cart";

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("menuItemId", item.getString("menuItemId"));
                        jsonObject.put("qty", food_count);

                        // toppings
                        JSONArray jsonArray = new JSONArray();
                        for (String key : selTopping.keySet()){
                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("restMenuName", key);
                            jsonObject1.put("restMenuPrice", selTopping.get(key));
                            jsonArray.put(jsonObject1);
                        }
                            /*JSONObject jsonObject1 = new JSONObject();

							for (int i = 0; i < 1; i++) {
								jsonObject1.put("restMenuName", "Pepparoni");
								jsonObject1.put("restMenuPrice", "13");
							}
							jsonArray.put(jsonObject1);*/
                        jsonObject.put("restMenuToppings", jsonArray);
                        jsonObject.put("splCookIns", instruction.getText().toString());

                        // Size
                        if (smallCheck.getVisibility() == View.VISIBLE) {
                            jsonObject.put("restaurantMenuChos", MenuChos.get("Small"));
                        } else if (mediumCheck.getVisibility() == View.VISIBLE) {
                            jsonObject.put("restaurantMenuChos", MenuChos.get("Medium"));
                        } else if (largeCheck.getVisibility() == View.VISIBLE) {
                            jsonObject.put("restaurantMenuChos", MenuChos.get("Large"));
                        }

                        Log.e("Add to Cart Request", url + " " + jsonObject.toString());

                        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                                url, jsonObject,
                                new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {

                                        Log.e("Add to Cart Response", response.toString());
                                        try {
                                            if (response.has("status") && response.getString("status").equals("SUCCESS")) {
                                                if (fragment instanceof FoodServiceFragment) {
                                                    ((FoodServiceFragment) fragment).updateCartValueFromServer();
                                                }
                                            } else {
                                                AlertDialog.Builder alert = new AlertDialog.Builder(act);
                                                alert.setTitle("Error");
                                                if (response.has("message")) {
                                                    alert.setMessage(response.getString("message"));
                                                } else {
                                                    alert.setMessage("Failed to add record");
                                                }
                                                alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });
                                                alert.show();
                                            }
                                        } catch (JSONException e){

                                        }
                                        dialog.dismiss();
                                    }
                                }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });

                        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                    } catch (Exception e) {
                        System.out.println("Error :" + e.toString());
                    }
                } else {
                    dialog.dismiss();
                    addFragment(new LoginFragment(),false);
                }

            }
        });


        try {
            if (jsonStr != null) {
                JSONObject jsonObject = jsonStr;
                JSONArray jsonArray = jsonObject.getJSONArray("restMenuToppings");
                for (int i = 0; i < jsonArray.length(); i++) {
                    toppings_layout.setVisibility(View.VISIBLE);
                    final JSONObject items = jsonArray.getJSONObject(i);
                    View layout2 = LayoutInflater.from(act).inflate(R.layout.topping_layout, topping, false);
                    CheckBox check = (CheckBox) layout2.findViewById(R.id.check_box);
                    TextView priceCheck = (TextView) layout2.findViewById(R.id.price);
                    check.setText(items.getString("restMenuName"));
                    priceCheck.setText(currency + " " + items.getString("restMenuPrice"));
                    check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            try {
                                if (isChecked) {
                                    selTopping.put(items.getString("restMenuName"), items.getString("restMenuPrice"));
                                    priceval[0] = priceval[0] + items.getDouble("restMenuPrice");
                                } else {
                                    selTopping.remove(items.getString("restMenuName"));
                                    priceval[0] = priceval[0] - items.getDouble("restMenuPrice");
                                }
                                priceTxt.setText(Double.toString(food_count * priceval[0]));
                            } catch (JSONException e){

                            }
                        }
                    });
                    topping.addView(layout2);
                }

                JSONArray jsonArrayMenu = jsonObject.getJSONArray("restaurantMenuChos");
                for (int i = 0; i < jsonArrayMenu.length(); i++) {
                    sizes_layout.setVisibility(View.VISIBLE);
                    JSONObject items = jsonArrayMenu.getJSONObject(i);
                    if (items.getString("menuchosSize").equals("Small") && !items.getString("menuchosPrice").equals("null")) {
                        smallLinear.setVisibility(View.VISIBLE);
                        smallPrice.setText(currency + " " + items.getString("menuchosPrice"));
                        MenuChos.put("Small", items);
                    }
                    if (items.getString("menuchosSize").equals("Medium") && !items.getString("menuchosPrice").equals("null")) {
                        mediumLinear.setVisibility(View.VISIBLE);
                        mediumPrice.setText(currency + " " + items.getString("menuchosPrice"));
                        MenuChos.put("Medium", items);
                    }
                    if (items.getString("menuchosSize").equals("Large") && !items.getString("menuchosPrice").equals("null")) {
                        largeLinear.setVisibility(View.VISIBLE);
                        largePrice.setText(currency + " " + items.getString("menuchosPrice"));
                        MenuChos.put("Large", items);
                    }
                }

                if (smallLinear.getVisibility() == View.VISIBLE) {
                    priceval[0] = MenuChos.get("Small").getDouble("menuchosPrice");
                    smallCheck.setVisibility(View.VISIBLE);
                } else if (mediumLinear.getVisibility() == View.VISIBLE) {
                    priceval[0] = MenuChos.get("Medium").getDouble("menuchosPrice");
                    mediumCheck.setVisibility(View.VISIBLE);
                } else if (largeLinear.getVisibility() == View.VISIBLE) {
                    priceval[0] = MenuChos.get("Large").getDouble("menuchosPrice");
                    largeCheck.setVisibility(View.VISIBLE);
                }
                priceTxt.setText(Double.toString(priceval[0]));
            }

        } catch (Exception e) {

        }


        dialog.show();

    }

    @Override
    public void listDataChanged() {
        if(getParentFragment() instanceof FoodServiceFragment){
            ArrayList<JSONObject>objects = ((FoodServiceFragment)getParentFragment()).listDataChild.get(getArguments().getString("key"));
            menu_grid.setAdapter(new FoodGridAdapter(objects,getActivity()));
        }
    }
}
