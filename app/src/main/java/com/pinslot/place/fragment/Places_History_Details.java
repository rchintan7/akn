package com.pinslot.place.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.model.FoodPkg;
import com.pinslot.place.model.PlaceBookedDetail;
import com.pinslot.place.widget.CornerImageView;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Places_History_Details extends Fragment implements View.OnClickListener {


    String JSONvalue, Lattitude, Langitude;
    TextView name, address, confirmation, roomName, dates, more, status, cancelText, statusHead;
    TextView label,contact_user_txt,exp_contact_user_txt,exp_cancel_txt;
    Button photosBtn, mapBtn, review;
    JSONObject jsonObject;
    ArrayList<String> imageUrl = new ArrayList<>();
    ImageView placeImages, collapse, camera, video, panorama, tele, stream;
    RelativeLayout topRelative;
    View view;
    LinearLayout share, userLocation, cal, cancel, summary, facility, food_package, uloc, accept, contact_user, user_options, exp_accept, exp_cancel, exp_contact;
    CardView user_layout;
    ArrayList<Date> checkIn = new ArrayList<>();
    ArrayList<Date> checkOut = new ArrayList<>();
    String orderId, businssId;
    public JSONObject shareImageRestaurant = null;
    public String CaptureImagepath = "";
    Fragment fragment;
    ImageView map_icon;
    PlaceBookedDetail bookedDetail = null;
    JSONObject cancelClickObject;
    JSONArray amenites;
    CornerImageView image;
    TextView uname,email,phone;
    RatingBar rating;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.places_history_details_screen, container, false);
        JSONvalue = getArguments().getString("jsonResponse");
        fragment = Places_History_Details.this;
        name = (TextView) view.findViewById(R.id.restaurant_name);
        cancelText = (TextView) view.findViewById(R.id.cancel_text);
        confirmation = (TextView) view.findViewById(R.id.confirmation_number);
        status = (TextView) view.findViewById(R.id.status);
        address = (TextView) view.findViewById(R.id.address);
        placeImages = (ImageView) view.findViewById(R.id.place_images);
        video = (ImageView) view.findViewById(R.id.video);
        panorama = (ImageView) view.findViewById(R.id.panorama);
        tele = (ImageView) view.findViewById(R.id.telecast);
        stream = (ImageView) view.findViewById(R.id.stream);
        userLocation = view.findViewById(R.id.user_location);
        contact_user_txt = view.findViewById(R.id.contact_user_txt);
        exp_contact_user_txt = view.findViewById(R.id.exp_contact_user_txt);
        exp_cancel_txt = view.findViewById(R.id.exp_cancel_text);
        label = view.findViewById(R.id.label);

        photosBtn = (Button) view.findViewById(R.id.photos_btn);
        review = (Button) view.findViewById(R.id.review);
        mapBtn = (Button) view.findViewById(R.id.map_btn);

        collapse = (ImageView) view.findViewById(R.id.collapse);
        camera = (ImageView) view.findViewById(R.id.camera);
        topRelative = (RelativeLayout) view.findViewById(R.id.top_relative);
        share = (LinearLayout) view.findViewById(R.id.share_layout);
        cal = (LinearLayout) view.findViewById(R.id.cal_layout);
        cancel = (LinearLayout) view.findViewById(R.id.cancel_layout);
        summary = (LinearLayout) view.findViewById(R.id.summary_layout);
        facility = (LinearLayout) view.findViewById(R.id.facility_layout);
        food_package = (LinearLayout) view.findViewById(R.id.food_pkg_layout);
        uloc = view.findViewById(R.id.user_location);
        accept = view.findViewById(R.id.accept_layout);
        contact_user = view.findViewById(R.id.contact_user);
        map_icon = view.findViewById(R.id.map_icon);
        roomName = view.findViewById(R.id.room_name);
        dates = view.findViewById(R.id.dates);
        more = view.findViewById(R.id.more);
        user_layout = view.findViewById(R.id.user_layout);
        image = view.findViewById(R.id.image);
        uname = view.findViewById(R.id.name);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        user_options = view.findViewById(R.id.user_options);
        rating = view.findViewById(R.id.rating);
        exp_accept = view.findViewById(R.id.exp_accept_layout);
        exp_cancel = view.findViewById(R.id.exp_cancel_layout);
        exp_contact = view.findViewById(R.id.exp_contact_user);
        statusHead = (TextView) view.findViewById(R.id.statushead);

        JSONParsing(JSONvalue);
        photosBtn.setOnClickListener(this);
        mapBtn.setOnClickListener(this);
        collapse.setOnClickListener(this);
        share.setOnClickListener(this);
        cal.setOnClickListener(this);
        cancel.setOnClickListener(this);
        camera.setOnClickListener(this);
        contact_user.setOnClickListener(this);
        exp_contact.setOnClickListener(this);
        summary.setOnClickListener(this);
        facility.setOnClickListener(this);
        food_package.setOnClickListener(this);
        review.setOnClickListener(this);
        video.setOnClickListener(this);
        panorama.setOnClickListener(this);
        tele.setOnClickListener(this);
        stream.setOnClickListener(this);
        map_icon.setOnClickListener(this);
        more.setOnClickListener(this);
        accept.setOnClickListener(this);
        uloc.setOnClickListener(this);
        exp_accept.setOnClickListener(this);
        exp_cancel.setOnClickListener(this);
        return view;
    }

    public void onClick(View v) {
        if (v == photosBtn) {
            /*----Fragment fragment = PhotosFragment.newInstance(imageUrl);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();*/
        }
        if (v == map_icon) {
            /*---Intent location = new Intent(fragment.getActivity(), LocationActivity.class);
            location.putExtra("name", name.getText().toString());
            location.putExtra("lat", Double.parseDouble(Lattitude));
            location.putExtra("lon", Double.parseDouble(Langitude));
            location.putExtra("address", address.getText().toString());
            location.putExtra("name", "");
            fragment.startActivity(location);*/
        }
        if (v == tele) {
            //Constants.wowzaId = businssId;
            //Log.e("Wowza Id", Constants.wowzaId);

        }
        if (v == stream) {
            /*try {
                CSPlace place = new CSPlace(bookedDetail.place);

                String resManagerId = "";
                String phone = "";
                Bundle args = new Bundle();
                if (place.managerProfile!=null) {
                    resManagerId = place.managerProfile.profileId;
                    phone = place.managerProfile.contactDetails.mobilePhone;
                    args.putString("obj", Utils.getChatObj(name.getText().toString(), place.managerProfile.srcJson).toString());
                }
                Fragment fragment = new Communication();
                args.putString("orderId", place.id);
                args.putString("type", "PlaceDetails");
                args.putString("opponentID", resManagerId);
                args.putString("topicId", "PlaceDetails");
                args.putString("chatId", bookedDetail.chatId);
                args.putBoolean("disable_video",true);
                args.putBoolean("disable_stream",true);
                args.putString("phone", phone);
                fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }*/

        }

        if (v == video) {
        }
        if (v == panorama) {
            //Intent video = new Intent(getActivity(), PanoramaActivity.class);
            //startActivity(video);

        }
        if (v == collapse) {

            placeImages.setScaleType(ImageView.ScaleType.CENTER_CROP);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    500);
            // Setting the parameters on the TextView
            topRelative.setLayoutParams(lp);
            collapse.setVisibility(View.GONE);
        }

        if (v == share) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/html");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://www.pinslots.com");
            startActivity(Intent.createChooser(sharingIntent, "Share using"));
        }
        if (v == cal) {
            try {
                Calendar cal = Calendar.getInstance();
                if(checkIn.size()>0)
                    cal.setTime(checkIn.get(0));
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, cal.getTimeInMillis());
                intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, cal.getTimeInMillis() + 60 * 60 * 1000);
                intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, 1);
                intent.putExtra(CalendarContract.Events.DTSTART, cal.getTimeInMillis());
                intent.putExtra(CalendarContract.Events.DTEND, cal.getTimeInMillis());
                intent.putExtra(CalendarContract.Events.TITLE, name.getText().toString());
                intent.putExtra(CalendarContract.Events.DESCRIPTION, "Welcome All");
                intent.putExtra(CalendarContract.Events.EVENT_LOCATION, address.getText().toString());
                intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
                startActivity(intent);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
        }
        if (v == cancel) {
            String message, posButton;
            if (getArguments().containsKey("screentype")) {
                message = "Are you sure want to reject this order?";
                posButton = "Reject";
            } else {
                message = "Are you sure want to cancel this order?";
                posButton = "Cancel";
            }
            if(getArguments()!=null&&getArguments().containsKey("screentype")&&(getArguments().getString("screentype").equals("manager")||getArguments().getString("screentype").equals("exp_manager"))){
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Confirmation");
                alert.setMessage(message);
                alert.setPositiveButton(posButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Date date = new Date(bookedDetail.bookedDates.get(0).checkIn);
                        if (date.before(new Date())) {
                            Log.d("Past Date", "True");
                            Utils.ShowAlert("Error", "The time has exceeded",getActivity());
                        } else {
                            if (bookedDetail != null)
                                webservicecallforResponse("REJECTED", bookedDetail.id);
                        }

                    }
                });
                alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alert.show();
            } else {
                try {
                    if (bookedDetail != null) {
                        Date date = new Date(cancelClickObject.getLong("checkIn"));
                        if (date.before(new Date())) {
                            Log.d("Past Date", "True");
                            Utils.ShowAlert("Error", "The time has exceeded",getActivity());
                        } else {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    getActivity());
                            // set title
                            alertDialogBuilder.setTitle("Alert");
                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Do you want to cancel this order!")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // if this button is clicked, close
                                            // current activity
                                            webServiceCall(orderId);
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // the dialog box and do nothing
                                            dialog.cancel();
                                        }
                                    });
                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            // show it
                            alertDialog.show();
                        }
                    }
                } catch (Exception e) {

                }
            }


        }
        if (v == camera) {
            try {
                shareImageRestaurant = new JSONObject(JSONvalue);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            showCameraSharePopup();
        }
        if (v == summary) {
            try {
                //Log.d("Book Details", expBookedDetail.toString());
                if (bookedDetail != null) {
                    //Alert_Dialog alert = new Alert_Dialog();
                    //alert.Place_History_Summary(getActivity(), bookedDetail, expBookedDetail);
                }

            } catch (Exception e) {
                Log.d("Exception ", e.toString());
            }

        }
        if (v == facility) {
            try {
                if(amenites!=null&&amenites.length()>0) {
                    //Alert_Dialog alert = new Alert_Dialog();
                    //alert.Place_History_Facilities(getActivity(), amenites);
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Message");
                    alert.setMessage("User not Prefered particular facilities");
                    alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.show();
                }
            } catch (Exception e) {

            }

        }
        if (v == food_package) {
            if(bookedDetail!=null){
                if(bookedDetail.foodMenuPackage==null) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Message");
                    alert.setMessage("No food package available in this booking");
                    alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.show();
                } else {
                    ShowFoodPackage();
                }
            }
        }
        if (v == review) {
            /*if (bookedDetail != null) {
                try {
                    PlaceSpaceTab.businessID = bookedDetail.place.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Fragment placesReview = new PlacesReviewsFragment();
                /*PlacesReviewsFragment.newInstance(0, "", jsonObject);
                android.support.v4.app.FragmentTransaction tx = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                tx.add(R.id.fragment_place, placesReview);
                tx.addToBackStack(placesReview.getClass().getName());
                tx.commit();*----/
                Fragment placesReview = PlacesReviewsFragment.newInstance(0, "", jsonObject);
                android.support.v4.app.FragmentTransaction tx = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                tx.add(R.id.fragment_place, placesReview);
                tx.addToBackStack(placesReview.getClass().getName());
                tx.commit();
            }*/
        }
        if (v == more) {
            /*Alert_Dialog alert = new Alert_Dialog();
            if (bookedDetail != null || expBookedDetail != null) {
                alert.Place_booked_dates(getActivity(), bookedDetail);
            }*/
        }
        if (v == accept){
            Log.d("Place Date_Obj", bookedDetail.toString());

            Date date = new Date(bookedDetail.bookedDates.get(0).checkIn);
            if (date.before(new Date())) {
                Log.d("Past Date", "True");
                Utils.ShowAlert("Error", "The time has exceeded",getActivity());
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Confirmation");
                alert.setMessage("Please Confirm do you want Approve or reject this order request?");
                alert.setPositiveButton("Approve", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        webservicecallforResponse("APPROVED", bookedDetail.id);
                    }
                });
                alert.setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        webservicecallforResponse("REJECTED", bookedDetail.id);
                    }
                });
                alert.setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.show();
            }
        }
        if (v == contact_user){
            /*String userid = "";
            String phone = "";
            JSONObject obj = null;
            Fragment fragment = new Communication();
            Bundle args = new Bundle();
            if(bookedDetail!=null) {
                if (bookedDetail != null && !bookedDetail.phone.isEmpty())
                    phone = bookedDetail.phone;
                if (bookedDetail != null && bookedDetail.customer != null) {
                    userid = bookedDetail.customer.profileId;
                    obj = Utils.getChatObj(name.getText().toString(), bookedDetail.customer.srcJson);
                    if (phone.isEmpty() && bookedDetail.customer.contactDetails != null)
                        phone = bookedDetail.customer.contactDetails.mobilePhone;
                }
                if (bookedDetail != null && bookedDetail.place != null) {
                    try {
                        args.putString("type", "PlaceDetails");
                        args.putString("orderId", bookedDetail.place.getString("id"));
                        args.putString("topicId", "PlaceDetails");
                        args.putString("chatId", bookedDetail.chatId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(!userid.isEmpty()&&userid.equals(PrefManager.getInstance(getContext()).getUserId())){
                Toast.makeText(getContext(),"Cannot Contact yourself",Toast.LENGTH_LONG).show();
            }else {
                if (!userid.isEmpty()) {
                    args.putString("opponentID", userid);
                    args.putString("obj", obj.toString());
                } else
                    args.putBoolean("disable_chat", true);
                if (!phone.isEmpty())
                    args.putString("phone", phone);
                else
                    args.putBoolean("disable_phone", true);
                args.putBoolean("disable_video", true);
                args.putBoolean("disable_stream", true);
                fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }*/
        }
        if(v == uloc){
            /*---if(bookedDetail!=null&&bookedDetail.customer!=null&&bookedDetail.customer.location!=null) {
                if (bookedDetail.customer.location.coordinate != null) {
                    Intent location = new Intent(fragment.getActivity(), LocationActivity.class);
                    location.putExtra("name", bookedDetail.customer.contactDetails.getName());
                    location.putExtra("lat", bookedDetail.customer.location.coordinate.latitude);
                    location.putExtra("lon", bookedDetail.customer.location.coordinate.longitude);
                    location.putExtra("address", bookedDetail.customer.location.addressString);
                    location.putExtra("hide_street", true);
                    if (!bookedDetail.customer.photoLocation.isEmpty())
                        location.putExtra("markerUrl", bookedDetail.customer.photoLocation);
                    else
                        location.putExtra("markerId", R.mipmap.profile_image_placeholder);
                    fragment.startActivity(location);
                } else {
                    Alert_Dialog alert_dialog = new Alert_Dialog();
                    alert_dialog.AlertView(getActivity(), "Location not available, Please check the server side");
                }

            }*/
        }
    }

    public void ShowFoodPackage(){

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.food_package_adapter);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);

        TextView category = (TextView) dialog.findViewById(R.id.category);
        TextView price = (TextView) dialog.findViewById(R.id.price);

        LinearLayout toppings = (LinearLayout) dialog.findViewById(R.id.topping_view);
        category.setText(bookedDetail.foodMenuPackage.packageName);
        price.setText(bookedDetail.foodMenuPackage.getCurrencySymbol() + " " + String.format("%.2f",bookedDetail.foodMenuPackage.price));
        ArrayList<FoodPkg.PlaceMenu>items = bookedDetail.foodMenuPackage.menuItems;
        for (int i = 0; i < items.size(); i++) {
            final View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.food_package_items, toppings, false);
            TextView itemName = (TextView) layout2.findViewById(R.id.items);
            try {
                itemName.setText(items.get(i).itemName + "(" + items.get(i).qty + ")");
            } catch (Exception e) {
            }
            layout2.setTag(i);
            toppings.addView(layout2);
        }
        dialog.show();
    }

    public void webservicecallforResponse(final String status, final String id){
        try {
            String tag_json_obj = "json_obj_req";

            String url = Constants.URL+"placeAdmin/ejectOrAcceptPlaceBookingHistory";
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status",status);
            jsonObject.put("orderId",id);
            Log.d("Request", jsonObject + url);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(final JSONObject response) {
                            try {
                                Log.d("Response", response.toString());
                                pDialog.dismiss();
                                if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                    alert.setTitle("Success");
                                    if(status.equals("APPROVED")){
                                        alert.setMessage("You successfully Approved this order");
                                    } else {
                                        alert.setMessage("You successfully Reject this order");
                                    }
                                    alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                            //AdminNotificationFragment fragment = (AdminNotificationFragment) getActivity().getSupportFragmentManager().findFragmentByTag(AdminNotificationFragment.class.getName());
                                            //if (fragment != null)
                                            //    fragment.removeRecord(id);
                                            getActivity().getSupportFragmentManager().popBackStack();
                                        }
                                    });
                                    alert.show();
                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                    alert.setTitle("Falied");
                                    if(response.has("message"))
                                        alert.setMessage(response.getString("message"));
                                    else if(status.equals("APPROVED")){
                                        alert.setMessage("failed Approved this order");
                                    } else {
                                        alert.setMessage("failed Reject this order");
                                    }
                                    alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    alert.show();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }


    public void webServiceCall(String orderID) {

        try {
            String tag_json_obj = "json_obj_req";
            String url = "";
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();

            Log.d("Request", jsonObject.toString());
            JsonObjectRequest jsonObjReq = null;
            url = Constants.CANCEL_PLACES + orderID;
            Log.d("Request", url.toString());
            jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pDialog.dismiss();
                                Log.d("Response", response.toString());
                                if (response.getString("status").equals("SUCCESS")) {
                                    Toast.makeText(fragment.getContext(), "Your order got cancelled", Toast.LENGTH_LONG).show();
                                    getActivity().getSupportFragmentManager().popBackStack();
                                } else {
                                    if (response.has("message")) {
                                        Utils.ShowAlert("Failure", response.getString("message"),getActivity());
                                    }
                                }

                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });
// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void JSONParsing(String json) {
        try {
            Log.d("Place History Data", json);
            if (getArguments().containsKey("screentype")) {
                cancelText.setText("Reject\nBooking");
                userLocation.setVisibility(View.GONE);
                map_icon.setVisibility(View.GONE);
            }
            jsonObject = new JSONObject(json);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                bookedDetail = new PlaceBookedDetail(jsonObject);
                if (bookedDetail.bookedSpace != null)
                    roomName.setText(bookedDetail.bookedSpace.name);
                JSONObject places = bookedDetail.place;
                name.setText(places.getString("name"));
                roomName.setText(places.getString("placeType") + " : " + (jsonObject.has("space")?jsonObject.getJSONObject("space").getString("name"):""));
                address.setText(places.getJSONObject("location").getString("addressString"));
                if (jsonObject.has("reqAmenities")) {
                    amenites = jsonObject.getJSONArray("reqAmenities");
                }
                if (jsonObject.has("place")) {
                    rating.setRating(jsonObject.getJSONObject("place").getInt("rating"));
                }
                if (jsonObject.has("status")) {
                    if (jsonObject.getString("status").equalsIgnoreCase("Active")) {
                        status.setText("The booking has been active");
                    } else if (jsonObject.getString("status").equalsIgnoreCase("Cancelled")) {
                        if (jsonObject.has("cancelledOn")) {
                            Date date = new Date(jsonObject.getLong("cancelledOn"));
                            String stringDate = sdf.format(date);
                            status.setText("The booking has been cancelled on " + stringDate);
                        }
                        cancelText.setText("Booking Cancelled");
                        cancel.setEnabled(false);
                        cancel.setClickable(false);
                    } else if (jsonObject.getString("status").equalsIgnoreCase("Rejected")) {
                        if (jsonObject.has("cancelledOn")) {
                            Date date = new Date(jsonObject.getLong("cancelledOn"));
                            String stringDate = sdf.format(date);
                            status.setText("The booking has been rejected on " + stringDate);
                        }
                        cancelText.setText("Booking Rejected");
                        cancel.setEnabled(false);
                        cancel.setClickable(false);
                    }
                    statusHead.setText(jsonObject.getString("status"));

                }

                if (bookedDetail.bookedDates.size() > 3) {
                    more.setVisibility(View.VISIBLE);
                } else {
                    more.setVisibility(View.GONE);
                }
                String bookingdates = "";
                for (int i = 0; i < 3 && i < bookedDetail.bookedDates.size(); i++) {
                    if (bookingdates.isEmpty()) {
                        bookingdates = bookedDetail.bookedDates.get(i).getCheckinCheckout();
                    } else {
                        bookingdates = bookingdates + "\n" + bookedDetail.bookedDates.get(i).getCheckinCheckout();
                    }
                }
                dates.setText(bookingdates);
                Lattitude = places.getJSONObject("location").getJSONObject("coordinate").getString("latitude");
                Langitude = places.getJSONObject("location").getJSONObject("coordinate").getString("longitude");
                // Space details parsing
                imageUrl.add(places.getString("imageUrl"));
                orderId = jsonObject.getString("id");
                businssId = jsonObject.getString("businessId");
                confirmation.setText("Confirmation Number : " + jsonObject.getString("bookingId"));
                if(jsonObject.has("bookedDates")) {
                    JSONArray bookedDate = jsonObject.getJSONArray("bookedDates");
                    cancelClickObject = bookedDate.getJSONObject(0);
                    for (int i = 0; i < bookedDate.length(); i++) {
                        JSONObject dateObject = bookedDate.getJSONObject(i);
                        if(dateObject.has("checkIn")&&dateObject.has("checkOut")) {
                            Date checkin = new Date(dateObject.getLong("checkIn"));
                            Date checkout = new Date(dateObject.getLong("checkOut"));
                            checkIn.add(checkin);
                            checkOut.add(checkout);
                        }
                    }
                }
                if(getArguments()!=null&&getArguments().containsKey("screentype")&&getArguments().getString("screentype").equals("manager")){
                    cancel.setVisibility(View.GONE);
                    share.setVisibility(View.GONE);
                    contact_user.setVisibility(View.VISIBLE);
                    user_options.setVisibility(View.GONE);
                    rating.setVisibility(View.GONE);
                    if(bookedDetail.status.equals("ACTIVE")) {
                        accept.setVisibility(View.VISIBLE);
                    } else if(bookedDetail.status.equals("APPROVED")){
                        cancel.setVisibility(View.VISIBLE);
                    }
                    statusHead.setText(bookedDetail.status);
                    user_layout.setVisibility(View.VISIBLE);
                    if(!bookedDetail.name.isEmpty()){
                        uname.setText(bookedDetail.name);
                    } else if(bookedDetail.customer!=null&&bookedDetail.customer.contactDetails!=null){
                        uname.setText(bookedDetail.customer.contactDetails.getName());
                    }
                    if(!bookedDetail.email.isEmpty())
                        email.setText(bookedDetail.email);
                    else if(bookedDetail.customer!=null&&bookedDetail.customer.contactDetails!=null)
                        email.setText(bookedDetail.customer.contactDetails.emailAddress);
                    if(!bookedDetail.phone.isEmpty())
                        phone.setText(bookedDetail.phone);
                    else if(bookedDetail.customer!=null&&bookedDetail.customer.contactDetails!=null)
                        phone.setText(bookedDetail.customer.contactDetails.mobilePhone);
                    if(bookedDetail.customer!=null&&!bookedDetail.customer.photoLocation.isEmpty())
                        image.setImageUrl(bookedDetail.customer.photoLocation);
                    if(bookedDetail.customer==null||bookedDetail.customer.location==null)
                        uloc.setVisibility(View.GONE);
                }
                if (imageUrl.size() >= 1) {
                    PicassoUtil.with(getActivity())
                            .load(imageUrl.get(0))
                            .into(placeImages);
                    Log.d("Image URL", imageUrl.get(0));
                } else {
                    placeImages.setImageResource(R.mipmap.restaurant_img);
                }
        } catch (Exception e) {
            Log.d("Exception 1", e.toString());
        }
    }

    public void showCameraSharePopup() {
        final PopupWindow popup = new PopupWindow(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_image_share, null);
        Button take_image = (Button) layout.findViewById(R.id.take_image);
        TextView headTxt = (TextView) layout.findViewById(R.id.headtxt);
        headTxt.setText("We are offer 1% rebate on your property expense!");
        popup.setContentView(layout);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
        take_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Select image using");
                //alert.SetMessage ("");
                alert.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if ((int) Build.VERSION.SDK_INT > 22 && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                        if ((int) Build.VERSION.SDK_INT > 22 && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{android.Manifest.permission.CAMERA}, 3);
                        if ((int) Build.VERSION.SDK_INT > 22 && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 4);
                        else {
                            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Image_" + System.currentTimeMillis() + ".png");
                            Uri outputfileuri = Uri.fromFile(file);
                            CaptureImagepath = outputfileuri.getPath();
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputfileuri);
                            fragment.startActivityForResult(intent, 0);
                        }
                    }
                });


                alert.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if ((int) Build.VERSION.SDK_INT > 22 && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                            fragment.requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                        else {
                            Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            imageIntent.setType("image/*");
                            fragment.startActivityForResult(Intent.createChooser(imageIntent, "Select photo"), 1);
                        }
                    }
                });

                alert.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                Dialog dialog = alert.create();
                dialog.show();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data.getData().toString().contains("image") || data.getData().toString().contains("photo")) {
                String path = getRealPathFromURI(data.getData());
                if (path != null && !path.isEmpty()) {
                    shareImage(path);
                }
            } else
                Toast.makeText(getActivity(), "Please select valid image file", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            shareImage(CaptureImagepath);
        }
    }


    public void shareImage(String path) {
        if (shareImageRestaurant != null) {
            String rec_id = "";
            String res_name = "";
            String res_address = "";
            String dateandTime = "";
            JSONObject res_detail = null;
            try {
                res_detail = shareImageRestaurant.getJSONObject("place");
                JSONObject res_location = res_detail.getJSONObject("location");
                if (shareImageRestaurant.getString("bookingId") != null) {
                    rec_id = shareImageRestaurant.getString("bookingId");
                }
                if (res_detail != null && res_detail.getString("name") != null) {
                    res_name = res_detail.getString("name");
                }
                if (res_location != null && res_location.getString("addressString") != null) {
                    res_address = res_location.getString("addressString");
                }
                Date date = new Date(shareImageRestaurant.getJSONArray("bookedDate").getJSONObject(0).getLong("checkIn"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy hh:mm a", Locale.ENGLISH);
                dateandTime = sdf.format(date);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SendEmail(getActivity(), "receipts@pinslots.com", "", "Order Receipt attachment to get rebate", "Please find the attached screenshot of order receipt. \n Order Id : " + rec_id
                    + "\nPlaceName: " + res_name + "\n Address: " + res_address + "\nDate&Time: " + dateandTime, path);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        android.database.Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static void SendEmail(Context context, String emailTo, String emailCC, String subject, String emailText, String Attachment_path) {
        final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailTo});
        emailIntent.putExtra(android.content.Intent.EXTRA_CC, new String[]{emailCC});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailText);
        ArrayList<Uri> uris = new ArrayList<Uri>();
        File fileIn = new File(Attachment_path);
        Uri u = Uri.fromFile(fileIn);
        uris.add(u);
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }



    public void AddTaxItem(LinearLayout layout, String name, String cost, int color, String currency) {
        View v = LayoutInflater.from(layout.getContext()).inflate(R.layout.tax_row_view_history, null);
        TextView taxName1 = (TextView) v.findViewById(R.id.tax_name);
        TextView taxCost1 = (TextView) v.findViewById(R.id.tax_cost);
        taxCost1.setTextColor(color);
        taxName1.setText(name);
        taxCost1.setText(currency + " " + cost);
        layout.addView(v);
    }

}
