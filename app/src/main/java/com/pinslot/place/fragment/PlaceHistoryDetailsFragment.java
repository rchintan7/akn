package com.pinslot.place.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.adapter.ExtraItemsAdapter;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.model.PlaceBookedDetail;

import org.json.JSONException;
import org.json.JSONObject;

public class PlaceHistoryDetailsFragment extends BaseFragment implements View.OnClickListener{

    GridView extraItems;
    CardView food_services;
    Button personized;
    TextView room_name,guest_cout,size,date,status,price,top_bar;
    PlaceBookedDetail detail;

    public static PlaceHistoryDetailsFragment getInstance(JSONObject json){
        PlaceHistoryDetailsFragment fragment = new PlaceHistoryDetailsFragment();
        Bundle args = new Bundle();
        args.putString("json",json.toString());
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_history_detail,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            detail = new PlaceBookedDetail(new JSONObject(getArguments().getString("json")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        extraItems = view.findViewById(R.id.extra_items);
        extraItems.setAdapter(new ExtraItemsAdapter());
        food_services = view.findViewById(R.id.food_services);
        personized = view.findViewById(R.id.personized);
        room_name = view.findViewById(R.id.room_name);
        guest_cout = view.findViewById(R.id.guest_count);
        size = view.findViewById(R.id.size);
        price = view.findViewById(R.id.price);
        date = view.findViewById(R.id.date);
        status = view.findViewById(R.id.status);
        top_bar = view.findViewById(R.id.top_bar);
        room_name.setText(detail.bookedSpace.name);
        String dates = "";
        for (PlaceBookedDetail.PlaceBookedDate date : detail.bookedDates){
            if(dates.isEmpty())
                dates = date.getCheckinCheckout();
            else
                dates = dates+"\n"+date.getCheckinCheckout();
        }
        date.setText(dates);
        status.setText(detail.status);
        CSPlace place = new CSPlace(detail.place);
        price.setText("₹"+" "+String.format("%.2f",detail.finalPrice));
        food_services.setOnClickListener(this);
        personized.setOnClickListener(this);
    }

    public void showServiceAlert(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.food_service_alert);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);
        dialog.show();
        Button btn1 = dialog.findViewById(R.id.btn1);
        Button btn2 = dialog.findViewById(R.id.btn2);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(FoodServiceFragment.Instance(detail.id),true);
                dialog.dismiss();
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(FoodHistoryFragment.getInstance(detail.id),true);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.food_services : showServiceAlert();break;
            case R.id.personized : addFragment(new PersonizedRoomSettingFragment(),true);break;
        }
    }
}
