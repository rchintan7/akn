package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.adapter.Coupon_List_Adapter;
import com.pinslot.place.adapter.ResCouponAdapter;
import com.pinslot.place.helpers.Payment_Method;
import com.pinslot.place.model.PlaceRequest;
import com.pinslot.place.model.Restaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.pinslot.place.fragment.Restaurant_Menu_Items.tv_amt;
import static com.pinslot.place.fragment.Restaurant_Menu_Items.tv_qty;

public class OrderDetails extends BaseFragment implements View.OnClickListener {

    ListView list;
    ArrayList<String> RestName = new ArrayList<>();
    ArrayList<String> menuName = new ArrayList<>();
    ArrayList<String> punch = new ArrayList<>();
    ArrayList<String> price = new ArrayList<>();
    ArrayList<String> quantity = new ArrayList<>();

    Button payNow, payRestaurant,applyCoupon,continue_delivery;
    SharedPreferences pref;
    TextView subTotal, Tax, dineIn, takeAway, subTotalDis, promoText, facebookText, facebookPromotion, splDiscount;
    public static TextView Total;
    ImageView facebook;
    int discount;
    public static int newTotalPrice;
    Double total = 0.0, tax = 0.0;
    int subTotalval = 0;
    LinearLayout cartLayout, promoLayout, facebookLayout, promoView, facebookView, taxParent;
    View view1, view2;
    public JSONObject cartObj;
    JSONArray couponsArray;
    public JSONObject applied_coupon = null;
    public JSONObject fb_coupon = null;
    ResCouponAdapter adapter;

    JSONArray taxList;
    JSONArray jsonArray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.order_details_new, container, false);
    }

    int totalAmount, qty;
    int event_count = 1;
    String currency = "";

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        subTotal = (TextView) view.findViewById(R.id.sub_total);
        subTotal.setText(currency + "0.00");
        subTotalDis = (TextView) view.findViewById(R.id.sub_total_dis);
        subTotalDis.setText(currency + "0.00");
        facebookPromotion = (TextView) view.findViewById(R.id.facebook_promotion);
        facebookPromotion.setText(currency + "0.00");
        splDiscount = (TextView) view.findViewById(R.id.spl_discount);
        splDiscount.setText(currency + "0.00");
        Total = (TextView) view.findViewById(R.id.total_amount);
        Total.setText(currency + "0.00");

        promoText = (TextView) view.findViewById(R.id.promo_text);
        facebookText = (TextView) view.findViewById(R.id.facebook_text);

        taxParent = (LinearLayout) view.findViewById(R.id.tax_parent);
        cartLayout = (LinearLayout) view.findViewById(R.id.cart_view);
        promoLayout = (LinearLayout) view.findViewById(R.id.promo_layout);
        facebookLayout = (LinearLayout) view.findViewById(R.id.facebook_layout);
        promoView = (LinearLayout) view.findViewById(R.id.promo_view);
        facebookView = (LinearLayout) view.findViewById(R.id.facebook_view);
        view1 = (View) view.findViewById(R.id.view1);
        view2 = (View) view.findViewById(R.id.view2);
        payNow = (Button) view.findViewById(R.id.pay_now);
        applyCoupon = view.findViewById(R.id.apply_coupon);
        payRestaurant=(Button)view.findViewById(R.id.pay_at_restaurant);
        continue_delivery = view.findViewById(R.id.continue_delivery);
        //facebook=(ImageView) view.findViewById(R.id.facebook);
        if(((NavigationDrawerActivity)getActivity()).resReq.flow.equals("Restaurant_delivery")){
            continue_delivery.setVisibility(View.VISIBLE);
            payNow.setVisibility(View.GONE);
            payRestaurant.setVisibility(View.GONE);
        }

        payNow.setOnClickListener(this);
        applyCoupon.setOnClickListener(this);
        promoLayout.setOnClickListener(this);
        facebookLayout.setOnClickListener(this);
        payRestaurant.setOnClickListener(this);
        continue_delivery.setOnClickListener(this);

        if (Constants.PROMO_CODE == true) {
            promoText.setText("Please Enter Your Promo Code to get discount");
        } else {
            promoText.setText("Promo Code Not Available");
        }

        if (Constants.FACEBOOK_OFFER == true) {
            promoText.setText("Share with Facebook to get discount");
        } else {
            promoText.setText("Facebook Offer Not Available");
        }
        //showCartItem();

        //getOffer();

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("Back", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("Back", "onKey Back listener is working!!!");
                    //getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        webServiceCallOrderItems();
        if (((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.currency.equals("Rs."))
            currency = "₹";
        else
            currency = "$";

        RestName.add("PANNER TIKAS");
        RestName.add("CHICKEN BRIYANI");
        RestName.add("DHANTOORI CHICKEN");
        RestName.add("ALU - GOBI MASALA");
        RestName.add("FISH MASALA");
        getPromos();

    }

    public void onClick(View v) {
        if (v == payNow) {
            if(PrefManager.getInstance(getContext()).getUserId().isEmpty()){
                addFragment(new LoginFragment(),true);
            } else if(getArguments().getString("linkedOrderId","").isEmpty()||((NavigationDrawerActivity)getActivity()).resReq.requestType.equals("Delivery")){
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setMessage("Confirmation");
                alert.setMessage("This Booking Not Associate with any rooms. So we Can't Able to Deliver this item in your Room. stiil you can checkout with Dinein or takeAway in Restaurant");
                alert.setPositiveButton("Book Room", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((NavigationDrawerActivity)getActivity()).placeRequest = new PlaceRequest();
                        replaceFragment(new Book_Room(),true);
                    }
                });
                alert.setNegativeButton("Dine In", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((NavigationDrawerActivity)getActivity()).showDineIn(new RestaurantCheckin.RequestTypeChangeCallbak() {
                            @Override
                            public void requestTypeChanged() {

                            }
                        });
                    }
                });
                alert.setNeutralButton("TakeAway", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((NavigationDrawerActivity)getActivity()).showTakeAway(new RestaurantCheckin.RequestTypeChangeCallbak() {
                            @Override
                            public void requestTypeChanged() {

                            }
                        });
                    }
                });
                alert.show();
            } else {
                String promo = "";
                try {
                    if (applied_coupon != null)
                        promo = applied_coupon.getString("id");
                    if (fb_coupon != null) {
                        if (promo.isEmpty())
                            promo = fb_coupon.getString("id");
                        else
                            promo = promo + "," + fb_coupon.getString("id");
                    }
                } catch (JSONException e) {

                }
                final String finalPromo = promo;
                ((NavigationDrawerActivity) getActivity()).resReq.total = total;
                ((NavigationDrawerActivity) getActivity()).resReq.tax = tax;
                ((NavigationDrawerActivity) getActivity()).resReq.subTotalval = subTotalval;
                Payment_Method.BookRestaurant(getArguments().getString("linkedOrderId",""),finalPromo, (NavigationDrawerActivity) getActivity(), "RAZORPAY", new Payment_Method.BookingCallBack() {
                    @Override
                    public void bookingSucess(String bookingId, final String token, final JSONObject response) {
                        if(total>0) {
                            ((NavigationDrawerActivity) getActivity()).startPayment(new NavigationDrawerActivity.PaymentCallback() {
                                @Override
                                public void paymentSuccess(String paymentId, String signature, String orderId) {
                                    Payment_Method.webServiceCallForPaymentUpdate(paymentId, signature, orderId, "SUCCESS", getActivity(), new Payment_Method.PaymentUpdateCallback() {
                                        @Override
                                        public void PaymentUpdateSucess() {
                                            Payment_Method.openRestaurantBookedScreen(response, getActivity());
                                        }

                                        @Override
                                        public void PaymentUpdateFailed() {
                                            Utils.ShowAlert(R.string.failed,R.string.payment_succes_booking_failed,getContext());
                                        }
                                    });
                                }

                                @Override
                                public void paymentError(int i, String s) {
                                    Utils.ShowAlert(R.string.failed,R.string.payment_failed,getContext());
                                    Payment_Method.webServiceCallForPaymentUpdate("","",token,"FAILED",getContext(),null);
                                }
                            }, token, "Restaurant Booking",false);
                        } else {
                            Payment_Method.openRestaurantBookedScreen(response, getActivity());
                        }
                    }

                    @Override
                    public void bookingFailed(String failed) {

                    }
                });
            }
        }

        if (v == applyCoupon) {
            ShowPromos();
        }

        if (v == promoLayout) {
            promoView.setVisibility(View.VISIBLE);
            facebookView.setVisibility(View.GONE);
            view1.setBackgroundColor(getActivity().getResources().getColor(R.color.red));
            view2.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
        }

        if (v == facebookLayout) {
            promoView.setVisibility(View.GONE);
            facebookView.setVisibility(View.VISIBLE);
            view1.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
            view2.setBackgroundColor(getActivity().getResources().getColor(R.color.red));
        }

        if(v == payRestaurant) {
            if(PrefManager.getInstance(getContext()).getUserId().isEmpty()){
                addFragment(new LoginFragment(),true);
            } else if(getArguments().getString("linkedOrderId","").isEmpty()&&((NavigationDrawerActivity)getActivity()).resReq.requestType.equals("Delivery")){
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setMessage("Confirmation");
                alert.setMessage("This Booking Not Associate with any rooms. So we Can't Able to Deliver this item in your Room. stiil you can checkout with Dinein or takeAway in Restaurant");
                alert.setPositiveButton("Book Room", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((NavigationDrawerActivity)getActivity()).placeRequest = new PlaceRequest();
                        replaceFragment(new Book_Room(),true);
                    }
                });
                alert.setNegativeButton("Dine In", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((NavigationDrawerActivity)getActivity()).showDineIn(new RestaurantCheckin.RequestTypeChangeCallbak() {
                            @Override
                            public void requestTypeChanged() {

                            }
                        });
                    }
                });
                alert.setNeutralButton("TakeAway", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((NavigationDrawerActivity)getActivity()).showTakeAway(new RestaurantCheckin.RequestTypeChangeCallbak() {
                            @Override
                            public void requestTypeChanged() {

                            }
                        });
                    }
                });
                alert.show();
            } else {
                String promo = "";
                try {
                    if (applied_coupon != null)
                        promo = applied_coupon.getString("id");
                    if (fb_coupon != null) {
                        if (promo.isEmpty())
                            promo = fb_coupon.getString("id");
                        else
                            promo = promo + "," + fb_coupon.getString("id");
                    }
                } catch (JSONException e) {

                }
                ((NavigationDrawerActivity) getActivity()).resReq.total = total;
                ((NavigationDrawerActivity) getActivity()).resReq.tax = tax;
                ((NavigationDrawerActivity) getActivity()).resReq.subTotalval = subTotalval;
                Payment_Method.BookRestaurant(getArguments().getString("linkedOrderId",""),promo, (NavigationDrawerActivity) getActivity(), "PAR", new Payment_Method.BookingCallBack() {
                    @Override
                    public void bookingSucess(String bookingId,String token, JSONObject response) {
                        Payment_Method.openRestaurantBookedScreen(response,getActivity());
                    }

                    @Override
                    public void bookingFailed(String failed) {
                        Utils.ShowAlert(R.string.failed,R.string.booking_failed,getActivity());
                    }
                });
            }
        }
    }

    public void getPromos(){
        try{
            String tag_json_obj = "json_obj_req";
            String url = Constants.PROMOCODE_SEARCH;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("businessId",((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.id);
            jsonObject.put("businessType","RESTAURANT");
            //jsonObject.put("expiredPeriod","TODAY");
            jsonObject.put("customerId", pref.getString("userId", ""));
            jsonObject.put("pickupDate",((NavigationDrawerActivity)getActivity()).resReq.startDate);
            SimpleDateFormat tf = new SimpleDateFormat("hh:mm a");
            jsonObject.put("pickupTime",tf.format(new Date(((NavigationDrawerActivity)getActivity()).resReq.startDate)));

            JSONObject dateObj = new JSONObject();
            dateObj.put("fromDateTimeStr", pref.getString("RestDate", "") + " 12:00 AM");
            dateObj.put("toDateTimeStr", pref.getString("RestDate", "") + " 11:55 PM");
            JSONArray dateList = new JSONArray();
            dateList.put(dateObj);
            jsonObject.put("dateTimeList", dateList);
            Log.d("Offer Request", jsonObject.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Experience Response", response.toString());
                            try {
                                if(response.getString("status").equals("SUCCESS"))
                                {
                                    couponsArray = response.getJSONObject("data").getJSONArray("result");
                                }
                            }catch (Exception e){

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }catch (Exception e){
            System.out.println("Error :"+e.toString());
        }
    }

    public void ShowPromos(){
        ApplyCouponFragment fragment = new ApplyCouponFragment();
        if(applied_coupon!=null)
            fragment.applied_coupon = applied_coupon;
        else if(fb_coupon!=null)
            fragment.fb_coupon = fb_coupon;
        Bundle args = new Bundle();
        args.putString("type","Restauant");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("businessId", ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.id);
            jsonObject.put("businessType", "RESTAURANT");
            //jsonObject.put("expiredPeriod","TODAY");
            jsonObject.put("customerId", pref.getString("userId", ""));
            jsonObject.put("pickupDate", ((NavigationDrawerActivity) getActivity()).resReq.startDate);
            SimpleDateFormat tf = new SimpleDateFormat("hh:mm a");
            jsonObject.put("pickupTime", tf.format(new Date(((NavigationDrawerActivity) getActivity()).resReq.startDate)));

            JSONObject dateObj = new JSONObject();
            dateObj.put("fromDateTimeStr", pref.getString("RestDate", "") + " 12:00 AM");
            dateObj.put("toDateTimeStr", pref.getString("RestDate", "") + " 11:55 PM");
            JSONArray dateList = new JSONArray();
            dateList.put(dateObj);
            jsonObject.put("dateTimeList", dateList);
        } catch (JSONException e){

        }
        args.putString("json",jsonObject.toString());
        args.putString("url", Constants.PROMOCODE_SEARCH);
        fragment.setArguments(args);
        fragment.listener = new Coupon_List_Adapter.ApplyListener() {
            @Override
            public void Apply(JSONObject jsonObject) {
                applied_coupon = jsonObject;
                if(jsonArray!=null)
                    UpdateCart();
            }

            @Override
            public void fbApply(JSONObject jsonObject) {
                fb_coupon = jsonObject;
                if(jsonArray!=null)
                    UpdateCart();
            }
        };
        addFragment(fragment,true);
        /*final android.app.Dialog dialog = new android.app.Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_show_coupons);
        ListView list = dialog.findViewById(R.id.list);
        TextView no_promos = dialog.findViewById(R.id.no_promos);
        TextView login_text = dialog.findViewById(R.id.login_text);
        if(!pref.getString("userId", "").equals("")) {
            if (couponsArray != null&&couponsArray.length()>0) {
                list.setVisibility(View.VISIBLE);
                no_promos.setVisibility(View.GONE);
                login_text.setVisibility(View.GONE);
                String coupon = "",applied_fb_coupon ="";
                if(applied_coupon!=null) {
                    try {
                        coupon = applied_coupon.getString("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(fb_coupon!=null){
                    try {
                        applied_fb_coupon = fb_coupon.getString("id");
                    } catch (JSONException e){

                    }
                }
                adapter = new ResCouponAdapter(this, couponsArray, coupon,applied_fb_coupon, dialog,new Date(((NavigationDrawerActivity)getActivity()).resReq.startDate));
                adapter.setApplyListener(this);
                list.setAdapter(adapter);
            } else {
                list.setVisibility(View.GONE);
                login_text.setVisibility(View.GONE);
                no_promos.setVisibility(View.VISIBLE);
            }
        } else {
            list.setVisibility(View.GONE);
            login_text.setVisibility(View.VISIBLE);
            no_promos.setVisibility(View.GONE);
            login_text.setMovementMethod(LinkMovementMethod.getInstance());
            String clickableText = "You may get special offer or personalized coupon if you login. {click here} to login";
            login_text.setText(addClickablePart(clickableText), TextView.BufferType.SPANNABLE);
        }
        dialog.show();*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        adapter.activityResult(requestCode,resultCode,data);
    }

    private SpannableStringBuilder addClickablePart(String str) {
        String original = str.replace('{',' ').replace('}',' ');
        SpannableStringBuilder ssb = new SpannableStringBuilder(original);
        int idx1 = str.indexOf("{");
        int idx2 = 0;
        while (idx1 != -1) {
            idx2 = str.indexOf("}", idx1) + 1;

            final String clickString = str.substring(idx1, idx2);
            ssb.setSpan(new MyClickableSpan() {
                @Override
                public void onClick(View widget) {
                    if(clickString.equals("{click here}")){
                        addFragment(new LoginFragment(),true);
                    }
                }
            }, idx1, idx2, 0);
            idx1 = str.indexOf("{", idx2);
        }

        return ssb;
    }

    abstract class MyClickableSpan extends ClickableSpan {
        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(getActivity().getResources().getColor(R.color.colorAccent));
            ds.setUnderlineText(false);
            ds.linkColor = getActivity().getResources().getColor(R.color.colorAccent);
        }
    }

    public void webServiceCallOrderItems() {

        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.GET_ORDER_LIST + "customer/" + pref.getString("userId", null) + "/business/" + Constants.res_id + "/cart";
            Log.d("Request", url.toString());
            menuName.clear();
            punch.clear();
            price.clear();
            quantity.clear();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    cartLayout.removeAllViews();
                                    JSONObject data = response.getJSONObject("data");
                                    jsonArray = data.getJSONArray("cartItems");
                                    UpdateCart();
                                }
                            } catch (Exception e) {
                                Log.d("Error :", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void UpdateCart(){
        try {
            cartLayout.removeAllViews();
            menuName.clear();
            punch.clear();
            price.clear();
            quantity.clear();
            ((NavigationDrawerActivity) getContext()).menuItems.clear();
            for (int i = 0; i < jsonArray.length(); i++) {
                final JSONObject items = jsonArray.getJSONObject(i);
                menuName.add(items.getJSONObject("menuItem").getString("menuItemName"));
                if (items.getJSONObject("menuItem").has("menuItemPunchline")) {
                    punch.add(items.getJSONObject("menuItem").getString("menuItemPunchline"));
                }
                price.add(items.getString("finalPrice"));
                quantity.add(items.getString("qty"));
                ((NavigationDrawerActivity) getContext()).menuItems.add(items.getJSONObject("menuItem").getString("menuItemName") + "(" + items.getString("qty") + ")");

                final View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.cart_list, cartLayout, false);
                final TextView priceTxt = (TextView) layout2.findViewById(R.id.price);
                TextView name = (TextView) layout2.findViewById(R.id.name);
                TextView punch = (TextView) layout2.findViewById(R.id.punch);
                final TextView count = (TextView) layout2.findViewById(R.id.count);
                ImageView plus = (ImageView) layout2.findViewById(R.id.plus);
                ImageView minus = (ImageView) layout2.findViewById(R.id.minus);
                ImageView delete = (ImageView) layout2.findViewById(R.id.delete);
                LinearLayout discountLayout = (LinearLayout) layout2.findViewById(R.id.discount_layout);
                TextView discount = (TextView) layout2.findViewById(R.id.discount);
                TextView oldPrice = (TextView) layout2.findViewById(R.id.old_price);
                LinearLayout people = layout2.findViewById(R.id.people);
                TextView peopleCount = layout2.findViewById(R.id.people_count);
                oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                if (items.has("members") && items.getJSONArray("members").length() > 0) {
                    peopleCount.setText(Integer.toString(items.getJSONArray("members").length()));
                    peopleCount.setVisibility(View.VISIBLE);
                } else
                    peopleCount.setVisibility(View.INVISIBLE);

                if (items.has("appliedPromoCoupons") && !items.isNull("appliedPromoCoupons")) {
                    // Do something with object.
                    if (items.getJSONArray("appliedPromoCoupons").length() > 0) {
                        discountLayout.setVisibility(View.VISIBLE);
                        discount.setText(String.valueOf(items.getInt("discountPrice")));
                        oldPrice.setText(String.valueOf(items.getInt("totalPrice")));
                    } else {
                        discountLayout.setVisibility(View.GONE);
                    }
                } else {
                    discountLayout.setVisibility(View.GONE);
                }

                name.setText(items.getJSONObject("menuItem").getString("menuItemName"));
                if(items.getJSONObject("menuItem").has("menuItemPunchline"))
                    punch.setText(Html.fromHtml(items.getJSONObject("menuItem").getString("menuItemPunchline")));
                double Itemprice = Double.parseDouble(items.getString("finalPrice"));
                priceTxt.setText(currency + " " + String.format("%.2f",Itemprice));
                count.setText(items.getString("qty"));

                people.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            updatePeople(items);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        event_count = (Integer.parseInt(count.getText().toString()));
                        event_count = event_count + 1;
                        qty = event_count;
                        count.setText(String.valueOf(event_count));

                        int position = 0;
                        if (layout2.getTag() instanceof Integer) {
                            position = (Integer) layout2.getTag();
                        }

                        //int priceCount=(int)Double.parseDouble(price.get(position));
                        //totalAmount=priceCount*event_count;
                        //priceTxt.setText(currency+" "+String.valueOf(priceCount*event_count));

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("menuItemId", items.getJSONObject("menuItem").getString("menuItemId"));

                            jsonObject.put("qty", qty);
                            JSONArray jsonArray = new JSONArray();
                            if(items.has("restMenuToppings"))
                                jsonArray = items.getJSONArray("restMenuToppings");
                            jsonObject.put("restMenuToppings", jsonArray);

                            // Size
                            if (items.has("restaurantMenuChos") && !items.getString("restaurantMenuChos").equals("null") && !items.getJSONObject("restaurantMenuChos").getString("menuchosPrice").equals("null"))
                                jsonObject.put("restaurantMenuChos", items.getJSONObject("restaurantMenuChos"));
                            updateItem(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        event_count = (Integer.parseInt(count.getText().toString()));
                        if (event_count > 0) {
                            event_count = event_count - 1;
                            count.setText(String.valueOf(event_count));
                            qty = event_count;

                            int position = 0;
                            if (layout2.getTag() instanceof Integer) {
                                position = (Integer) layout2.getTag();
                            }

                            //int priceCount=(int)Double.parseDouble(price.get(position));
                            //totalAmount=priceCount*event_count;
                            //priceTxt.setText(currency+" "+String.valueOf(priceCount*event_count));

                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("menuItemId", items.getJSONObject("menuItem").getString("menuItemId"));

                                jsonObject.put("qty", qty);
                                JSONArray jsonArray = new JSONArray();
                                if(items.has("restMenuToppings"))
                                    jsonArray = items.getJSONArray("restMenuToppings");
                                jsonObject.put("restMenuToppings", jsonArray);

                                // Size
                                if (items.has("restaurantMenuChos") && !items.getString("restaurantMenuChos").equals("null") && !items.getJSONObject("restaurantMenuChos").getString("menuchosPrice").equals("null"))
                                    jsonObject.put("restaurantMenuChos", items.getJSONObject("restaurantMenuChos"));
                                updateItem(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }

                    }
                });

                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                        builder.setTitle("Do you want to delete");
                        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //TODO
                                //((LinearLayout) layout2.getParent()).removeView(layout2);
                                cartLayout.removeView(layout2);

                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("menuItemId", items.getJSONObject("menuItem").getString("menuItemId"));

                                    jsonObject.put("qty", 0);
                                    JSONArray jsonArray = new JSONArray();
                                    if(items.has("restMenuToppings"))
                                        jsonArray = items.getJSONArray("restMenuToppings");
                                    jsonObject.put("restMenuToppings", jsonArray);

                                    // Size
                                    if (items.has("restaurantMenuChos") && !items.getString("restaurantMenuChos").equals("null") && !items.getJSONObject("restaurantMenuChos").getString("menuchosPrice").equals("null"))
                                        jsonObject.put("restaurantMenuChos", items.getJSONObject("restaurantMenuChos"));
                                    updateItem(jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //TODO
                            }
                        });
                        builder.show();
                    }
                });

                layout2.setTag(i);
                cartLayout.addView(layout2);
            }
                                   /* OrderItemAdapter adapter=new OrderItemAdapter(getActivity(),menuName,punch,price,quantity);
                                    list.setAdapter(adapter);*/

            double subtotal = 0;
            for (int j = 0; j < price.size(); j++) {
                double priceCount = Double.parseDouble(price.get(j))/*Integer.parseInt(quantity.get(j))*/;
                subtotal = subtotal + priceCount;
                //subtotal=subtotal+Integer.parseInt(String.valueOf(Math.round(Double.valueOf(price.get(j)))))*Integer.parseInt(quantity.get(j));
            }
            subTotal.setText(currency + " " + String.format("%.2f",subtotal));
            subTotalDis.setText(currency + " " + String.format("%.2f",subtotal));

            int cTax = 0;
            int sTax = 0;
            Double totalCost = 0.0;
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            Restaurant resObj = ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant;
            int off = 0;
            double offCost = 0;
            taxParent.removeAllViews();
            if(applied_coupon!=null){
                View child = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view, null);
                TextView taxName = child.findViewById(R.id.tax_name);
                TextView taxCost = child.findViewById(R.id.tax_cost);
                taxCost.setTextColor(getResources().getColor(R.color.green));
                taxName.setText(applied_coupon.getString("promoName")+" "+applied_coupon.getInt("discount")+"% off");
                off = applied_coupon.getInt("discount");
                offCost = ((subtotal/100)*off);
                taxCost.setText(currency+String.format("%.2f",offCost));
                taxParent.addView(child);
            }
            int fboff = 0;
            double fboffCost = 0;
            if(fb_coupon!=null){
                View child = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view, null);
                TextView taxName = child.findViewById(R.id.tax_name);
                TextView taxCost = child.findViewById(R.id.tax_cost);
                taxCost.setTextColor(getResources().getColor(R.color.green));
                taxName.setText(fb_coupon.getString("promoName")+" "+fb_coupon.getInt("discount")+"% off");
                fboff = fb_coupon.getInt("discount");
                fboffCost = ((subtotal/100)*fboff);
                taxCost.setText(currency+String.format("%.2f",fboffCost));
                taxParent.addView(child);
            }
            subtotal = subtotal - (offCost+fboffCost);
            if (resObj.taxList != null) {
                taxList = resObj.taxList;
                for (int i = 0; i < taxList.length(); i++) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view, null);
                    TextView taxName = (TextView) view.findViewById(R.id.tax_name);
                    TextView taxCost = (TextView) view.findViewById(R.id.tax_cost);

                    cTax = taxList.getJSONObject(i).getInt("percent");
                    double taxValue = (double) cTax / (double) 100 * subtotal;
                    totalCost = totalCost + taxValue;
                    taxName.setText("Tax-" + taxList.getJSONObject(i).getString("name") + "(" + cTax + "%)");
                    taxCost.setText(currency + String.format("%.2f", taxValue));

                    taxParent.addView(view);
                }
                totalCost = totalCost + subtotal;
            }
            total = totalCost;
            Total.setText(currency + " " + String.format("%.2f", totalCost));
            tv_qty.setText("Qty : " + jsonArray.length());
            tv_amt.setText(currency + " " + String.format("%.2f", subtotal));
        } catch (JSONException e){

        }
    }

    public void updatePeople(final JSONObject item) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        if(item.has("members")){
            jsonArray = item.getJSONArray("members");
        }
        final JSONArray peoples = jsonArray;
        final android.app.Dialog dialog = new android.app.Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_add_people);
        final LinearLayout itemLayout = dialog.findViewById(R.id.peoples);
        TextView addMore = dialog.findViewById(R.id.add_more);
        Button submit = dialog.findViewById(R.id.submit);
        for (int i=0;i<peoples.length();i++){
            AddView(itemLayout,peoples.getJSONObject(i).getString("firstName"),peoples.getJSONObject(i).getString("lastName"));
        }
        AddView(itemLayout,"","");
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONArray jArray = new JSONArray();
                    boolean changed = false;
                    for (int i = 0; i < itemLayout.getChildCount(); i++) {
                        String f_name = ((TextView) itemLayout.getChildAt(i).findViewById(R.id.fname)).getText().toString();
                        String l_name = ((TextView) itemLayout.getChildAt(i).findViewById(R.id.lname)).getText().toString();
                        if (!f_name.isEmpty() || !l_name.isEmpty()) {
                            JSONObject object = new JSONObject();
                            object.put("firstName", f_name);
                            object.put("lastName", l_name);
                            jArray.put(object);
                            if (!changed) {
                                boolean found = false;
                                for (int j = 0; j < peoples.length(); j++) {
                                    String name1 = peoples.getJSONObject(j).getString("firstName");
                                    String name2 = peoples.getJSONObject(j).getString("lastName");
                                    if (f_name.equals(name1) && l_name.equals(name2)) {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                    changed = true;
                            }
                        }
                    }
                    if(changed||peoples.length()!=jArray.length()){
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("menuItemId", item.getJSONObject("menuItem").getString("menuItemId"));

                        jsonObject.put("qty", item.getString("qty"));
                        JSONArray jsonArray = new JSONArray();
                        if(item.has("restMenuToppings"))
                            jsonArray = item.getJSONArray("restMenuToppings");
                        jsonObject.put("restMenuToppings", jsonArray);

                        // Size
                        if (item.has("restaurantMenuChos") && !item.getString("restaurantMenuChos").equals("null") && !item.getJSONObject("restaurantMenuChos").getString("menuchosPrice").equals("null"))
                            jsonObject.put("restaurantMenuChos", item.getJSONObject("restaurantMenuChos"));
                        jsonObject.put("members",jArray);
                        updateItem(jsonObject);
                    }
                } catch (JSONException e){

                }
                dialog.dismiss();
            }
        });
        addMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddView(itemLayout,"","");
            }
        });
        dialog.show();
    }

    public void AddView(final LinearLayout parent,String fname,String lname){
        final View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.name_layout, parent, false);
        TextView f_name = layout2.findViewById(R.id.fname);
        TextView l_name = layout2.findViewById(R.id.lname);
        f_name.setText(fname);
        l_name.setText(lname);
        ImageView remove = layout2.findViewById(R.id.remove);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.removeView(layout2);
                if(parent.getChildCount()==0){
                    AddView(parent,"","");
                }
            }
        });
        parent.addView(layout2);
    }

    public void updateItem(JSONObject jsonObject) {
        if (pref.getString("loginSession", null) != null) {
            if (pref.getString("loginSession", null).equals("Valid")) {
                try {
                    String tag_json_obj = this.getClass().getName()+"_update"+jsonObject.getString("menuItemId");
                    String url = Constants.URL + "customer/" + pref.getString("userId", null) + "/business/" + pref.getString("restaurantId", null) + "/cart";


                    Log.e("Add to Cart Request", jsonObject.toString());

                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                            url, jsonObject,
                            new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("Add to Cart Response", response.toString());
                                    webServiceCallOrderItems();
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                    AppController.getInstance().cancelPendingRequests(tag_json_obj);
                    AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                } catch (Exception e) {
                    System.out.println("Error :" + e.toString());
                }
            } else {
                //Intent i = new Intent();
                //i.setClassName(getActivity(), ResSigninActivity.class.getName());
                //getActivity().startActivity(i);
                addFragment(new LoginFragment(),true);
            }
        } else {
            addFragment(new LoginFragment(),true);
        }
    }

    public void showCartItem() {
        //String Cartval = pref.getString("cart_items","");
        cartLayout.removeAllViews();
        if (cartObj != null) {
            try {
                final JSONArray jsonArray = cartObj.getJSONArray("cartItems");
                subTotalval = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    final JSONObject items = jsonArray.getJSONObject(i);

                    final View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.cart_list, cartLayout, false);
                    final TextView priceTxt = (TextView) layout2.findViewById(R.id.price);
                    TextView name = (TextView) layout2.findViewById(R.id.name);
                    final TextView count = (TextView) layout2.findViewById(R.id.count);
                    ImageView plus = (ImageView) layout2.findViewById(R.id.plus);
                    ImageView minus = (ImageView) layout2.findViewById(R.id.minus);
                    ImageView delete = (ImageView) layout2.findViewById(R.id.delete);
                    name.setText(items.getJSONObject("menuItem").getString("menuItemName"));

                    int Itemsprice = 0;
                    if (!items.getString("restaurantMenuChos").equals("null") && !items.getJSONObject("restaurantMenuChos").getString("menuchosPrice").equals("null"))
                        Itemsprice = Integer.parseInt(items.getString("qty")) * (int) Double.parseDouble(items.getJSONObject("restaurantMenuChos").getString("menuchosPrice"));
                    else
                        Itemsprice = (int) items.getDouble("totalPrice");
                    subTotalval = subTotalval + Itemsprice;
                    priceTxt.setText(String.valueOf(Itemsprice));
                    count.setText(items.getString("qty"));

                    plus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            event_count = (Integer.parseInt(count.getText().toString()));
                            event_count = event_count + 1;
                            /*qty = event_count;
                            int position = 0;
                            if (layout2.getTag() instanceof Integer) {
                                position = (Integer) layout2.getTag();
                            }
                            items.remove("qty");
                            try {
                                items.put("qty",event_count);
                                jsonArray.remove(position);
                                jsonArray.put(position,items);
                                cartobject.remove("cart_items");
                                cartobject.put("cart_items",jsonArray);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("cart_items",cartobject.toString());
                                editor.commit();
                                showCartItem();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }*/
                            count.setText(String.valueOf(event_count));
//
//                            int priceCount = 0;
//                            try {
//                                priceCount = (int) Double.parseDouble(items.getJSONObject("restaurantMenuChos").getString("menuchosPrice"));
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            totalAmount = priceCount * event_count;
//                            priceTxt.setText(currency+" " + String.valueOf(priceCount * event_count));
                        }
                    });

                    minus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            event_count = (Integer.parseInt(count.getText().toString()));
                            if (event_count > 0) {
                                event_count = event_count - 1;
                                /*qty = event_count;
                                int position = 0;
                                if (layout2.getTag() instanceof Integer) {
                                    position = (Integer) layout2.getTag();
                                }
                                items.remove("qty");
                                try {
                                    items.put("qty",event_count);
                                    jsonArray.remove(position);
                                    jsonArray.put(position,items);
                                    cartobject.remove("cart_items");
                                    cartobject.put("cart_items",jsonArray);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("cart_items",cartobject.toString());
                                    editor.commit();
                                    showCartItem();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }*/
                                count.setText(String.valueOf(event_count));


//                                int priceCount = 0;
//                                try {
//                                    priceCount = (int) Double.parseDouble(items.getJSONObject("restaurantMenuChos").getString("menuchosPrice"));
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                totalAmount = priceCount * event_count;
//                                priceTxt.setText(currency+" " + String.valueOf(priceCount * event_count));
                            }

                        }
                    });

                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                            builder.setTitle("Do you want to delete");
                            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //TODO
                                    ((LinearLayout) layout2.getParent()).removeView(layout2);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        try {
                                            if (jsonArray.getJSONObject(i) == items) {
                                                jsonArray.remove(i);
                                                cartObj.remove("cart_items");
                                                cartObj.put("cart_items", jsonArray);
                                                showCartItem();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //TODO
                                }
                            });
                            builder.show();
                        }
                    });

                    cartLayout.addView(layout2);
                }
                                       /* OrderItemAdapter adapter=new OrderItemAdapter(getActivity(),menuName,punch,price,quantity);
                                        list.setAdapter(adapter);*/

                /*int subtotal = 0;
                for (int j = 0; j < price.size(); j++) {
                    int priceCount = (int) Double.parseDouble(price.get(j)) * Integer.parseInt(quantity.get(j));
                    subtotal = subtotal + priceCount;
                    //subtotal=subtotal+Integer.parseInt(String.valueOf(Math.round(Double.valueOf(price.get(j)))))*Integer.parseInt(quantity.get(j));
                }*/
                subTotal.setText(currency + " " + String.valueOf(subTotalval));
                subTotalDis.setText(currency + " " + String.valueOf(subTotalval));

                double CGST = (double) 5 / (double) 100 * subTotalval;
                double SGST = (double) 15 / (double) 100 * subTotalval;
                tax = CGST + SGST;
                total = tax + subTotalval;
                Total.setText(currency + " " + String.valueOf(total));
                String lGST = String.valueOf(SGST).substring(0, String.valueOf(SGST).indexOf(".") + 2);
                String lCST = String.valueOf(CGST).substring(0, String.valueOf(CGST).indexOf(".") + 2);

//                sGST.setText(String.valueOf(lGST));
//                cGST.setText(String.valueOf(lCST));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}