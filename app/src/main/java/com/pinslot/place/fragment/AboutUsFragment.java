package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pinslot.place.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Selva on 10/12/2018.
 */

public class AboutUsFragment extends Fragment {
    TextView desc,readMore;
    LinearLayout descLayout;
    CircleImageView profileImage;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_us,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        desc=(TextView)view.findViewById(R.id.desc_text);
        readMore=(TextView)view.findViewById(R.id.read_more);
        descLayout=(LinearLayout) view.findViewById(R.id.desc_layout);
        profileImage=view.findViewById(R.id.image);
        profileImage.setScaleType(CircleImageView.ScaleType.CENTER_CROP);
        //desc.setText(R.string.desc);
        readMore.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if(readMore.getText().toString().equals("Read More")) {
                    desc.setMaxLines(Integer.MAX_VALUE);
                    ViewGroup.LayoutParams params = descLayout.getLayoutParams();
                    descLayout.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
                    descLayout.setLayoutParams(params);
                    readMore.setText("Less");

                }else{
                    readMore.setText("Read More");
                    desc.setMaxLines(3);

                }
            }
        });

    }
}
