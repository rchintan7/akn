package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.MyPagerSlidingStrip;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.helpers.GPSService;
import com.pinslot.place.helpers.Payment_Method;
import com.pinslot.place.model.Restaurant;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Restaurant_Menu_Items extends Fragment implements View.OnClickListener {
    public static String restaurantID, Description;
    public JSONArray menuJsonArray;
    public JSONArray menuJsonArrayFilter;
    public JSONObject reviewObject;

    private MyPagerSlidingStrip tabs;
    private ViewPager tab_pager;
    boolean isFav = false;

    Resources res;
    JSONArray jsonArray;
    View view;
    Button coupons, experience, photos, ratingBtn, all;
    public ArrayList<String> photosList = new ArrayList<>();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    TextView distance, opening, name, review_count, cousine,moreInfo,address;
    public static TextView tv_qty, tv_amt;
    LinearLayout checkOut, reviewBtn;
    Button dineBtn;
    ImageView fav, res_image, contact, calIcon;
    double lati = 0.0, langi = 0.0;
    String res_name = "", res_image_url = "";
    String fourcastId = "";
    ArrayList<String> opening_times = new ArrayList<>();
    String currency = "";
    JSONObject cartObj;
    JSONArray RestaurantReviews = new JSONArray();
    ArrayList<String>photo_urls = new ArrayList<>();
    String reviewType;
    String[] weeks = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    MenuRequestListener menuListener = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.restaurant_menu_items, container, false);
        photosList.clear();
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();

        ratingBtn = (Button) view.findViewById(R.id.rating_button);
        moreInfo = view.findViewById(R.id.moreInfo);
        all = (Button) view.findViewById(R.id.all);
        coupons = (Button) view.findViewById(R.id.coupons);
        experience = (Button) view.findViewById(R.id.experience);
        photos = (Button) view.findViewById(R.id.photos);
        dineBtn = (Button) view.findViewById(R.id.dine_btn);
        distance = (TextView) view.findViewById(R.id.distance);
        opening = (TextView) view.findViewById(R.id.open_now);
        name = (TextView) view.findViewById(R.id.restaurant_name);
        cousine = (TextView) view.findViewById(R.id.cuisine);
        address = view.findViewById(R.id.address);

        review_count = (TextView) view.findViewById(R.id.review_count);
        tv_qty = (TextView) view.findViewById(R.id.qty);
        tv_amt = (TextView) view.findViewById(R.id.amount);
        checkOut = (LinearLayout) view.findViewById(R.id.check_out);
        reviewBtn = (LinearLayout) view.findViewById(R.id.review_btn);
        res_image = (ImageView) view.findViewById(R.id.rest_image);
        contact = (ImageView) view.findViewById(R.id.contact);
        tabs = view.findViewById(R.id.tabs);
        tab_pager = view.findViewById(R.id.tab_pager);
        tab_pager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        if(((NavigationDrawerActivity)getActivity()).resReq.coupon!=null){
            tab_pager.setCurrentItem(2);
        }
        tabs.SetPager(tab_pager);
        tabs.setPadding(20.0f);
        tabs.setIndicatorColor(getResources().getColor(R.color.white));
        if(Build.VERSION.SDK_INT>19)
            tabs.setIndicatorHeight(0);
        else
            tabs.setIndicatorHeight(20);
        tabs.setTextColor(Color.WHITE);
        tabs.setSelectedTextColor(getResources().getColor(R.color.app_orange));
        tabs.setNormalTextColor(Color.WHITE);

        // Attach the page change listener to tab strip and **not** the view pager inside the activity
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
            }
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                tabs.setCurrentPosition(position);
                tabs.setCurrentPositionOffset(positionOffset);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        calIcon = (ImageView) view.findViewById(R.id.cal_icon);
        JSONParsing();
        fav = (ImageView) view.findViewById(R.id.fav);
        if (isFav)
            fav.setImageResource(R.mipmap.heart_fill);
        else
            fav.setImageResource(R.mipmap.rest_heart);
        opening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowAlert();
            }
        });
        dineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DineIn();
            }
        });
        name.setText(res_name);
        if (!res_image_url.isEmpty())
            Picasso.with(getActivity()).load(res_image_url).into(res_image);

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (cartObj != null && cartObj.getJSONArray("cartItems").length() > 0) {
                        OrderDetails fragment = new OrderDetails();
                        fragment.cartObj = cartObj;
                        Bundle args = new Bundle();
                        args.putString("resId", restaurantID);
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager
                                .beginTransaction();
                        fragmentTransaction.add(R.id.fragment_place, fragment);
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();
                    } else {
                        if (((NavigationDrawerActivity) getActivity()).RestaurantBookingType.equals("DineIn")) {
                            if(((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.menuItemsCount>0){
                                //Fragment fragment = new Payment_Method();
                                //Bundle args = new Bundle();
                                //args.putString("Type", "Restaurant");
                                ((NavigationDrawerActivity) getActivity()).resReq.total = 0.0;
                                ((NavigationDrawerActivity) getActivity()).resReq.tax = 0.0;
                                ((NavigationDrawerActivity) getActivity()).resReq.subTotalval = 0;
                                Payment_Method.BookRestaurant("","", ((NavigationDrawerActivity) getActivity()), "", new Payment_Method.BookingCallBack() {
                                    @Override
                                    public void bookingSucess(String bookingId, String token, JSONObject response) {
                                        Payment_Method.openRestaurantBookedScreen(response,getActivity());
                                    }

                                    @Override
                                    public void bookingFailed(String failed) {

                                    }
                                });
                            } else {
                                Fragment fragment = new RestaurantCheckin();
                                Bundle args = new Bundle();
                                args.putDouble("Total", 0.0);
                                args.putDouble("Tax", 0.0);
                                args.putInt("subTotal", 0);
                                args.putString("resId", restaurantID);
                                fragment.setArguments(args);
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager
                                        .beginTransaction();
                                fragmentTransaction.add(R.id.fragment_place, fragment);
                                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                                fragmentTransaction.commit();
                            }
                        } else if (((NavigationDrawerActivity) getActivity()).RestaurantBookingType.equals("TakeAway")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Alert");
                            builder.setMessage("your cart is empty!. please add item to your cart to Take Away.");
                            builder.setNeutralButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            builder.show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Alert");
                            builder.setMessage("your cart is empty!. So TAKE AWAY not posible. Do you want to DINEIN?");
                            builder.setPositiveButton("DINEIN", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ((NavigationDrawerActivity) getActivity()).RestaurantBookingType = "DineIn";
                                    Fragment fragment = new RestaurantCheckin();
                                    Bundle args = new Bundle();
                                    args.putDouble("Total", 0.0);
                                    args.putDouble("Tax", 0.0);
                                    args.putInt("subTotal", 0);
                                    args.putString("resId", restaurantID);
                                    fragment.setArguments(args);
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager
                                            .beginTransaction();
                                    fragmentTransaction.add(R.id.fragment_place, fragment);
                                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                                    fragmentTransaction.commit();
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            builder.show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });



        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*RestaurantReviewsFragment fragment = new RestaurantReviewsFragment();
                fragment.restaurantReviews = RestaurantReviews;
                fragment.reviewObject = reviewObject;
                fragment.restaurant = ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant;
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();*/
            }
        });

        experience.setOnClickListener(this);
        moreInfo.setOnClickListener(this);
        all.setOnClickListener(this);
        photos.setOnClickListener(this);
        coupons.setOnClickListener(this);
        webServiceCallForReviews();
        updateCartValueFromServer();

        return view;
    }

    public void setMenuListener(MenuRequestListener listener){
        menuListener = listener;
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        String[] titles = {"Menu"/*,"Experience","Coupon","Photo","Review"*/};
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public Fragment getItem(int position) {
            if(position ==0){
                return new RestaurantMenuFragment();
            } else if(position == 1) {
                /*----Fragment exp = new PlaceExpTab();
                Bundle args = new Bundle();
                args.putString("Type","res_exp");
                exp.setArguments(args);
                return exp;*/
            }else if(position == 2){
                /*----Fragment promotion = new PromotionalOffer();
                Bundle args = new Bundle();
                args.putString("navigationType", "Restaurant");
                promotion.setArguments(args);
                return promotion;*/
            }else if(position == 3){
                //----return PhotosFragment.newInstance(photosList);
            } else {
                /*----RestaurantReviewsFragment review = new RestaurantReviewsFragment();
                review.restaurantReviews = RestaurantReviews;
                review.reviewObject = reviewObject;
                review.restaurant = ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant;
                return review;*/
            }
            return new RestaurantMenuFragment();
        }
    }


    public void DineIn(){

        Fragment fragment = new RestaurantCheckin();
        ((NavigationDrawerActivity) getActivity()).RestaurantBookingType = "DineIn";
        ((NavigationDrawerActivity)getActivity()).resReq.requestType = "DineIn";
        fragment.setArguments(getArguments());
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.add(R.id.fragment_place, fragment);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.commit();

    }

    public void updateFav(boolean isFav) {
        this.isFav = isFav;
        if (isFav)
            fav.setImageResource(R.mipmap.heart_fill);
        else
            fav.setImageResource(R.mipmap.rest_heart);
    }

    public void ShowAlert() {

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout view = new LinearLayout(getActivity());
        view.setOrientation(LinearLayout.VERTICAL);
        view.setBackgroundResource(R.color.white);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);
        int week = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        String currentDay = weeks[week - 1];
        if (opening_times.size() > 0) {
            for (int i = 0; i < opening_times.size(); i++) {
                TextView textView = new TextView(getActivity());
                textView.setPadding((int) getpxfordp(16), (int) getpxfordp(5), (int) getpxfordp(16), (int) getpxfordp(5));
                textView.setSingleLine(true);
                textView.setMaxLines(1);
                textView.setTextSize(16);
                if (opening_times.get(i).contains(currentDay))
                    textView.setTextColor(getResources().getColor(R.color.app_orange));
                else
                    textView.setTextColor(Color.BLACK);
                textView.setText(opening_times.get(i));
                view.addView(textView);
            }
        }
        dialog.show();
    }

    public float getpxfordp(int dp) {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((NavigationDrawerActivity) getActivity()).selectedResGoogleDetail = null;
        ((NavigationDrawerActivity) getActivity()).RestaurantBookingType = "";
    }

    public void updateCartValue() {
        try {
            if (cartObj != null) {
                    /*if (cartobject.getString("businessId").equals(restaurantID)) {
                        isCheckOutDiff = false;
                    } else {
                        isCheckOutDiff = true;
                    }*/
                JSONArray cart_item_array = cartObj.getJSONArray("cartItems");
                Log.d("Cart Items", cart_item_array.toString());
                int qty = 0;
                double amt = 0;
                if (cart_item_array.length() > 0) {
                    for (int i = 0; i < cart_item_array.length(); i++) {
                        qty = qty + cart_item_array.getJSONObject(i).getInt("qty");
                        amt = amt + cart_item_array.getJSONObject(i).getDouble("finalPrice");
                        //Karthee given final price so we don't need to calculate with quantity, So we don't need to use below code.
                            /*if(!cart_item_array.getJSONObject(i).getString("restaurantMenuChos").equals("null")&&!cart_item_array.getJSONObject(i).getJSONObject("restaurantMenuChos").getString("menuchosPrice").equals("null"))
                                amt = amt + cart_item_array.getJSONObject(i).getInt("qty") * cart_item_array.getJSONObject(i).getJSONObject("restaurantMenuChos").getDouble("menuchosPrice");
                            else
                                amt = amt + cart_item_array.getJSONObject(i).getDouble("finalPrice");*/
                    }
                }
                tv_qty.setText("Qty : " + Integer.toString(qty));
                tv_amt.setText(currency + " " + Double.toString(amt));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void webServiceCallForReviews() {

        try {
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            String tag_json_obj = "json_obj_req";
            String url = Constants.REVIEWS + restaurantID + "/reviews";
            Log.d("Review Request", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    reviewObject = response.getJSONObject("data");
                                    pDialog.dismiss();
                                    //ratingBtn.setText(jsonObject.getString("rating"));
                                    if (!reviewObject.getString("totalReviews").equals("null"))
                                        review_count.setText(reviewObject.getString("totalReviews") + " Reviews");
                                    if (!reviewObject.getString("avgOverallRating").equals("null"))
                                        ratingBtn.setText(String.format("%.1f",reviewObject.getDouble("avgOverallRating")));
                                    // Service Call for Reviews
                                }
                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();

                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void updateCartValueFromServer() {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.GET_ORDER_LIST + "customer/" + pref.getString("userId", null) + "/business/" + restaurantID + "/cart";
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    cartObj = response.getJSONObject("data");
                                    //JSONArray jsonArray=data.getJSONArray("cartItems");
                                    updateCartValue();
                                }
                            } catch (Exception e) {
                                Log.d("Error :", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void onClick(View v) {
        if (v == moreInfo) {
            /*ResMoreInfoFragment fragment = new ResMoreInfoFragment();
            fragment.photo_urls = photosList;
            //new InfoFragment();
            Bundle args = new Bundle();
            args.putString("fourCastId", fourcastId);
            args.putString("Source", "Restaurant");
            args.putString("category", "Restaurant");
            fragment.setArguments(args);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();*/
        }
        if (v == photos) {
            /*---moreInfo.setBackgroundResource(R.drawable.round_cornor_blue);
            coupons.setBackgroundResource(R.drawable.round_cornor_blue);
            experience.setBackgroundResource(R.drawable.round_cornor_blue);
            photos.setBackgroundResource(R.drawable.round_cornor_blue_fill_solid);
            all.setBackgroundResource(R.drawable.round_cornor_blue);

            ArrayList<GalleryData> datas = new ArrayList<>();
            datas.add(new GalleryData("Photos",photosList));
            Fragment fragment = new GalleryFragment().newInstance(datas,0);
            //Fragment fragment = PhotosFragment.newInstance(photosList);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.add(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();*/
        }
        if (v == ratingBtn) {
            /*----Fragment fragment = new ReviewsFragment();
            Bundle args = new Bundle();
            args.putString("reviewType", reviewType);
            args.putString("restaurantId", Restaurant_Menu_Items.restaurantID);
            fragment.setArguments(args);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.replace(R.id.fragment_place, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();*/
        }
    }

    public void JSONParsing() {
        try {
            Restaurant resObj = ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant;
            //name.setText(jsonObject.getString("name"));
            if (resObj.source != null) {
                Log.d("Source", resObj.source);
            }
            cousine.setText(resObj.cuisines);
            restaurantID = resObj.id;
            editor.putString("restaurantId", restaurantID);
            editor.commit();
            Description = resObj.desc;
            res_name = resObj.name;
            address.setText(resObj.location.getAddress());
            if(!resObj.imageURL.isEmpty())
                res_image_url = resObj.imageURL;
            else if (resObj.bannerPhotoUrls.length > 0)
                res_image_url = resObj.bannerPhotoUrls[0];
            if (resObj.currency.equals("Rs."))
                currency = "₹";
            else
                currency = "$";
            if(resObj.managerProfile!=null){
                contact.setEnabled(true);
            } else {
                contact.setEnabled(false);
                contact.setColorFilter(Color.GRAY);
            }
            isFav = resObj.isFavourite;
            lati = resObj.location.coordinate.latitude;
            langi = resObj.location.coordinate.longitude;

            //String lati1=jsonObject.getJSONObject("geometry").getJSONObject("location").getString("lat");
            //String lng1=jsonObject.getJSONObject("geometry").getJSONObject("location").getString("lng");

            GPSService mGPSService = new GPSService(getActivity());
            mGPSService.getLocation();
            double latitude = mGPSService.getLatitude();
            double longitude = mGPSService.getLongitude();

            Location location1 = new Location("locationA");
            location1.setLatitude(latitude);
            location1.setLongitude(longitude);
            Location location2 = new Location("locationB");
            if (Double.valueOf(lati) > 0 || Double.valueOf(langi) > 0) {
                location2.setLatitude(Double.valueOf(lati));
                location2.setLongitude(Double.valueOf(langi));

                double distances = location1.distanceTo(location2) / 1000;
                distance.setText(String.format("%.2f",distances) + " " + "KM");
            } else {
                distance.setVisibility(View.GONE);
            }

            Constants.MANUAL = resObj.manual;
            Constants.FACEBOOK_OFFER = resObj.hasFacebookOffer;
            Constants.PROMO_CODE = resObj.dealsAvailable;

            photo_urls.clear();
            if (((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.menuCardImageUrls != null) {
                JSONArray jarray = ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.menuCardImageUrls;
                for (int i = 0; i < jarray.length(); i++) {
                    photo_urls.add(jarray.getJSONObject(i).getString("path"));
                }
            }
            if(menuListener!=null)
                menuListener.AddPhotos();

            if (Constants.MANUAL == true) {
                webserviceCallforRestaurant();
                placeWebServiceCall(res_name);
                reviewType = "Google";
            } else {
                String opendata = "";
                reviewType = "PinSlots";
                placeWebServiceCall(res_name);
                /*if (jsonObject.has("open")) {
                    if (!jsonObject.getString("open").equals("null")) {
                        opendata = jsonObject.getJSONObject("open").getString("day") + " " + jsonObject.getJSONObject("open").getString("checkInOne") + "-" + jsonObject.getJSONObject("open").getString("checkOutOne");
                        try {
                            if (!(jsonObject.getJSONObject("open").getString("checkInTwo").equals("") || jsonObject.getJSONObject("open").getString("checkInTwo").equals("null"))) {
                                opendata = opendata + " & " + jsonObject.getJSONObject("open").getString("checkInTwo") + "-" + jsonObject.getJSONObject("open").getString("checkOutTwo");
                            }
                        } catch (JSONException e) {
                        }
                    }
                }*/
                try {
                    JSONArray timings = null;
                    if(((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.storeOpen!=null)
                        timings = ((NavigationDrawerActivity)getActivity()).resReq.selectedRestaurant.storeOpen;
                    if(timings!=null){
                        opening_times.clear();
                        for(int i =0;i<timings.length();i++){
                            String opentime = timings.getJSONObject(i).getString("day")+": ";
                            if(timings.getJSONObject(i).has("checkInOne")){
                                opentime = opentime + timings.getJSONObject(i).getString("checkInOne");
                            }
                            if(timings.getJSONObject(i).has("checkOutOne"))
                                opentime = opentime + " - " +timings.getJSONObject(i).getString("checkOutOne");
                            if(timings.getJSONObject(i).has("checkInTwo"))
                                opentime = opentime + ", " +timings.getJSONObject(i).getString("checkInTwo");
                            if(timings.getJSONObject(i).has("checkOutTwo"))
                                opentime = opentime + " - " +timings.getJSONObject(i).getString("checkOutTwo");
                            opening_times.add(opentime);
                            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                            if(opentime.toUpperCase().contains(sdf.format(Calendar.getInstance().getTime()).toUpperCase()))
                                opendata = opentime;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!opendata.isEmpty()) {
                    opening.setText(opendata);
                    opening.setVisibility(View.VISIBLE);
                    calIcon.setVisibility(View.VISIBLE);
                }
                webServiceCallForMenuItem();
            }

            for (int i = 0; i < resObj.bannerPhotoUrls.length; i++) {
                photosList.add(resObj.bannerPhotoUrls[i]);
            }
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        }
    }

    public void webServiceCallForMenuItem() {

        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.MENU_ITEM + restaurantID;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Menu Request", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    JSONObject data = new JSONObject(response.toString());
                                    menuJsonArray = data.getJSONArray("data");
                                    if (menuJsonArray.length() > 0)
                                        checkOut.setVisibility(View.VISIBLE);
                                    pDialog.dismiss();
                                    // Call Filter Items
                                    webServiceCallForMenuFilter();


                                }
                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void webServiceCallForMenuFilter() {

        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.MENU_ITEM_FILTER + restaurantID + "/menuitems/filter";
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Menu Request", url.toString());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("searchText", "");
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    JSONObject data = new JSONObject(response.toString());
                                    menuJsonArrayFilter = data.getJSONObject("data").getJSONArray("menuItems");
                                    if (menuJsonArrayFilter.length() > 0)
                                        checkOut.setVisibility(View.VISIBLE);
                                    pDialog.dismiss();
                                    if(menuListener!=null)
                                        menuListener.updateList();

                                }
                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            if(menuListener!=null)
                menuListener.updateList();
            System.out.println("Error :" + e.toString());
        }
    }

    public void placeWebServiceCall(String SearchString) {
        try {
            SearchString = SearchString.toLowerCase().replace(" ", "%20");
            String search_type = "";

            String tag_json_obj = "json_obj_req";
            //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.0827,80.2707&radius=5000&types=Restaurant&name=Al%20Reef&key=AIzaSyD_NimyNx1PGBxVbS0W1Aw3iPcI-2qSQ6U
            //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=12.9162717,80.15222935&types=Restaurant&radius=5000&name=rannaghor&key=AIzaSyD_NimyNx1PGBxVbS0W1Aw3iPcI-2qSQ6U
            String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lati + "," + langi + "&types=" + "Restaurant" + "&radius=5000&name=" + SearchString + "&key="+getString(R.string.place_web_api_key);
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("URL", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pDialog.dismiss();
                                Log.d("Response", response.toString());
                                JSONObject jsonObject = response.getJSONArray("results").getJSONObject(0);
                                String placeID = jsonObject.getString("place_id");
                                placeDetailsCall(placeID);
                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error.toString());

                    Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                    //webServiceCall();

                    pDialog.dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void webServiceCallforDownloadPhoto(String id) {
        fourcastId = id;
        String tag_json_obj = "json_obj_req";
        String url = "https://api.foursquare.com/v2/venues/" + id + "/photos?client_id=JMSXBBV2TACV03BMA4QAZAKIRHRSV2SV4EYICJZ14KDRCA3B&client_secret=4A23XFIX31PNQRMQKLJUC13JIDYN4ZR3ND2044PXSLQCGMSY&v=20170712";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, "",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONObject("response").getJSONObject("photos").getJSONArray("items");
                            if (results.length() > 0) {
                                //photosList.clear();
                                //String id = results.getJSONObject(0).getString("id");
                                for (int i = 0; i < results.length(); i++) {
                                    String url = results.getJSONObject(i).getString("prefix") + "300x300" + results.getJSONObject(i).getString("suffix");
                                    photosList.add(url);
                                }
                                Log.d("Photo List", photosList.toString());
                            }
                        } catch (Exception e) {
                            Log.d("Error", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", error.toString());

                Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                //webServiceCall();

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void webserviceCallforRestaurant() {
        String SearchString = res_name.toLowerCase().replace(" ", "%20");
        String tag_json_obj = "json_obj_req";
        String url = "https://api.foursquare.com/v2/venues/search?client_id=JMSXBBV2TACV03BMA4QAZAKIRHRSV2SV4EYICJZ14KDRCA3B&client_secret=4A23XFIX31PNQRMQKLJUC13JIDYN4ZR3ND2044PXSLQCGMSY&v=20170712" +
                "&ll=" + lati + "," + langi + "&query=" + SearchString + "&radius=10000";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, "",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONObject("response").getJSONArray("venues");
                            if (results.length() > 0) {
                                String id = results.getJSONObject(0).getString("id");
                                webServiceCallforDownloadPhoto(id);
                            }
                        } catch (Exception e) {
                            Log.d("Error", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error", error.toString());

                Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                //webServiceCall();

            }
        });

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    public void placeDetailsCall(String placeId) {
        try {
            String search_type = "";

            String tag_json_obj = "json_obj_req";

            String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeId + "&key="+getString(R.string.place_web_api_key);
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("URL", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pDialog.dismiss();
                                Log.d("Response", response.toString());
                                JSONObject jsonObject = response.getJSONObject("result");
                                ((NavigationDrawerActivity) getActivity()).selectedResGoogleDetail = jsonObject;
                                if(res_name.isEmpty())
                                    name.setText(jsonObject.getString("name"));

                                int week = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
                                String currentDay = weeks[week - 1];
                                JSONArray weekdays = jsonObject.getJSONObject("opening_hours").getJSONArray("weekday_text");
                                String OpenDetail = "";
                                if(opening_times.size()==0) {
                                    if (weekdays.length() > 0) {
                                        OpenDetail = "close Now : " + currentDay;
                                        for (int i = 0; i < weekdays.length(); i++) {
                                            opening_times.add(weekdays.getString(i));
                                            if (weekdays.getString(i).contains(currentDay)) {
                                                OpenDetail = "Open Now : " + weekdays.getString(i);
                                            }
                                        }
                                    } else {
                                    }
                                    if (!OpenDetail.isEmpty()) {
                                        opening.setText(OpenDetail);
                                        opening.setVisibility(View.VISIBLE);
                                        calIcon.setVisibility(View.VISIBLE);
                                    }
                                }
                                RestaurantReviews = jsonObject.getJSONArray("reviews");
                                Log.d("Review Details", RestaurantReviews.toString());

                                review_count.setText(String.valueOf(RestaurantReviews.length()) + " Reviews");
                                if (!jsonObject.getString("photos").equals("null")) {
                                    JSONArray photo_Array = jsonObject.getJSONArray("photos");
                                    if (photo_Array.length() > 0) {
                                        photosList.clear();
                                        for (int i = 0; i < photo_Array.length(); i++) {
                                            String photo_url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + photo_Array.getJSONObject(i).getString("photo_reference") + "&key="+getString(R.string.place_web_api_key);
                                            photosList.add(photo_url);
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error.toString());

                    Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                    //webServiceCall();

                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    interface MenuRequestListener {
        void updateList();
        void AddPhotos();
    }

}
