package com.pinslot.place.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.R;
import com.pinslot.place.adapter.RestaurantReviewsAdapter;
import com.pinslot.place.model.Restaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by New System 2 on 7/17/2017.
 */

public class RestaurantReviewsFragment extends Fragment {

    ListView list;
    Button writeReview;
    SharedPreferences pref;
    TextView totalReview;
    public JSONArray restaurantReviews;
    public JSONArray googleReviews;
    public JSONObject reviewObject;
    public Restaurant restaurant;
    RatingBar ratingBar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getContext().getTheme().applyStyle(R.style.AppTheme, true);
        View view = inflater.inflate(R.layout.fragment_restaurant_review, container, false);
        list = (ListView) view.findViewById(R.id.listView);
        totalReview = (TextView) view.findViewById(R.id.total_review);
        writeReview = (Button) view.findViewById(R.id.write_review);
        ratingBar = view.findViewById(R.id.rating_bar);
        TextView tv_title = view.findViewById(R.id.title);
        if(getArguments()!=null&&!getArguments().getString("title","").isEmpty()){
            tv_title.setVisibility(View.VISIBLE);
            tv_title.setText(getArguments().getString("title"));
        }

        writeReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
                Write_Review(getActivity(),restaurant.id,pref.getString("userId", null));
            }
        });
        if(reviewObject!=null&&restaurantReviews!=null) {
            AssignValues();
        } else if(restaurant!=null){
            webServiceCallForReviews(restaurant.id);
            placeWebServiceCall(restaurant.name,restaurant.location.coordinate.latitude,restaurant.location.coordinate.longitude);
        }

        return view;
    }

    public void AssignValues(){
        if(reviewObject!=null){
            try {
                Log.d("PinSlots Reviews", reviewObject.toString());
                Log.d("Google Reviews",restaurantReviews.toString());

                googleReviews = reviewObject.getJSONArray("reviews");
                totalReview.setText(String.format("%.2f",reviewObject.getDouble("avgOverallRating")));
                ratingBar.setRating((float) reviewObject.getDouble("avgOverallRating"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        RestaurantReviewsAdapter adapter = new RestaurantReviewsAdapter(getActivity(), googleReviews,restaurantReviews);
        list.setAdapter(adapter);
    }

    public void webServiceCallForReviews(String id) {

        try {
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            String tag_json_obj = "json_obj_req";
            String url = Constants.REVIEWS + id + "/reviews";
            Log.d("Review Request", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    reviewObject = response.getJSONObject("data");
                                    pDialog.dismiss();
                                    if(reviewObject!=null&&restaurantReviews!=null) {
                                        AssignValues();
                                    }
                                    // Service Call for Reviews
                                }
                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();

                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void placeWebServiceCall(String SearchString, double lati, double langi) {
        try {
            SearchString = SearchString.toLowerCase().replace(" ", "%20");
            String tag_json_obj = "json_obj_req";
            String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lati + "," + langi + "&types=" + "Restaurant" + "&radius=5000&name=" + SearchString + "&key="+getString(R.string.place_web_api_key);
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("URL", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pDialog.dismiss();
                                Log.d("Response", response.toString());
                                if(response.getJSONArray("results").length()>0) {
                                    JSONObject jsonObject = response.getJSONArray("results").getJSONObject(0);
                                    String placeID = jsonObject.getString("place_id");
                                    placeDetailsCall(placeID);
                                } else {
                                    restaurantReviews = new JSONArray();
                                }
                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error.toString());

                    Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                    //webServiceCall();

                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void placeDetailsCall(String placeId) {
        try {
            String search_type = "";

            String tag_json_obj = "json_obj_req";

            String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeId + "&key="+getString(R.string.place_web_api_key);
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("URL", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pDialog.dismiss();
                                Log.d("Response", response.toString());
                                JSONObject jsonObject = response.getJSONObject("result");
                                if(jsonObject.has("reviews"))
                                    restaurantReviews = jsonObject.getJSONArray("reviews");
                                else
                                    restaurantReviews = new JSONArray();
                                if(reviewObject!=null&&restaurantReviews!=null) {
                                    AssignValues();
                                }
                            } catch (Exception e) {
                                Log.d("Error", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error.toString());

                    Toast.makeText(getActivity(), "Connectivity issue, Please wait", Toast.LENGTH_LONG).show();
                    //webServiceCall();

                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }


    public void Write_Review(final Context act, final String businessId, final String userId){
        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rate_reviews);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);

        LinearLayout subRating = dialog.findViewById(R.id.sub_rating);
        LinearLayout starRating = dialog.findViewById(R.id.star_rating);
        final EditText comments = dialog.findViewById(R.id.comments);
        final Button submit = dialog.findViewById(R.id.submit);
        final RatingBar ratingBar = dialog.findViewById(R.id.ratingBar);
        subRating.setVisibility(View.GONE);
        starRating.setVisibility(View.VISIBLE);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String url = "";
                    String tag_json_obj = "json_obj_req";
                    final ProgressDialog pDialog = new ProgressDialog(act);
                    pDialog.setMessage("Loading...");
                    pDialog.show();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("text", comments.getText().toString());
                    jsonObject.put("userId", userId);
                    url = Constants.WRITE_RESTAURANT_REVIEW;
                    jsonObject.put("restaurantId", businessId);
                    jsonObject.put("rating", ratingBar.getRating());
                    Log.d("Write Request", jsonObject.toString());

                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                            url, jsonObject,
                            new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        Log.d("Write Response", response.toString());
                                        pDialog.dismiss();

                                        if (response.getString("status").equals("SUCCESS")) {
                                            dialog.dismiss();
                                            Toast.makeText(act, "Review Successfully Added", Toast.LENGTH_LONG).show();
                                            webServiceCallForReviews(restaurant.id);
                                        } else {
                                            Toast.makeText(act, "Failed", Toast.LENGTH_LONG).show();
                                        }

                                    } catch (Exception e) {

                                    }
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            pDialog.dismiss();
                        }
                    });

                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                } catch (JSONException e){

                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
