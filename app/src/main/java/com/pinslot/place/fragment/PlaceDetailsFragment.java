package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.adapter.ImagePagerAdapter;
import com.pinslot.place.model.CSPlace;

import org.json.JSONObject;

import java.util.ArrayList;

public class PlaceDetailsFragment extends BaseFragment{

    ViewPager banner_images;
    PagerSlidingTabStrip tabs;
    ViewPager tab_pager;
    CSPlace placeData;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_details,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try{
            placeData=new CSPlace(new JSONObject(getArguments().getString("JSON")));
        }catch (Exception e){
        }
        banner_images = view.findViewById(R.id.banner_images);
        tabs = view.findViewById(R.id.tabs);
        tab_pager = view.findViewById(R.id.tab_pager);
        banner_images.setAdapter(new ImagePagerAdapter(placeData.galleries,getActivity()));
        tab_pager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        tabs.setShouldExpand(true);
        tabs.setViewPager(tab_pager);
        tabs.setIndicatorColorResource(R.color.colorAccent);
        tabs.setTextColorResource(R.color.gray);
        tabs.setIndicatorHeight((int) Utils.getPXforDP(3, getContext()));
        tabs.setTextSize((int) Utils.getPXforDP(12, getContext()));
        tabs.setDividerColorResource(R.color.transparent);
        tab_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setStripSelectedColor(position, getResources().getColor(R.color.exp_theme), getResources().getColor(R.color.gray));
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    public void setStripSelectedColor(int position, int selectedTextColor, int normalTextColor) {
        if (tabs.getChildCount() > 0) {
            LinearLayout tabsContainer = (LinearLayout) tabs.getChildAt(0);
            if (tabsContainer.getChildCount() > 0) {
                for (int i = 0; i < tabsContainer.getChildCount(); i++) {
                    View v = tabsContainer.getChildAt(i);
                    if (v instanceof TextView) {
                        if (i == position)
                            ((TextView) v).setTextColor(selectedTextColor);
                        else
                            ((TextView) v).setTextColor(normalTextColor);
                    }
                }
            }
        }
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        private String[] TITLES = {"Details", "Gallery", "Rating"};
        ArrayList<String> items = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            items.clear();
            for (int i = 0; i < TITLES.length; i++) {
                items.add(TITLES[i]);
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return items.get(position);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Fragment getItem(int position) {
            switch (items.get(position)) {
                case "Details":
                    return PlaceInfoFragment.instance(getArguments().getString("JSON"));
                case "Gallery":
                    return PlacePhotosFragment.instance(getArguments().getString("JSON"));
                case "Rating":
                    Fragment rating = new placeRatingFragment();
                    return rating;
                    default:
                    return new PlaceInfoFragment();
            }
        }
    }
}
