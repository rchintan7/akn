package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.pinslot.place.R;
import com.pinslot.place.adapter.FacilityAdapter;

import java.util.ArrayList;

/**
 * Created by Selva on 10/24/2018.
 */

public class AboutFacilities extends Fragment {
    GridView list;
    String arr[]={"Outdoor","WIFI Area","Buffet","Lounge","Pool Area","Live Music","Game Zone","Barbeque"};
    int image[]={R.mipmap.outdoor,R.mipmap.wifi,R.mipmap.buffet,R.mipmap.lounge,R.mipmap.pool,R.mipmap.live_musics,R.mipmap.game,R.mipmap.barbeque};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_facilities, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = (GridView) view.findViewById(R.id.grid_view);
        list.setAdapter(new FacilityAdapter(AboutFacilities.this,arr,image));

    }
}
