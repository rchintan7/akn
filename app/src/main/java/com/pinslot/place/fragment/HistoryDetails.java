package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pinslot.place.R;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Selva on 10/25/2018.
 */

public class HistoryDetails extends Fragment {
    TextView name,guest,size,price,dateStr,status,user_name,email,reservedDate,orderId;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.history_details,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        name=view.findViewById(R.id.name);
        guest=view.findViewById(R.id.guest);
        price=view.findViewById(R.id.price);
        dateStr=view.findViewById(R.id.date);
        status=view.findViewById(R.id.status);
        user_name=view.findViewById(R.id.user_name);
        email=view.findViewById(R.id.email);
        reservedDate=view.findViewById(R.id.reserved_date);
        size=view.findViewById(R.id.size);
        orderId=view.findViewById(R.id.order_id);

        try{
            JSONObject jsonObject=new JSONObject(getArguments().getString("JSON"));
            Date date = new Date(jsonObject.getLong("checkIn"));
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            String stringDate = sdf.format(date);
            dateStr.setText(stringDate);
            reservedDate.setText(stringDate);
            name.setText(jsonObject.getJSONObject("place").getString("name"));
            orderId.setText("Order ID :"+jsonObject.getString("bookingId"));
            String currency="";
            if(jsonObject.getString("currencyCode").equals("RS")||jsonObject.getString("currencyCode").equals("INR"))
                currency= "₹";
            else
                currency= "₹";
            price.setText(currency+""+jsonObject.getString("finalPrice"));
            status.setText(jsonObject.getString("status"));

            user_name.setText(jsonObject.getString("name"));
            email.setText(jsonObject.getString("email"));

        }catch (Exception e){

        }

        try{
            JSONObject jsonObject=new JSONObject(getArguments().getString("JSON"));
            name.setText(jsonObject.getString("name"));
            guest.setText("Guest Count :"+jsonObject.getString("guestCount"));
            //size.setText("Room Size :"+jsonObject.getString(""));
            price.setText("");
        }catch (Exception e){

        }

    }
}
