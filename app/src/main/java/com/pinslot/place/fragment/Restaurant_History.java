package com.pinslot.place.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.R;
import com.pinslot.place.adapter.PlaceHistoryAdapter;
import com.pinslot.place.adapter.RestaurantHistoryAdapter;
import com.pinslot.place.widget.Calendar_View_Greetings_History;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

public class Restaurant_History extends BaseFragment {

    ProgressBar please;
    ListView list;
    public static HashSet<Date> events;
    Calendar currentDate;
    SharedPreferences pref;
    View view;
    BaseAdapter adapter = null;
    LinearLayout cal_parent;
    ImageView arrow;
    boolean is_colapsed = true;
    Calendar_View_Greetings_History calenderview;
    TextView space,exp;
    String selectedType = "ROOM";
    JSONObject JSONValue;
    ArrayList<JSONObject> hisItems = new ArrayList<>();
    ArrayList<JSONObject> selectedItems = new ArrayList<>();
    //----ArrayList<PlaceExpBookedDetail> exps = new ArrayList<>();
    //----ArrayList<PlaceExpBookedDetail> selected_exps = new ArrayList<>();
    Date selectedDate = null;
    LinearLayout no_result;

    public static Restaurant_History newInstance(int position, String category_name) {
        Restaurant_History f = new Restaurant_History();
        Bundle b = new Bundle();
        b.putString("category_key", category_name);
        f.setArguments(b);
        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.restaurent_history,container,false);
        please=view.findViewById(R.id.please);
        list = (ListView)view.findViewById(R.id.event_list);
        list.setVisibility(View.GONE);
        please.setVisibility(View.VISIBLE);
        arrow = (ImageView) view.findViewById(R.id.arrow);
        cal_parent = (LinearLayout)view.findViewById(R.id.calender_parent);
        calenderview = view.findViewById(R.id.calendar_view);
        space = view.findViewById(R.id.space);
        exp = view.findViewById(R.id.exp);
        no_result = view.findViewById(R.id.no_result);
        is_colapsed = true;
        currentDate = Calendar.getInstance();
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        space.setText("Rooms");

        Calendar_View_Greetings_History.btnNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //CalendarViewHistory.currentDate.add(Calendar.MONTH, 1);
                currentDate.add(Calendar.MONTH, 1);
                webServiceCallForHistory();
            }
        });

        // subtract one month and refresh UI
        Calendar_View_Greetings_History.btnPrev.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //CalendarViewHistory.currentDate.add(Calendar.MONTH, -1);
                currentDate.add(Calendar.MONTH, -1);
                webServiceCallForHistory();
            }
        });
        space.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedType = "ROOM";
                space.setBackgroundResource(R.drawable.rounded_white_button);
                space.setTextColor(Color.BLACK);
                exp.setBackgroundResource(R.color.transparent);
                exp.setTextColor(Color.WHITE);
                webServiceCallForHistory();
            }
        });

        if (getArguments()!=null&&getArguments().getString("category_key","").equals("res_exp")) {
            selectedType = "WITHOUT_ROOM";
            exp.setBackgroundResource(R.drawable.rounded_white_button);
            exp.setTextColor(Color.BLACK);
            space.setBackgroundResource(R.color.transparent);
            space.setTextColor(Color.WHITE);
            webServiceCallForExpHistory();
        } else {
            webServiceCallForHistory();
        }

        exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedType = "WITHOUT_ROOM";
                exp.setBackgroundResource(R.drawable.rounded_white_button);
                exp.setTextColor(Color.BLACK);
                space.setBackgroundResource(R.color.transparent);
                space.setTextColor(Color.WHITE);
                webServiceCallForHistory();
            }
        });

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(is_colapsed==true){
                    cal_parent.getLayoutParams().height = (int)getpxfordp(270);
                    calenderview.getLayoutParams().height = (int)getpxfordp(230);
                    cal_parent.requestLayout();
                    calenderview.requestLayout();
                    arrow.setImageResource(R.mipmap.arrow_up);
                    is_colapsed = false;
                    Log.d("Check","False");

                } else {
                    is_colapsed = true;
                    Log.d("Check","True");
                    cal_parent.getLayoutParams().height = (int)getpxfordp(150);
                    calenderview.getLayoutParams().height = (int)getpxfordp(110);
                    cal_parent.requestLayout();
                    calenderview.requestLayout();
                    arrow.setImageResource(R.mipmap.arrow_down);
                }

            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(adapter instanceof RestaurantHistoryAdapter) {
                    JSONObject obj = (JSONObject) adapter.getItem(position);
                    try {
                        android.support.v4.app.Fragment fragment = new RestaurantHistoryDetailFragment();
                        Bundle args = new Bundle();
                        args.putString("id", obj.getString("id"));
                        args.putString("json", obj.toString());
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager
                                .beginTransaction();
                        fragmentTransaction.add(R.id.fragment_place, fragment, fragment.getClass().getName());
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    JSONObject obj = (JSONObject) adapter.getItem(position);
                    if(selectedType.equals("ROOM")) {
                        addFragment(PlaceHistoryDetailsFragment.getInstance(obj), true);
                    } else {
                        Fragment fragment = new Places_History_Details();
                        Bundle args = new Bundle();
                        args.putString("jsonResponse", obj.toString());
                        fragment.setArguments(args);
                        addFragment(fragment,true);
                    }
                    /*---PlaceExpBookedDetail obj = (PlaceExpBookedDetail) adapter.getItem(position);
                    android.support.v4.app.Fragment fragment = new RestaurantHistoryDetailFragment();
                    Bundle args = new Bundle();
                    args.putString("Type","exp");
                    args.putString("json", obj.srcJson.toString());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager
                            .beginTransaction();
                    fragmentTransaction.add(R.id.fragment_place, fragment, fragment.getClass().getName());
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();*/
                }
            }
        });

        calenderview.setItemClickListener(new Calendar_View_Greetings_History.ItemClickListener() {
            @Override
            public void ItemClick(Date clickedDate, int position, boolean select) {
                selectedDate = clickedDate;
                selectedItems.clear();
                //----selected_exps.clear();
                //if(selectedType.equals("menu")) {
                    for (int i = 0; i < hisItems.size(); i++) {
                        JSONObject jsonObject = hisItems.get(i);
                        Date date = null;
                        try {
                            date = new Date(jsonObject.getLong("pickupDate"));
                        } catch (JSONException e) {

                        }
                        if (isSameDate(clickedDate, date))
                            selectedItems.add(jsonObject);

                    }
                    if (selectedItems.size() > 0) {
                        no_result.setVisibility(View.GONE);
                        list.setVisibility(View.VISIBLE);
                        adapter = new RestaurantHistoryAdapter(getActivity(), selectedItems, Restaurant_History.this);
                        list.setAdapter(adapter);
                    } else {
                        list.setVisibility(View.GONE);
                        no_result.setVisibility(View.VISIBLE);
                    }
                //} else{
                    /*---for(PlaceExpBookedDetail detail: exps){
                        if(isSameDate(clickedDate,new Date(detail.slot.startDateTime)))
                            selected_exps.add(detail);
                    }
                    if (selected_exps.size() > 0) {
                        list.setVisibility(View.VISIBLE);
                        no_result.setVisibility(View.GONE);
                        adapter = new ResExpHistoryAdapter(selected_exps,Restaurant_History.this);
                        list.setAdapter(adapter);
                    } else {
                        list.setVisibility(View.GONE);
                        no_result.setVisibility(View.VISIBLE);
                    }*/
                //}
            }
        });

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return view;
    }

    public boolean isSameDate(Date d1, Date d2){
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(d1);
        cal2.setTime(d2);
        if(cal1.get(Calendar.YEAR)==cal2.get(Calendar.YEAR)&&cal1.get(Calendar.MONTH)==cal2.get(Calendar.MONTH)&&cal1.get(Calendar.DATE)==cal2.get(Calendar.DATE))
            return true;
        else
            return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data.getData().toString().contains("image") || data.getData().toString().contains("photo"))
            {
                String path = getRealPathFromURI(data.getData());
                if(path!=null&&!path.isEmpty()){
                    shareImage(path);
                }
            }
            else
                Toast.makeText(getActivity(), "Please select valid image file", Toast.LENGTH_SHORT).show();
        }
        if(requestCode == 0 && resultCode == Activity.RESULT_OK) {
            if(adapter instanceof RestaurantHistoryAdapter)
                shareImage(((RestaurantHistoryAdapter)adapter).CaptureImagepath);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length>0) {
            if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                imageIntent.setType("image/*");
                this.startActivityForResult(Intent.createChooser(imageIntent, "Select photo"), 1);
            } else if ((requestCode == 2 && grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                    || (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    || (requestCode == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                /*File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Image_" + System.currentTimeMillis() + ".png");
                Uri outputfileuri = Uri.fromFile(file);
                if(adapter instanceof RestaurantHistoryAdapter)
                    ((RestaurantHistoryAdapter)adapter).CaptureImagepath = outputfileuri.getPath();
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputfileuri);
                this.startActivityForResult(intent, 0);*/
            }
        }
    }

    public float getpxfordp(int dp) {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public void shareImage(String path){
        if(adapter instanceof RestaurantHistoryAdapter && ((RestaurantHistoryAdapter)adapter).shareImageRestaurant!=null) {
            String rec_id = "";
            String res_name = "";
            String res_address = "";
            String dateandTime = "";
            JSONObject res_detail = null;
            try {
                res_detail = ((RestaurantHistoryAdapter)adapter).shareImageRestaurant.getJSONObject("restaurant");
                JSONObject res_location = res_detail.getJSONObject("location");
                if(((RestaurantHistoryAdapter)adapter).shareImageRestaurant.getString("bookingId")!=null) {
                    rec_id = ((RestaurantHistoryAdapter)adapter).shareImageRestaurant.getString("bookingId");
                }
                if(res_detail!=null&&res_detail.getString("name")!=null){
                    res_name = res_detail.getString("name");
                }
                if(res_location!=null && res_location.getString("addressString")!=null) {
                    res_address = res_location.getString("addressString");
                }
                Date date = new Date(((RestaurantHistoryAdapter)adapter).shareImageRestaurant.getLong("pickupDate"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy hh:mm a", Locale.ENGLISH);
                dateandTime = sdf.format(date);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SendEmail(getActivity(), "receipts@pinslots.com", "", "Order Receipt attachment to get rebate", "Please find the attached screenshot of order receipt. \n Order Id : " + rec_id
                    + "\nPlaceName: " + res_name + "\n Address: " + res_address + "\nDate&Time: "+dateandTime, path);
        } else {
            /*---String rec_id = "";
            String res_name = "";
            String res_address = "";
            String dateandTime = "";
            PlaceExpBookedDetail exp = ((ResExpHistoryAdapter)adapter).selectedExp;
            rec_id = exp.bookingId;
            res_name = exp.experience.title;
            res_address = exp.restaurant.location.addressString;
            Date date = new Date(exp.slot.startDateTime);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy hh:mm a", Locale.ENGLISH);
            dateandTime = sdf.format(date);
            SendEmail(getActivity(), "receipts@pinslots.com", "", "Order Receipt attachment to get rebate", "Please find the attached screenshot of order receipt. \n Order Id : " + rec_id
                    + "\nPlaceName: " + res_name + "\n Address: " + res_address + "\nDate&Time: "+dateandTime, path);*/
        }
    }

    public String getRealPathFromURI(Uri contentUri)
    {
        String[] proj = { MediaStore.Audio.Media.DATA };
        android.database.Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static void SendEmail(Context context, String emailTo, String emailCC, String subject, String emailText, String Attachment_path)
    {
        final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailTo});
        emailIntent.putExtra(android.content.Intent.EXTRA_CC, new String[]{emailCC});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailText);
        ArrayList<Uri> uris = new ArrayList<Uri>();
        File fileIn = new File(Attachment_path);
        Uri u = Uri.fromFile(fileIn);
        uris.add(u);
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void webServiceCallForHistory() {
        Date nextMonthFirstDay = null, nextMonthLastDay = null;
        String startDate, endDate;
        currentDate.set(Calendar.DATE, currentDate.getActualMinimum(Calendar.DAY_OF_MONTH));
        nextMonthFirstDay = currentDate.getTime();
        currentDate.set(Calendar.DATE, currentDate.getActualMaximum(Calendar.DAY_OF_MONTH));
        nextMonthLastDay = currentDate.getTime();
        DateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        startDate = sdf.format(nextMonthFirstDay) + " 12:05 AM";
        endDate = sdf.format(nextMonthLastDay) + " 11:55 PM";
        try {
            String tag_json_obj = "json_obj_req";
            please.setVisibility(View.VISIBLE);
            no_result.setVisibility(View.GONE);
            list.setVisibility(View.GONE);
            String url = Constants.PLACES_HISTORY;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("startDateTimeStr", startDate);
            jsonObject.put("endDateTimeStr", endDate);
            jsonObject.put("searchText", "");
            jsonObject.put("spaceFilterType",selectedType);
            JSONObject pagination = new JSONObject();
            pagination.put("pageNo", 1);
            pagination.put("limit", 10);
            JSONObject sort = new JSONObject();
            sort.put("fieldName", "createOn");
            sort.put("sortType", "DESC");
            jsonObject.put("sort", sort);
            url = url + pref.getString("userId", null);
            Log.d("Request", jsonObject + url);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("Response", response.toString());
                                JSONValue = response;
                                if (response.getJSONObject("data") != null) {
                                    JSONObject data = response.getJSONObject("data");
                                    if (data.getJSONArray("orders").length() > 0) {
                                        please.setVisibility(View.GONE);
                                        list.setVisibility(View.VISIBLE);
                                        hisItems = getArrayList(data.getJSONArray("orders"));
                                        adapter = new PlaceHistoryAdapter(getActivity(), hisItems, Restaurant_History.this);
                                        list.setAdapter(adapter);
                                        events = new HashSet<>();
                                        try {
                                            for (int i = 0; i < data.getJSONArray("orders").length(); i++) {
                                                JSONObject jsonObject = data.getJSONArray("orders").getJSONObject(i);
                                                JSONArray placearray = jsonObject.getJSONArray("bookedDates");
                                                if (placearray.length() > 0 && (placearray.getJSONObject(0).getString("checkIn") != null && !placearray.getJSONObject(0).getString("checkIn").equals("null"))) {
                                                    Date date = new Date(placearray.getJSONObject(0).getLong("checkIn"));
                                                    events.add(date);
                                                } else {
                                                    Date date = new Date(jsonObject.getLong("createOn"));
                                                    events.add(date);
                                                }
                                                Log.d("Events Date :", events.toString());
                                            }
                                            Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
                                            cv.updateCalendar(currentDate, events);

                                        } catch (Exception e) {
                                        }
                                    } else {
                                        Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
                                        cv.updateCalendar(currentDate, events);
                                        please.setVisibility(View.GONE);
                                        list.setVisibility(View.GONE);
                                        no_result.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
                                    cv.updateCalendar(currentDate, events);
                                    please.setVisibility(View.GONE);
                                    list.setVisibility(View.GONE);
                                    no_result.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Calendar_View_Greetings_History cv = ((Calendar_View_Greetings_History) view.findViewById(R.id.calendar_view));
                    cv.updateCalendar(currentDate, events);
                    please.setVisibility(View.GONE);
                    list.setVisibility(View.GONE);
                    no_result.setVisibility(View.VISIBLE);
                }
            });

// Adding request to request queue
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

//    public void webServiceCallForHistory(final String modeds){
//
//        currentDate.set(Calendar.DATE, currentDate.getActualMinimum(Calendar.DAY_OF_MONTH));
//        Date nextMonthFirstDay = currentDate.getTime();
//        currentDate.set(Calendar.DATE, currentDate.getActualMaximum(Calendar.DAY_OF_MONTH));
//        Date nextMonthLastDay = currentDate.getTime();
//
//        DateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
//        String startDate = sdf.format(nextMonthFirstDay);
//        String endDate = sdf.format(nextMonthLastDay);
//
//        please.setVisibility(View.VISIBLE);
//        no_result.setVisibility(View.GONE);
//        list.setVisibility(View.GONE);
//        try{
//            String tag_json_obj = "json_obj_req";
//            String url = Constants.RESTAURENT_HISTORY;
//            JSONObject jsonObject=new JSONObject();
//            jsonObject.put("startDateTimeStr", startDate + " 12:00 AM");
//            jsonObject.put("endDateTimeStr", endDate + " 11:59 PM");
//            jsonObject.put("searchText","");
//            JSONObject pagination = new JSONObject();
//            pagination.put("pageNo",1);
//            pagination.put("limit",10);
//            //jsonObject.put("pagination",pagination);
//            JSONObject sort = new JSONObject();
//            sort.put("fieldName","createOn");
//            sort.put("sortType","DESC");
//            jsonObject.put("sort",sort);
//            url = url + pref.getString("userId", null);
//            Log.d("Request", url + " " + jsonObject.toString());
//            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                    url, jsonObject,
//                    new Response.Listener<JSONObject>() {
//
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try{
//                                Log.d("Response", response.toString());
//                                if(response.getJSONObject("data")!=null) {
//                                    JSONObject data = response.getJSONObject("data");
//                                    if(data.getJSONArray("orders").length()>0) {
//                                        please.setVisibility(View.GONE);
//                                        list.setVisibility(View.VISIBLE);
//                                        hisItems = getArrayList(data.getJSONArray("orders"));
//                                        adapter = new PlaceHistoryAdapter(getActivity(),hisItems,Restaurant_History.this);
//                                        //adapter = new RestaurantHistoryAdapter(getActivity(),hisItems ,Restaurant_History.this);
//                                        list.setAdapter(adapter);
//                                        events = new HashSet<>();
//                                        try {
//                                            for (int i = 0; i < data.getJSONArray("orders").length(); i++) {
//                                                JSONObject jsonObject = data.getJSONArray("orders").getJSONObject(i);
//                                                Date date = new Date(jsonObject.getLong("pickupDate"));
//                                                events.add(date);
//                                                Log.d("Events Date :", events.toString());
//                                            }
//                                            Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
//                                            cv.updateCalendar(currentDate,events);
//
//                                        } catch (Exception e) {
//                                        }
//                                    } else {
//                                        Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
//                                        cv.updateCalendar(currentDate,events);
//                                        please.setVisibility(View.GONE);
//                                        list.setVisibility(View.GONE);
//                                        no_result.setVisibility(View.VISIBLE);
//                                    }
//
//                                }else{
//                                    Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
//                                    cv.updateCalendar(currentDate,events);
//                                    please.setVisibility(View.GONE);
//                                    list.setVisibility(View.GONE);
//                                    no_result.setVisibility(View.VISIBLE);
//                                }
//                                /*Wait_dine_adapter adapter=new Wait_dine_adapter(getActivity(),response.getJSONObject("data").getJSONArray("Dining"));
//                                list.setAdapter(adapter);*/
//
//
//                            }catch (Exception e){
//
//                            }
//                        }
//                    }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                }
//            });
//
//// Adding request to request queue
//            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
//        }catch (Exception e){
//            System.out.println("Error :"+e.toString());
//        }
//
//    }

    public ArrayList<JSONObject> getArrayList(JSONArray jsonArray){
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        if(jsonArray!=null){
            for (int i = 0;i<jsonArray.length();i++)
                try {
                    arrayList.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
        return arrayList;
    }

    public void webServiceCallForExpHistory() {

        /*currentDate.set(Calendar.DATE, currentDate.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date nextMonthFirstDay = currentDate.getTime();
        currentDate.set(Calendar.DATE, currentDate.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date nextMonthLastDay = currentDate.getTime();
        DateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        String startDate = sdf.format(nextMonthFirstDay);
        String endDate = sdf.format(nextMonthLastDay);
        try {
            String tag_json_obj = "json_obj_req";
            please.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
            no_result.setVisibility(View.GONE);
            String url = Constants.RESTAURANTS_EXP_HISTORY;
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("startDateTimeStr", startDate + " 12:00 AM");
            jsonObject.put("endDateTimeStr", endDate + " 11:59 PM");
            jsonObject.put("searchText","");
            JSONObject pagination = new JSONObject();
            pagination.put("pageNo", 1);
            pagination.put("limit", 10);
            jsonObject.put("pagination", pagination);
            JSONObject sort = new JSONObject();
            sort.put("fieldName", "createOn");
            sort.put("sortType", "DESC");
            jsonObject.put("sort", sort);
            jsonObject.put("customerId",pref.getString("userId",null));
            url = url + pref.getString("userId", null)+"/book/history";
            Log.d("Request", jsonObject + url);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("Response", response.toString());
                                JSONValue = response;
                                events = new HashSet<>();
                                exps.clear();
                                if (response.getJSONObject("data") != null) {
                                    JSONObject data = response.getJSONObject("data");
                                    if (data.getJSONArray("restExpBookList").length() > 0) {
                                        please.setVisibility(View.GONE);
                                        list.setVisibility(View.VISIBLE);
                                        JSONArray bookedData = data.getJSONArray("restExpBookList");
                                        for(int i =0;i<bookedData.length();i++){
                                            exps.add(new PlaceExpBookedDetail(bookedData.getJSONObject(i)));
                                        }
                                        adapter = new ResExpHistoryAdapter(exps,Restaurant_History.this);
                                        list.setAdapter(adapter);
                                        try {
                                            for (PlaceExpBookedDetail placeExp : exps) {
                                                events.add(new Date(placeExp.slot.startDateTime));
                                            }
                                            Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
                                            cv.updateCalendar(currentDate,events);

                                        } catch (Exception e) {
                                        }
                                    } else {
                                        Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
                                        cv.updateCalendar(currentDate,events);
                                        please.setVisibility(View.GONE);
                                        list.setVisibility(View.GONE);
                                        no_result.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    Calendar_View_Greetings_History cv = view.findViewById(R.id.calendar_view);
                                    cv.updateCalendar(currentDate,events);
                                    please.setVisibility(View.GONE);
                                    list.setVisibility(View.GONE);
                                    no_result.setVisibility(View.VISIBLE);
                                }


                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CalendarViewHistory cv = ((CalendarViewHistory) view.findViewById(R.id.calendar_view));
                    cv.updateCalendar(events);
                    please.setVisibility(View.GONE);
                    list.setVisibility(View.GONE);
                    no_result.setVisibility(View.VISIBLE);
                }
            });

// Adding request to request queue
            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonObjReq.setRetryPolicy(policy);
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }*/
    }

}
