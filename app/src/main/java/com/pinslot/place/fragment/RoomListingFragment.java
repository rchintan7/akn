package com.pinslot.place.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.adapter.Place_Listing_Adapter;
import com.pinslot.place.adapter.RoomListingAdapter;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.model.PlaceSpace;
import com.pinslot.place.widget.PicassoUtil;
import com.pinslot.place.widget.PlaceLoadingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RoomListingFragment extends BaseFragment{

    ListView list;
    ArrayList<PlaceSpace> spaces = new ArrayList<>();
    RatingBar rating;
    TextView placeName;
    ImageView placeImage;
    public CSPlace csPlace;
    PlaceLoadingBar pleaseText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_room_listing,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = view.findViewById(R.id.list);
        placeName = view.findViewById(R.id.place_name);
        placeImage = view.findViewById(R.id.place_image);
        pleaseText=view.findViewById(R.id.please_txt);
        rating = view.findViewById(R.id.rating);
        csPlace = ((NavigationDrawerActivity)getActivity()).placeRequest.place;
        placeName.setText(csPlace.name);
        rating.setRating((float) csPlace.rating);
        if(csPlace.imageUrl.length()>0) {
            PicassoUtil.with(getActivity())
                    .load(csPlace.imageUrl).placeholder(R.mipmap.image_1)
                    .into(placeImage);
        }

        getRooms();
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                addFragment(Place_Hall.instance(((PlaceSpace) list.getAdapter().getItem(i)).srcJson,true),true);
            }
        });
    }

    void getRooms(){
        String url = "http://54.148.10.188:8081/wl-cst-customer-api/businesses/getMultipleSpacesByPlaceId/"+((NavigationDrawerActivity)getActivity()).placeRequest.place.id+"?name=";
        if(((NavigationDrawerActivity)getActivity()).placeRequest.is_space)
            url = url + "others";
        else
            url = url + "room";
        Log.d("Request",url);
        pleaseText.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                Log.d("Response",response.toString());

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            pleaseText.setVisibility(View.GONE);
                            list.setVisibility(View.VISIBLE);
                            JSONArray data = response.getJSONArray("data");
                            spaces.clear();
                            for (int i=0;i<data.length();i++){
                                spaces.add(new PlaceSpace(data.getJSONObject(i)));
                            }
                            list.setAdapter(new RoomListingAdapter(getActivity(),spaces));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, 2000);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(req,"");
    }
}
