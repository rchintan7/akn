package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.model.PlaceSpace;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Place_Hall extends BaseFragment {
    //Slider
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;

    private LinearLayout dotsLayout;
    private int dotsCount;
    private TextView[] dots;

    TextView descriptionTxt, readMore, name, calendarText, liveText;
    LinearLayout descLayout, videoLayout, threeLayout, liveLayout,room_type,contact_layout;
    View view;
    String spaceJson, JSONvalue;
    LinearLayout submit, sendEnquiry;
    boolean showCalendar;
    SharedPreferences pref;
    ImageView video_icon, liveImage;
    PlaceSpace roomObject;
    LinearLayout facilityLayout;
    LinearLayout spaceFacility;

    public static Place_Hall instance(JSONObject json,Boolean showCalendar){
        Place_Hall hall = new Place_Hall();
        Bundle args = new Bundle();
        args.putString("spaceJson",json.toString());
        args.putString("jsonvalue",json.toString());
        args.putBoolean("showCalendar",showCalendar);
        hall.setArguments(args);
        return hall;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.place_hall, container, false);
        spaceJson = getArguments().getString("spaceJson");
        JSONvalue = getArguments().getString("jsonvalue");
        showCalendar = getArguments().getBoolean("showCalendar");
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        sendEnquiry = (LinearLayout) view.findViewById(R.id.send_enquiry);
        submit = (LinearLayout) view.findViewById(R.id.submit);
        name = (TextView) view.findViewById(R.id.name);
        calendarText = (TextView) view.findViewById(R.id.calendar_text);
        descriptionTxt = (TextView) view.findViewById(R.id.description_value);
        liveImage = (ImageView) view.findViewById(R.id.live_image);
        liveText = (TextView) view.findViewById(R.id.live_text);
        room_type = view.findViewById(R.id.room_type_layout);
        facilityLayout = view.findViewById(R.id.facility_layout);
        spaceFacility = view.findViewById(R.id.space_facility);
        ImageView deg360 = view.findViewById(R.id.deg360_icon);
        contact_layout = view.findViewById(R.id.contact_layout);

        try {
            roomObject =new PlaceSpace(new JSONObject(spaceJson));
            if(roomObject.amenities!=null&&roomObject.amenities.size()>0){
                setfacilities(roomObject.amenities);
            } else {
                spaceFacility.setVisibility(View.GONE);
            }
            if(roomObject.roomType.length()>0){
                room_type.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setViewPagerItemsWithAdapter();
        setUiPageViewController();
        descriptionTxt.setText(Html.fromHtml(roomObject.description));
        descriptionTxt.post(new Runnable() {
            @Override
            public void run() {
                int lineCnt = descriptionTxt.getLineCount();
                // Perform any actions you want based on the line count here.
                if (lineCnt <= 4) {
                    readMore.setVisibility(View.GONE);
                } else {
                    descriptionTxt.setMaxLines(4);
                }
            }
        });

        name.setText(roomObject.name);
        readMore = (TextView) view.findViewById(R.id.read_more);
        descLayout = (LinearLayout) view.findViewById(R.id.desc_layout);
        videoLayout = (LinearLayout) view.findViewById(R.id.video_layout);
        threeLayout = (LinearLayout) view.findViewById(R.id.three_layout);
        liveLayout = (LinearLayout) view.findViewById(R.id.live_layout);
        video_icon = view.findViewById(R.id.video_icon);
        if (!roomObject.videoUrl.isEmpty()) {
            video_icon.setImageResource(R.mipmap.video_inactive);
        }
        if(!roomObject.youtubeLink.isEmpty()){
            deg360.setImageResource(R.mipmap.deg360_disable);
        }
        readMore.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (readMore.getText().toString().equals("Read More")) {
                    descriptionTxt.setMaxLines(Integer.MAX_VALUE);
                    ViewGroup.LayoutParams params = descLayout.getLayoutParams();
                    descLayout.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
                    descLayout.setLayoutParams(params);
                    readMore.setText("Less");
                } else {
                    readMore.setText("Read More");
                    descriptionTxt.setMaxLines(4);
                }
            }
        });
        videoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!roomObject.videoUrl.isEmpty()) {
                    //Constants.wowzaId = PlaceSpaceTab.businessID;
                    //Intent videointent = new Intent(getActivity(), VideoPlayerActivity.class);
                    //videointent.putExtra("vurl", roomObject.videoUrl);
                    //startActivity(videointent);
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("Message");
                    alert.setMessage("Video not available");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.show();
                }
            }
        });
        threeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!roomObject.youtubeLink.isEmpty()) {
                    String[] urls = roomObject.youtubeLink.split("v=");
                    if(urls.length>1) {
                        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + urls[1]));
                        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(roomObject.youtubeLink));
                        try {
                            startActivity(appIntent);
                        } catch (Exception ex) {
                            startActivity(webIntent);
                        }
                    } else {
                        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(roomObject.youtubeLink));
                        startActivity(webIntent);
                    }
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("Message");
                    alert.setMessage("360 Video not available");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.show();
                }
            }
        });

        room_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Fragment fragment = new RoomTypeFragment();
                Bundle args = new Bundle();
                args.putString("room_types",roomObject.roomType.toString());
                fragment.setArguments(args);
                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.add(R.id.fragment_place,fragment);
                tx.addToBackStack(fragment.getClass().getName());
                tx.commit();*/
            }
        });

        try {
            JSONObject jsonObject = new JSONObject(JSONvalue);
            if (!jsonObject.has("managerProfile")) {
                //liveImage.setAlpha(0.5f);
                //liveText.setTextColor(getActivity().getResources().getColor(R.color.grey));
                liveLayout.setAlpha(0.5f);
            } else if(jsonObject.getJSONObject("managerProfile").getString("profileId").equals(PrefManager.getInstance(getContext()).getUserId())){
                liveLayout.setVisibility(View.VISIBLE);
                contact_layout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
        }

        contact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!pref.getString("userId", "").equals("")) {
                    /*try {
                        JSONObject jsonObject = new JSONObject(JSONvalue);
                        if (jsonObject.has("managerProfile")) {
                            Fragment fragment = new Communication();
                            Bundle args = new Bundle();
                            args.putString("orderId", jsonObject.getString("id"));
                            args.putString("type", "Places");
                            args.putString("opponentID", jsonObject.getJSONObject("managerProfile").getString("profileId"));
                            args.putString("topicId", "PlaceDetails");
                            args.putString("obj",jsonObject.toString());
                            if (jsonObject.has("chatId"))
                                args.putString("chatId", jsonObject.getString("chatId"));
                            else
                                args.putString("chatId", "");
                            fragment.setArguments(args);

                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager
                                    .beginTransaction();
                            fragmentTransaction.add(R.id.fragment_place, fragment);
                            fragmentTransaction.addToBackStack(fragment.getClass().getName());
                            fragmentTransaction.commit();
                        }

                    } catch (Exception e) {
                    }*/
                } else {
                    Toast.makeText(getActivity(), "Please login to access Live Stream, Chat and other options", Toast.LENGTH_SHORT).show();
                }
            }
        });

        liveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Fragment fragment = new PlaceAvailabilityFragment();
                Bundle args = new Bundle();
                args.putString("spaceJSONData", spaceJson);
                args.putString("placeObj",getArguments().getString("jsonvalue"));
                fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment, fragment.getClass().getName());
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();*/
                try {
                    ((NavigationDrawerActivity)getActivity()).placeRequest.space = new PlaceSpace(new JSONObject(spaceJson));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                addFragment(new PlaceBookingComplete(),true);
            }
        });
        sendEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PrefManager.getInstance(getContext()).getUserId().isEmpty()){
                    addFragment(new LoginFragment(),true);
                } else {
                    /*----try {
                        JSONObject jsonObject = new JSONObject(spaceJson);
                        Fragment fragment = new Send_Place_Enquiry();
                        Bundle args = new Bundle();
                        args.putString("spaceId", jsonObject.getString("id"));
                        args.putString("placeObj", getArguments().getString("jsonvalue"));
                        args.putString("requestType", "place");
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager
                                .beginTransaction();
                        fragmentTransaction.add(R.id.fragment_place, fragment);
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();
                    } catch (Exception e) {
                        Log.d("Error", e.toString());
                    }*/
                }
            }
        });

        if (showCalendar == true) {
            calendarText.setText("Book\nNow");
            sendEnquiry.setVisibility(View.VISIBLE);
            submit.setEnabled(true);
            submit.setClickable(true);
            calendarText.setTextColor(getResources().getColor(R.color.black));
        } else {
            calendarText.setText("Calendar\nNot Available");
            submit.setEnabled(false);
            submit.setClickable(false);
            calendarText.setTextColor(getResources().getColor(R.color.grey));
        }
        return view;
    }

    public void setfacilities(ArrayList<String> amenties) {
        for (int i = 0; i < amenties.size(); i++) {
            View v = getActivity().getLayoutInflater().inflate(R.layout.amenties_item, null);
            ImageView icon = (ImageView) v.findViewById(R.id.icon);
            TextView value = (TextView) v.findViewById(R.id.value);
            /*---if (PlaceInfoFragment.facilities.containsKey(amenties.get(i))) {
                icon.setImageResource(PlaceInfoFragment.facilities.get(amenties.get(i)));
                value.setText(amenties.get(i));
            }*/
            facilityLayout.addView(v);
        }
    }

    private void setUiPageViewController() {
        dotsLayout = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        dotsCount = myViewPagerAdapter.getCount();
        if (dotsCount != 0) {
            dots = new TextView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new TextView(getActivity());
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(30);
                dots[i].setTextColor(getResources().getColor(android.R.color.darker_gray));
                dotsLayout.addView(dots[i]);
            }
            dots[0].setTextColor(getResources().getColor(R.color.green));
        }
    }

    private void setViewPagerItemsWithAdapter() {
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(viewPagerPageChangeListener);
    }

    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setTextColor(getResources().getColor(android.R.color.darker_gray));
            }
            dots[position].setTextColor(getResources().getColor(R.color.green));
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    //	adapter
    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;
        private ArrayList<String> items = new ArrayList<>();

        public MyViewPagerAdapter() {
            for(String img : roomObject.photoUrls)
                this.items.add(img);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.marketing_view_pager, container, false);
            ImageView imageview = (ImageView) view.findViewById(R.id.imageView3);
            if (items.size() > position && !items.get(position).isEmpty()) {
                PicassoUtil.with(getActivity())
                        .load(items.get(position))
                        .into(imageview);
            }
            ((ViewPager) container).addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            ((ViewPager) container).removeView(view);
        }
    }

}
