package com.pinslot.place.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pinslot.place.R;
import com.pinslot.place.adapter.AmmentiesAdapter;
import com.pinslot.place.model.CSPlace;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class PlaceInfoFragment extends BaseFragment {

    GridView ammentiesGrid;
    TextView descriptionTxt, readMore,name;
    LinearLayout descLayout;
    CSPlace placeData;
    HashMap<String,Integer> ammenties = new HashMap<String, Integer>(){
        {
            put("Free Wifi",R.mipmap.wifi_inactive);
            put("Spa",R.mipmap.spa_inactive);
            put("Beach",R.mipmap.beach_inactive);
            put("Restaurant",R.mipmap.restaurant_inactive);
            put("Pool",R.mipmap.pool_inactive);
            put("Bar",R.mipmap.bar_inactive);
            put("Gym",R.mipmap.gym_inactive);
            put("A/C",R.mipmap.ac_inactive);
            put("Parking",R.mipmap.parking_incative);
        }
    };
    String[] all_Amenties = new String[]{"Free Wifi","Spa","Beach","Restaurant","Pool","Bar","Gym","A/C","Parking"};
    HashSet<String> selected = new HashSet<String>() {
        {
            add("Free Wifi");
            add("Restaurant");
            add("Pool");
            add("Bar");
            add("A/C");
            add("Parking");
        }
    };

    public static PlaceInfoFragment instance(String place){
        PlaceInfoFragment fragment = new PlaceInfoFragment();
        Bundle args = new Bundle();
        args.putString("JSON",place);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_place_info,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ammentiesGrid = view.findViewById(R.id.amenties_grid);
        ammentiesGrid.setAdapter(new AmmentiesAdapter(all_Amenties,ammenties,selected));

        descriptionTxt = (TextView) view.findViewById(R.id.description_value);
        readMore = (TextView) view.findViewById(R.id.read_more);
        descLayout = (LinearLayout) view.findViewById(R.id.desc_layout);
        name=view.findViewById(R.id.name);
        try{
            placeData = new CSPlace(new JSONObject(getArguments().getString("JSON")));
        }catch (Exception e){
        }
        descriptionTxt.setText(placeData.description);
        name.setText("About "+placeData.name);
        if (placeData.description.length() > 0) {
            descriptionTxt.setText(Html.fromHtml(placeData.description));
            descriptionTxt.post(new Runnable() {
                @Override
                public void run() {
                    int lineCnt = descriptionTxt.getLineCount();
                    // Perform any actions you want based on the line count here.
                    if (lineCnt <= 3) {
                        readMore.setVisibility(View.GONE);
                    } else {
                        descriptionTxt.setMaxLines(3);
                    }
                }
            });
        }
        readMore.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (readMore.getText().toString().equals("Read More")) {
                    descriptionTxt.setMaxLines(Integer.MAX_VALUE);
                    ViewGroup.LayoutParams params = descLayout.getLayoutParams();
                    descLayout.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
                    descLayout.setLayoutParams(params);
                    readMore.setText("Less");
                } else {
                    readMore.setText("Read More");
                    descriptionTxt.setMaxLines(3);
                    ViewGroup.LayoutParams params = descLayout.getLayoutParams();
                    descLayout.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
                    descLayout.setLayoutParams(params);
                }
            }
        });
    }
}
