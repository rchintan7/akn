package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.R;
import com.pinslot.place.Utils;

import org.json.JSONObject;

/**
 * Created by Selva on 10/15/2018.
 */

public class ForgotPassword extends Fragment {

    EditText email;
    Button send;
    TextView txt_signup;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_password_new,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        email = (EditText)view.findViewById(R.id.email);
        send = (Button)view.findViewById(R.id.btn_send);
        txt_signup = view.findViewById(R.id.txt_signup);
        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.regvalue=1;
                addFragment(new LoginFragment());
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.getText()!=null && !email.getText().toString().isEmpty()) {
                    if(Utils.validateEmail(email.getText().toString())) {
                        try {
                            String tag_json_obj = "json_obj_req";
                            String url = Constants.FORGOT_PASSWORD;
                            final ProgressDialog pDialog = new ProgressDialog(getActivity());
                            pDialog.setMessage("Loading...");
                            pDialog.show();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("emailId", email.getText().toString());
                            //jsonObject.put("passwordResetUrl", "http://www.pinslots.com/#/restPassword?reset_token=:reset_token");
                            jsonObject.put("passwordResetUrl", "http://www.connectslot.com/#/restPassword?reset_token=:reset_token");
                            Log.d("Request", jsonObject.toString());

                            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                                    url, jsonObject,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                Log.d("Response", response.toString());
                                                pDialog.dismiss();

                                                if (response.getString("status").equals("SUCCESS")) {
                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                    alert.setTitle("Success");
                                                    alert.setMessage("Password Reset Link sent to your Email id.");
                                                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {

                                                        }
                                                    });
                                                    alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                                        @Override
                                                        public void onDismiss(DialogInterface dialog) {
                                                            getActivity().getSupportFragmentManager().popBackStackImmediate();
                                                        }
                                                    });
                                                    alert.show();
                                                } else {
                                                    Toast.makeText(getActivity(), "Failed to reset password", Toast.LENGTH_LONG).show();
                                                }

                                            } catch (Exception e) {

                                            }
                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    pDialog.dismiss();
                                }
                            });

                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                        } catch (Exception e) {
                            System.out.println("Error :" + e.toString());
                        }
                    } else {
                        Toast.makeText(getActivity(), "Enter Correct Email", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Enter your Email id", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction2 = getActivity().getSupportFragmentManager()
                .beginTransaction();
        fragmentTransaction2.add(R.id.fragment_place, fragment);
        fragmentTransaction2.addToBackStack(fragment.getClass().getName());
        fragmentTransaction2.commit();
    }
}
