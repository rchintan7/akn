package com.pinslot.place.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.adapter.Coupon_List_Adapter;
import com.pinslot.place.model.Coupon;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class PromotionalOffer extends BaseFragment {
    ListView list;
    String navType = "";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    RelativeLayout promotionTop;
    public static String jsonValue, showCalendar;
    JSONArray couponsArray;
    TextView menu_offer, gen_offer;
    RelativeLayout no_results;
    ProgressBar progress;
    TextView message, interest;
    String offerType = "general";
    ArrayList<Coupon> menu_coupons = new ArrayList<>();
    ArrayList<Coupon> general_coupons = new ArrayList<>();
    Coupon_List_Adapter adapter;
    ImageView select1,select2;
    public static PromotionalOffer newInstance(int position, String category_name, String type, String jsonValue1, String showCalendar1) {
        jsonValue = jsonValue1;
        showCalendar = showCalendar1;
        PromotionalOffer f = new PromotionalOffer();
        Bundle b = new Bundle();
        b.putString("navigationType", category_name);
        b.putString("type", type);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.promotionaloffers, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (ListView) view.findViewById(R.id.listView);
        menu_offer = view.findViewById(R.id.menu_offer);
        gen_offer = view.findViewById(R.id.gen_offer);
        promotionTop = view.findViewById(R.id.promotion_top);
        no_results = view.findViewById(R.id.no_results);
        progress = view.findViewById(R.id.progress);
        message = view.findViewById(R.id.message);
        interest = view.findViewById(R.id.interest);
        select1 = view.findViewById(R.id.select_1);
        select2 = view.findViewById(R.id.select_2);
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();
        offerType = "General";
        adapter = new Coupon_List_Adapter(this,general_coupons,menu_coupons);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JSONObject jsonObject = null;
                Boolean restriction = false, favorites = false;
                try {
                    boolean show_res_login = false;
                    JSONObject json = ((Coupon) list.getAdapter().getItem(position)).srcJson;
                    JSONObject restrictionObject = json.getJSONObject("restriction");
                    if (restrictionObject.has("shareFacebook") && !restrictionObject.isNull("shareFacebook")) {
                        restriction = json.getJSONObject("restriction").getBoolean("shareFacebook");
                    }
                    if (json.has("isFavourite") && !json.isNull("isFavourite")) {
                        favorites = json.getBoolean("isFavourite");
                    }
                    //String menuId = json.getJSONObject("business").getString("menuItemId");
                    //String restId = json.getJSONObject("business").getJSONObject("restaurant").getString("id");

                    //Log.d("RestaurantId", restId);
                    //editor.putString("restaurantId", restId);
                    editor.commit();
                            Fragment fragment = new Coupon_Details();
                            Bundle args = new Bundle();
                            args.putString("jsonResponse", json.toString());
                            args.putString("favorites", favorites.toString());
                            //args.putString("menuId", menuId);
                            //args.putString("restId", restId);
                            args.putString("type", "Restaurant");
                            args.putString("from","coupon");
                            args.putBoolean("show_res_login", show_res_login);
                            args.putBoolean("restriction", restriction);
                            fragment.setArguments(args);
                            addFragment(fragment,true);

                } catch (Exception e) {
                    Log.d("Error", e.toString());
                }

            }
        });
        menu_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offerType = "Menu";
                menu_offer.setBackgroundResource(R.color.colorAccent);
                //menu_offer.setTextColor(Color.WHITE);
                gen_offer.setBackgroundResource(R.color.color_accent_disable_dark);
                select2.setVisibility(View.VISIBLE);
                select1.setVisibility(View.GONE);
                //gen_offer.setTextColor(getResources().getColor(R.color.app_orange));
                if (menu_coupons.size()>0) {
                    no_results.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    list.setAdapter(new Coupon_List_Adapter(PromotionalOffer.this,new ArrayList<Coupon>(),menu_coupons));
                } else {
                    no_results.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);
                }
                message.setText("We Couldn't find any Menu offer for this restaurant. if you would like this restaurant to provide coupon, please click the button below");
            }
        });
        gen_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offerType = "General";
                gen_offer.setBackgroundResource(R.color.colorAccent);
                //gen_offer.setTextColor(Color.WHITE);
                menu_offer.setBackgroundResource(R.color.color_accent_disable_dark);
                select2.setVisibility(View.GONE);
                select1.setVisibility(View.VISIBLE);
                //menu_offer.setTextColor(getResources().getColor(R.color.app_orange));
                list.setVisibility(View.GONE);
                if (general_coupons.size()>0) {
                    //general_offer_list.setAdapter(new ResCouponAdapter(PromotionalOffer.this, couponsArray, "","", null,null));
                    no_results.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    list.setAdapter(new Coupon_List_Adapter(PromotionalOffer.this,general_coupons,new ArrayList<Coupon>()));
                } else {
                    list.setVisibility(View.GONE);
                    no_results.setVisibility(View.VISIBLE);
                }
                message.setText("We Couldn't find any general offer for this restaurant. if you would like this restaurant to provide coupon, please click the button below");
            }
        });
        webServiceCallCoupons();
        getPromos();

        interest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendInterest(navType);
            }
        });
    }

    public void sendInterest(String type) {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.URL + "business/interest";
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("customerId", PrefManager.getInstance(getContext()).getUserId());
            if (type.equals("Restaurant")) {

                if (offerType.equalsIgnoreCase("Menu"))
                    jsonObject.put("bussinessType", "REST_MENU_COUPON");
                else
                    jsonObject.put("bussinessType", "REST_COUPON");

                jsonObject.put("businessId", ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.id);
            }
            Log.d("Menu Request", url.toString() + jsonObject.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            try {
                                pDialog.dismiss();
                                if (response.getString("status").equals("SUCCESS")) {
                                    Toast.makeText(getActivity(), "Interest successfully sent", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void getPromos() {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.PROMOCODE_SEARCH;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("businessId", ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.id);
            jsonObject.put("businessType", "RESTAURANT");
            jsonObject.put("customerId", pref.getString("userId", ""));
            jsonObject.put("pickupDate", Calendar.getInstance().getTime().getTime());
            SimpleDateFormat tf = new SimpleDateFormat("hh:mm a");
            jsonObject.put("pickupTime", tf.format(Calendar.getInstance().getTime()));
            progress.setVisibility(View.VISIBLE);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Experience Response", response.toString());
                            progress.setVisibility(View.GONE);
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    couponsArray = response.getJSONObject("data").getJSONArray("result");
                                    general_coupons.clear();
                                    if (couponsArray != null && couponsArray.length() > 0) {
                                        no_results.setVisibility(View.GONE);
                                        list.setVisibility(View.VISIBLE);
                                        for (int i = 0;i<couponsArray.length();i++){
                                            general_coupons.add(new Coupon(couponsArray.getJSONObject(i)));
                                        }
                                        list.setAdapter(new Coupon_List_Adapter(PromotionalOffer.this,general_coupons,new ArrayList<Coupon>()));
                                    } else if(general_coupons.size()==0&&menu_coupons.size()==0){
                                        list.setVisibility(View.GONE);
                                        no_results.setVisibility(View.VISIBLE);
                                    }
                                } else if(general_coupons.size()==0&&menu_coupons.size()==0){
                                    list.setVisibility(View.GONE);
                                    no_results.setVisibility(View.VISIBLE);

                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progress.setVisibility(View.GONE);
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void webServiceCallCoupons() {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.COUPON_SEARCH;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("businessType", "MENU_ITEM");
            if (pref.getString("loginSession", null) != null) {
                if (pref.getString("loginSession", null).equals("Valid")) {
                    jsonObject.put("customerId", pref.getString("userId", ""));
                } else {
                }
            } else {
            }
            jsonObject.put("businessId", ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.id);
            jsonObject.put("pickupDate", Calendar.getInstance().getTime().getTime());
            SimpleDateFormat tf = new SimpleDateFormat("hh:mm a");
            jsonObject.put("pickupTime", tf.format(Calendar.getInstance().getTime()));
            Log.d("Coupon Request", url.toString() + " " + jsonObject.toString());
            progress.setVisibility(View.VISIBLE);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Coupon Response", response.toString());
                            progress.setVisibility(View.GONE);
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    pDialog.dismiss();
                                    JSONArray data = response.getJSONObject("data").getJSONArray("result");
                                    menu_coupons.clear();
                                    if (data.length() == 0&&general_coupons.size()==0) {
                                        //list.setVisibility(View.GONE);
                                        //no_results.setVisibility(View.VISIBLE);
                                    } else {
                                        for (int i=0;i<data.length();i++){
                                            menu_coupons.add(new Coupon(data.getJSONObject(i)));
                                        }
                                        //adapter.setMenu_coupons(menu_coupons);
                                        //adapter.notifyDataSetChanged();
                                        //list.setVisibility(View.VISIBLE);
                                        //no_results.setVisibility(View.GONE);
                                    }
                                } else {
                                    //list.setVisibility(View.GONE);
                                    //no_results.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    progress.setVisibility(View.GONE);
                }
            });

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent data) {
        System.out.println("On Success" + requestCode);
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        if (pref.getString("loginSession", null) != null) {
            if (pref.getString("loginSession", null).equals("Valid")) {
                sharePhotoToFacebook();
                webServiceCallRedeem();
            } else {
                addFragment(new LoginFragment(),true);
            }
        } else {
            addFragment(new LoginFragment(),true);
        }
        super.onActivityResult(requestCode, responseCode, data);
    }

    private void sharePhotoToFacebook() {
        Bitmap image = BitmapFactory.decodeResource(getActivity().getResources(), R.mipmap.calendar_ic);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("https://pinslots.com/")
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        ShareApi.share(content, null);
    }

    public void webServiceCallRedeem() {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.COUPON_REDEEM;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            Log.d("Menu Request", url.toString() + jsonObject);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    pDialog.dismiss();
                                    OrderDetails fragment = new OrderDetails();
                                    Bundle args = new Bundle();
                                    args.putString("resId", getArguments().getString("restId"));
                                    fragment.setArguments(args);
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager
                                            .beginTransaction();
                                    fragmentTransaction.add(R.id.fragment_place, fragment, fragment.getClass().getName());
                                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                                    fragmentTransaction.commit();
                                } else {
                                    pDialog.dismiss();
                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

}
