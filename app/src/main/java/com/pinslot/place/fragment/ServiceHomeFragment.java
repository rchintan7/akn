package com.pinslot.place.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.RecyclerItemClickListener;
import com.pinslot.place.adapter.ExtraItemsAdapter;
import com.pinslot.place.adapter.ServicesAdapter;

import org.json.JSONObject;

public class ServiceHomeFragment extends BaseFragment {

    GridView extras;
    RecyclerView services;
    CardView food_services;
    int food_count = 1;
    ImageView img_all;
    LinearLayout lay_all;
    TextView txt_title;
    int position=0;
    RelativeLayout lay_re;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_home,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        extras = view.findViewById(R.id.extra_items);
        services = view.findViewById(R.id.services);
        food_services = view.findViewById(R.id.food_services);
        lay_all = view.findViewById(R.id.lay_all);
        img_all = view.findViewById(R.id.img_all);

        txt_title = view.findViewById(R.id.txt_title);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        services.setLayoutManager(manager);
        extras.setAdapter(new ExtraItemsAdapter());
        services.setAdapter(new ServicesAdapter());
        food_services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerActivity)getActivity()).setDelivery();
                addFragment(new FoodServiceFragment(),true);
            }
        });

        extras.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Food_Details(getActivity());
            }
        });

      /*  lay_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lay_all.setVisibility(View.GONE);
                txt_title.setVisibility(View.GONE);
                img_all.setVisibility(View.VISIBLE);
                img_all.setImageResource(R.drawable.s1);
                position++;
            }
        });*/

        services.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), services ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int positionone) {
                        lay_all.setVisibility(View.GONE);
                        txt_title.setVisibility(View.GONE);
                        img_all.setVisibility(View.VISIBLE);
                        img_all.setImageResource(R.drawable.s1);
                        position++;
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        img_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position==1){
                    img_all.setImageResource(R.drawable.s2);
                    position++;
                    return;
                }
                if(position==2){
                    img_all.setImageResource(R.drawable.s3);
                    position++;
                    return;
                }

                if(position==3){
                    img_all.setImageResource(R.drawable.s4);
                    position++;
                    return;
                }
                if(position==4){
                    img_all.setImageResource(R.drawable.s5);
                    position++;
                    return;
                }
                if(position==5){
                    img_all.setImageResource(R.drawable.s6);
                    position++;
                    return;
                }
                if(position==6){
                    img_all.setImageResource(R.drawable.s7);
                    position++;
                    return;
                }
                if(position==7){
                    img_all.setImageResource(R.drawable.s8);
                    position++;
                    return;
                }
                if(position==8){
                    img_all.setImageResource(R.drawable.s9);
                    position++;
                    return;
                }
                if(position==9){
                    replaceFragment(new PlaceHomeFragment(),true);
                    position=0;

                }
            }
        });
    }


    public void Food_Details(final Context act) {
        //Log.d("JSON String", jsonStr.toString());
        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.service_request_popup);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);

        Button send=dialog.findViewById(R.id.send);
        Button add = (Button) dialog.findViewById(R.id.add);
        Button less = (Button) dialog.findViewById(R.id.less);
        final Button count = (Button) dialog.findViewById(R.id.count);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                food_count = food_count + 1;
                count.setText(String.valueOf(food_count));

            }
        });

        less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (food_count > 0) {
                    food_count = food_count - 1;
                    count.setText(String.valueOf(food_count));

                    //price=price-Integer.parseInt(priceTxt.getText().toString());
                }

            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(new ThanksFragment(),true);
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
