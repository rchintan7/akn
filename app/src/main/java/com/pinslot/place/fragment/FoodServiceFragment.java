package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.astuetz.PagerSlidingTabStrip;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.helpers.Payment_Method;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.pinslot.place.fragment.RestaurantMenuFragment.CompareTimeString;

public class FoodServiceFragment extends BaseFragment {

    ViewPager pager;
    PagerSlidingTabStrip tabs;
    JSONArray menuJsonArray,menuJsonArrayFilter;
    ArrayList<String> listDataHeader = new ArrayList<String>();
    HashMap<String, ArrayList<JSONObject>> listDataChild = new HashMap<String, ArrayList<JSONObject>>();
    ArrayList<String> tabCategory = new ArrayList<>();
    TextView tv_qty,tv_amt;
    LinearLayout check_out;
    JSONObject cartObj;
    EditText search;
    ArrayList<ListDataChangeListener> listeners = new ArrayList<>();

    public static FoodServiceFragment Instance(String linkedOrderId){
        FoodServiceFragment fragment = new FoodServiceFragment();
        Bundle args = new Bundle();
        args.putString("linkedOrderId",linkedOrderId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_food_service,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabs = view.findViewById(R.id.tabs);
        pager = view.findViewById(R.id.pager);
        tv_qty = (TextView) view.findViewById(R.id.qty);
        tv_amt = (TextView) view.findViewById(R.id.amount);
        check_out = view.findViewById(R.id.check_out);
        search = view.findViewById(R.id.search);
        webServiceCallForMenuItem();
        updateCartValueFromServer();
        ((NavigationDrawerActivity)getActivity()).setDelivery();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                RenderData(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (cartObj != null && cartObj.getJSONArray("cartItems").length() > 0) {
                        OrderDetails fragment = new OrderDetails();
                        fragment.cartObj = cartObj;
                        Bundle args = new Bundle();
                        args.putString("resId", Constants.res_id);
                        if(getArguments()!=null)
                            args.putString("linkedOrderId",getArguments().getString("linkedOrderId",""));
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager
                                .beginTransaction();
                        fragmentTransaction.add(R.id.fragment_place, fragment);
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void webServiceCallForMenuItem() {

        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.MENU_ITEM + Constants.res_id;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Menu Request", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    JSONObject data = new JSONObject(response.toString());
                                    menuJsonArray = data.getJSONArray("data");
                                    pDialog.dismiss();
                                    // Call Filter Items
                                    webServiceCallForMenuFilter();


                                }
                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }
    }

    public void addListener(ListDataChangeListener listener){
        listeners.add(listener);
    }

    public void removeListener(ListDataChangeListener listener){
        listeners.remove(listener);
    }

    public void webServiceCallForMenuFilter() {

        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.MENU_ITEM_FILTER + Constants.res_id + "/menuitems/filter";
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Menu Request", url.toString());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("searchText", "");
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    JSONObject data = new JSONObject(response.toString());
                                    menuJsonArrayFilter = data.getJSONObject("data").getJSONArray("menuItems");
                                    pDialog.dismiss();
                                    RenderData("");

                                }
                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            RenderData("");
            System.out.println("Error :" + e.toString());
        }
    }

    public void RenderData(String searchString) {
        try {
            if (menuJsonArray != null) {
                JSONArray jsonArray = menuJsonArray;
                listDataHeader.clear();
                listDataChild.clear();
                if (jsonArray.length() > 0) {
                    // Get all menu items
                    ArrayList<JSONObject> Avaliable = new ArrayList<>();
                    ArrayList<JSONObject> notAvailable = new ArrayList<>();
                    ArrayList<JSONObject> others = new ArrayList<>();

                    try {
                        for (int j = 0; j < menuJsonArrayFilter.length(); j++) {
                            JSONObject menuItems = menuJsonArrayFilter.getJSONObject(j);
                            if (searchString.isEmpty() || menuItems.getString("menuItemName").toLowerCase().contains(searchString.toLowerCase())) {
                                String avalilability = CheckAvalability(menuItems);
                                menuItems.put("availability", avalilability);
                                if (avalilability.isEmpty()) {
                                    others.add(menuItems);
                                } else if (avalilability.equals("available")) {
                                    Avaliable.add(menuItems);
                                } else if (avalilability.equals("not_available")) {
                                    notAvailable.add(menuItems);
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    Avaliable.addAll(others);
                    Avaliable.addAll(notAvailable);
                    listDataHeader.add("All Menu Items" + "(" + String.valueOf(Avaliable.size())+")");
                    listDataChild.put(listDataHeader.get(0), Avaliable);

                    // Category Menu Items
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        tabCategory.add(jsonObject.getJSONObject("menuCategory").getString("menuCategoryName"));
                        Log.d("Category Name", tabCategory.toString());
                        try {
                            JSONArray menus = jsonObject.getJSONArray("menuItems");
                            ArrayList<JSONObject> Avaliable1 = new ArrayList<>();
                            ArrayList<JSONObject> notAvailable1 = new ArrayList<>();
                            ArrayList<JSONObject> others1 = new ArrayList<>();
                            for (int j = 0; j < menus.length(); j++) {
                                JSONObject menuItems = menus.getJSONObject(j);
                                if (searchString.isEmpty() || menuItems.getString("menuItemName").toLowerCase().contains(searchString.toLowerCase())) {
                                    String avalilability = CheckAvalability(menuItems);
                                    menuItems.put("availability", avalilability);
                                    if (avalilability.isEmpty())
                                        others1.add(menuItems);
                                    else if (avalilability.equals("available"))
                                        Avaliable1.add(menuItems);
                                    else if (avalilability.equals("not_available"))
                                        notAvailable1.add(menuItems);
                                }
                            }
                            Avaliable1.addAll(others1);
                            Avaliable1.addAll(notAvailable1);
                            listDataHeader.add(jsonObject.getJSONObject("menuCategory").getString("menuCategoryName") + "(" + String.valueOf(Avaliable1.size())+")");
                            listDataChild.put(listDataHeader.get(i + 1), Avaliable1);
                            CreateViews();
                        } catch (Exception e) {
                        }
                    }
                    //if (listDataChild.size() > 0) {
                    //}
                } else {
                    //searchLayout.setVisibility(View.GONE);

                    //Snackbar snackbar = Snackbar
                    //        .make(view, "There is no data to show", Snackbar.LENGTH_LONG);
                    //snackbar.show();
                }
            }
        } catch (Exception e) {
        }
    }

    public String CheckAvalability(JSONObject item){
        String Availablity = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String selectedTime = sdf.format(new Date(((NavigationDrawerActivity)getActivity()).resReq.startDate));
        if(item.has("availableSlot1")){
            try {
                if(CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("startTime"))>=0 &&
                        CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("endTime"))<=0)
                    Availablity = "available";
                else
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(item.has("availableSlot2")){
            try {
                if(CompareTimeString(selectedTime,item.getJSONObject("availableSlot2").getString("startTime"))>=0 &&
                        CompareTimeString(selectedTime,item.getJSONObject("availableSlot2").getString("endTime"))<=0)
                    Availablity = "available";
                else if(Availablity.isEmpty())
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(Availablity.isEmpty())
            Availablity = "available";
        return Availablity;
    }

    public void updateCartValueFromServer() {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.GET_ORDER_LIST + "customer/" + PrefManager.getInstance(getContext()).getUserId() + "/business/" + Constants.res_id + "/cart";
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response", response.toString());
                            //jsonResponse=response.toString();
                            try {
                                if (response.getString("status").equals("SUCCESS")) {
                                    cartObj = response.getJSONObject("data");
                                    //JSONArray jsonArray=data.getJSONArray("cartItems");
                                    updateCartValue();
                                }
                            } catch (Exception e) {
                                Log.d("Error :", e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }

    public void updateCartValue() {
        try {
            if (cartObj != null) {
                    /*if (cartobject.getString("businessId").equals(restaurantID)) {
                        isCheckOutDiff = false;
                    } else {
                        isCheckOutDiff = true;
                    }*/
                JSONArray cart_item_array = cartObj.getJSONArray("cartItems");
                Log.d("Cart Items", cart_item_array.toString());
                int qty = 0;
                double amt = 0;
                if (cart_item_array.length() > 0) {
                    check_out.setVisibility(View.VISIBLE);
                    for (int i = 0; i < cart_item_array.length(); i++) {
                        qty = qty + cart_item_array.getJSONObject(i).getInt("qty");
                        amt = amt + cart_item_array.getJSONObject(i).getDouble("finalPrice");
                        //Karthee given final price so we don't need to calculate with quantity, So we don't need to use below code.
                            /*if(!cart_item_array.getJSONObject(i).getString("restaurantMenuChos").equals("null")&&!cart_item_array.getJSONObject(i).getJSONObject("restaurantMenuChos").getString("menuchosPrice").equals("null"))
                                amt = amt + cart_item_array.getJSONObject(i).getInt("qty") * cart_item_array.getJSONObject(i).getJSONObject("restaurantMenuChos").getDouble("menuchosPrice");
                            else
                                amt = amt + cart_item_array.getJSONObject(i).getDouble("finalPrice");*/
                    }
                } else {
                    check_out.setVisibility(View.INVISIBLE);
                }
                tv_qty.setText("Qty : " + Integer.toString(qty));
                tv_amt.setText(getResources().getString(R.string.indian_currency) + " " + Double.toString(amt));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void CreateViews(){
        pager.setAdapter(new MyPagerAdapter(getChildFragmentManager(),listDataHeader));
        tabs.setViewPager(pager);
        tabs.setIndicatorColorResource(R.color.app_yellow);
        tabs.setTextColorResource(R.color.white);
        tabs.setIndicatorHeight((int) Utils.getPXforDP(3, getContext()));
        tabs.setTextSize((int) Utils.getPXforDP(12, getContext()));
        tabs.setDividerColorResource(R.color.transparent);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setStripSelectedColor(position, getResources().getColor(R.color.app_yellow), getResources().getColor(R.color.white));
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setStripSelectedColor(int position, int selectedTextColor, int normalTextColor) {
        if (tabs.getChildCount() > 0) {
            LinearLayout tabsContainer = (LinearLayout) tabs.getChildAt(0);
            if (tabsContainer.getChildCount() > 0) {
                for (int i = 0; i < tabsContainer.getChildCount(); i++) {
                    View v = tabsContainer.getChildAt(i);
                    if (v instanceof TextView) {
                        if (i == position)
                            ((TextView) v).setTextColor(selectedTextColor);
                        else
                            ((TextView) v).setTextColor(normalTextColor);
                    }
                }
            }
        }
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<String> items = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fm,ArrayList<String> items) {
            super(fm);
            this.items = items;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return items.get(position);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Fragment getItem(int position) {
            return FoodsFragment.instance(items.get(position));
        }
    }

    interface ListDataChangeListener{
        void listDataChanged();
    }

}
