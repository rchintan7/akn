package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.CircleImageView;
import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.PhotosView;
import com.pinslot.place.PrefManager;
import com.pinslot.place.R;
import com.pinslot.place.adapter.ExpandableListAdapterFaq;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class RestaurantMenuFragment extends BaseFragment {
    RelativeLayout noMenuContent,menuAvailable;
    LinearLayout photos_container,photos_layout,searchLayout;
    Button btn_dinein;
    ViewPager viewPager;
    Button prev, next, needMenu;
    ExpandableListView expListView;
    ExpandableListAdapter listAdapter;
    EditText searchItem;
    List<String> listDataHeader = new ArrayList<String>();
    HashMap<String, List<JSONObject>> listDataChild = new HashMap<String, List<JSONObject>>();
    WebView web_view;
    int food_count = 1;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String type;
    String autoSelectedItems;
    ArrayList<String> tabCategory = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.restaurant_menu,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        noMenuContent = view.findViewById(R.id.no_menu_content);
        photos_container = view.findViewById(R.id.photos_container);
        photos_layout = (LinearLayout)view.findViewById(R.id.menu_photos);
        btn_dinein = (Button) view.findViewById(R.id.btn_dinein);
        viewPager = view.findViewById(R.id.view_photos);
        prev = view.findViewById(R.id.prev);
        next = view.findViewById(R.id.next);
        searchLayout = (LinearLayout) view.findViewById(R.id.search_layout);
        searchItem = (EditText) view.findViewById(R.id.search_item);
        needMenu = (Button) view.findViewById(R.id.need_menu);
        menuAvailable = view.findViewById(R.id.menu_available);
        btn_dinein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DineIn();
            }
        });
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        searchItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                RenderingList(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        expListView.setGroupIndicator(null);
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                ImageView groupIndicator = (ImageView) v.findViewById(R.id.arrow);
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                    groupIndicator.setImageResource(R.drawable.arrow_down);
                } else {
                    parent.expandGroup(groupPosition);
                    groupIndicator.setImageResource(R.drawable.arrow_up);
                }
                return true;
            }
        });
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            public void onGroupExpand(int groupPosition) {
            }
        });
        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            public void onGroupCollapse(int groupPosition) {

            }
        });
        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                final JSONObject item = listDataChild.get(
                        listDataHeader.get(groupPosition)).get(childPosition);

                try {
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("menuItemId", item.getString("menuItemId"));
                    editor.commit();

                    String tag_json_obj = "json_obj_req";
                    String url = Constants.SEARCH_REST_MENU + item.getString("menuItemId");
                    Log.d("Request :", url);
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                            url, "",
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("Response", response.toString());
                                    //jsonResponse=response.toString();
                                    try {
                                        if (response.getString("status").equals("SUCCESS")) {
                                            //JSONObject data = new JSONObject(response.toString());
                                            JSONObject top = null;
                                            if (response.isNull("data")) {
                                            } else {
                                                top = response.getJSONObject("data");
                                            }
                                            Restaurant_Menu_Items parent = (Restaurant_Menu_Items)getParentFragment();
                                            if(parent!=null) {
                                                Food_Details(getActivity(), top, parent.restaurantID, parent, parent.currency, item);
                                            }

                                        }
                                    } catch (Exception e) {
                                        Log.d("Error", e.toString());
                                    }
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });

// Adding request to request queue
                    AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                } catch (Exception e) {
                    System.out.println("Error :" + e.toString());
                }

                return false;
            }
        });
        UpdatePhotos();
        RenderingList("");
        Restaurant_Menu_Items parent = (Restaurant_Menu_Items) getParentFragment();
        if(parent!=null){
            parent.setMenuListener(new Restaurant_Menu_Items.MenuRequestListener() {
                @Override
                public void updateList() {
                    RenderingList("");
                }

                @Override
                public void AddPhotos() {
                    UpdatePhotos();
                }
            });
        }

        needMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendInterest();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Restaurant_Menu_Items parent = (Restaurant_Menu_Items) getParentFragment();
        if(parent!=null)
            parent.setMenuListener(null);
    }

    public void DineIn(){
        Fragment fragment = new RestaurantCheckin();
        ((NavigationDrawerActivity) getActivity()).RestaurantBookingType = "DineIn";
        ((NavigationDrawerActivity)getActivity()).resReq.requestType = "DineIn";
        fragment.setArguments(getArguments());
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.add(R.id.fragment_place, fragment);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.commit();

    }

    public void sendInterest() {
        try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.URL + "business/interest";
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("customerId", PrefManager.getInstance(getContext()).getUserId());
            jsonObject.put("bussinessType", "REST_MENU");
            jsonObject.put("businessId", ((NavigationDrawerActivity) getActivity()).resReq.selectedRestaurant.id);
            Log.d("Menu Request", url.toString() + jsonObject.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            try {
                                pDialog.dismiss();
                                if (response.getString("status").equals("SUCCESS")) {
                                    Toast.makeText(getActivity(), "Interest successfully sent", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }

    }
    private void UpdatePhotos(){
        final Restaurant_Menu_Items parent = (Restaurant_Menu_Items)getParentFragment();
        if(parent!=null&&parent.photo_urls.size()>0){
            photos_layout.setVisibility(View.VISIBLE);
            photos_container.setVisibility(View.GONE);
            //dineinLayout.setVisibility(View.GONE);
            photos_layout.removeAllViews();
            for (int i =0;i<parent.photo_urls.size();i++){
                RelativeLayout relativeLayout = new RelativeLayout(getActivity());
                relativeLayout.setLayoutParams(new ViewGroup.LayoutParams((int)getpxfordp(70),(int)getpxfordp(70)));
                relativeLayout.setPadding((int)getpxfordp(8),(int)getpxfordp(0),(int)getpxfordp(8),(int)getpxfordp(16));
                ImageView imageView = new ImageView(getActivity());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
                final int position = i;
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhotoAlert(parent.photo_urls,position);
                    }
                });
                Picasso.with(getActivity()).load(parent.photo_urls.get(i)).into(imageView);
                relativeLayout.addView(imageView);
                photos_layout.addView(relativeLayout);
            }
        } else {
            //dineinLayout.setVisibility(View.VISIBLE);
            photos_layout.setVisibility(View.GONE);
            photos_container.setVisibility(View.GONE);
        }

        // Full page Menu Items
        if(parent!=null) {
            viewPager.setAdapter(new CustomPagerAdapter(parent.photo_urls));
            viewPager.setCurrentItem(0);
            prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewPager.getCurrentItem() > 0)
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                }
            });
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewPager.getCurrentItem() < parent.photo_urls.size() - 1)
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }
            });

            viewPager.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Fragment fragment = new PhotosView();
                    Bundle args = new Bundle();
                    args.putStringArrayList("photos", parent.photo_urls);
                    args.putInt("position", viewPager.getCurrentItem());
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager
                            .beginTransaction();
                    fragmentTransaction.add(R.id.fragment_place, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                }
            });
        }

    }

    public float getpxfordp(int dp) {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public void PhotoAlert(final ArrayList<String> photos, int position){
        android.app.Dialog dialog = new android.app.Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.photos_view);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);
        final ViewPager viewPager = dialog.findViewById(R.id.photos);
        Button prev = dialog.findViewById(R.id.prev);
        Button next = dialog.findViewById(R.id.next);
        viewPager.setAdapter(new CustomPagerAdapter(photos));
        viewPager.setCurrentItem(position);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewPager.getCurrentItem()>0)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewPager.getCurrentItem()<photos.size()-1)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
            }
        });
        dialog.show();
    }

    class CustomPagerAdapter extends PagerAdapter {

        LayoutInflater mLayoutInflater;
        ArrayList<String> photos;

        public CustomPagerAdapter(ArrayList<String> photos) {
            mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.photos = photos;
        }

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.image_page_item, container, false);

            ImageView imageView = itemView.findViewById(R.id.imageView);
            String url = photos.get(position);
            if(!url.isEmpty())
                Picasso.with(getActivity()).load(url).into(imageView);

            container.addView(itemView);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Restaurant_Menu_Items parent = (Restaurant_Menu_Items) RestaurantMenuFragment.this.getParentFragment();
                    if(parent!=null) {
                        /*Fragment fragment = new PhotosView();
                        Bundle args = new Bundle();
                        args.putStringArrayList("photos", parent.photo_urls);
                        args.putInt("position", viewPager.getCurrentItem());
                        fragment.setArguments(args);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager
                                .beginTransaction();
                        fragmentTransaction.add(R.id.fragment_place, fragment);
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();*/

                    }
                }
            });

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    public void RenderingList(String searchString) {
        try {
            Restaurant_Menu_Items parent = (Restaurant_Menu_Items)getParentFragment();

            if (parent!=null && parent.menuJsonArray != null) {
                JSONArray jsonArray = parent.menuJsonArray;
                listDataHeader.clear();
                listDataChild.clear();
                if (jsonArray.length() > 0) {
                    // Get all menu items
                    ArrayList<JSONObject> Avaliable = new ArrayList<>();
                    ArrayList<JSONObject> notAvailable = new ArrayList<>();
                    ArrayList<JSONObject> others = new ArrayList<>();

                    try {
                        for (int j = 0; j < parent.menuJsonArrayFilter.length(); j++) {
                            JSONObject menuItems = parent.menuJsonArrayFilter.getJSONObject(j);
                            if (searchString.isEmpty() || menuItems.getString("menuItemName").toLowerCase().contains(searchString.toLowerCase())) {
                                String avalilability = CheckAvalability(menuItems);
                                menuItems.put("availability", avalilability);
                                if (avalilability.isEmpty()) {
                                    others.add(menuItems);
                                } else if (avalilability.equals("available")) {
                                    Avaliable.add(menuItems);
                                } else if (avalilability.equals("not_available")) {
                                    notAvailable.add(menuItems);
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    Avaliable.addAll(others);
                    Avaliable.addAll(notAvailable);
                    listDataHeader.add("All Menu Items" + "," + String.valueOf(Avaliable.size()));
                    listDataChild.put(listDataHeader.get(0), Avaliable);

                    // Category Menu Items
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        tabCategory.add(jsonObject.getJSONObject("menuCategory").getString("menuCategoryName"));
                        Log.d("Category Name", tabCategory.toString());
                        try {
                            JSONArray menus = jsonObject.getJSONArray("menuItems");
                            ArrayList<JSONObject> Avaliable1 = new ArrayList<>();
                            ArrayList<JSONObject> notAvailable1 = new ArrayList<>();
                            ArrayList<JSONObject> others1 = new ArrayList<>();
                            for (int j = 0; j < menus.length(); j++) {
                                JSONObject menuItems = menus.getJSONObject(j);
                                if (searchString.isEmpty() || menuItems.getString("menuItemName").toLowerCase().contains(searchString.toLowerCase())) {
                                    String avalilability = CheckAvalability(menuItems);
                                    menuItems.put("availability", avalilability);
                                    if (avalilability.isEmpty())
                                        others1.add(menuItems);
                                    else if (avalilability.equals("available"))
                                        Avaliable1.add(menuItems);
                                    else if (avalilability.equals("not_available"))
                                        notAvailable1.add(menuItems);
                                }
                            }
                            Avaliable1.addAll(others1);
                            Avaliable1.addAll(notAvailable1);
                            listDataHeader.add(jsonObject.getJSONObject("menuCategory").getString("menuCategoryName") + "," + String.valueOf(Avaliable1.size()));
                            listDataChild.put(listDataHeader.get(i + 1), Avaliable1);

                        } catch (Exception e) {
                        }
                    }
                    if (listDataChild.size() > 0) {
                        parent.dineBtn.setVisibility(View.GONE);
                        searchLayout.setVisibility(View.VISIBLE);
                        expListView.setVisibility(View.VISIBLE);
                        menuAvailable.setVisibility(View.VISIBLE);
                        photos_container.setVisibility(View.VISIBLE);
                        noMenuContent.setVisibility(View.GONE);
                        listAdapter = new ExpandableListAdapterFaq(getActivity(), listDataHeader, listDataChild, parent.currency, "");
                        expListView.setAdapter(listAdapter);
                        expListView.expandGroup(0);
                        //dineinLayout.setVisibility(View.GONE);
                    } else {
                        photos_container.setVisibility(View.GONE);
                        parent.dineBtn.setVisibility(View.VISIBLE);
                        searchLayout.setVisibility(View.GONE);
                        if (parent.photo_urls.size() < 0) {
                            parent.dineBtn.setVisibility(View.GONE);
                            //dineinLayout.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    searchLayout.setVisibility(View.GONE);

                    //Snackbar snackbar = Snackbar
                    //        .make(view, "There is no data to show", Snackbar.LENGTH_LONG);
                    //snackbar.show();
                }
            }
        } catch (Exception e) {
        }
    }

    public String CheckAvalability(JSONObject item){
        String Availablity = "";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String selectedTime = sdf.format(new Date(((NavigationDrawerActivity)getActivity()).resReq.startDate));
        if(item.has("availableSlot1")){
            try {
                if(CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("startTime"))>=0 &&
                        CompareTimeString(selectedTime,item.getJSONObject("availableSlot1").getString("endTime"))<=0)
                    Availablity = "available";
                else
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(item.has("availableSlot2")){
            try {
                if(CompareTimeString(selectedTime,item.getJSONObject("availableSlot2").getString("startTime"))>=0 &&
                        CompareTimeString(selectedTime,item.getJSONObject("availableSlot2").getString("endTime"))<=0)
                    Availablity = "available";
                else if(Availablity.isEmpty())
                    Availablity = "not_available";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(Availablity.isEmpty())
            Availablity = "available";
        return Availablity;
    }
    public static int CompareTimeString(String time1, String time2) {
        int hr1 = 0;
        int min1 = 0;
        int hr2 = 0;
        int min2 = 0;
        String[] a = time1.split(" ");
        if (a.length > 0) {
            String[] b = a[0].split(":");
            hr1 = Integer.parseInt(b[0]);
            min1 = Integer.parseInt(b[1]);
        }
        if (hr1 == 12)
            hr1 = 0;
        if (a.length > 1 && a[1].toUpperCase().equals("PM"))
            hr1 = hr1 + 12;
        a = time2.split(" ");
        if (a.length > 0) {
            String[] b = a[0].split(":");
            hr2 = Integer.parseInt(b[0]);
            min2 = Integer.parseInt(b[1]);
        }
        if (hr2 == 12)
            hr2 = 0;
        if (a.length > 1 && a[1].toUpperCase().equals("PM"))
            hr2 = hr2 + 12;
        if (hr1 > hr2)
            return 1;
        else if (hr1 < hr2)
            return -1;
        else if (min1 > min2)
            return 1;
        else if (min1 < min2)
            return -1;
        else
            return 0;
    }
    public void Food_Details(final Context act, JSONObject jsonStr, final String resId, final Fragment fragment, String currency,final JSONObject item) {
        //Log.d("JSON String", jsonStr.toString());
       final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.food_details);
        pref = act.getApplicationContext().getSharedPreferences("MyPref", 0);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);

        Button add = (Button) dialog.findViewById(R.id.add);
        Button less = (Button) dialog.findViewById(R.id.less);
        final Button count = (Button) dialog.findViewById(R.id.count);
        final Button add_cart = (Button) dialog.findViewById(R.id.add_cart_btn);
        LinearLayout topping = (LinearLayout) dialog.findViewById(R.id.topping_view);
        TextView smallPrice = (TextView) dialog.findViewById(R.id.small_price);
        TextView mediumPrice = (TextView) dialog.findViewById(R.id.medium_price);
        TextView largePrice = (TextView) dialog.findViewById(R.id.large_price);
        TextView itemName = (TextView) dialog.findViewById(R.id.item_name);
        final EditText instruction = (EditText) dialog.findViewById(R.id.instruction);
        final TextView priceTxt = (TextView) dialog.findViewById(R.id.price);
        LinearLayout smallLinear = (LinearLayout) dialog.findViewById(R.id.small_linear);
        LinearLayout mediumLinear = (LinearLayout) dialog.findViewById(R.id.medium_linear);
        LinearLayout largeLinear = (LinearLayout) dialog.findViewById(R.id.large_linear);
        final ImageView smallCheck = (ImageView) dialog.findViewById(R.id.small_check);
        final ImageView mediumCheck = (ImageView) dialog.findViewById(R.id.medium_check);
        final ImageView largeCheck = (ImageView) dialog.findViewById(R.id.large_check);
        final CircleImageView dishImage = (CircleImageView) dialog.findViewById(R.id.imageView7);
        final TextView or = dialog.findViewById(R.id.or);
        final Button dinein = dialog.findViewById(R.id.dine_in);
        final Button takeAway = dialog.findViewById(R.id.take_away);
        final LinearLayout buttons = dialog.findViewById(R.id.buttons);
        LinearLayout avail_layout = dialog.findViewById(R.id.avail_layout);
        TextView avail_time = dialog.findViewById(R.id.avail_time);
        LinearLayout sizes_layout = dialog.findViewById(R.id.sizes_layout);
        LinearLayout toppings_layout = dialog.findViewById(R.id.toppings_layout);
        final HashMap<String,String> selTopping = new HashMap<>();
        String availability = "";
        if(((NavigationDrawerActivity)getActivity()).resReq.requestType.equals("Delivery")){
            add_cart.setText("Add To Cart For Delivery");
        } else {
            add_cart.setText("Add To Cart");
            or.setVisibility(View.GONE);
            buttons.setVisibility(View.GONE);
        }
        if(item.has("availableSlot1")){
            try {
                availability = item.getJSONObject("availableSlot1").getString("startTime")+" to "+item.getJSONObject("availableSlot1").getString("endTime");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(item.has("availableSlot2")){
            if(!availability.isEmpty())
                availability = availability + "\n";
            try {
                availability = availability+item.getJSONObject("availableSlot2").getString("startTime")+" to "+item.getJSONObject("availableSlot2").getString("endTime");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(!availability.isEmpty()){
            avail_layout.setVisibility(View.VISIBLE);
            avail_time.setText(availability);
        }

        final Double[] priceval = new Double[1];
        try {
            if (item.getString("imageUrl").length() > 0) {
                Picasso.with(act)
                        .load(item.getString("imageUrl"))
                        .into(dishImage);
            }

            //final String[]menuId = new String[1];
            priceval[0] = Double.parseDouble(item.getString("price"));

            itemName.setText(item.getString("menuItemName"));
            priceTxt.setText(item.getString("price"));
        }catch (Exception e){

        }
        final HashMap<String, JSONObject> MenuChos = new HashMap<>();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                food_count = food_count + 1;
                count.setText(String.valueOf(food_count));

                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (food_count > 0) {
                    food_count = food_count - 1;
                    count.setText(String.valueOf(food_count));

                    //price=price-Integer.parseInt(priceTxt.getText().toString());
                    priceTxt.setText(Double.toString(food_count * priceval[0]));
                }

            }
        });

        smallLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smallCheck.setVisibility(View.VISIBLE);
                mediumCheck.setVisibility(View.GONE);
                largeCheck.setVisibility(View.GONE);
                try {
                    priceval[0] = MenuChos.get("Small").getDouble("menuchosPrice");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (String key : selTopping.keySet()){
                    priceval[0] = priceval[0] + Double.parseDouble(selTopping.get(key));
                }
                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        mediumLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smallCheck.setVisibility(View.GONE);
                mediumCheck.setVisibility(View.VISIBLE);
                largeCheck.setVisibility(View.GONE);
                try {
                    priceval[0] = MenuChos.get("Medium").getDouble("menuchosPrice");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (String key : selTopping.keySet()){
                    priceval[0] = priceval[0] + Double.parseDouble(selTopping.get(key));
                }
                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        largeLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                smallCheck.setVisibility(View.GONE);
                mediumCheck.setVisibility(View.GONE);
                largeCheck.setVisibility(View.VISIBLE);
                try {
                    priceval[0] = MenuChos.get("Large").getDouble("menuchosPrice");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (String key : selTopping.keySet()){
                    priceval[0] = priceval[0] + Double.parseDouble(selTopping.get(key));
                }
                priceTxt.setText(Double.toString(food_count * priceval[0]));
            }
        });

        dinein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerActivity)getActivity()).showDineIn(new RestaurantCheckin.RequestTypeChangeCallbak() {
                    @Override
                    public void requestTypeChanged() {
                        RenderingList("");
                    }
                });
                dialog.dismiss();
            }
        });

        takeAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerActivity)getActivity()).showTakeAway(new RestaurantCheckin.RequestTypeChangeCallbak() {
                    @Override
                    public void requestTypeChanged() {
                        RenderingList("");
                    }
                });
                dialog.dismiss();
            }
        });

        add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("SessionCheck :", String.valueOf(pref.getString("loginSession", null)));
                String availability = "";
                try {
                    availability = item.getString("availability");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                if(availability.equals("not_available")){
                    AlertDialog.Builder alert = new AlertDialog.Builder(act);
                    alert.setTitle("Not Available");
                    if(((NavigationDrawerActivity)getActivity()).resReq.requestType.equals("Delivery"))
                        alert.setMessage("This menu Item not Available Now so we are unable to deliver this item now. Choose DineIn or TakeAway with Available");
                    else
                        alert.setMessage("This menu Item not Available in your "+((NavigationDrawerActivity)act).resReq.requestType+" timing. adjest your timing for add this item");
                    alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alert.show();
                }else if (pref.getString("loginSession", null) != null) {
                    if (pref.getString("loginSession", null).equals("Valid")) {
                        try {
                            String tag_json_obj = "json_obj_req";
                            String url = Constants.URL + "customer/" + pref.getString("userId", null) + "/business/" + resId + "/cart";

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("menuItemId", item.getString("menuItemId"));
                            jsonObject.put("qty", food_count);

                            // toppings
                            JSONArray jsonArray = new JSONArray();
                            for (String key : selTopping.keySet()){
                                JSONObject jsonObject1 = new JSONObject();
                                jsonObject1.put("restMenuName", key);
                                jsonObject1.put("restMenuPrice", selTopping.get(key));
                                jsonArray.put(jsonObject1);
                            }
                            /*JSONObject jsonObject1 = new JSONObject();

							for (int i = 0; i < 1; i++) {
								jsonObject1.put("restMenuName", "Pepparoni");
								jsonObject1.put("restMenuPrice", "13");
							}
							jsonArray.put(jsonObject1);*/
                            jsonObject.put("restMenuToppings", jsonArray);
                            jsonObject.put("splCookIns", instruction.getText().toString());

                            // Size
                            if (smallCheck.getVisibility() == View.VISIBLE) {
                                jsonObject.put("restaurantMenuChos", MenuChos.get("Small"));
                            } else if (mediumCheck.getVisibility() == View.VISIBLE) {
                                jsonObject.put("restaurantMenuChos", MenuChos.get("Medium"));
                            } else if (largeCheck.getVisibility() == View.VISIBLE) {
                                jsonObject.put("restaurantMenuChos", MenuChos.get("Large"));
                            }

                            Log.e("Add to Cart Request", url + " " + jsonObject.toString());

                            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                                    url, jsonObject,
                                    new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {

                                            Log.e("Add to Cart Response", response.toString());
                                            try {
                                                if (response.has("status") && response.getString("status").equals("SUCCESS")) {
                                                    if (fragment instanceof Restaurant_Menu_Items) {
                                                        ((Restaurant_Menu_Items) fragment).updateCartValueFromServer();
                                                    }
                                                } else {
                                                    AlertDialog.Builder alert = new AlertDialog.Builder(act);
                                                    alert.setTitle("Error");
                                                    if (response.has("message")) {
                                                        alert.setMessage(response.getString("message"));
                                                    } else {
                                                        alert.setMessage("Failed to add record");
                                                    }
                                                    alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {

                                                        }
                                                    });
                                                    alert.show();
                                                }
                                            } catch (JSONException e){

                                            }
                                            dialog.dismiss();
                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });

                            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                        } catch (Exception e) {
                            System.out.println("Error :" + e.toString());
                        }
                    } else {
                        dialog.dismiss();
                        addFragment(new LoginFragment(),false);
                    }
                } else {
                    dialog.dismiss();
                    addFragment(new LoginFragment(),false);
                }

            }
        });


        try {
            if (jsonStr != null) {
                JSONObject jsonObject = jsonStr;
                JSONArray jsonArray = jsonObject.getJSONArray("restMenuToppings");
                for (int i = 0; i < jsonArray.length(); i++) {
                    toppings_layout.setVisibility(View.VISIBLE);
                    final JSONObject items = jsonArray.getJSONObject(i);
                    View layout2 = LayoutInflater.from(act).inflate(R.layout.topping_layout, topping, false);
                    CheckBox check = (CheckBox) layout2.findViewById(R.id.check_box);
                    TextView priceCheck = (TextView) layout2.findViewById(R.id.price);
                    check.setText(items.getString("restMenuName"));
                    priceCheck.setText(currency + " " + items.getString("restMenuPrice"));
                    check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            try {
                                if (isChecked) {
                                    selTopping.put(items.getString("restMenuName"), items.getString("restMenuPrice"));
                                    priceval[0] = priceval[0] + items.getDouble("restMenuPrice");
                                } else {
                                    selTopping.remove(items.getString("restMenuName"));
                                    priceval[0] = priceval[0] - items.getDouble("restMenuPrice");
                                }
                                priceTxt.setText(Double.toString(food_count * priceval[0]));
                            } catch (JSONException e){

                            }
                        }
                    });
                    topping.addView(layout2);
                }

                JSONArray jsonArrayMenu = jsonObject.getJSONArray("restaurantMenuChos");
                for (int i = 0; i < jsonArrayMenu.length(); i++) {
                    sizes_layout.setVisibility(View.VISIBLE);
                    JSONObject items = jsonArrayMenu.getJSONObject(i);
                    if (items.getString("menuchosSize").equals("Small") && !items.getString("menuchosPrice").equals("null")) {
                        smallLinear.setVisibility(View.VISIBLE);
                        smallPrice.setText(currency + " " + items.getString("menuchosPrice"));
                        MenuChos.put("Small", items);
                    }
                    if (items.getString("menuchosSize").equals("Medium") && !items.getString("menuchosPrice").equals("null")) {
                        mediumLinear.setVisibility(View.VISIBLE);
                        mediumPrice.setText(currency + " " + items.getString("menuchosPrice"));
                        MenuChos.put("Medium", items);
                    }
                    if (items.getString("menuchosSize").equals("Large") && !items.getString("menuchosPrice").equals("null")) {
                        largeLinear.setVisibility(View.VISIBLE);
                        largePrice.setText(currency + " " + items.getString("menuchosPrice"));
                        MenuChos.put("Large", items);
                    }
                }

                if (smallLinear.getVisibility() == View.VISIBLE) {
                    priceval[0] = MenuChos.get("Small").getDouble("menuchosPrice");
                    smallCheck.setVisibility(View.VISIBLE);
                } else if (mediumLinear.getVisibility() == View.VISIBLE) {
                    priceval[0] = MenuChos.get("Medium").getDouble("menuchosPrice");
                    mediumCheck.setVisibility(View.VISIBLE);
                } else if (largeLinear.getVisibility() == View.VISIBLE) {
                    priceval[0] = MenuChos.get("Large").getDouble("menuchosPrice");
                    largeCheck.setVisibility(View.VISIBLE);
                }
                priceTxt.setText(Double.toString(priceval[0]));
            }

        } catch (Exception e) {

        }


        dialog.show();

    }

}
