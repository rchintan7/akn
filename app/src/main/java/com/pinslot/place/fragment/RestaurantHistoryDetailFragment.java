package com.pinslot.place.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pinslot.place.AppController;
import com.pinslot.place.Constants;
import com.pinslot.place.R;
import com.pinslot.place.Utils;
import com.pinslot.place.model.CSUser;
import com.pinslot.place.widget.CornerImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RestaurantHistoryDetailFragment extends android.support.v4.app.Fragment implements View.OnClickListener {

    TextView receipt_id, res_name, res_address, time, subtotal, tax, total, paid, sgst_text, orderType, noOfPeople, total_lbl, total_val, noFood;
    LinearLayout dishes, taxParent,exp_layout;
    ImageView rest_image,rate;
    JSONObject restaurent_detail;
    JSONObject res_detail;
    String Restaurent_booking_id;
    RelativeLayout bottomSection;
    String type = "";
    //PlaceExpBookedDetail exp;
    TextView expName,expDesc,expTime,status,payment;
    LinearLayout paymentDetails;
    LinearLayout user_location;
    RelativeLayout bottom_buttons;
    CardView accept,reject,contact,user,cancel;
    CornerImageView image;
    TextView uname,email,phone;
    LinearLayout response_layout;
    CSUser customer;
    JSONObject res_booking_detail;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("Type","");
        if(type.equals("exp")){
            /*try {
                exp = new PlaceExpBookedDetail(new JSONObject(getArguments().getString("json")));
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        } else {
            Restaurent_booking_id = getArguments().getString("id");
            if (Restaurent_booking_id != null && !Restaurent_booking_id.isEmpty()) {
                webServiceCallForGetResvation(Restaurent_booking_id);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant_history,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        receipt_id = (TextView) view.findViewById(R.id.receipt_id);
        res_name = (TextView) view.findViewById(R.id.restaurant_name);
        res_address = (TextView) view.findViewById(R.id.res_address);
        time = (TextView) view.findViewById(R.id.pickupTime);
        subtotal = (TextView) view.findViewById(R.id.sub_total);
        tax = (TextView) view.findViewById(R.id.tax);
        total = (TextView) view.findViewById(R.id.total);
        noFood = (TextView) view.findViewById(R.id.no_food);
        dishes = (LinearLayout) view.findViewById(R.id.dishes);
        exp_layout = view.findViewById(R.id.exp_layout);
        rest_image = (ImageView) view.findViewById(R.id.rest_image);
        rate = (ImageView) view.findViewById(R.id.rating);
        orderType = view.findViewById(R.id.order_type);
        noOfPeople = view.findViewById(R.id.no_of_people);
        taxParent = (LinearLayout) view.findViewById(R.id.tax_parent);
        bottomSection = (RelativeLayout) view.findViewById(R.id.bottom_section);
        total_val = view.findViewById(R.id.total_val);
        sgst_text = view.findViewById(R.id.sgst);
        expName = view.findViewById(R.id.exp_name);
        expDesc = view.findViewById(R.id.exp_des);
        expTime = view.findViewById(R.id.exp_time);
        status = view.findViewById(R.id.status);
        paymentDetails = view.findViewById(R.id.payment_details);
        payment = view.findViewById(R.id.payment);
        bottom_buttons = view.findViewById(R.id.bottom_buttons);
        accept = view.findViewById(R.id.accept);
        reject = view.findViewById(R.id.reject);
        cancel = view.findViewById(R.id.cancel);
        contact = view.findViewById(R.id.contact);
        image = view.findViewById(R.id.image);
        uname = view.findViewById(R.id.name);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.phone);
        user = view.findViewById(R.id.user);
        user_location = view.findViewById(R.id.user_location);
        response_layout = view.findViewById(R.id.response_layout);
        accept.setOnClickListener(this);
        reject.setOnClickListener(this);
        cancel.setOnClickListener(this);
        contact.setOnClickListener(this);
        user_location.setOnClickListener(this);
        /*rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                Bundle args = new Bundle();
                if(type.equals("exp")){
                    fragment = new ResExperienceReviewsFragment();
                    args.putString("exp", exp.experience.srcJson.toString());
                    args.putString("Type", "res_exp");
                    args.putString("res",exp.restaurant.srcJson.toString());
                    args.putString("exp_id",exp.experience.id);
                    args.putString("title",exp.experience.title);
                    args.putDouble("rate",0);
                } else if(res_detail!=null){
                    RestaurantReviewsFragment restaurantReviewsFragment = new RestaurantReviewsFragment();
                    restaurantReviewsFragment.restaurant = new Restaurant(res_detail);
                    args.putString("title",restaurantReviewsFragment.restaurant.name);
                    fragment = restaurantReviewsFragment;
                }
                fragment.setArguments(args);
                fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fragment,fragment.getClass().getName());
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }
        });*/
        /*---if(type.equals("exp")&& exp!=null){
            AssignSlot();
        }*/
        if(type.equals("Manager")){
            bottom_buttons.setVisibility(View.VISIBLE);
            user_location.setVisibility(View.GONE);
            rate.setVisibility(View.GONE);
            LoadBottom();
        }
        if(restaurent_detail!=null && res_detail!=null) {
            JSONObject res_location = null;
            try {
                res_location = res_detail.getJSONObject("location");

                if (restaurent_detail.getString("bookingId") != null) {
                    receipt_id.setText(restaurent_detail.getString("bookingId"));
                }
                if (res_detail != null && res_detail.getString("name") != null) {
                    res_name.setText(res_detail.getString("name"));
                }
                if (res_location != null && res_location.getString("addressString") != null) {
                    res_address.setText(res_location.getString("addressString"));
                }
                String currency = restaurent_detail.getString("currency");
                if (currency == null)
                    currency = "";
                if (restaurent_detail.getJSONArray("menuItemForHistory") != null) {
                    if(restaurent_detail.has("taxList"))
                        AddFoodItems(restaurent_detail.getJSONArray("menuItemForHistory"), currency,restaurent_detail.getJSONArray("taxList"));
                    else
                        AddFoodItems(restaurent_detail.getJSONArray("menuItemForHistory"), currency,null);
                }
                if (res_detail != null && res_detail.getJSONArray("photoUrls").length() > 0)
                    Picasso.with(getActivity()).load(res_detail.getJSONArray("photoUrls").getString(0)).into(rest_image);
                Date date = new Date(restaurent_detail.getLong("pickupDate"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy hh:mm a", Locale.ENGLISH);
                String stringDate = sdf.format(date);
                time.setText(stringDate);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void LoadBottom(){
        if(getArguments().containsKey("json")||restaurent_detail!=null) {
            try {
                CSUser user = null;

                JSONObject obj = restaurent_detail;
                if(restaurent_detail==null&&getArguments().containsKey("json"))
                    obj = new JSONObject(getArguments().getString("json"));
                if(obj.has("customer")){
                    user = new CSUser(obj.getJSONObject("customer"));
                    customer = user;
                }
                if(customer==null||customer.location==null||customer.location.coordinate==null)
                    user_location.setVisibility(View.GONE);
                //else
                //    user_location.setVisibility(View.VISIBLE);
                if(obj.has("status")){
                    String statusStr = obj.getString("status");
                    if(statusStr.equals("ACTIVE"))
                        status.setText("Pending Confirmation");
                    else if(statusStr.equals("APPROVED")){
                        cancel.setVisibility(View.VISIBLE);
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                    } else {
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                    }
                }
                if(obj.has("firstName"))
                    uname.setText(obj.getString("firstName"));
                else if(user!=null&&user.contactDetails!=null)
                    uname.setText(user.contactDetails.getName());
                if(obj.has("emailId"))
                    email.setText(obj.getString("emailId"));
                else if(user!=null&&user.contactDetails!=null)
                    email.setText(user.contactDetails.emailAddress);
                if(obj.has("phoneNumber"))
                    phone.setText(obj.getString("phoneNumber"));
                else if(user!=null&&user.contactDetails!=null)
                    phone.setText(user.contactDetails.mobilePhone);
                if(user!=null&&!user.photoLocation.isEmpty())
                    image.setImageUrl(user.photoLocation);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void webServiceCallForGetResvation(String id){

        try{
            String tag_json_obj = "json_obj_req";
            String url = Constants.RESTAURANT_HISTORY_DETAIL + id;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Menu Request", url.toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, "",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            pDialog.dismiss();
                            //jsonResponse=response.toString();
                            try {
                                if(response.getString("status").equals("SUCCESS"))
                                {
                                    res_booking_detail = response.getJSONObject("data");
                                    restaurent_detail = res_booking_detail;
                                    res_detail = res_booking_detail.getJSONObject("restaurant");
                                    JSONObject res_location = res_detail.getJSONObject("location");
                                    if(res_booking_detail.getString("bookingId")!=null && receipt_id!=null) {
                                        receipt_id.setText(res_booking_detail.getString("bookingId"));
                                    }
                                    if(res_booking_detail.has("noOfPeople"))
                                        noOfPeople.setText("No of people : "+res_booking_detail.getString("noOfPeople"));
                                    if(res_detail!=null&&res_detail.getString("name")!=null && res_name!=null){
                                        res_name.setText(res_detail.getString("name"));
                                    }
                                    if(res_location!=null && res_location.getString("addressString")!=null && res_address!=null) {
                                        res_address.setText(res_location.getString("addressString"));
                                    }
                                    String currency = res_booking_detail.getString("currency");
                                    if(currency==null)
                                        currency = "";
                                    if(res_detail!=null && res_detail.getJSONArray("photoUrls").length()>0 && rest_image!=null)
                                        Picasso.with(getActivity()).load(res_detail.getJSONArray("photoUrls").getString(0)).into(rest_image);
                                    Date date = new Date(res_booking_detail.getLong("pickupDate"));
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yy hh:mm a", Locale.ENGLISH);
                                    String stringDate = sdf.format(date);
                                    if(time!=null)
                                        time.setText(stringDate);
                                    if(res_booking_detail.getJSONArray("menuItemForHistory")!=null && dishes!=null) {
                                        if(res_booking_detail.has("taxList"))
                                            AddFoodItems(res_booking_detail.getJSONArray("menuItemForHistory"),currency,res_booking_detail.getJSONArray("taxList"));
                                        else
                                            AddFoodItems(res_booking_detail.getJSONArray("menuItemForHistory"),currency,null);
                                    } else {
                                        if(res_booking_detail.has("taxList"))
                                            AddFoodItems(new JSONArray(),currency,res_booking_detail.getJSONArray("taxList"));
                                        else
                                            AddFoodItems(new JSONArray(),currency,null);
                                    }
                                    paymentDetails.setVisibility(View.VISIBLE);
                                    if(res_booking_detail.has("status")){
                                        String statusStr = res_booking_detail.getString("status");
                                        status.setText(statusStr);
                                        if(statusStr.equals("ACTIVE"))
                                            status.setText("Pending Confirmation");
                                    }
                                    if(type.equals("Manager"))
                                        LoadBottom();
                                    if(res_booking_detail.has("paymentType")&&res_booking_detail.getString("paymentType").equals("PAR")){
                                        payment.setText("Pay At Restaurant");
                                    } else {
                                        payment.setText("Prepaid");
                                    }
                                }
                            }catch (Exception e){
                                Log.d("JSON Parsing Error",e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

// Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }catch (Exception e){
            System.out.println("Error :"+e.toString());
        }

    }

    /*----public void AssignSlot(){
        dishes.setVisibility(View.GONE);
        exp_layout.setVisibility(View.VISIBLE);
        expName.setText(exp.experience.title);
        expDesc.setText(exp.experience.desc);
        res_name.setText(exp.restaurant.name);
        res_address.setText(exp.restaurant.location.addressString);
        receipt_id.setText(exp.bookingId);
        orderType.setText("exp Time : ");
        noOfPeople.setText("No of people : "+Integer.toString(exp.noOfPeople));

        SimpleDateFormat tf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        Date d1 = new Date(exp.slot.startDateTime);
        Date d2 = new Date(exp.slot.endDateTime);
        expTime.setText(tf.format(d1)+" to "+tf.format(d2));
        time.setText(sdf.format(d1));
        if(exp.experience.imageUrls!=null&&exp.experience.imageUrls.length>0){
            PicassoUtil.with(getContext()).load(exp.experience.imageUrls[0]).into(rest_image);
        }else if(!exp.experience.bannerImgUrl.isEmpty()){
            PicassoUtil.with(getContext()).load(exp.experience.bannerImgUrl).into(rest_image);
        } else if(!exp.experience.imageUrl.isEmpty()){
            PicassoUtil.with(getContext()).load(exp.experience.imageUrl).into(rest_image);
        } else if(exp.experience.galleries.size()>0){
            PicassoUtil.with(getContext()).load(exp.experience.galleries.get(0).imageUrl).into(rest_image);
        }

        View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
        TextView taxName = (TextView) view1.findViewById(R.id.tax_name);
        TextView taxCost = (TextView) view1.findViewById(R.id.tax_cost);
        taxName.setText("Unit Total");
        taxCost.setText(exp.restaurant.currency + " " + String.format("%.2f", exp.totalPrice));
        taxParent.addView(view1);

        JSONObject jsonObject = null;
        if (exp.promoCodes.length() > 0) {
            try {
                jsonObject = exp.promoCodes.getJSONObject(0);
                View view2 = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
                TextView taxNames = (TextView) view2.findViewById(R.id.tax_name);
                TextView taxCosts = (TextView) view2.findViewById(R.id.tax_cost);
                taxNames.setText("Offer ("+jsonObject.getString("promoName")+")");
                taxCosts.setText(exp.restaurant.currency + " " + String.format("%.2f", jsonObject.getDouble("discountPrice")));
                taxParent.addView(view2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //General Offer
            View view3 = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
            TextView taxName1 = (TextView) view3.findViewById(R.id.tax_name);
            TextView taxCost1 = (TextView) view3.findViewById(R.id.tax_cost);
            taxName1.setText("Sub Total");
            try {
                taxCost1.setText(exp.restaurant.currency + " " + String.format("%.2f", exp.totalPrice - jsonObject.getDouble("discountPrice")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            taxParent.addView(view3);
        }
        if (exp.taxList != null) {
            for (int i = 0; i < exp.taxList.length(); i++) {

                View v = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
                TextView taxName2 = (TextView) v.findViewById(R.id.tax_name);
                TextView taxCost2 = (TextView) v.findViewById(R.id.tax_cost);
                try {
                    taxName2.setText(exp.taxList.getJSONObject(i).getString("name") + "(" + exp.taxList.getJSONObject(i).getString("percent") + ")");
                    taxCost2.setText(exp.restaurant.currency + " " + String.format("%.2f", exp.taxList.getJSONObject(i).getDouble("price")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                taxParent.addView(v);
            }
        }
        total_val.setText(exp.restaurant.currency + " " + String.format("%.2f", exp.finalPrice));
    }*/
    public void AddFoodItems(JSONArray foodItems, String currency,JSONArray Tax){
        Double subtol = 0.0;
        dishes.removeAllViews();
        try {
            if (foodItems.length() == 0) {
                bottomSection.setVisibility(View.GONE);
                noFood.setVisibility(View.VISIBLE);
            }
            JSONObject obj = null;
            if (getArguments().containsKey("json")) {
                obj = new JSONObject(getArguments().getString("json"));
                if (obj != null && obj.has("taxList"))
                    Tax = obj.getJSONArray("taxList");
            } else {
                if (res_booking_detail != null)
                    obj = res_booking_detail;
            }
            for (int i = 0; i < foodItems.length(); i++) {
                final JSONObject fooditem = (JSONObject) foodItems.get(i);
                View v = getActivity().getLayoutInflater().inflate(R.layout.food_item,null);
                TextView foodName = (TextView) v.findViewById(R.id.food_name);
                TextView qty = (TextView) v.findViewById(R.id.qty);
                TextView price = (TextView) v.findViewById(R.id.price);
                TextView offerName = (TextView) v.findViewById(R.id.offer_name);
                TextView offerPrice = (TextView) v.findViewById(R.id.offer_price);
                ImageView foodImage = (ImageView) v.findViewById(R.id.food_image);
                TextView size = (TextView) v.findViewById(R.id.size);

                if (fooditem.has("restaurantMenuChos")) {
                    size.setVisibility(View.VISIBLE);
                    size.setText("Size: " + fooditem.getJSONObject("restaurantMenuChos").getString("menuchosSize"));
                }

                LinearLayout people = v.findViewById(R.id.people);
                TextView people_count = v.findViewById(R.id.people_count);
                if(fooditem.has("members")&&fooditem.getJSONArray("members").length()>0){
                    people.setVisibility(View.VISIBLE);
                    people_count.setText(Integer.toString(fooditem.getJSONArray("members").length()));
                } else
                    people.setVisibility(View.GONE);
                if(fooditem.getString("menu_name")!=null)
                    foodName.setText(fooditem.getString("menu_name"));
                people.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            ShowPeople(fooditem);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                qty.setText(Integer.toString(fooditem.getInt("quantity")));
                if(fooditem.getString("price")!=null)
                    price.setText(currency + " " + String.format("%.2f", fooditem.getDouble("finalPrice")));
                subtol = subtol+/*fooditem.getInt("quantity")*/fooditem.getDouble("price");
                if(fooditem.getString("item_url")!=null&&!fooditem.getString("item_url").isEmpty())
                    Picasso.with(getActivity()).load(fooditem.getString("item_url")).into(foodImage);

                // Update Offer
                if (fooditem.has("appliedPromocodes")) {
                    if (fooditem.getJSONArray("appliedPromocodes").length() > 0) {
                        JSONObject jsonObject = fooditem.getJSONArray("appliedPromocodes").getJSONObject(0);
                        offerName.setText(jsonObject.getString("promoName"));
                        offerPrice.setText(currency + " " + jsonObject.getString("discountPrice") + "0");
                    } else {
                        offerName.setVisibility(View.GONE);
                        offerPrice.setVisibility(View.GONE);
                    }
                } else {
                    offerName.setVisibility(View.GONE);
                    offerPrice.setVisibility(View.GONE);
                }

                dishes.addView(v);
            }
            int cgst = 0;
            int sgst = 0;
            if(Tax!=null){
                for(int i = 0;i<Tax.length();i++){
                    if(i==0)
                        cgst = Tax.getJSONObject(i).getInt("percent");
                    if(i==1)
                        sgst = Tax.getJSONObject(i).getInt("percent");
                }
            }
            Double tot = subtol * (100 + cgst+sgst)/100;

            View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
            TextView taxName = (TextView) view1.findViewById(R.id.tax_name);
            TextView taxCost = (TextView) view1.findViewById(R.id.tax_cost);
            taxName.setText("Unit Total ");
            taxCost.setText(currency + " " + String.format("%.2f", obj.getDouble("totalPrice")));
            JSONObject jsonObject = null;
            if (restaurent_detail.has("appliedPromocodes")) {
                if (restaurent_detail.getJSONArray("appliedPromocodes").length() > 0) {
                    taxParent.addView(view1);
                    for(int i =0;i<restaurent_detail.getJSONArray("appliedPromocodes").length();i++) {
                        jsonObject = restaurent_detail.getJSONArray("appliedPromocodes").getJSONObject(i);
                        //General Offer
                        View view2 = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
                        TextView taxNames = (TextView) view2.findViewById(R.id.tax_name);
                        TextView taxCosts = (TextView) view2.findViewById(R.id.tax_cost);
                        taxNames.setText("Offer ("+jsonObject.getString("promoName")+")");
                        taxCosts.setText(currency + " " + String.format("%.2f", jsonObject.getDouble("discountPrice")));
                        taxParent.addView(view2);
                    }
                }
            }

            View view3 = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
            TextView taxName1 = (TextView) view3.findViewById(R.id.tax_name);
            TextView taxCost1 = (TextView) view3.findViewById(R.id.tax_cost);
            taxName1.setText("Sub Total ");
            double tot_val = obj.getDouble("totalPrice");
            if(obj.has("discountPrice")&&!obj.getString("discountPrice").isEmpty())
                tot_val = tot_val - obj.getDouble("discountPrice");
            taxCost1.setText(currency + " " + String.format("%.2f",tot_val));
            taxParent.addView(view3);

            if (Tax != null) {
                for (int i = 0; i < Tax.length(); i++) {

                    View v = LayoutInflater.from(getActivity()).inflate(R.layout.tax_row_view_history, null);
                    TextView taxName2 = (TextView) v.findViewById(R.id.tax_name);
                    TextView taxCost2 = (TextView) v.findViewById(R.id.tax_cost);
                    cgst = Tax.getJSONObject(i).getInt("percent");

                    taxName2.setText(Tax.getJSONObject(i).getString("name") + "(" + Tax.getJSONObject(i).getString("percent") + ")");
                    taxCost2.setText(currency + " " + String.format("%.2f", Tax.getJSONObject(i).getDouble("price")));

                    taxParent.addView(v);

                }
            }
            total_val.setText(currency + " " + String.format("%.2f", obj.getDouble("finalPrice")));

        }catch (JSONException e){
            Log.d("Exception", e.toString());
        }
        //}
    }

    public void ShowPeople(final JSONObject item) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        if(item.has("members")){
            jsonArray = item.getJSONArray("members");
        }
        final JSONArray peoples = jsonArray;
        final android.app.Dialog dialog = new android.app.Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_add_people);
        final LinearLayout itemLayout = dialog.findViewById(R.id.peoples);
        TextView addMore = dialog.findViewById(R.id.add_more);
        Button submit = dialog.findViewById(R.id.submit);
        for (int i=0;i<peoples.length();i++){
            AddView(itemLayout,peoples.getJSONObject(i).getString("firstName"),peoples.getJSONObject(i).getString("lastName"));
        }
        addMore.setVisibility(View.GONE);
        submit.setVisibility(View.GONE);
        dialog.show();
    }

    public void AddView(final LinearLayout parent,String fname,String lname){
        final View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.event_who_contact_layout, parent, false);
        TextView name = layout2.findViewById(R.id.name);
        ImageView user_icon = layout2.findViewById(R.id.user_icon);
        String fullname = fname;
        fullname = !lname.isEmpty()?fullname.isEmpty()?lname:fullname+" "+lname:fullname;
        name.setText(fullname);
        user_icon.setImageBitmap(Utils.getRandomColorImage(getActivity(),fullname.substring(0,1)));
        ImageView remove = layout2.findViewById(R.id.remove);
        ImageView msg_icon = layout2.findViewById(R.id.message_icon);
        ImageView call_icon = layout2.findViewById(R.id.call_icon);
        remove.setVisibility(View.GONE);
        msg_icon.setVisibility(View.GONE);
        call_icon.setVisibility(View.GONE);
        parent.addView(layout2);
    }

    @Override
    public void onClick(View v) {
        if(v==accept){
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Confirmation");
            alert.setMessage("Are you sure want to accept this order?");
            alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    webServiceCallForResponse(getArguments().getString("id"), "APPROVED");
                }
            });
            alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert.show();
        }
        if(v == reject){
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Confirmation");
            alert.setMessage("Are you sure want to reject this order?");
            alert.setPositiveButton("Reject", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    webServiceCallForResponse(getArguments().getString("id"), "REJECTED");
                }
            });
            alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert.show();
        }
        if(v == cancel){
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Confirmation");
            alert.setMessage("Are you sure want to Reject this order?");
            alert.setPositiveButton("Reject", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    webServiceCallForResponse(getArguments().getString("id"), "REJECTED");
                }
            });
            alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert.show();
        }
        if(v == contact){
            /*String userid = "";
            String phone = "";
            JSONObject obj = null;
            Fragment fragment = new Communication();
            Bundle args = new Bundle();
            try {
                if (restaurent_detail != null) {
                    if (restaurent_detail.has("phoneNumber"))
                        phone = restaurent_detail.getString("phoneNumber");
                    if (customer!=null) {
                        userid = customer.profileId;
                        obj = Utils.getChatObj(res_name.getText().toString(), customer.srcJson);
                        if (phone.isEmpty() && customer.contactDetails != null)
                            phone = customer.contactDetails.mobilePhone;
                    }
                    if (res_detail != null) {
                        args.putString("type", "RestaurantDetails");
                        args.putString("orderId", res_detail.getString("id"));
                        args.putString("topicId", "RestaurantDetails");
                        if (res_detail.has("chatId"))
                            args.putString("chatId", res_detail.getString("chatId"));
                        else
                            args.putString("chatId", "");
                    }
                }
            } catch (JSONException e){

            }
            if(!userid.isEmpty()&&userid.equals(PrefManager.getInstance(getContext()).getUserId())){
                Toast.makeText(getContext(),"Cannot Contact yourself",Toast.LENGTH_LONG).show();
            } else {
                if (!userid.isEmpty()) {
                    args.putString("opponentID", userid);
                    args.putString("obj", obj.toString());
                } else
                    args.putBoolean("disable_chat", true);
                if (!phone.isEmpty())
                    args.putString("phone", phone);
                else
                    args.putBoolean("disable_phone", true);
                args.putBoolean("disable_video", true);
                args.putBoolean("disable_stream", true);
                fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }*/
        }
        if(v == user_location){
            /*if(restaurent_detail!=null&&restaurent_detail.has("customer")){
                try {
                    CSUser customer = new CSUser(restaurent_detail.getJSONObject("customer"));
                    if(customer.location!=null&&customer.location.coordinate!=null) {
                        Intent location = new Intent(getActivity(), LocationActivity.class);
                        location.putExtra("name", customer.contactDetails.getName());
                        location.putExtra("lat", customer.location.coordinate.latitude);
                        location.putExtra("lon", customer.location.coordinate.longitude);
                        location.putExtra("address", customer.location.addressString);
                        location.putExtra("hide_street", true);
                        if (!customer.photoLocation.isEmpty())
                            location.putExtra("markerUrl", customer.photoLocation);
                        else
                            location.putExtra("markerId", R.mipmap.profile_image_placeholder);
                        startActivity(location);
                    } else {
                        Toast.makeText(getContext(),"User location not available",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }

    public void webServiceCallForResponse(final String reqId, String res) {
        /*try {
            String tag_json_obj = "json_obj_req";
            String url = Constants.URL+"connectslot-admin-api/restaurantAdmin/ejectOrAcceptRestaurantBookingHistory";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", res);
            jsonObject.put("orderId", reqId);

            //String url = Constants.URL + "/retail/business/coupon/manager/" + PrefManager.getInstance(getContext()).getUserId() + "/redeem/request/" + reqId + "/" + res;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            Log.d("Menu Request", url.toString() + jsonObject);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Menu Response", response.toString());
                            try {
                                pDialog.dismiss();
                                if (response.getString("status").equals("SUCCESS")) {

                                    if (response.has("message"))
                                        Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();

                                    AdminNotificationFragment fragment = (AdminNotificationFragment) getActivity().getSupportFragmentManager().findFragmentByTag(AdminNotificationFragment.class.getName());
                                    if (fragment != null)
                                        fragment.removeRecord(reqId);
                                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                                } else {
                                    if (response.has("message"))
                                        Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } catch (Exception e) {
            System.out.println("Error :" + e.toString());
        }*/

    }
}
