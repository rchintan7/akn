package com.pinslot.place.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pinslot.place.Constants;
import com.pinslot.place.NavigationDrawerActivity;
import com.pinslot.place.R;
import com.pinslot.place.adapter.CheckoutDateAdapter;
import com.pinslot.place.model.CSPlace;
import com.pinslot.place.widget.PicassoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Ticket_Confirmation extends BaseFragment {
    ImageView share, history, cal, invite, imageView;
    TextView bookingId, totalPrice, eventName, time, more, address;
    JSONObject jsonObject;
    CSPlace placeObj = null;
    TextView foodpkg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ticket_confirmation, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        share = (ImageView) view.findViewById(R.id.share);
        history = (ImageView) view.findViewById(R.id.history);
        cal = (ImageView) view.findViewById(R.id.cal);
        invite = (ImageView) view.findViewById(R.id.invite);
        bookingId = (TextView) view.findViewById(R.id.bookingId);
        totalPrice = (TextView) view.findViewById(R.id.price);
        eventName = (TextView) view.findViewById(R.id.event_name);
        address = (TextView) view.findViewById(R.id.address);
        foodpkg = view.findViewById(R.id.food_pkg);
        time = (TextView) view.findViewById(R.id.time);
        more = (TextView) view.findViewById(R.id.more);
        imageView = view.findViewById(R.id.image_view);
        placeObj = ((NavigationDrawerActivity)getActivity()).placeRequest.place;
        bookingId.setText(getArguments().getString("bookingId"));
        totalPrice.setText("Booking Amount : "+ getArguments().getString("currency","\u20B9")+" " + getArguments().getString("totalPrice"));
        eventName.setText(placeObj.name);
        if (!placeObj.imageUrl.isEmpty()) {
            PicassoUtil.with(getActivity())
                    .load(placeObj.imageUrl)
                    .into(imageView);
        } else {
            imageView.setImageResource(R.mipmap.restaurant_img);
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy  hh:mm a");
        try {
            if(placeObj!=null) {
                address.setText(placeObj.location.addressString);
            }
            if(getArguments().containsKey("selected_package"))
                foodpkg.setVisibility(View.VISIBLE);
            else
                foodpkg.setVisibility(View.GONE);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
            if(((NavigationDrawerActivity)getActivity()).placeRequest.type.equals("Hourly")){
                Date date = ((NavigationDrawerActivity)getActivity()).placeRequest.dates.get(0);
                time.setText(sdf.format(date));
            } else {
                Date date = ((NavigationDrawerActivity)getActivity()).placeRequest.dates.get(0);
                time.setText(sdf.format(date)+" - 11:59 PM");
            }
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        }
        if (((NavigationDrawerActivity)getActivity()).placeRequest.dates.size()>1) {
            more.setVisibility(View.VISIBLE);
        }
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookedDates(getActivity());
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ActionBar.quickAction.show(v);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                getActivity().startActivity(Intent.createChooser(shareIntent, "Share text using"));
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
                if(!pref.getString("userId", "").equals("")) {
                    ((NavigationDrawerActivity) getActivity()).ClearFragment();
                    addFragment(new Restaurant_History(),true);
                } else {
                    addFragment(new LoginFragment(),true);
                }

            }
        });

        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.WHERE_STR = "";
                Constants.EVENT_NAME = eventName.getText().toString();
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(new Date(jsonObject.getJSONObject("data").getLong("checkIn")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, cal.getTimeInMillis());
                intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, cal.getTimeInMillis() + 60 * 60 * 1000);
                intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, 1);
                intent.putExtra(CalendarContract.Events.DTSTART, cal.getTimeInMillis());
                intent.putExtra(CalendarContract.Events.DTEND, cal.getTimeInMillis());
                intent.putExtra(CalendarContract.Events.TITLE, Constants.EVENT_NAME);
                intent.putExtra(CalendarContract.Events.DESCRIPTION, "Welcome All");
                intent.putExtra(CalendarContract.Events.EVENT_LOCATION, Constants.WHERE_STR);
                intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
                getActivity().startActivity(intent);
            }
        });

        /*invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new InviteUserFragment();
                Bundle args = new Bundle();
                args.putString("type", "events");
                args.putString("id", Upcoming_Events.eventId);
                fragment.setArguments(args);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager
                        .beginTransaction();
                fragmentTransaction.add(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }
        });*/

        foodpkg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowFoodPackage();
            }
        });
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                FragmentManager fm = getActivity().getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount() - 1; ++i) {
                    fm.popBackStack();
                }
                return false;
            }
        });

    }

    public void ShowFoodPackage(){
        /*FoodPkg foodMenuPackage = null;
        try {
            foodMenuPackage = new FoodPkg(new JSONObject(getArguments().getString("selected_package")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.food_package_adapter);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setDimAmount(0.5f);

        TextView category = (TextView) dialog.findViewById(R.id.category);
        TextView price = (TextView) dialog.findViewById(R.id.price);

        LinearLayout toppings = (LinearLayout) dialog.findViewById(R.id.topping_view);
        LinearLayout top = (LinearLayout) dialog.findViewById(R.id.top);
        LinearLayout middle = (LinearLayout) dialog.findViewById(R.id.middle);
        category.setText(foodMenuPackage.packageName);
        price.setText(foodMenuPackage.getCurrencySymbol() + " " + String.format("%.2f",foodMenuPackage.price));
        ArrayList<FoodPkg.PlaceMenu>items = foodMenuPackage.menuItems;
        for (int i = 0; i < items.size(); i++) {
            final View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.food_package_items, toppings, false);
            TextView itemName = (TextView) layout2.findViewById(R.id.items);
            try {
                itemName.setText(items.get(i).itemName + "(" + items.get(i).qty + ")");
            } catch (Exception e) {
            }
            layout2.setTag(i);
            toppings.addView(layout2);
        }
        dialog.show();*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void bookedDates(final Context act) {
        final Dialog dialog = new android.app.Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.booked_dates_alert);
        ListView list = (ListView) dialog.findViewById(R.id.list);
        Button submit = (Button) dialog.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        CheckoutDateAdapter adapter = new CheckoutDateAdapter(Ticket_Confirmation.this,((NavigationDrawerActivity)getActivity()).placeRequest.dates,((NavigationDrawerActivity)getActivity()).placeRequest.type,Double.parseDouble(getArguments().getString("totalPrice")),getArguments().getString("currency"));
        if(adapter!=null)
            list.setAdapter(adapter);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}