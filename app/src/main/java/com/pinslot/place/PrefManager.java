package com.pinslot.place;

import android.content.Context;
import android.content.SharedPreferences;

import com.pinslot.place.model.Restaurant;

import org.json.JSONException;
import org.json.JSONObject;

public class PrefManager {
    private Context context;
    private static PrefManager prefManager = null;
    private SharedPreferences pref;
    public static PrefManager getInstance(Context context){
        if (prefManager == null)
            prefManager = new PrefManager(context);
        return prefManager;
    }
    private PrefManager(Context context){
        this.context = context;
        pref = context.getSharedPreferences("MyPref", 0);
    }

    public void setStatus(String status){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("delivery_status",status);
        editor.commit();
    }

    public String getStatus(){
        return pref.getString("delivery_status","");
    }

    public SharedPreferences getPref() {
        return pref;
    }

    public String getName(){
        String name = "";
        if(!pref.getString("firstName","").isEmpty())
            name = pref.getString("firstName","");
        if(!pref.getString("lastName","").isEmpty()){
            if(name.isEmpty())
                name = pref.getString("lastName","");
            else
                name = name + " "+pref.getString("lastName","");
        }
        return name;
    }

    public String getFirstName(){
        return pref.getString("firstName","");
    }

    public String getLastName(){
        return pref.getString("lastName","");
    }

    public String getEmail(){
        return pref.getString("email","");
    }

    public String getPhone(){
        return pref.getString("mobile","");
    }

    public String getCountryCode(){
        return pref.getString("mobilePhoneCountryCode", "");
    }

    public int getCountryCodevalue(){
        return Integer.parseInt(pref.getString("mobilePhoneCountryCode","+91").replace("+",""));
    }

    public String getPhoneWithoutCC(){
        return getPhone().replace(getCountryCode(),"");
    }

    public void setPreferLocations(String locations){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("pref_locations",locations);
        editor.commit();
    }

    public String getPreferLocations(){
        return pref.getString("pref_locations","");
    }

    public void setDeliveryEnable(boolean status){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("delivery_enable",status);
        editor.commit();
    }

    public String getImage(){
        String image = pref.getString("image", "");
        if(!image.startsWith("http")){
            image = Constants.URL + "resources" + image;
        }
        return image;
    }
    public boolean getDeliveryEnable(){
        return pref.getBoolean("delivery_enable",false);
    }

    public String getUserId(){
        return pref.getString("userId", "");
    }

    public void setBirthDate(int date){
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("birth_date",date);
        editor.commit();
    }

    public int getBirthDate(){
        return pref.getInt("birth_date",0);
    }

    public void setBirthMonth(String month){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("birth_month",month);
        editor.commit();
    }

    public String getBirthMonth(){
        return pref.getString("birth_month","");
    }

    public String getBirthDay(){
        if(!getBirthMonth().isEmpty()){
            return Integer.toString(getBirthDate())+" "+getBirthMonth();
        } else
            return "";
    }

    public void setAcceptorRating(float rating){
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat("acceptor_rating",rating);
        editor.commit();
    }

    public float getAcceptorRating(){
        return pref.getFloat("acceptor_rating",0f);
    }

    public void setRequestorRating(float rating){
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat("requestor_rating",rating);
        editor.commit();
    }

    public float getRequestorRating(){
        return pref.getFloat("requestor_rating",0f);
    }

    /*public void setMylatlng(LatLng latLng){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("mylat",Double.toString(latLng.latitude));
        editor.putString("mylng",Double.toString(latLng.longitude));
        editor.commit();
    }

    public LatLng getMylatlng(){
        double lat = Double.parseDouble(pref.getString("mylat","0.0"));
        double lng = Double.parseDouble(pref.getString("mylng","0.0"));
        return new LatLng(lat,lng);
    }*/

    public void setServerGCMid(String gcm_id){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("gcm_id",gcm_id);
        editor.commit();
    }

    public void setTwilioGCMid(String gcm_id){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("t_gcm_id",gcm_id);
        editor.commit();
    }

    public String getTwilioGCMid(){
        return pref.getString("t_gcm_id","");
    }

    public void setNotificationId(int id){
        if(id==1000)
            id = 100;
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("notif_id",id);
        editor.commit();
    }

    public int getNotificationId(){
        int id = pref.getInt("notif_id",100);
        setNotificationId(id+1);
        return id;
    }

    public String getServerGCMid(){
        return pref.getString("gcm_id","");
    }

    public void clearGcm(){
        SharedPreferences.Editor edit = pref.edit();
        edit.remove("gcm_id");
        edit.commit();
    }

    public void setTwillioAccessToken(String token){
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("twillio_token",token);
        edit.commit();
    }

    public String getTwillioAccessToken(){
        return pref.getString("twillio_token","");
    }

    /*public CSUser getCurrentUser(){
        CSUser user = new CSUser();
        user.profileId = pref.getString("userId","");
        if(!pref.getString("image","").isEmpty())
            user.photoLocation = pref.getString("image","");
        if(!user.photoLocation.startsWith("http"))
            user.photoLocation = Constants.URL + "resources"+ user.photoLocation;
        user.contactDetails.firstName = pref.getString("firstName","");
        user.contactDetails.lastName = pref.getString("lastName","");
        user.contactDetails.emailAddress = pref.getString("email","");
        user.contactDetails.mobilePhone = pref.getString("mobile","");
        user.contactDetails.mobilePhoneCountryCode = pref.getString("mobilePhoneCountryCode","");
        return user;
    }*/

    public void setUserFb(String fb){
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("user_fb",fb);
        edit.commit();
    }

    public String getUserFb(){
        return pref.getString("user_fb","");
    }

    public void setUserTw(String tw){
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("user_tw",tw);
        edit.commit();
    }

    public String getUserTw(){
        return pref.getString("user_tw","");
    }

    public void setUserIn(String In){
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("user_tw",In);
        edit.commit();
    }

    public String getUserIn(){
        return pref.getString("user_in","");
    }

    public void setUserLi(String li){
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("user_tw",li);
        edit.commit();
    }

    public SplashModel getSplashModel(){
        return new SplashModel(this);
    }

    public String getUserLi(){
        return pref.getString("user_li","");
    }

    public void saveDeliveryBusiness(JSONObject data){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("delivery_business",data.toString());
        editor.apply();
    }

    public void saveRestaurant(JSONObject res){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("res",res.toString());
        editor.apply();
    }

    public Restaurant getRestaurant(){
        Restaurant res = null;
        if(!pref.getString("res","").isEmpty()){
            try {
                res = new Restaurant(new JSONObject(pref.getString("res","")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public String getReferalCode(){
        return pref.getString("referralCode","");
    }

    public void clearPref(){
        SharedPreferences.Editor edit = pref.edit();
        edit.remove("loginSession");
        edit.remove("userId");
        edit.remove("name");
        edit.remove("email");
        edit.remove("mobile");
        edit.remove("image");
        edit.remove("delivery_status");
        edit.remove("firstName");
        edit.remove("lastName");
        edit.remove("email");
        edit.remove("mobilePhoneCountryCode");
        edit.remove("user_fb");
        edit.remove("user_tw");
        edit.remove("user_in");
        edit.remove("user_li");
        edit.remove("pref_locations");
        edit.remove("acceptor_rating");
        edit.remove("requestor_rating");
        edit.remove("twillio_token");
        edit.remove("t_gcm_id");
        edit.remove("gcm_id");
        edit.remove("delivery_business");
        edit.remove("referralCode");
        edit.apply();
    }

    public class SplashModel{
        public boolean hide_delivery_splash = false;
        public boolean hide_browse_coupon_splash = false;
        PrefManager prefManager;
        public SplashModel(PrefManager prefManager){
            this.prefManager = prefManager;
            this.hide_delivery_splash = prefManager.pref.getBoolean("delivery_splash_hide",false);
            this.hide_browse_coupon_splash = prefManager.pref.getBoolean("browse_coupon_splash_hide",false);
        }

        public void hideDeliverSplash(){
            SharedPreferences.Editor edit = prefManager.pref.edit();
            edit.putBoolean("delivery_splash_hide",true);
            edit.apply();
        }

        public void hideBrowseCouponSplash(){
            SharedPreferences.Editor edit = prefManager.pref.edit();
            edit.putBoolean("browse_coupon_splash_hide",true);
            edit.apply();
        }
    }
}
